$(".feedback-show-button").on("click", function () {
    var button = this;
    $("#feedback-details").hide();
    if (document.getElementById("feedbackModal")) {
        $("#feedbackModal").modal("show");
    }
    var feedback_id = $(this).data("feedbackId");
    $(".feedback-show-button").css("backgroundColor", "").removeAttr("disabled");
    $(button).attr("disabled", "disabled").css("backgroundColor", "#000").css("backgroundColor", "#000").find(".fa-spinner").show();
    $.ajax({
        url: "/Panel/Feedback/getFeedbackJson/" + feedback_id,
        dataType: "JSON",
        success: function (json) {
            if (json.status !== undefined && json.feedback !== undefined && json.status === true) {
                var feedback = json.feedback;
                $("#feedback-details").fadeIn().data("feedback-id", feedback.feedback_id);
                $("#feedback-header").html("[" + feedback.region + "] " + feedback.summoner_name);
                $("#feedback-email").html(feedback.email);
                $("#feedback-title").html(feedback.message);
                $("#feedback-footer").html(feedback.date);
                $("#feedback-browser").html(feedback.browser);
                $("#feedback-platform").html(feedback.os);
                $("#feedback-ip").html(feedback.ip).attr("href", 'https://ipfind.co/?ip=' + feedback.ip + '&auth=6a79ec81-95e9-4d23-a49f-0c24f06dcf4d');
                $("#feedback-referrer").html(feedback.referrer).attr("href", feedback.referrer);
                $("#feedback-replies-container").html("");
                if (json.feedback.trello_status !== undefined && json.feedback.trello_status === "1") {
                    $("#feedback-trello-button").attr("disabled", "disabled").find("span").html("<i class=\"fa fa-trello\"></i> Saved to Trello");
                } else {
                    $("#feedback-trello-button").removeAttr("disabled").find("span").html("<i class=\"fa fa-trello\"></i> Send to Trello");
                }
                if (json.feedback.important_status !== undefined && parseInt(json.feedback.important_status) === 1) {
                    $("#feedback-important-button").data("importantStatus", 1).find("span").html("Important");
                } else {
                    $("#feedback-important-button").data("importantStatus", 0).find("span").html("Mark as Important");
                }
                if (json.replies !== undefined) {
                    $.each(json.replies, function (index, reply) {
                        var template = $("#feedback-reply-card-template").clone();
                        $(template).find("#feedback-replies-text").html(reply.message);
                        $(template).find("#feedback-replies-footer").html(reply.date);
                        $(template).removeAttr("id").fadeIn();
                        $("#feedback-replies-container").append(template);
                    });
                }

            } else {
                alert("HATA");
            }
        },
        complete: function () {
            $(button).find(".fa-spinner").hide();
        }
    });
});

$("#feedback-reply-button").on("click", function () {
    var button = this;
    $(button).attr("disabled", "disabled").find(".fa-spinner").show();
    var feedback_id = $("#feedback-details").data("feedbackId");
    var reply_text = $("#feedback-reply-text").val();

    $.ajax({
        url: "/Panel/Feedback/replyFeedback/" + feedback_id,
        method: "POST",
        data: {reply_text: reply_text},
        dataType: "JSON",
        success: function (json) {
            if (json.status !== undefined && json.status === true) {
                $("#feedback-reply-text").val("");

                var template = $("#feedback-reply-card-template").clone();
                $(template).find("#feedback-replies-text").html(json.message);
                $(template).find("#feedback-replies-footer").html(json.date);
                $(template).removeAttr("id").fadeIn();
                $(template).removeAttr("id").fadeIn();
                $("#feedback-replies-container").append(template);
                $("#feedback-replied-badge-" + feedback_id).html('<span class="badge badge-success">OK</span>');
            } else {
                var error = "ERROR";
                if (json.error !== undefined && json.error) {
                    error = json.error;
                }
                $("#feedback-reply-error-div").fadeIn("fast");
                $("#feedback-reply-error").html(error);
            }
        },
        complete: function () {
            $(button).removeAttr("disabled").find(".fa-spinner").hide();
        }
    });
});

$("#feedback-trello-button").on("click", function () {
    var button = this;
    $(button).attr("disabled", "disabled").find(".fa-spinner").show();
    var feedback_id = $("#feedback-details").data("feedbackId");

    var note = prompt("Note");

    $.ajax({
        url: "/Panel/Feedback/sendFeedbackToTrello/" + feedback_id,
        method: "POST",
        data: {note: note},
        dataType: "JSON",
        success: function (json) {
            if (json.status !== undefined && json.status === true) {
                $(button).find("span").html("Saved to Trello");
            } else {
                var error = "ERROR";
                if (json.error !== undefined && json.error) {
                    error = json.error;
                }
                alert(error);
            }
        },
        complete: function () {
            $(button).find(".fa-spinner").hide();
        }
    });
});

$("#feedback-important-button").on("click", function () {
    var button = this;
    $(button).attr("disabled", "disabled").find(".fa-spinner").show();
    var feedback_id = $("#feedback-details").data("feedbackId");
    var important_status = parseInt($("#feedback-important-button").data("importantStatus")) === 1 ? 0 : 1;
    $.ajax({
        url: "/Panel/Feedback/markFeedbackImportant/" + feedback_id,
        method: "POST",
        data: {important_status: important_status},
        dataType: "JSON",
        success: function (json) {
            if (json.status !== undefined && json.status === true) {
                $("#feedback-important-button").data("importantStatus", parseInt(json.important_status));
                if (parseInt(json.important_status) === 1) {
                    $(button).find("span").html("Important");
                    $("#feedback-important-badge-" + feedback_id).html('<span class="badge badge-danger">Important</span>');
                } else {
                    $(button).find("span").html("Mark as Important");
                    $("#feedback-important-badge-" + feedback_id).html('');
                }
            } else {
                var error = "ERROR";
                if (json.error !== undefined && json.error) {
                    error = json.error;
                }
                alert(error);
            }
        },
        complete: function () {
            $(button).removeAttr("disabled").find(".fa-spinner").hide();
        }
    });
});

$("#feedbackModal").on('hidden.bs.modal', function () {
    $(".feedback-show-button").css("backgroundColor", "").removeAttr("disabled");
});

$("#feedback-save-draft-button").on("click", function () {
    var button = this;
    $(button).attr("disabled", "disabled").find(".fa-spinner").show();
    var name = $("#feedback-draft-name").val();
    var draft = $("#feedback-draft-text").val();

    $.ajax({
        url: "/Panel/Feedback/addDraft",
        method: "POST",
        data: {draft: draft, name: name},
        dataType: "JSON",
        success: function (json) {
            if (json.status !== undefined && json.status === true) {
                $("#feedback-draft-name").val("");
                $("#feedback-draft-text").val("");
                var button_html = '';
                button_html += '<div id="feedback-draft-id-' + json.feedback_draft_id + '" class="float-left">';
                button_html += '<div class="btn-group mr-2 mb-2">';
                button_html += '<button style="color:#fff;" type="button" data-toggle="tooltip" ' +
                    'data-placement="top" class="btn btn-success btn-sm feedback-draft-select-button" title="' + json.draft + '" data-message="' + json.draft + '" data-original-title="">' + json.name + '</button>';
                button_html += '<button class="btn btn-danger btn-sm ' +
                    'feedback-draft-remove-button" onclick="deleteFeedbackDraft(this);" data-id="' + json.feedback_draft_id + '" ';

                if ($("#draft-delete-button").data("status") !== "true") {
                    button_html += 'style="display: none;"';
                }
                button_html += '>x</button>';
                button_html += '</div>';
                button_html += '</div>';

                $("#draft-container").append(button_html);

                $("#feedback-save-draft-success").fadeIn();

                $('[data-toggle="tooltip"]').tooltip();

                $('#feedback-draft-id-' + json.feedback_draft_id).find(".feedback-draft-select-button").on("click", function () {
                    $("#feedback-reply-text").val($("#feedback-reply-text").val() + $(this).data('message'));
                });

            } else {
                var error = "ERROR";
                if (json.error !== undefined && json.error) {
                    error = json.error;
                }
                alert(error);
            }
        },
        complete: function () {
            $(button).removeAttr("disabled").find(".fa-spinner").hide();
        }
    });
});

$(".feedback-draft-select-button").on("click", function () {
    $("#feedback-reply-text").val($("#feedback-reply-text").val() + $(this).data('message'));
});

function deleteFeedbackDraft(btn) {
    if (confirm('Are you sure?')) {
        var feedback_draft_id = $(btn).data("id");
        $.ajax({
            url: "/Panel/Feedback/deleteDraft/" + feedback_draft_id,
            dataType: "JSON",
            success: function (json) {
                if (json.status !== undefined && json.status === true) {
                    $("#feedback-draft-id-" + json.feedback_draft_id).remove();
                } else {
                    var error = "ERROR";
                    if (json.error !== undefined && json.error) {
                        error = json.error;
                    }
                    alert(error);
                }
            }
        });
    }
}

$("#draft-delete-button").on("click", function () {
    var status = $(this).data("status");
    if (status === "true") {
        $(".feedback-draft-remove-button").hide();
        $(this).data("status", "false");
    } else {
        $(".feedback-draft-remove-button").show();
        $(this).data("status", "true");
    }
});
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});