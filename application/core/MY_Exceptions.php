<?php

class MY_Exceptions extends CI_Exceptions
{
    function log_exception($severity, $message, $filepath, $line)
    {
        $severity = (!isset($this->levels[$severity])) ? $severity : $this->levels[$severity];

        error_log(sprintf("PHP %s:  %s in %s on line %d", $severity, $message, $filepath, $line));

        parent::log_exception($severity, $message, $filepath, $line);

    }

}