<div class="container-fluid">
    <div>
        <?= isset($pagination) ? $pagination : '' ?>
    </div>

    <div class="row">
        <div class="col-md-6">
            <table class="table table-responsive table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Summoner</th>
                    <?php if (!$is_mobile) { ?>
                        <th scope="col" class="responsive-invisibility">Message</th>
                        <th scope="col">Status</th>
                    <?php } ?>
                    <th scope="col">Date</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody class="feedback-list-tbody">
                <? if (isset($results) && $results) {
                    foreach ($results as $feedback) { ?>
                        <tr>
                            <td scope="row"><?= $feedback->feedback_id ?></td>
                            <td scope="row">[<?= $feedback->region ?>] <?= $feedback->summoner_name ?></td>
                            <?php if (!$is_mobile) { ?>
                                <td scope="row" class="responsive-invisibility"><?= $feedback->message_short ?></td>
                                <td scope="row">
                                    <div id="feedback-replied-badge-<?= $feedback->feedback_id ?>">
                                        <?php if ($feedback->feedback_reply_id) { ?>
                                            <span class="badge badge-success">OK</span>
                                        <?php } ?>
                                    </div>
                                    <div id="feedback-important-badge-<?= $feedback->feedback_id ?>">
                                        <?php if ($feedback->important_status == 1) { ?>
                                            <span class="badge badge-danger">Important</span>
                                        <?php } ?>
                                    </div>

                                </td>
                            <?php } ?>


                            <td scope="row" class="date"><?= $feedback->date ?></td>
                            <td scope="row">
                                <button type="button" class="feedback-show-button btn btn-info"
                                        data-feedback-id="<?= $feedback->feedback_id ?>" style="min-width: 90px;">Show
                                    <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;"></i>
                                </button>

                            </td>
                        </tr>
                    <? }
                } ?>
                </tbody>
            </table>
        </div>
        <?php if (!$is_mobile) { ?>
            <div class="col-md-6" id="feedback-details" data-feedback-id="" style="display: none;">
                <div class="feedback-details-container">
                    <div class="card text-center text-white bg-dark">
                        <div class="card-header" id="feedback-header">
                            feedback-header
                        </div>
                        <div class="card-block p-3">
                            <h4 class="card-title mb-0 my-2" id="feedback-title">feedback-title</h4>
                        </div>
                        <div class="card-footer text-muted">
                            <div class="row no-gutters">
                                <div class="col text-left">
                                    <button id="feedback-trello-button" type="button" class="btn btn-sm btn-secondary">
                                        <span><i class="fa fa-trello"></i> Send to Trello</span>
                                        <i
                                                class="fa fa-spinner fa-spin"
                                                style="font-size:16px; display: none;"></i>
                                    </button>
                                    <button id="feedback-important-button" data-important-status="" type="button"
                                            class="btn btn-danger btn-sm"><span>Mark as Important</span> <i
                                                class="fa fa-spinner fa-spin"
                                                style="font-size:16px; display: none;"></i>
                                    </button>
                                </div>
                                <div class="col text-right" id="feedback-footer" style="padding-top: 5px; color: #fff;">
                                    feedback-footer
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card text-center text-white bg-dark mt-2">
                        <div class="card-block p-3">
                            <p class="card-text">
                                <strong>Browser:</strong> <span id="feedback-browser">feedback-browser</span>
                                <strong>OS:</strong> <span id="feedback-platform">feedback-platform</span>
                                <strong>IP:</strong> <a id="feedback-ip" target="_blank">feedback-ip</a>
                            </p>
                            <p class="card-text">
                                <strong>Referrer:</strong> <a id="feedback-referrer"
                                                              target="_blank">feedback-referrer</a>
                            </p>
                            <p class="card-text">
                                <strong>Email:</strong> <span id="feedback-email">feedback-email</span>
                            </p>
                        </div>
                    </div>
                    <div id="feedback-replies-container">

                    </div>
                    <div class="card text-center text-white bg-dark mt-2">
                        <div class="card-header">Drafts</div>
                        <div class="card-block p-3">
                            <div class="card-text border border-dark mb-3">
                                <div class="row no-gutters">
                                    <div class="col-9 text-left">
                                        <div id="draft-container" style="width: 80%;">
                                            <?php if (isset($drafts)) {
                                                foreach ($drafts as $draft) { ?>
                                                    <div id="feedback-draft-id-<?= $draft->feedback_draft_id ?>"
                                                         class="float-left">
                                                        <div class="btn-group mr-2 mb-2">
                                                            <button style="color:#fff;" type="button"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    class="btn btn-success btn-sm feedback-draft-select-button"
                                                                    title="<?= quotes_to_entities($draft->draft) ?>"
                                                                    data-message="<?= quotes_to_entities($draft->draft) ?>"><?= $draft->name ?>
                                                            </button>
                                                            <button class="btn btn-danger btn-sm feedback-draft-remove-button"
                                                                    onclick="deleteFeedbackDraft(this);"
                                                                    data-id="<?= $draft->feedback_draft_id ?>"
                                                                    style="display: none;">x
                                                            </button>
                                                        </div>
                                                    </div>
                                                <?php }
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="col-3 text-right">
                                        <button type="button" class="btn btn-success btn-sm m-1" data-toggle="modal"
                                                data-target="#draftModal"><i class="fa fa-plus"></i> Add
                                        </button>
                                        <button type="button" class="btn btn-danger btn-sm m-1" id="draft-delete-button"
                                                data-status="false"><i class="fa fa-times"></i> Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <p class="card-text">
                                <textarea class="form-control" id="feedback-reply-text" rows="6"
                                          onkeydown="$('#feedback-reply-error-div').hide();"></textarea>
                            </p>
                        </div>
                        <div class="card-footer">
                            <div class="row no-gutters">
                                <div class="col text-left">
                                    <div id="feedback-reply-error-div" class="alert alert-danger" role="alert"
                                         style="display: none;padding: 6px; margin: 0;">
                                        <button type="button" class="close" aria-label="Close"
                                                onclick="$('#feedback-reply-error-div').hide();">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <span id="feedback-reply-error"></span>
                                    </div>
                                </div>
                                <div class="col text-right">
                                    <button id="feedback-reply-button" type="button" class="btn btn-primary"><i
                                                class="fa fa-send"></i> Send Reply <i
                                                class="fa fa-spinner fa-spin"
                                                style="font-size:16px; display: none;"></i>
                                    </button>
                                    <button type="button" class="btn btn-secondary"
                                            onclick="$('#feedback-reply-text').val('');"><i class="fa fa-trash-o"></i>
                                        Clear
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <?= isset($pagination) ? $pagination : '' ?>
</div>
<?php if ($is_mobile) { ?>
    <!-- Feedback Mobile Modal -->
    <div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="feedbackModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="feedbackModalLabel">Feedback</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-6" id="feedback-details" data-feedback-id="">
                        <div class="card text-center">
                            <div class="card-header" id="feedback-header">
                                feedback-header
                            </div>
                            <div class="card-block p-3">
                                <h4 class="card-title" id="feedback-title">feedback-title</h4>
                            </div>
                            <div class="card-footer text-muted" id="feedback-footer">
                                feedback-footer
                            </div>
                        </div>
                        <div class="card text-center mt-2">
                            <div class="card-block p-3">
                                <p class="card-text">
                                    <strong>Browser:</strong> <span id="feedback-browser">feedback-browser</span>
                                    <strong>OS:</strong> <span id="feedback-platform">feedback-platform</span>
                                    <strong>IP:</strong> <a id="feedback-ip" target="_blank">feedback-ip</a>
                                </p>
                                <p class="card-text">
                                    <strong>Referrer:</strong> <a id="feedback-referrer" target="_blank">feedback-referrer</a>
                                </p>
                                <p class="card-text">
                                    <strong>Email:</strong> <span id="feedback-email">feedback-email</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="card text-center border-info text-white bg-dark mt-2" style="display: none;border-width: 6px;"
     id="feedback-reply-card-template">
    <div class="card-header">
        Response
    </div>
    <div class="card-block p-3">
        <p class="card-text" id="feedback-replies-text">feedback-replies-text</p>
    </div>
    <div class="card-footer text-muted" id="feedback-replies-footer">
        feedback-reply-footer
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="draftModal" tabindex="-1" role="dialog" aria-labelledby="draftModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="draftModalLabel">Add Draft</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" id="draft-add-container">
                    <div class="feedback-details-container">
                        <div class="card text-center mt-2">
                            <div class="card-block p-3">
                                <p class="card-text">
                                    Name: <input type="text" id="feedback-draft-name"> <br> <br>
                                    <textarea class="form-control" id="feedback-draft-text" rows="3"></textarea>
                                </p>
                                <button id="feedback-save-draft-button" type="button" class="btn btn-primary">Save Draft<i
                                            class="fa fa-spinner fa-spin" style="font-size:16px; display: none;"></i>
                                </button>
                                <div id="feedback-save-draft-success" class="alert alert-success mt-3" role="alert"
                                     style="display: none;">
                                    <button type="button" class="close" aria-label="Close"
                                            onclick="$('#feedback-save-draft-success').hide();">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    Feedback draft added
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
