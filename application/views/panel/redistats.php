
<div class="container">
    <table class="table table-bordered" style="margin-top:50px;">
        <thead>
            <tr>
                <th>Name</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($cache_info as $name => $val){?>
                <tr>
                    <td><?=$name?></td>
                    <td><?=$val?></td>
                </tr>
            <? } ?>
        </tbody>
    </table>
</div>
