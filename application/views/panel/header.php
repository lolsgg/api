<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=base_url()?>assets/panel/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/panel/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/panel/css/panel.css">
    <title>Lols.gg Api Panel | <?=$page_title?></title>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?=$page_title=='RiotApiStats'?'active':''?>">
                    <a class="nav-link" href="<?=site_url('Panel/RiotApiStats')?>">RiotApiStats</a>
                </li>
                <li class="nav-item <?=$page_title=='RedisStats'?'active':''?>">
                    <a class="nav-link" href="<?=site_url('Panel/RedisStats')?>">RedisStats</a>
                </li>
                <li class="nav-item <?=$page_title=='RiotLimit'?'active':''?>">
                    <a class="nav-link" href="<?=site_url('Panel/RiotLimit')?>">RiotLimit</a>
                </li>
                <li class="nav-item <?=$page_title=='AutoLive'?'active':''?>">
                    <a class="nav-link" href="<?=site_url('Panel/AutoLive')?>">AutoLive</a>
                </li>
                <li class="nav-item <?=$page_title=='CacheCreate'?'active':''?>">
                    <a class="nav-link" href="<?=site_url('Panel/CacheCreate')?>">CacheCreate</a>
                </li>
                <li class="nav-item <?=$page_title=='Feedback'?'active':''?>">
                    <a class="nav-link" href="<?=site_url('Panel/Feedback')?>">Feedback</a>
                </li>
                <li>
                    <a class="nav-link" href="<?=site_url('Panel/Login/Logout')?>">Logout</a>
                </li>
            </ul>
            <span style="color:#fff;">
               Server Time: <?=date('Y-m-d H:i:s')?>
            </span>
        </div>
    </nav>
