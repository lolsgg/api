<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            Current API Version: <?=$current_version?><br><br>
            <form action="<?=site_url('Panel/CacheCreate')?>" method="post">
                <h2>Create Champion Cache</h2>
                <label for="version">Riot API Version:</label> <input type="text" name="version" value="<?=set_value('version')?>"><br>
                <label for="lang">Riot API Lang:</label> <input type="text" name="lang" value="<?=set_value('lang')?>"><br>
                <input type="submit" name="create_champion_cache" value="Create">
                <input type="submit" name="clear_all_champion_cache" value="Clear">
                <br>
            </form>
            <br>
            <br>
            <form action="<?=site_url('Panel/CacheCreate')?>" method="post">
                <h2>Delete Champion Cache</h2>
                <label for="champion_id">Champion ID:</label> <input type="text" name="champion_id" value="<?=set_value('champion_id')?>"><br>
                <label for="version">Riot API Version:</label> <input type="text" name="version" value="<?=set_value('version')?>"><br>
                <label for="lang">Riot API Lang:</label> <input type="text" name="lang" value="<?=set_value('lang')?>"><br>
                <input type="submit" name="clear_champion_cache" value="Delete"><br>
            </form>
            <br>
            <br>
            <form action="<?=site_url('Panel/CacheCreate')?>" method="post">
                <h2>Create Build Cache</h2>
                <label for="version">Build Version:</label> <input type="text" name="build_version" value="<?=set_value('build_version', 1528182845)?>"><br>
                <label for="lang">Build Lang:</label> <input type="text" name="lang" value="<?=set_value('lang')?>"><br>
                <input type="submit" name="create_build_cache" value="Create">
                <input type="submit" name="clear_build_cache" value="Clear">
                <br>
            </form>
            <br>
            <br>
            <br>
            <?=$log?>
        </div>
    </div>
</div>
