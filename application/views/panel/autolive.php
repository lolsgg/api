
<div class="container">
    <div class="row" style="margin-top:50px;">
        <div class="col-md-6">
            <form action="<?=site_url('Panel/AutoLive')?>" method="POST" id="addform" <?=!$this->input->post()?'style="display:none;"':''?>>
                <div class="form-group">
                    <label for="player_name">Player Name:</label>
                    <input type="player_name" class="form-control" id="player_name" name="player_name" value="<?=set_value('player_name')?>">
                </div>
                <div class="form-group">
                    <label for="region">Region:</label>
                    <select class="form-control" id="region" name="region">
                        <option>BR</option>
                        <option>EUNE</option>
                        <option>EUW</option>
                        <option>JP</option>
                        <option>KR</option>
                        <option>LAN</option>
                        <option>LAS</option>
                        <option>NA</option>
                        <option>OCE</option>
                        <option>TR</option>
                        <option>RU</option>
                        <option>PBE</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="twitter">Twitter Username:</label>
                    <input type="twitter" class="form-control" id="twitter" name="twitter" value="<?=set_value('twitter')?>">
                </div>
                <div class="form-group">
                    <label for="twitch">Twitch Username:</label>
                    <input type="twitch" class="form-control" id="twitch" name="twitch" value="<?=set_value('twitch')?>">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
                <div class="btn btn-danger" onclick="$('#addnewplayerbtn').toggle();$('#addform').toggle();">Cancel</div>

                <div class="row" style="margin-top:20px;">
                    <?=validation_errors('<div class="alert alert-danger">','</div>')?>
                </div>
            </form>
            <button <?=$this->input->post()?'style="display:none;"':''?> class="btn btn-default" id="addnewplayerbtn" onclick="$('#addnewplayerbtn').toggle();$('#addform').toggle();">Add New Player</button>
        </div>
    </div>
    <? if($success = $this->session->flashdata('success')){ ?>
        <div class="alert alert-success" style="margin-top:20px;" id="success">
            <strong>Success!</strong> <?=$success?>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#success").delay(1500).fadeOut();
            });
        </script>
    <? } ?>
    <table class="table table-bordered" style="margin-top:20px;">
        <thead>
            <tr>
                <th>Player Name</th>
                <th>Region</th>
                <th>Twitter</th>
                <th>Twitch</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($results as $autolive){?>
                <tr>
                    <td><?=$autolive->player_name?></td>
                    <td><?=$autolive->region?></td>
                    <td><?=$autolive->twitter?></td>
                    <td><?=$autolive->twitch?></td>
                    <td><a href="<?=site_url('Panel/AutoLive/delete/'.$autolive->autolive_id)?>" onclick="return confirm('Are you sure?');" class="btn btn-danger">Delete</a></td>
                </tr>
            <?}?>
        </tbody>
    </table>
</div>
