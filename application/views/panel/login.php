<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/panel/css/login.css">
    <title>Lols.gg Api Panel | Login</title>
</head>

<body>
    <div class="container">

        <form class="form-signin" action="<?=site_url('Panel/Login')?>" method="POST">
            <label for="inputUsername" class="sr-only">Username</label>
            <input type="username" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus value="<?=set_value('username')?>">
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <?=isset($error)?'<div style="font-size:1.2em;margin-top:10px;text-align:center;font-weight:bold;">'.$error.'</div>':''?>
        </form>

    </div>
</body>

</html>
