
<div class="container">
    <table class="table table-bordered" style="margin-top:50px;">
        <thead>
            <tr>
                <th colspan="6" style="text-align:right;"><a href="<?=site_url('Panel/RiotApiStats/resetLogs')?>">Reset Logs</a></th>
            </tr>
            <tr>
                <th>#</th>
                <th>region</th>
                <th>req</th>
                <th>http_code</th>
                <th>expire</th>
                <th>date</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($logs as $key => $log){?>
                <tr>
                    <td><?=$key?></td>
                    <td><?=$log['region']?></td>
                    <td><?=$log['req']?> <a href="<?=$log['href']?>" target="_blank">Test</a></td>
                    <td>_<?=$log['http_code']?></td>
                    <td><?=$log['exp']?></td>
                    <td><?=$log['date']?></td>
                </tr>
            <? } ?>
        </tbody>
    </table>
</div>
