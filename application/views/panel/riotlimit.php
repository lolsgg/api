<div class="container-fluid">
    <div class="row">
    <? foreach ($riot_limits as $region => $region_limits){ ?>
        <div class="col-12 col-md-6 col-lg-4 col-xl-3">
            <table class="table table-bordered limits-table">
                <thead>
                    <tr>
                        <th><?=strtoupper($region)?></th>
                        <th>Limit</th>
                    </tr>
                </thead>
                <tbody>
                    <? foreach ($region_limits as $limit_url => $limits){?>
                        <tr>
                            <td><?=$limit_url?></td>
                            <td style="text-align: right;<?=$limits[2]==0?'color:#fff;':($limits[2]>=$limits[0]?'color:#ff0000;font-weight:bold;':'')?>"><?=$limits[0]?>:<?=$limits[1]?><br><?=$limits[2]?>:<?=$limits[1]?></td>
                        </tr>
                    <? } ?>
                </tbody>
            </table>
        </div>
    <? } ?>
    </div>
</div>
