<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lane Test</title>
    <link rel="stylesheet" href="<?= base_url() ?>assets/panel/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/panel/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/panel/css/font-awesome.min.css">
</head>
<body>
(Tüm kurallar kalanlar arasındaki kişilere uygulanacak
<ul>
    <li>1-Takımda 1 kişide çarp varsa o kişi Jungle+</li>
    <li>2-Takımda 1 ADC Varsa o kişi ADC+</li>
    <li>3-Eğer 2 ADC Varsa ve Işınlanı olan varsa ışınlanı olan Top diğeri ADC+</li>
    <li>4-Eğer 2 ADC varsa ve ışınlan hiç yoksa şifa olanı ADC+</li>
    <li>5-Eğer 2 ADC varsa ve ışınlanı yoksa ve şifa olanı yoksa yüksek oranlı olanı ADC+</li>
    <li>6-Rolünde Top olanların arasında Işınlan olan 1 kişi varsa ve middle rolüne uygun en az 1 kişi varsa o kişi
        Top+
    </li>
    <li>7-Rolünde Support olan kişilerden Bitkinlik olan 1 kişi varsa o kişi Support+</li>
    <li>8-Eğer rolünde support olan 1 kişi varsa o kişi supporttur+</li>
    <li>9-Eğer support yok ise ve takımda bitkinliği olan 1 kişi varsa o kişi Support+</li>
    <li>10-Supporttan başka rolü yoksa ve en yüksek Support oranına sahipse Support</li>
    <li>11-Middle rolüne uygun 1 den fazla kişi varsa ve bu kişiler arasından 1 kişide bitkinlik varsa o kişi Support+
    </li>
    <li>12-Eğer rolünde Middle olan varsa en yüksek oranlısı Middle+</li>
    <li>13-Eğer rolünde Top olan varsa en yüksek oranlısı Top+</li>
    <li>14-(Özel Kural) Son kalan kişiyi son kalan role yerleştir+</li>
</ul>


<div class="container-fluid">
    <h1><?=$ok_count?> OK</h1>
    <h1><?=$ram?> MB RAM USAGE</h1>
    <? foreach ($teams as $team) { ?>
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-6">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Champion</th>
                        <th scope="col">Spells</th>
                        <th scope="col">Position</th>
                        <th scope="col">Game Position</th>
                        <th scope="col">Test</th>
                        <th scope="col">Point</th>
                        <th scope="col">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($team as $champion) { ?>
                        <tr>
                            <td>
                                <img src="https://cdn.lols.gg/img/champion/<?=$champion->champDetails->key?>.png" style="width: 40px;">
                                <?= $champion->champDetails->name ?>
                            </td>
                            <td>
                                <img src="http://api.test/assets/api/images/spells/20/<?=$champion->spell1->key?>.png" style="width: 25px;" title"<?=$champion->spell1->name?>">
                                <img src="http://api.test/assets/api/images/spells/20/<?=$champion->spell2->key?>.png" style="width: 25px;" title"<?=$champion->spell2->name?>">
                            </td>
                            <td><?= $champion->position ?></td>
                            <td><?= $champion->game_position ?></td>
                            <td><?= $champion->test ?></td>
                            <td>point <?= $champion->point ?>.</td>
                            <td style="<?= $champion->status == 'OK' ? 'background-color: #6bff6b;' : '' ?>"><?= $champion->status ?></td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
            </div>
        </div>

    <? } ?>
</div>

<script src="<?= base_url() ?>assets/panel/js/jquery-3.3.1.min.js"></script>
<script src="<?= base_url() ?>assets/panel/js/bootstrap.min.js"></script>
</body>
</html>