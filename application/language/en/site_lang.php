<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang["site_meta_default_title"] = "LoL Profiles, Live Match, Summoner Stats, Match History";
$lang["site_meta_default_description"] = "League of Legends Real Time Live Match, Lookup Summoner, Match History, Champions, Leagues, Runes, Masteries and MMR. Up to date Builds and Statistics.";

$lang["site_meta_default_about_title"] = "About Us - LoL Profiles, Live Match, Summoner Stats, Match History";
$lang["site_meta_default_about_description"] = "Lols.gg is the fastest and the most accurate source you can use for Live Match Analysis. We choose the word ‘analysis’ because in Lols.gg we process the complex statistics data and give you the simplest form of it. Unlike others we actually aim on giving the most recent and the most accurate information available while providing the fastest service possible using the best technology.";

$lang["site_meta_default_recent_title"] = "Recent Games - LoL Profiles, Live Match, Summoner Stats, Match History";
$lang["site_meta_default_recent_description"] = "Lols.gg Recent Games Page for League of Legends Live Match, Active Players and Summoners, Match History and Match Archive.";

$lang["site_meta_default_strategy_board_title"] = "Strategy Board - LoL Stratgy Board, Tactics, Map, Summoner's Rift";
$lang["site_meta_default_strategy_board_description"] = "Lols.gg Strategy Board Page for League of Legends - Draw and Create Tactics, Save Image and Share with E-Sports Teams, New Tactics, New Meta, Summoner's Rift, Twisted Treeline, ARAM, LoL Strategy Board and Art.";

$lang["site_meta_default_live_title"] = "Live Match Lookup";
$lang["site_meta_default_live_description"] = "Refresh, Spectate, Share, Live Match, Summary, Champion, League, Runes, Masteries.";

$lang["site_meta_default_stream_badges_title"] = "Stream Badges";
$lang["site_meta_default_stream_badges_description"] = "Stream Badges, Twitch Streamers, Summoner Info, Live Match Info";

$lang["site_meta_default_masteries_title"] = "Masteries Page and Key Masteries";
$lang["site_meta_default_masteries_description"] = "Masteries Pages and Key Masteries – Masteries Build – Ferocity, Cunning, Resolve, Keystone, Key Mastery, Mastery Tree, Warlord’s Bloodlust, Fervor of Battle, Deathfire Touch, Stormraider’s Surge, Thunderlord’s Decree, Windspeaker’s Blessing, Grasp of Undying, Courage of Colossus, Stoneborn Pact, League of Legends.";

$lang["site_meta_default_runes_title"] = "Runes, Rune Pages & Rune Page Costs";
$lang["site_meta_default_runes_description"] = "Runes, Rune Pages, Rune Page Costs and Prices of Each Rune Piece, Total Rune Effects, Rune Pieces, Greater, Quintessence, Mark, Glyph, Seal, Rune Cost, Rune Price, IP, League of Legends.";

$lang["site_meta_default_leagues_title"] = "Ranked League Page";
$lang["site_meta_default_leagues_description"] = "League of Legends Ranked League – Solo / Duo – Flex – Challanger, Master, Diamond, Platinum, Gold, Silver, Bronze, Provisional, Unranked, Tier, Division, V, IV, III, II, I – Veteran,Hot Streak, Fresh Blood, Inactivity, Inactive Player - Summoner Stats";

$lang["site_meta_default_champions_title"] = "Champions & Champion Masteries";
$lang["site_meta_default_champions_description"] = "Champions & Champion Mastery Page – Champion Mastery Level, Champion Mastery Points, Hextech Chest Granted, Champion Token Earned, Characters, Heroes, LoL, League of Legends.";

$lang["site_meta_default_summary_title"] = "Profile and Summary – Summoner Stats";
$lang["site_meta_default_summary_description"] = "Profile and Summary of the Summoner. Everything you need to know. Stats, Graphs, Leagues, Ranked, Solo / Duo, Flex, 5v5, 3v3, Match History, Match Details, Spectate, Match Replay, Victory, Defeat, WinRate, Roles, Recently Played With, Friends, Builds, Live Match, Summary, Champions, Leagues, Runes, Masteries, Lolking, Lolskill, League of Legends.";

$lang["site_meta_champions_title"] = "{{summoner}} - {{region}} - Champions & Champion Masteries";
$lang["site_meta_champions_description"] = "{{summoner}} - {{region}} - Total Mastery Score: {{totalMasteryPoints}} - Champions & Champion Mastery Page – Champion Level, Champion Points, Hextech Chest Granted, Champion Token Earned, Characters, Heroes, LoL, League of Legends.";

$lang["site_meta_leagues_title"] = "{{summoner}} - {{region}} - Ranked League Page";
$lang["site_meta_leagues_description"] = "{{summoner}} - {{region}} – League of Legends Ranked League – Solo / Duo – Flex – Challanger, Master, Diamond, Platinum, Gold, Silver, Bronze, Provisional, Unranked, Tier, Division, V, IV, III, II, I – Veteran,Hot Streak, Fresh Blood, Inactivity, Inactive Player - Summoner Stats";

$lang["site_meta_runes_title"] = "{{summoner}} - {{region}} - Runes, Rune Pages & Rune Page Costs";
$lang["site_meta_runes_description"] = "{{summoner}} - {{region}} – Runes, Rune Pages, Rune Page Costs and Prices of Each Rune Piece, Total Rune Effects, Rune Pieces, Greater, Quintessence, Mark, Glyph, Seal, Rune Cost, Rune Price, IP, League of Legends.";

$lang["site_meta_masteries_title"] = "{{summoner}} - {{region}} - Masteries Page and Key Masteries";
$lang["site_meta_masteries_description"] = "{{summoner}} - {{region}} – Masteries Pages and Key Masteries – Masteries Build – Ferocity, Cunning, Resolve, Keystone, Key Mastery, Mastery Tree, Warlord’s Bloodlust, Fervor of Battle, Deathfire Touch, Stormraider’s Surge, Thunderlord’s Decree, Windspeaker’s Blessing, Grasp of Undying, Courage of Colossus, Stoneborn Pact, League of Legends.";

$lang["site_meta_live_title"] = "{{summoner}} - {{region}} - Live Match Lookup";
$lang["site_meta_live_description"] = "{{region}} - {{gameType}} - {{summoner}} - {{mapName}} – Refresh, Spectate, Share, Live Match, Summary, Champion, League, Runes, Masteries.";

$lang["site_meta_summary_title"] = "{{summoner}} - {{region}} - Profile and Summary – Summoner Stats";
$lang["site_meta_summary_description"] = "{{summoner}} - {{region}} – Profile and Summary of the Summoner. Everything you need to know. Stats, Graphs, Leagues, Ranked, Solo / Duo, Flex, 5v5, 3v3, Match History, Match Details, Spectate, Match Replay, Victory, Defeat, WinRate, Roles, Recently Played With, Friends, Builds, Live Match, Summary, Champions, Leagues, Runes, Masteries, Lolking, Mobafire, Op.gg, Lolskill, League of Legends.";

$lang["site_meta_default_leaderboards_title"] = "Leaderboards - LoL Profiles, Live Match, Summoner Stats, Match History";
$lang["site_meta_default_leaderboards_description"] = "Leaderboards - Lols.gg Summoner and Champion Ranking, Champions, Champion Mastery, KDA, Ranking, League Points, LP Ranking, World Ranking, Global Leaderboards";

$lang["site_meta_be_calculator_title"] = "Blue Essence Calculator - LoL Profiles, Live Match, Summoner Stats, Match History";
$lang["site_meta_be_calculator_description"] = "Blue Essence Calculator - Lols.gg Summoner and Champion Ranking, Champions, Champion Mastery, KDA, Ranking, League Points, LP Ranking, World Ranking, Global Leaderboards";

$lang["site_meta_builds_title"] = "Builds - LoL Profiles, Live Match, Summoner Stats, Match History";
$lang["site_meta_builds_description"] = "Builds - Lols.gg Summoner and Champion Ranking, Champions, Champion Mastery, KDA, Ranking, League Points, LP Ranking, World Ranking, Global Leaderboards";

$lang["site_cookies_law"] = "We are using cookies to give you the best experience on our site. Cookies are files stored in your browser and are used by most websites to help personalise your web experience. By continuing to use our website without changing the settings, you are agreeing to our use of cookies.";

$lang["site_under_construction"] = "Under Construction.";
$lang["site_menu_home"] = "Home"; // Home menu
$lang["site_menu_about"] = "About Us"; // About Us menu
$lang["site_menu_builds"] = "Builds";
$lang["site_menu_leaderboards"] = "Leaderboards";
$lang["site_menu_champions"] = "Champions"; // Champions menu
$lang["site_menu_recent_games"] = "Recent Games"; // Recent Games menu
$lang["site_menu_tools"] = "Tools"; // Tools menu
$lang["site_menu_strategy_board"] = "Strategy Board"; // Strategy Board Menu
$lang["site_menu_stream_badges"] = "Stream Badges";
$lang["site_menu_theory_crafting"] = "Theory Crafting";
$lang["site_menu_be_calculator"] = "Blue Essence Calculator";
$lang["site_menu_summoner_live"] = "Live Match"; // Live Match menu
$lang["site_menu_summoner_summary"] = "Summary"; // Summary menu
$lang["site_menu_summoner_champions"] = "Champions"; // Champions menu
$lang["site_menu_summoner_leagues"] = "Leagues"; // Leagues menu
$lang["site_menu_summoner_runes"] = "Runes"; // Runes menu
$lang["site_menu_summoner_masteries"] = "Masteries"; // Masteries menu
$lang["site_search_summoner"] = "Choose region, search summoner."; // Search summoner input placeholder
$lang["site_search_summoner_not_found"] = "{{summonerName}} is not a valid name!"; // Player name is not a valid name message.
$lang["site_search_languages"] = "Search languages.."; // Language selector placeholder
$lang["site_search_languages_not_found"] = "No Match."; // Language selector filter no matching language.
$lang["site_summoner_data_not_found"] = "Cannot retrieve summoner data.<br> Please try again."; // Player cant get server
$lang["site_cannot_retrieve"] = "Cannot retrieve '{{page}}' data.<br> Please try again."; // Page data cannot retrieved
$lang["site_recent_header"] = "Recent"; // Recent search list header
$lang["site_no_recent"] = "No recent searches."; // Recent search list empty text
$lang["site_favorite_header"] = "Favorite"; // Favorite list header
$lang["site_no_favorite"] = "No favorites yet."; // Favorite list empty text
$lang["site_text_ago"] = "ago"; // 1 days ago
$lang["site_text_live_match"] = "Live Match"; // Recent games match list live match button.
$lang["site_text_short_lp"] = "LP"; // Short league points
$lang["site_text_short_win"] = "W"; // Short 'Win' text. exp: 10W 5L 3P
$lang["site_text_short_lose"] = "L"; // Short 'Lose' text. exp: 10W 5L 3P
$lang["site_text_short_played"] = "P"; // Short 'Played' text. exp: 10W 5L 3P
$lang["site_text_top"] = "Top";
$lang["site_text_middle"] = "Middle";
$lang["site_text_bot"] = "Bottom";
$lang["site_text_support"] = "Support";
$lang["site_text_jungle"] = "Jungle";
$lang["site_text_fill"] = "Fill";
$lang["site_text_win"] = "Win";
$lang["site_text_lose"] = "Lose";
$lang["site_text_played"] = "Played";
$lang["site_text_short_kda"] = "KDA"; // Kill Death Assist short text.
$lang["site_text_add_favorite"] = "Add Favorite"; // Add Favorite button text
$lang["site_text_unranked"] = "Unranked"; // Unranked text
$lang["site_text_refreshing"] = "Refreshing"; // Some page (summary..) data refreshing state button.
$lang["site_text_refresh_data"] = "Refresh Data"; // Some page (summary..) data refresh button.
$lang["site_text_loading"] = "Loading"; // Loading text
$lang["site_text_load_more"] = "Load More"; // Load More text (summary, match list)
$lang["site_text_gold"] = "Gold"; // Gold text
$lang["site_text_damage"] = "Damage"; // Damage text
$lang["site_text_wards"] = "Wards"; // Wards text
$lang["site_text_short_cs"] = "CS"; // Minions and Jungle monsters short text
$lang["site_text_team_1"] = "Team 1"; // Team 1 text
$lang["site_text_team_2"] = "Team 2"; // Team 2 text
$lang["site_text_victory"] = "Victory"; // Victory text
$lang["site_text_defeat"] = "Defeat"; // Defeat text
$lang["site_text_remake"] = "Remake"; // Remake text
$lang["site_text_items"] = "Items"; // Items text
$lang["site_text_skills"] = "Skills"; // Skills text
$lang["site_text_cooldown"] = "Cooldown"; // Cooldown text
$lang["site_text_cost"] = "Cost"; // Cost text
$lang["site_text_range"] = "Range"; // Range text
$lang["site_text_series"] = "Series"; // Series text
$lang["site_text_free"] = "Free";
$lang["site_text_finished"] = "Finished";
$lang["site_text_vilemaw"] = "Vilemaw";
$lang["site_text_rift_herald"] = "Rift Herald";
$lang["site_text_baron"] = "Baron Nashor";
$lang["site_text_dragon"] = "Dragon";
$lang["site_text_inhibitor"] = "Inhibitor";
$lang["site_text_turret"] = "Turret";
$lang["site_text_other"] = "Other";
$lang["site_text_rank"] = "Rank";
$lang["site_text_cr_maintenance"] = "Champion Rankings Under Maintenance";
$lang["site_text_maintenance"] = "Under Maintenance";
$lang["site_text_league_maintenance"] = "The leaderboard of this region is being updated. It will be accessible once the update is done.";
$lang["site_loading_summoner_header"] = "Loading Summoner"; // Summoner loading bar header text
$lang["site_loading_summoner_content"] = "Fetching Summoner Data"; // Summoner loading bar description
$lang["site_game_type_solo_duo"] = "Solo / Duo"; // Solo / Duo game type text
$lang["site_game_type_flex_5v5"] = "Flex"; // Flex 5v5 Game Type Text
$lang["site_game_type_flex_3v3"] = "Flex 3v3"; // Flex 3v3 game type text
$lang["site_game_type_normal"] = "Normal";
$lang["site_game_type_aram"] = "ARAM";
$lang["site_game_type_custom"] = "Custom";
$lang["site_game_type_event"] = "Event Game";
$lang["site_color_red"] = "Red";
$lang["site_color_green"] = "Green";
$lang["site_color_blue"] = "Blue";
$lang["site_color_cyan"] = "Cyan";
$lang["site_color_yellow"] = "Yellow";
$lang["site_color_white"] = "White";
$lang["site_color_orange"] = "Orange";
$lang["site_color_black"] = "Black";
$lang["site_top_live_stream"] = "Top Live Stream";
$lang["site_social_follow_text"] = "Follow us in social media for fancy stuff!";
$lang["site_text_direct_link"] = "Direct Link";
$lang["site_text_copy"] = "Copy";
$lang["site_text_calculate"] = "Calculate";
$lang["site_text_free_champion_rotation"] = "Free Champion Rotation";

$lang["site_feedback_notice"] = "Please tell us what do you think!";
$lang["site_feedback_header"] = "Feedback Form";
$lang["site_feedback_message_text"] = "Message";
$lang["site_feedback_summoner_text"] = "Summoner Name";
$lang["site_feedback_send"] = "Submit Feedback";
$lang["site_feedback_success_header"] = "Feedback is sent!";
$lang["site_feedback_success_text"] = "Thank you! All these words mean a lot to us! We'll get back to you as soon as possible.";
$lang["site_feedback_error_header"] = "Feedback was not sent!";
$lang["site_feedback_error_text"] = "We are sorry :( Apparently feedback systems got broken. Could you try again later? Or send an e-mail? social@lols.gg";
$lang["site_feedback_validate_message"] = "Please enter a message";
$lang["site_feedback_validate_email"] = "Please enter a valid email address";
$lang["site_feedback_validate_captcha"] = "Captcha is required";

$lang["site_summoner_not_in_league_header"] = "You Are Below 30Lv and Have No Ranked Match :(";
$lang["site_summoner_not_in_league_content"] = "Riot Games (aka. Rito) does not provide normal games data anymore. (Other sites will be able to give you that information until July 24th 2017). Since it's going to be temprorary
we decided on not working on the old datas which are going to be deleted. However, you can view your Live Match, Champions, Runes and Masteries data.";
$lang["site_summoner_not_in_league_end_content"] = "We'd like you to check this page again when you have some ranked games though.";

// Home page bottom content 1 header
$lang["site_page_home_header_1"] = "Accuracy Equals Truth";

// Home page bottom content 1
$lang["site_page_home_content_1"] = "In League of Legends almost everything changes moment to moment. It is not only about you but also the opponents. We have developed a very intelligent system that allows us to show you what you need to see at present. Not the junk from the past. We are not like others. When you ask our system something, you will only get the truth.";

// Home page bottom content 2 header
$lang["site_page_home_header_2"] = "Lightspeed, Lightweight";

// Home page bottom content 2
$lang["site_page_home_content_2"] = "As gamers, we know that your time is invaluable. We respect that. In order to save your time and give the best service possible we use the latest technology (many of them actually) to give you what you need in no time. You will not even realize that the page loads :) Go ahead, try clicking on the Strategy Board link above for example.";

// Home page bottom content 3 header
$lang["site_page_home_header_3"] = "Higher Chance of Success";

// Home page bottom content 3
$lang["site_page_home_content_3"] = "We want you all to enjoy the game, no discrimination. When you use our system, you will see significant information about you, your teammates and especially about your opponents. So, you can shape your gameplay according to the statistics we give you which will lead you to success. How often they play, how good are they, do they focus on the kills or the mission, are they experienced or just started, a safe or aggressive player? We tell you all and more. The rest is in your hands though...";

// Home page bottom content 4 header
$lang["site_page_home_header_4"] = "We Are Just Getting Started";

// Home page bottom content 4
$lang["site_page_home_content_4"] = "So far we have just put what we had in our hands. There is still so much in our agenda to add in the system. There are the ideas we think are essential and the other ideas that would be fun and great. However, our purpose is to give what you want and what you need. In order to achieve that, we need your thoughts and feedback. Feel free to tell us what would you like. Is there any information that you would like to know, any other way of viewing it? Any feature... Please tell us. social@lols.gg (Or the bubble on the right bottom)";


// About Us page header
$lang["site_page_about_header"] = "About Us";

/* About Us Page Content */
$lang["site_page_about_content"] = "<p> Lols.gg is <b>the fastest</b> and the <b>most accurate</b> source you can use for Live Match Analysis and Summoner Profile Lookups. We choose the word ‘analysis’ because in Lols.gg we process the complex statistics data and give you the <b>simplest</b> form of it. Unlike others we actually aim on giving the most recent and the most accurate information available while providing the fastest service possible using the best technology. </p> <p><ins>Right now</ins> in Lols.gg you can…</p> <ul> <li>Use our system in 17 languages and 11 different LoL regions.</li> <li> Utilize the Live Match Lookup Feature and… <ul> <li>Check whether a summoner is in a match, or not.</li> <li>Learn the basic info about the ongoing match such as the game mode and map etc.</li> <li>See every summoner and chosen champions in a match and see their ranked success, runes, masteries and more.</li> <li>Get tips about the enemy champions in a Live Match.</li> <li>Spectate the match on your League of Legends client.</li> </ul> </li> <li> Browse through Summoner Profiles and… <ul> <li>Skim through the summary and get quick information about the summoner.</li> <li>View detailed Match Histories and even share them on social media.</li> <li>Find out the people they play with.</li> <li>See their champions and their champion masteries and view however you like.</li> <li>Examine their league and compare them with other people in the table.</li> <li>Glance at their Rune and Masteries Pages in the modernized new style and see the cost of the Rune Pages you view. (Only at Lols.gg)</li> </ul> </li> <li>Use Recent Games page and find an active match for yourself and spectate it if you like. </li> </ul> <p>As gamers, we know what do gamers need and want. </p> <p>We shape our system according to the needs and the demands of gamers.</p> <p>(This means that we are looking for feedbacks. Tell us, ask us. We will consider any ideas and respond to that. social@lols.gg)</p> <h2 class='py-3'>What is next?</h2> <p>We have many ideas waiting to be implemented in the system. Some of them are essentials and some of them are for fun. We keep changing order of them, so it is not very possible to say ‘this’ will be our next feature.</p> <p>However, detailed champion statistics and builds are around the corner. :)</p> <h2 class='py-3'>How about the future?</h2> <p>The <ins>future</ins> of Lols.gg is what you know as AI aka Artificial Intelligence. The specialists in our team has been developing an unimaginable Machine Learning system that feeds itself and analyzes uncountable number of variables. By using the outcome of the mentioned analyses the we are going to tell our users exactly what they need.</p> <p>So that we can …</p> <ul> <li>Improve their gaming experience.</li> <li>Tell gamers what they should do to be better than others.</li> <li>Recommend what they should NOT do. </li> <li>Give you and them a higher chance… to WIN!</li> </ul> <h4 class='type-font text-italic text-right'>We do the complex... so that you get the simple.</h4> <div class='type-font text-italic text-right'>- Lols.gg Team</div>";

$lang["site_page_stream_badges_header"] = "Stream Badges";
$lang["site_page_stream_badges_login_header"] = "Streamer Login";
$lang["site_page_stream_badges_login_description"] = "Hey! If you'd like to use our Stream Badge feature, please send an e-mail to us with the following informations!";
$lang["site_page_stream_badges_login_error"] = "Username or password is incorrect.";
$lang["site_page_stream_badges_login_username"] = "Username";
$lang["site_page_stream_badges_login_password"] = "Password";
$lang["site_page_stream_badges_login_sign_in"] = "Sign In";
$lang["site_page_stream_badges_save"] = "Save";
$lang["site_page_stream_badges_sign_out"] = "Sign Out";
$lang["site_page_stream_badges_new_badge"] = "New badge";
$lang["site_page_stream_badges_summoner_data"] = "Summoner Data";
$lang["site_page_stream_badges_animation_align"] = "Animation & Align";
$lang["site_page_stream_badges_show_hide"] = "Show/Hide";
$lang["site_page_stream_badges_background"] = "Background";
$lang["site_page_stream_badges_background_shadow"] = "Background Shadow";
$lang["site_page_stream_badges_champion"] = "Champion";
$lang["site_page_stream_badges_text_colors"] = "Text Colors";
$lang["site_page_stream_badges_text_shadow"] = "Text Shadow";
$lang["site_page_stream_badges_summoner_name"] = "Summoner Name";
$lang["site_page_stream_badges_region"] = "Region";
$lang["site_page_stream_badges_animation"] = "Animation";
$lang["site_page_stream_badges_text_align"] = "Text Align";
$lang["site_page_stream_badges_show_champion_image"] = "Show Champion Image";
$lang["site_page_stream_badges_show_league_tier"] = "Show League Tier";
$lang["site_page_stream_badges_show_winrate"] = "Show WinRate";
$lang["site_page_stream_badges_live_match_check"] = "Live Match Check";
$lang["site_page_stream_badges_border"] = "Border";
$lang["site_page_stream_badges_radius"] = "Radius";
$lang["site_page_stream_badges_horizontal"] = "Horizontal";
$lang["site_page_stream_badges_vertical"] = "Vertical";
$lang["site_page_stream_badges_blur"] = "Blur";
$lang["site_page_stream_badges_color"] = "Color";
$lang["site_page_stream_badges_border_color"] = "Border Color";
$lang["site_page_stream_badges_league"] = "League";
$lang["site_page_stream_badges_summoner"] = "Summoner";
$lang["site_page_stream_badges_winrate"] = "WinRate";
$lang["site_page_stream_badges_link"] = "Link";
$lang["site_page_stream_badges_left"] = "Left";
$lang["site_page_stream_badges_right"] = "Right";
$lang["site_page_stream_badges_center"] = "Center";

$lang["site_page_strategy_board_header"] = "Strategy Board";
$lang["site_page_strategy_board_how_to_save"] = "Save";
$lang["site_page_strategy_board_clear"] = "Clear";
$lang["site_page_strategy_board_map"] = "Map";
$lang["site_page_strategy_board_tools"] = "Tools";
$lang["site_page_strategy_board_remove"] = "Remove";
$lang["site_page_strategy_board_edit"] = "Edit";
$lang["site_page_strategy_board_share"] = "Screen Share";
$lang["site_page_strategy_board_share_end"] = "End Screen Share";
$lang["site_page_strategy_board_actions"] = "Actions";
$lang["site_page_strategy_board_upload"] = "Upload";
$lang["site_page_strategy_board_note"] = "Note";
$lang["site_page_strategy_board_reset_zoom"] = "Reset Zoom";
$lang["site_page_strategy_board_add"] = "Add";
$lang["site_page_strategy_board_help"] = "Help";
$lang["site_page_strategy_board_text_placeholder"] = "Add Text";
$lang["site_page_strategy_board_utilities"] = "Utilities";
$lang["site_page_strategy_board_champions"] = "Champions";
$lang["site_page_strategy_board_ward"] = "Sight Ward";
$lang["site_page_strategy_board_control_ward"] = "Control Ward";
$lang["site_page_strategy_board_minion_caster_blue"] = "Caster Minion - Blue";
$lang["site_page_strategy_board_minion_caster_red"] = "Caster Minion - Red";
$lang["site_page_strategy_board_minion_melee_blue"] = "Melee Minion - Blue";
$lang["site_page_strategy_board_minion_melee_red"] = "Melee Minion - Red";
$lang["site_page_strategy_board_minion_siege_blue"] = "Siege Minion - Blue";
$lang["site_page_strategy_board_minion_siege_red"] = "Siege Minion - Red";
$lang["site_page_strategy_board_minion_super_blue"] = "Super Minion - Blue";
$lang["site_page_strategy_board_minion_super_red"] = "Super Minion - Red";
$lang["site_page_strategy_board_blank"] = "Blank";
$lang["site_page_strategy_board_sr"] = "Summoner's Rift";
$lang["site_page_strategy_board_ha"] = "Howling Abyss";
$lang["site_page_strategy_board_tt"] = "Twisted Treeline";
$lang["site_page_strategy_board_help_drawing"] = "Drawing Tool";
$lang["site_page_strategy_board_help_move"] = "Move Object";
$lang["site_page_strategy_board_help_drag"] = "Canvas Drag";
$lang["site_page_strategy_board_help_reset"] = "Reset Zoom";
$lang["site_page_strategy_board_help_delete"] = "Delete Chosen Object";
$lang["site_page_strategy_board_help_undo"] = "Undo";
$lang["site_page_strategy_board_help_redo"] = "Redo";
$lang["site_page_strategy_board_help_save"] = "Save Current Canvas Image and Open in New Tab";
$lang["site_page_strategy_board_help_clear"] = "Clear Map Canvas (Complete Reset)";
$lang["site_page_strategy_board_help_copy"] = "Copy";
$lang["site_page_strategy_board_help_paste"] = "Paste";
$lang["site_page_strategy_board_help_cut"] = "Cut";
$lang["site_page_strategy_board_help_duplicate"] = "Duplicate (Clone)";
$lang["site_page_strategy_board_copied"] = "Copied";
$lang["site_page_strategy_board_get_link"] = "Generate Link";
$lang["site_page_strategy_board_generating"] = "Generating...";
$lang["site_page_strategy_board_edit_code_not_generated"] = "Key not generated";
$lang["site_page_strategy_board_edit_code_desc"] = "Keep this key to unlock editing later on.";
$lang["site_page_strategy_board_edit_code_header"] = "Edit Key";
$lang["site_page_strategy_board_delete_modal_header"] = "Delete";
$lang["site_page_strategy_board_delete_modal_content"] = "This will delete your work permanently.<br> Are you sure?";
$lang["site_page_strategy_board_delete_modal_cancel_button"] = "Cancel";
$lang["site_page_strategy_board_delete_modal_delete_button"] = "Delete";
$lang["site_page_strategy_board_link_get_success"] = "Canvas Loaded!";
$lang["site_page_strategy_board_link_get_error"] = "Can't Load Canvas!";
$lang["site_page_strategy_board_link_save"] = "Canvas Saved and Link Generated!";
$lang["site_page_strategy_board_link_save_error"] = "Canvas Could Not Be Saved!";
$lang["site_page_strategy_board_link_delete"] = "Canvas Has Been Deleted Permanently!";
$lang["site_page_strategy_board_link_delete_error"] = "Deleting Failed!";
$lang["site_page_strategy_board_edit_code_error"] = "Wrong or No Edit Key Entered!";
$lang["site_page_strategy_board_link_get_limit"] = "You can generate ONLY 20 links per hour.";
$lang['site_page_strategy_board_get_link_not_yet'] = "Click for generating a link to share your strategy.";
$lang["site_page_strategy_board_mobile_warning"] = "This feature is not available to mobile devices.";

$lang["site_page_recent_header"] = "Recent Games"; // Recent Games page header
$lang["site_page_recent_region_filter"] = "All Regions"; // Recent games region filter
$lang["site_page_recent_not_found"] = "No Matches Found in this Region."; // Recent games not found matches text

$lang["site_page_live_main_champs_not_notice"] = "Not played any games for two weeks";
$lang["site_page_live_main_champs_notice"] = "Last 2 weeks";
$lang["site_page_live_main_champ_tag_tooltip"] = "Main Champion";
$lang["site_page_live_champion_rank_info"] = "Champion Leaderboard Rank";
$lang["site_page_live_fetching"] = "Fetching Match Data"; // Live Match page loading text
$lang["site_page_live_btn_refresh"] = "Refresh"; // Live Match page refresh button
$lang["site_page_live_btn_share"] = "Share"; // Live Match page share button
$lang["site_page_live_btn_spectate"] = "Spectate"; // Live Match page spectate button
$lang["site_page_live_head_champion"] = "Champion"; // Live Match page champion table header
$lang["site_page_live_head_summoner"] = "Summoner"; // Live Match page summoner table header
$lang["site_page_live_head_ranked_wins"] = "Ranked Wins"; // Live Match page ranked wins table header
$lang["site_page_live_head_solo_duo"] = "Solo / Duo"; // Live Match page solo/duo table header
$lang["site_page_live_head_flex"] = "Flex"; // Live Match page flex table header
$lang["site_page_live_head_main_champ"] = "Main Champions";
$lang["site_page_live_head_kda"] = "KDA";
$lang["site_page_live_head_wins"] = "Wins";
$lang["site_page_live_tooltip_champion_games"] = "The number of games played with this champion.";
$lang["site_page_live_tooltip_mastery_points"] = "Mastery Points:"; // Live Match page champion stars tooltip mastery points text
$lang["site_page_live_tooltip_mastery_level"] = "Mastery Level:"; // Live Match page champion stars tooltip mastery level text
$lang["site_page_live_tooltip_champion_points"] = "Champion Points:"; // Live Match page champion start tooltip champion points
$lang["site_page_live_tooltip_runes"] = "Click for Rune Page"; // Live Match page rune button tooltip
$lang["site_page_live_tooltip_masteries"] = "Click for Mastery Page"; // Live Match page mastery button tooltip
$lang["site_page_live_banned_champions"] = "Banned Champions"; // Live Match page table footer banned champions text
$lang["site_page_live_no_data"] = "The summoner is not currently in a game."; // Live Match page player is not currently in a game text
$lang["site_page_live_text_now_playing"] = "Now Playing"; // Live Match page social share text.
$lang["site_page_live_spectate_text_1"] = "If you use BaronReplays, select <b>FILE → ANALYZE COMMAND</b> and Copy/Paste into the box."; // Live Match page spectate modal text 1
$lang["site_page_live_spectate_text_2"] = "To spectate this game, press <b>Win+R</b> (or open the Run Window) and paste this in:"; // Live Match page spectate modal text 2
$lang["site_page_live_spectate_text_3"] = "You may need to update the location to the League of Legends game executable."; // Live Match page spectate modal text 3
$lang["site_page_live_spectate_text_4"] = "After running <b>Terminal</b>, copy and paste the message below."; // Live Match page spectate modal text 4

$lang["site_page_summary_fetching"] = "Fetching summary data"; // Summary page loading text
$lang["site_page_summary_no_league_data"] = "No League Data"; // Summary page no league data text
$lang["site_page_summary_header_played_with"] = "Played With (in Last 20 Games)"; // Summary page played with block header text
$lang["site_page_summary_no_match_data"] = "No Match Data"; // Summary page no match data text
$lang["site_page_summary_header_summoner"] = "Summoner"; // Summary page played with block table header summoner text
$lang["site_page_summary_header_pwl"] = "P / W / L"; // Summary page played with block table header play/win/lose text
$lang["site_page_summary_header_ratio"] = "Ratio"; // Summary page played with block table header ratio text
$lang["site_page_summary_no_pw_data"] = "No Played With Data"; // Summary page no played with data text
$lang["site_page_summary_all_queues"] = "All Queues"; // Summary page top all queues button.
$lang["site_page_summary_no_champion_data"] = "No Champion Data"; // Summary page no champion data (max: 20 characters)
$lang["site_page_summary_header_wins"] = "Wins"; // Summary page wins block header
$lang["site_page_summary_details_overview"] = "Overview"; // Summary page match details overview tab header
$lang["site_page_summary_details_builds"] = "Builds"; // Summary page match details build tab header
$lang["site_page_summary_no_summary_data"] = "No Summary Data."; // Summary page no summary data
$lang["site_page_summary_overview_tooltip_total_damage_2_champ"] = "Damage to Champions:"; // Summary page details overview damage tooltip damage dealt
$lang["site_page_summary_overview_tooltip_total_damage"] = "Total Damage:"; // Summary page details overview damage tooltip total damage
$lang["site_page_summary_overview_tooltip_total_damage_ratio"] = "Damage Ratio:"; //
$lang["site_page_summary_overview_tooltip_control_ward"] = "Control Ward:"; // Summary page details overview wards tooltip control ward
$lang["site_page_summary_overview_tooltip_wards_placed"] = "Wards Placed:"; // Summary page details overview wards tooltip wards placed
$lang["site_page_summary_overview_tooltip_wards_killed"] = "Wards Killed:"; // Summary page details overview wards tooltip wards killed
$lang["site_page_summary_builds_final_build"] = "Final Build"; // Summary page details builds section final build text
$lang["site_page_summary_matches_updated"] = "Matches Updated"; // Summary page refresh data success status text
$lang["site_page_summary_load_more_loaded"] = "10 More Matches Loaded"; // Summary page load more button succes status text
$lang["site_page_summary_load_more_no_data"] = "No More Match Data"; // Summary page load more button no more match status text
$lang["site_page_summary_pkill_tooltip"] = "The Summoner's Participation Ratio on the Total Kill Number of the Team. (Kills and Assists)"; // Summary page handshake icon tooltip text
$lang["site_page_summary_get_your_profile_card"] = "Get Your Profile Card";
$lang["site_page_summary_create_profile_card"] = "Create Profile Card";
$lang["site_page_summary_mmr_heading"] = "Solo MMR";
$lang["site_page_summary_update_warning"] = "Your profile is not up to date. Click on 'Refresh Data' to update.";

$lang["site_page_champions_fetching"] = "Fetching Champions Data";
$lang["site_page_champions_total_mastery_score"] = "Total Mastery Score";
$lang["site_page_champions_search_champion"] = "Search Champion";
$lang["site_page_champions_filter_role_all"] = "All Roles";
$lang["site_page_champions_filter_role_assassin"] = "Assassin";
$lang["site_page_champions_filter_role_fighter"] = "Fighter";
$lang["site_page_champions_filter_role_mage"] = "Mage";
$lang["site_page_champions_filter_role_marksman"] = "Marksman";
$lang["site_page_champions_filter_role_support"] = "Support";
$lang["site_page_champions_filter_role_tank"] = "Tank";
$lang["site_page_champions_filter_mastery_points"] = "Mastery Points";
$lang["site_page_champions_filter_mastery_level"] = "Mastery Level";
$lang["site_page_champions_filter_last_played"] = "Last Played";
$lang["site_page_champions_filter_alphabetic"] = "Alphabetical";
$lang["site_page_champions_filter_chest_granted"] = "Chest Granted";
$lang["site_page_champions_filter_no_champions"] = "No matching champions";
$lang["site_page_champions_tooltip_chest_granted"] = "Chest Granted";
$lang["site_page_champions_tooltip_tokens_earned"] = "Tokens Earned:";
$lang["site_page_champions_tooltip_last_played"] = "Lastly Played";
$lang["site_page_champions_no_data"] = "No Champion Data.";

$lang["site_page_leagues_fetching"] = "Fetching League Data";
$lang["site_page_leagues_header_summoners"] = "Summoners";
$lang["site_page_leagues_header_emblems"] = "Emblems";
$lang["site_page_leagues_header_games"] = "Games";
$lang["site_page_leagues_header_victory"] = "Victories";
$lang["site_page_leagues_header_defeats"] = "Defeats";
$lang["site_page_leagues_header_series_lp"] = "Series / LP";
$lang["site_page_leagues_tooltip_veteran"] = "Veteran: Played 100 or more games in this league.";
$lang["site_page_leagues_tooltip_hot_streak"] = "Hot Streak: Won 3 or more games in a row.";
$lang["site_page_leagues_tooltip_fresh_blood"] = "Fresh Blood: Recently joined this league.";
$lang["site_page_leagues_tooltip_inactive_player"] = "Inactive Player";
$lang["site_page_leagues_no_data"] = "No League Data.";

$lang["site_page_runes_fetching"] = "Fetching Runes Data";
$lang["site_page_runes_rune_pieces"] = "Rune Pieces";
$lang["site_page_runes_rune_effects"] = "Rune Effects";
$lang["site_page_runes_page_cost"] = "Page Cost";
$lang["site_page_runes_no_data"] = "Due to the changes made with the new rune system, display of rune pages has been disabled by Riot Games.<br><br>Currently we are working on a new system that will enable us to display summoner rune pages. <br><br>Until then, this page will be useless :(<br><br>Stay Tuned!";

$lang["site_page_masteries_fetching"] = "Fetching Masteries Data";
$lang["site_page_masteries_ferocity"] = "Ferocity";
$lang["site_page_masteries_cunning"] = "Cunning";
$lang["site_page_masteries_resolve"] = "Resolve";
$lang["site_page_masteries_no_data"] = "No Mastery Data.";

$lang["site_page_lboards_search_summoner"] = "Search summoner";
$lang["site_page_lboards_tab_champion_stats"] = "Champion Stats";
$lang["site_page_lboards_tab_champion_masteries"] = "Champion Masteries";
$lang["site_page_lboards_h_champion_name"] = "Champion Name";
$lang["site_page_lboards_h_summoner"] = "Summoner";
$lang["site_page_lboards_h_ranked_tier"] = "Ranked Tier";
$lang["site_page_lboards_h_team"] = "Team";
$lang["site_page_lboards_h_lp"] = "League Points";
$lang["site_page_lboards_h_win_lose"] = "Win / Lose";
$lang["site_page_lboards_h_win_ratio"] = "Win Ratio";
$lang["site_page_lboards_h_nodata"] = "No newer data";
$lang["site_page_lboards_h_kda"] = "KDA";
$lang["site_page_lboards_h_played"] = "Played";
$lang["site_page_lboards_h_win"] = "Win";
$lang["site_page_lboards_h_lose"] = "Lose";
$lang["site_page_lboards_h_most_kill"] = "Most Kill";
$lang["site_page_lboards_h_pentakills"] = "Pentakills";
$lang["site_page_lboards_h_most_death"] = "Most Death";
$lang["site_page_lboards_h_avg_cs"] = "Avg. CS";
$lang["site_page_lboards_h_avg_gold"] = "Avg. Gold";
$lang["site_page_lboards_h_avg_death"] = "Avg. Death";
$lang["site_page_lboards_h_avg_kill"] = "Avg. Kill";
$lang["site_page_lboards_h_avg_assists"] = "Avg. Assits";
$lang["site_page_lboards_h_avg_dmg_dealt"] = "Avg. Dmg. Dealt";
$lang["site_page_lboards_h_avg_dmg_taken"] = "Avg. Dmg. Taken";
$lang["site_page_lboards_h_level"] = "Level";
$lang["site_page_lboards_h_points"] = "Points";
$lang["site_page_lboards_w_stat_selected"] = "stat selected";
$lang["site_page_lboards_w_stats_selected"] = "stats selected";
$lang["site_page_lboards_w_select_stats"] = "Select Stat";
$lang["site_page_lboards_all_champions"] = "All Champions";
$lang["site_page_lboards_max_filter"] = "Max 7 Filters";
$lang["site_page_lboards_min_played"] = "Min played";

$lang["site_page_be_calculator_blue_essence"] = "Blue Essence";
$lang["site_page_be_calculator_ip"] = "IP";
$lang["site_page_be_calculator_rune_pages"] = "Rune Pages (IP)";
$lang["site_page_be_calculator_mark"] = "Marks";
$lang["site_page_be_calculator_seal"] = "Seals";
$lang["site_page_be_calculator_glyph"] = "Glyphs";
$lang["site_page_be_calculator_quintessence"] = "Quintessences";
$lang["site_page_be_calculator_in_account"] = "In Account";
$lang["site_page_be_calculator_be_received"] = "Blue Essence Equivalents";
$lang["site_page_be_calculator_total_be_received"] = "Total Blue Essence to be Received";

$lang["site_builds_live_header"] = "Recommended Build";
$lang["site_builds_live_alternatives"] = "Alternatives:";
$lang["site_builds_live_against"] = "Against:";
$lang["site_builds_back_to_champions"] = "Back to Champions";

$lang["site_per_level"] = "per Level";
$lang["site_as_ally"] = "As an Ally";
$lang["site_as_enemy"] = "As an Enemy";
$lang["site_champion_kill"] = "Champion Kill";
$lang["site_gold_earned"] = "Gold Earned";
$lang["site_damage_dealt_to_champions"] = "Damage Dealt to Champions";
$lang["site_wards_placed"] = "Wards Placed";
$lang["site_damage_taken"] = "Damage Taken";
$lang["site_farsight_alteration"] = "Farsight Alteration";
$lang["site_sightstone"] = "Sightstone";
$lang["site_vision_ward"] = "Vision Ward";
$lang["site_warding_totem_trinket"] = "Warding Totem (Trinket)";
$lang["site_warding totem_upgrade_trinket"] = "Warding Totem Upgrade (Trinket)";
$lang["site_winner_team"] = "Victorious Team";
$lang["site_loser_team"] = "Defeated Team";
$lang["site_p_kill"] = "Kill Participation:";
$lang["site_tower"] = "Tower";
$lang["site_inhibitor"] = "Inhibitor";
$lang["site_minions"] = "Minions";
$lang["site_level"] = "Level";
$lang["site_year"] = "Year";
$lang["site_month"] = "Month";
$lang["site_week"] = "Week";
$lang["site_day"] = "Day";
$lang["site_hour"] = "Hour";
$lang["site_min"] = "Min";
$lang["site_sec"] = "Sec";
$lang["site_years"] = "Years";
$lang["site_months"] = "Months";
$lang["site_weeks"] = "Weeks";
$lang["site_days"] = "Days";
$lang["site_hours"] = "Hours";
$lang["site_mins"] = "Mins";
$lang["site_secs"] = "Secs";
