<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang["site_meta_default_title"] = "LoL Profiles, Live Match, Summoner Stats, Match History";
$lang["site_meta_default_description"] = "League of Legends Real Time Live Match, Lookup Summoner, Match History, Champions, Leagues, Runes, Masteries and MMR. Up to date Builds and Statistics.";

$lang["site_meta_default_about_title"] = "About Us - LoL Profiles, Live Match, Summoner Stats, Match History";
$lang["site_meta_default_about_description"] = "Lols.gg is the fastest and the most accurate source you can use for Live Match Analysis. We choose the word ‘analysis’ because in Lols.gg we process the complex statistics data and give you the simplest form of it. Unlike others we actually aim on giving the most recent and the most accurate information available while providing the fastest service possible using the best technology.";

$lang["site_meta_default_recent_title"] = "Recent Games - LoL Profiles, Live Match, Summoner Stats, Match History";
$lang["site_meta_default_recent_description"] = "Lols.gg Recent Games Page for League of Legends Live Match, Active Players and Summoners, Match History and Match Archive.";

$lang["site_meta_default_strategy_board_title"] = "Strategy Board - LoL Stratgy Board, Tactics, Map, Summoner's Rift";
$lang["site_meta_default_strategy_board_description"] = "Lols.gg Strategy Board Page for League of Legends - Draw and Create Tactics, Save Image and Share with E-Sports Teams, New Tactics, New Meta, Summoner's Rift, Twisted Treeline, ARAM, LoL Strategy Board and Art.";

$lang["site_meta_default_live_title"] = "Live Match Lookup";
$lang["site_meta_default_live_description"] = "Refresh, Spectate, Share, Live Match, Summary, Champion, League, Runes, Masteries.";

$lang["site_meta_default_stream_badges_title"] = "Stream Badges";
$lang["site_meta_default_stream_badges_description"] = "Stream Badges, Twitch Streamers, Summoner Info, Live Match Info";

$lang["site_meta_default_masteries_title"] = "Masteries Page and Key Masteries";
$lang["site_meta_default_masteries_description"] = "Masteries Pages and Key Masteries – Masteries Build – Ferocity, Cunning, Resolve, Keystone, Key Mastery, Mastery Tree, Warlord’s Bloodlust, Fervor of Battle, Deathfire Touch, Stormraider’s Surge, Thunderlord’s Decree, Windspeaker’s Blessing, Grasp of Undying, Courage of Colossus, Stoneborn Pact, League of Legends.";

$lang["site_meta_default_runes_title"] = "Runes, Rune Pages & Rune Page Costs";
$lang["site_meta_default_runes_description"] = "Runes, Rune Pages, Rune Page Costs and Prices of Each Rune Piece, Total Rune Effects, Rune Pieces, Greater, Quintessence, Mark, Glyph, Seal, Rune Cost, Rune Price, IP, League of Legends.";

$lang["site_meta_default_leagues_title"] = "Ranked League Page";
$lang["site_meta_default_leagues_description"] = "League of Legends Ranked League – Solo / Duo – Flex – Challanger, Master, Diamond, Platinum, Gold, Silver, Bronze, Provisional, Unranked, Tier, Division, V, IV, III, II, I – Veteran,Hot Streak, Fresh Blood, Inactivity, Inactive Player - Summoner Stats";

$lang["site_meta_default_champions_title"] = "Champions & Champion Masteries";
$lang["site_meta_default_champions_description"] = "Champions & Champion Mastery Page – Champion Mastery Level, Champion Mastery Points, Hextech Chest Granted, Champion Token Earned, Characters, Heroes, LoL, League of Legends.";

$lang["site_meta_default_summary_title"] = "Profile and Summary – Summoner Stats";
$lang["site_meta_default_summary_description"] = "Profile and Summary of the Summoner. Everything you need to know. Stats, Graphs, Leagues, Ranked, Solo / Duo, Flex, 5v5, 3v3, Match History, Match Details, Spectate, Match Replay, Victory, Defeat, WinRate, Roles, Recently Played With, Friends, Builds, Live Match, Summary, Champions, Leagues, Runes, Masteries, Lolking, Lolskill, League of Legends.";

$lang["site_meta_champions_title"] = "{{summoner}} - {{region}} - Champions & Champion Masteries";
$lang["site_meta_champions_description"] = "{{summoner}} - {{region}} - Total Mastery Score: {{totalMasteryPoints}} - Champions & Champion Mastery Page – Champion Level, Champion Points, Hextech Chest Granted, Champion Token Earned, Characters, Heroes, LoL, League of Legends.";

$lang["site_meta_leagues_title"] = "{{summoner}} - {{region}} - Ranked League Page";
$lang["site_meta_leagues_description"] = "{{summoner}} - {{region}} – League of Legends Ranked League – Solo / Duo – Flex – Challanger, Master, Diamond, Platinum, Gold, Silver, Bronze, Provisional, Unranked, Tier, Division, V, IV, III, II, I – Veteran,Hot Streak, Fresh Blood, Inactivity, Inactive Player - Summoner Stats";

$lang["site_meta_runes_title"] = "{{summoner}} - {{region}} - Runes, Rune Pages & Rune Page Costs";
$lang["site_meta_runes_description"] = "{{summoner}} - {{region}} – Runes, Rune Pages, Rune Page Costs and Prices of Each Rune Piece, Total Rune Effects, Rune Pieces, Greater, Quintessence, Mark, Glyph, Seal, Rune Cost, Rune Price, IP, League of Legends.";

$lang["site_meta_masteries_title"] = "{{summoner}} - {{region}} - Masteries Page and Key Masteries";
$lang["site_meta_masteries_description"] = "{{summoner}} - {{region}} – Masteries Pages and Key Masteries – Masteries Build – Ferocity, Cunning, Resolve, Keystone, Key Mastery, Mastery Tree, Warlord’s Bloodlust, Fervor of Battle, Deathfire Touch, Stormraider’s Surge, Thunderlord’s Decree, Windspeaker’s Blessing, Grasp of Undying, Courage of Colossus, Stoneborn Pact, League of Legends.";

$lang["site_meta_live_title"] = "{{summoner}} - {{region}} - Live Match Lookup";
$lang["site_meta_live_description"] = "{{region}} - {{gameType}} - {{summoner}} - {{mapName}} – Refresh, Spectate, Share, Live Match, Summary, Champion, League, Runes, Masteries.";

$lang["site_meta_summary_title"] = "{{summoner}} - {{region}} - Profile and Summary – Summoner Stats";
$lang["site_meta_summary_description"] = "{{summoner}} - {{region}} – Profile and Summary of the Summoner. Everything you need to know. Stats, Graphs, Leagues, Ranked, Solo / Duo, Flex, 5v5, 3v3, Match History, Match Details, Spectate, Match Replay, Victory, Defeat, WinRate, Roles, Recently Played With, Friends, Builds, Live Match, Summary, Champions, Leagues, Runes, Masteries, Lolking, Mobafire, Op.gg, Lolskill, League of Legends.";

$lang["site_meta_default_leaderboards_title"] = "Leaderboards - LoL Profiles, Live Match, Summoner Stats, Match History";
$lang["site_meta_default_leaderboards_description"] = "Leaderboards - Lols.gg Summoner and Champion Ranking, Champions, Champion Mastery, KDA, Ranking, League Points, LP Ranking, World Ranking, Global Leaderboards";

$lang["site_cookies_law"] = "Nous utilisons des cookies pour vous donner la meilleure expérience sur notre site. Ils sont des fichiers stockés dans votre navigateur et sont utilisés par la plupart des sites Web pour vous aider à personnaliser votre expérience Web. En continuant à utiliser notre site Web sans changer les paramètres, vous acceptez notre utilisation des cookies.";

$lang["site_under_construction"] = "En progression.";
$lang["site_menu_home"] = "Accueil"; // Home menu
$lang["site_menu_about"] = "À propos de nous"; // About Us menu
$lang["site_menu_leaderboards"] = "Tableaux de bord";
$lang["site_menu_champions"] = "Champions"; // Champions menu
$lang["site_menu_recent_games"] = "Jeux récents"; // Recent Games menu
$lang["site_menu_tools"] = "Outils"; // Tools menu
$lang["site_menu_strategy_board"] = "Conseil de stratégie"; // Strategy Board Menu
$lang["site_menu_stream_badges"] = "Assistance des badges";
$lang["site_menu_theory_crafting"] = "Création théorique";
$lang["site_menu_be_calculator"] = "Blue Essence Calculator";
$lang["site_menu_summoner_live"] = "Match en direct"; // Live Match menu
$lang["site_menu_summoner_summary"] = "Résumé"; // Summary menu
$lang["site_menu_summoner_champions"] = "Champions"; // Champions menu
$lang["site_menu_summoner_leagues"] = "Leagues"; // Leagues menu
$lang["site_menu_summoner_runes"] = "Runes"; // Runes menu
$lang["site_menu_summoner_masteries"] = "Maîtrises"; // Masteries menu
$lang["site_search_summoner"] = "Choisissez la région, recherche d’invocateur."; // Search summoner input placeholder
$lang["site_search_summoner_not_found"] = "{{summonerName}} n'est pas un nom valide!"; // Player name is not a valid name message.
$lang["site_search_languages"] = "Langues de recherche."; // Language selector placeholder
$lang["site_search_languages_not_found"] = "Pas de correspondance."; // Language selector filter no matching language.
$lang["site_summoner_data_not_found"] = "Impossible de récupérer les données de l'invocateur. <br> s'il vous plaît, essayer à nouveau."; // Player cant get server
$lang["site_cannot_retrieve"] = "Impossible de récupérer '{{page}} des données'. <br> s'il vous plaît, essayer à nouveau."; // Page data cannot retrieved
$lang["site_recent_header"] = "Récent"; // Recent search list header
$lang["site_no_recent"] = "Pas de recherches récentes."; // Recent search list empty text
$lang["site_favorite_header"] = "Favoris"; // Favorite list header
$lang["site_no_favorite"] = "Aucun favoris pour l'instant."; // Favorite list empty text
$lang["site_text_ago"] = "jadis"; // 1 days ago
$lang["site_text_live_match"] = "Match  en direct"; // Recent games match list live match button.
$lang["site_text_short_lp"] = "LP"; // Short league points
$lang["site_text_short_win"] = "W"; // Short 'Win' text. exp: 10W 5L 3P
$lang["site_text_short_lose"] = "L"; // Short 'Lose' text. exp: 10W 5L 3P
$lang["site_text_short_played"] = "P"; // Short 'Played' text. exp: 10W 5L 3P
$lang["site_text_top"] = "Haut";
$lang["site_text_middle"] = "Milieu";
$lang["site_text_bot"] = "Bas";
$lang["site_text_support"] = "Soutien";
$lang["site_text_jungle"] = "Jungle";
$lang["site_text_fill"] = "Remplir";
$lang["site_text_win"] = "Gagner";
$lang["site_text_lose"] = "Perdre";
$lang["site_text_played"] = "Joué";
$lang["site_text_short_kda"] = "KDA"; // Kill Death Assist short text.
$lang["site_text_add_favorite"] = "Ajouter un favori"; // Add Favorite button text
$lang["site_text_unranked"] = "Non classé"; // Unranked text
$lang["site_text_refreshing"] = "Rafraîchissant"; // Some page (summary..) data refreshing state button.
$lang["site_text_refresh_data"] = "Actualiser les données"; // Some page (summary..) data refresh button.
$lang["site_text_loading"] = "Chargement"; // Loading text
$lang["site_text_load_more"] = "Charger plus"; // Load More text (summary, match list)
$lang["site_text_gold"] = "Or"; // Gold text
$lang["site_text_damage"] = "Dégât"; // Damage text
$lang["site_text_wards"] = "Salles"; // Wards text
$lang["site_text_short_cs"] = "CS"; // Minions and Jungle monsters short text
$lang["site_text_team_1"] = "Equipe 1"; // Team 1 text
$lang["site_text_team_2"] = "Equipe 2"; // Team 2 text
$lang["site_text_victory"] = "Victoire"; // Victory text
$lang["site_text_defeat"] = "Défaite"; // Defeat text
$lang["site_text_remake"] = "Reprise"; // Remake text
$lang["site_text_items"] = "Éléments"; // Items text
$lang["site_text_skills"] = "Compétences"; // Skills text
$lang["site_text_cooldown"] = "Refroidir"; // Cooldown text
$lang["site_text_cost"] = "Coût"; // Cost text
$lang["site_text_range"] = "Gamme"; // Range text
$lang["site_text_series"] = "Série"; // Series text
$lang["site_text_free"] = "Gratuit";
$lang["site_text_finished"] = "Terminé";
$lang["site_text_vilemaw"] = "Monstre";
$lang["site_text_rift_herald"] = "Héraut de fossé";
$lang["site_text_baron"] = "Baron Nashor";
$lang["site_text_dragon"] = "Dragon";
$lang["site_text_inhibitor"] = "Inhibiteur";
$lang["site_text_turret"] = "Tourelle";
$lang["site_text_other"] = "Autre";
$lang["site_text_rank"] = "Rang";
$lang["site_text_cr_maintenance"] = "Champion classement en maintenance";
$lang["site_text_maintenance"] = "En maintenance";
$lang["site_text_league_maintenance"] = "Le classement de cette région est mis à jour. Il sera accessible une fois la mise à jour terminée.";
$lang["site_loading_summoner_header"] = "Invocateur de chargement"; // Summoner loading bar header text
$lang["site_loading_summoner_content"] = "Récupération des données d'invocateur"; // Summoner loading bar description
$lang["site_game_type_solo_duo"] = "Solo / Duo"; // Solo / Duo game type text
$lang["site_game_type_flex_5v5"] = "Flex"; // Flex 5v5 Game Type Text
$lang["site_game_type_flex_3v3"] = "Flex 3v3"; // Flex 3v3 game type text
$lang["site_game_type_normal"] = "Normal";
$lang["site_game_type_aram"] = "ARAM";
$lang["site_game_type_custom"] = "Douane";
$lang["site_game_type_event"] = "Êvênement";
$lang["site_color_red"] = "Rouge";
$lang["site_color_green"] = "Vert";
$lang["site_color_blue"] = "Bleu";
$lang["site_color_cyan"] = "Cyan";
$lang["site_color_yellow"] = "Jaune";
$lang["site_color_white"] = "Blanc";
$lang["site_color_orange"] = "Orange";
$lang["site_color_black"] = "Noir";
$lang["site_top_live_stream"] = "Meilleures vidéos en direct";
$lang["site_social_follow_text"] = "Suivez-nous dans les médias sociaux pour des affaires raffinées!";
$lang["site_text_direct_link"] = "Lien direct";
$lang["site_text_copy"] = "Copier";

$lang["site_feedback_notice"] = "S'il vous plaît, dites-nous ce que vous en pensez!";
$lang["site_feedback_header"] = "Formulaire de commentaires";
$lang["site_feedback_message_text"] = "Message";
$lang["site_feedback_summoner_text"] = "Nom de l'invocateur";
$lang["site_feedback_send"] = "Soumettre des commentaires";
$lang["site_feedback_success_header"] = "Le commentaire est envoyé!";
$lang["site_feedback_success_text"] = "Merci! Tous ces mots ont beaucoup de significations pour nous! Nous vous reviendrons dès que possible.";
$lang["site_feedback_error_header"] = "Le commentaire n'a pas été envoyé!";
$lang["site_feedback_error_text"] = "Nous sommes désolés ( Apparemment, les systèmes de rétroaction se sont rompus. Pourriez-vous réessayer plus tard? Ou envoyer un e-mail? social@lols.gg";
$lang["site_feedback_validate_message"] = "Veuillez saisir un message";
$lang["site_feedback_validate_email"] = "Veuillez saisir une adresse email valide";
$lang["site_feedback_validate_captcha"] = "Captcha est requis";

$lang["site_summoner_not_in_league_header"] = "Vous êtes en dessous de 30Lv et vous n'avez pas de match classé (";
$lang["site_summoner_not_in_league_content"] = "Riot Games (alias.) Rito) ne fournit plus des données de jeux normaux. (d'autres sites seront en mesure de vous donner cette information jusqu'au 24 juillet 2017). Puisque ça va être temporaire
Nous avons décidé de ne pas travailler sur les anciennes données qui vont être supprimées. Cependant, vous pouvez voir vos données des matchs, des champions, des runes et de dominations  en direct.";
$lang["site_summoner_not_in_league_end_content"] = "Nous aimerions que vous vérifiez cette page encore une fois que vous avez des jeux classés.";

// Home page bottom content 1 header
$lang["site_page_home_header_1"] = "La précision équivaut à la vérité";

// Home page bottom content 1
$lang["site_page_home_content_1"] = "Dans le tournoi de combat, presque tout change à tout instant. Il ne s'agit pas seulement de vous, mais aussi des adversaires. Nous avons développé un système très intelligent qui nous permet de vous montrer ce que vous avez besoin à l'heure actuelle. Pas la camelote du passé. Nous ne sommes pas comme les autres. Quand vous demandez à notre système quelque chose, vous obtiendrez seulement la vérité.";

// Home page bottom content 2 header
$lang["site_page_home_header_2"] = "Transparence, légèreté";

// Home page bottom content 2
$lang["site_page_home_content_2"] = "En tant que joueurs, nous savons que votre temps est précieux. Nous respectons cela. Afin de gagner votre temps et de donner le meilleur service possible, nous utilisons la dernière technologie (beaucoup d'entre eux en fait) pour vous donner ce dont vous avez besoin en un rien de temps. Vous ne vous rendrez même pas compte que la page charge:) Allez-y, essayez de cliquer sur le lien à tableau de stratégie ci-dessus par exemple.";

// Home page bottom content 3 header
$lang["site_page_home_header_3"] = "Plus de chances de succès";

// Home page bottom content 3
$lang["site_page_home_content_3"] = "Nous voulons que vous appréciez tout le jeu, aucune discrimination s'il vous plait. Lorsque vous utilisez notre système, vous verrez des informations importantes sur vous, vos coéquipiers et surtout sur vos adversaires. Ainsi, vous pouvez façonner votre gameplay selon les statistiques que nous vous donnons afin de vous mener au succès. Combien de fois ils jouent, comment sont-ils bons, ils se concentrent sur les tueries ou la mission, sont-ils expérimentés ou tout simplement des débutants ?, un joueur sûr ou agressif? Nous vous en informons sur tous et plus. Cependant, le reste est entre vos mains ...";

// Home page bottom content 4 header
$lang["site_page_home_header_4"] = "Nous venons juste de commencer";

// Home page bottom content 4
$lang["site_page_home_content_4"] = "Jusqu'à présent, nous n'avons que mis ce qui est prêt à l'usage. Il y a encore tellement de choses à ajouter dans notre ordre du jour dans le système. Il ya les idées que nous pensons qu'elles sont importantes et les autres idées qui seraient amusantes et immenses. Cependant, notre but est de donner ce que vous voulez et ce dont vous avez besoin. Afin d'y parvenir, nous avons besoin de vos réflexions et de vos commentaires. N'hésitez pas à nous dire ce que vous voulez. Y a-t-il des informations que vous aimeriez savoir, une autre façon de le visionner? Toute fonctionnalité ... Veuillez nous le dire. social@lols.gg (ou la bulle sur le fond droit)";


// About Us page header
$lang["site_page_about_header"] = "A propos de nous";

/* About Us Page Content */
$lang["site_page_about_content"] = "<p> lols.gg est <b> le plus rapide </b> et la <b> la plus précise </b> source que vous pouvez utiliser pour analyser les matchs en direct et rechercher le profil de l'invocateur. Nous choisissons le mot «analyse» parce que dans lols.gg, nous traitons les données de statistiques complexes et nous vous donnons la forme </b> la plus simple <b> de lui. Contrairement à d'autres, nous avons réellement l'intention de donner les informations les plus récentes et les plus précises disponibles tout en fournissant le service le plus rapide possible en utilisant la meilleure technologie. </p> <p> <ins> en ce moment </ins> dans lols.gg, vous pouvez ... </p> <ul> <li> utiliser notre système en 17 langues et 11 différentes régions lol. </li> <li> utiliser la fonction de recherche de match en direct et ... <ul> <li> vérifier si un invocateur est dans une correspondance, ou non. </li> <li> apprendre les informations de base sur le match en cours comme le mode de jeu et la carte, etc </li> <li> Voir tous les invocateurs et les champions choisis dans un match et voir leur succès classé, les runes, les maîtrises et plus. </li> <li> obtenir des conseils sur les champions  et les ennemis dans un match en direct. </li> <li> assistez le match de tournoi de combat sur votre client. </li> </ul> </li> <li> Parcourir les profils d'invocateur et ... <ul> <li> Parcourir le résumé et obtenir des informations rapides sur l'invocateur. </li> <li> voir les histoires de match détaillées et même les partager sur les médias sociaux. </li> <li> découvrir les gens avec qui ils jouent. </li> <li> voir leurs champions, leurs maîtres et de voir si vous le souhaitez. </li> <li> examiner leur ligue et les comparer avec d'autres personnes dans la table. </li> <li> regardez sur leur rune et les pages de maîtrises dans le nouveau style modernisé et voir le coût des pages Rune que vous voyez. (seulement à lols.gg) </li> </ul> </li> <li> utiliser la page des jeux récents et de trouver un match actif pour vous-même et le Specter si vous le souhaitez. </li> </ul> <p> En tant que joueurs, nous savons ce que les joueurs ont besoin et veulent. </p> <p> nous façonnons notre système en fonction des besoins et des exigences des joueurs. </p> <p> (cela signifie que nous sommes à la recherche de rétroactions.) Dites-nous, nous demander. Nous allons examiner toutes les idées et y répondre. social@lols.GG) </p> < h2 class ='py-3'>> qu'est-ce qui suit?</h2> <p> nous avons beaucoup d'idées qui attendent d'être mises en œuvre dans le système. Certains d'entre eux sont essentiels et certains d'entre eux sont pour le plaisir. Nous continuons à changer l'ordre d'entre eux, il n'est donc pas très possible de dire 'Ce' sera notre prochaine fonctionnalité. </p> <p> cependant, les statistiques détaillées de champion et les constructions sont autour du coin.:) </p> < h2 class ='py-3'>> que diriez-vous du futur?</h2> <p> l' <ins> avenir </ins> de lols.gg est ce que vous savez comme  l'intelligence artificielle AI aka. Les spécialistes de notre équipe ont développé un système d'apprentissage machine inimaginable qui se nourrit et analyse un nombre incalculable de variables. En utilisant le résultat des analyses mentionnées, nous allons dire à nos utilisateurs exactement ce dont ils ont besoin. </p> <p> afin que nous puissions ... </p> <ul> <li> améliorer leur expérience de jeu. </li> <li> dire aux joueurs ce qu'ils devraient faire pour être mieux que d'autres. </li> <li> recommander ce qu'ils ne devraient pas faire. </li> <li> vous donnez une plus grande chance ... pour gagner! </li> </ul> <h4 class='type-font text-italic text-right'>Nous faisons le complexe ... pour que vous obteniez le simple.</h4> < div class ='type-texte de police-texte en italique-droite'>>-de équipe lols.gg </div>";

$lang["site_page_stream_badges_header"] = "Badges de stream";
$lang["site_page_stream_badges_login_header"] = "Connexion aux streaming";
$lang["site_page_stream_badges_login_description"] = "Hé! Si vous souhaitez utiliser notre fonction de badge diffusé, veuillez nous envoyer un e-mail avec les informations suivantes!";
$lang["site_page_stream_badges_login_error"] = "L'identifiant ou le mot de passe est incorrect.";
$lang["site_page_stream_badges_login_username"] = "Nom d'utilisateur";
$lang["site_page_stream_badges_login_password"] = "Mot de passe";
$lang["site_page_stream_badges_login_sign_in"] = "Connexion";
$lang["site_page_stream_badges_save"] = "Enregistrer";
$lang["site_page_stream_badges_sign_out"] = "Déconnexion";
$lang["site_page_stream_badges_new_badge"] = "Nouveau badge";
$lang["site_page_stream_badges_summoner_data"] = "Données de l'invocateur";
$lang["site_page_stream_badges_animation_align"] = "Animation & alignement";
$lang["site_page_stream_badges_show_hide"] = "Afficher/masquer";
$lang["site_page_stream_badges_background"] = "Arrière-plan";
$lang["site_page_stream_badges_background_shadow"] = "Ombre de fond";
$lang["site_page_stream_badges_champion"] = "Champion";
$lang["site_page_stream_badges_text_colors"] = "Couleurs de texte";
$lang["site_page_stream_badges_text_shadow"] = "Ombre de texte";
$lang["site_page_stream_badges_summoner_name"] = "Nom de l'invocateur";
$lang["site_page_stream_badges_region"] = "Région";
$lang["site_page_stream_badges_animation"] = "Animation";
$lang["site_page_stream_badges_text_align"] = "Alignement du texte";
$lang["site_page_stream_badges_show_champion_image"] = "Voir l'image du champion";
$lang["site_page_stream_badges_show_league_tier"] = "Afficher le niveau de la ligue";
$lang["site_page_stream_badges_show_winrate"] = "Afficher le taux de victoires";
$lang["site_page_stream_badges_live_match_check"] = "Vérification du match en direct";
$lang["site_page_stream_badges_border"] = "Bordure";
$lang["site_page_stream_badges_radius"] = "Rayon";
$lang["site_page_stream_badges_horizontal"] = "Horizontal";
$lang["site_page_stream_badges_vertical"] = "Vertical";
$lang["site_page_stream_badges_blur"] = "Flou";
$lang["site_page_stream_badges_color"] = "Couleur";
$lang["site_page_stream_badges_border_color"] = "Couleur de la bordure";
$lang["site_page_stream_badges_league"] = "Ligue";
$lang["site_page_stream_badges_summoner"] = "Invocateur";
$lang["site_page_stream_badges_winrate"] = "Taux de réussite";
$lang["site_page_stream_badges_link"] = "Lien";
$lang["site_page_stream_badges_left"] = "Gauche";
$lang["site_page_stream_badges_right"] = "Droite";
$lang["site_page_stream_badges_center"] = "Centre";

$lang["site_page_strategy_board_header"] = "Tableau de stratégie";
$lang["site_page_strategy_board_how_to_save"] = "Enregistrer";
$lang["site_page_strategy_board_clear"] = "Effacer";
$lang["site_page_strategy_board_map"] = "Carte";
$lang["site_page_strategy_board_tools"] = "Outils";
$lang["site_page_strategy_board_remove"] = "Supprimer";
$lang["site_page_strategy_board_edit"] = "Modifier";
$lang["site_page_strategy_board_share"] = "Partage d'écran";
$lang["site_page_strategy_board_share_end"] = "Partage d'écran final";
$lang["site_page_strategy_board_actions"] = "Actions";
$lang["site_page_strategy_board_upload"] = "Télécharger";
$lang["site_page_strategy_board_note"] = "Remarque";
$lang["site_page_strategy_board_reset_zoom"] = "Réinitialiser le zoom";
$lang["site_page_strategy_board_add"] = "Ajouter";
$lang["site_page_strategy_board_help"] = "Aide";
$lang["site_page_strategy_board_text_placeholder"] = "Ajouter du texte";
$lang["site_page_strategy_board_utilities"] = "Utilités";
$lang["site_page_strategy_board_champions"] = "Champions";
$lang["site_page_strategy_board_ward"] = "Salle de scène";
$lang["site_page_strategy_board_control_ward"] = "Salle de contrôle";
$lang["site_page_strategy_board_minion_caster_blue"] = "Lanceur de roulette - bleue";
$lang["site_page_strategy_board_minion_caster_red"] = "Lanceur de roulette - rouge";
$lang["site_page_strategy_board_minion_melee_blue"] = "Sbire de mêlée - bleu";
$lang["site_page_strategy_board_minion_melee_red"] = "Sbire de mêlée - rouge";
$lang["site_page_strategy_board_minion_siege_blue"] = "Sbire de siège - bleu";
$lang["site_page_strategy_board_minion_siege_red"] = "Sbire de siège - rouge";
$lang["site_page_strategy_board_minion_super_blue"] = "Super sbire  - bleu";
$lang["site_page_strategy_board_minion_super_red"] = "Super sbire - rouge";
$lang["site_page_strategy_board_blank"] = "Vide";
$lang["site_page_strategy_board_sr"] = "Faille de l'invocateur";
$lang["site_page_strategy_board_ha"] = "Abîme hurlant";
$lang["site_page_strategy_board_tt"] = "Limite forestière tordue";
$lang["site_page_strategy_board_help_drawing"] = "Outil de dessin";
$lang["site_page_strategy_board_help_move"] = "Déplacer l'objet";
$lang["site_page_strategy_board_help_drag"] = "Glisser la toile";
$lang["site_page_strategy_board_help_reset"] = "Réinitialiser le zoom";
$lang["site_page_strategy_board_help_delete"] = "Supprimer l'objet choisi";
$lang["site_page_strategy_board_help_undo"] = "Défaire";
$lang["site_page_strategy_board_help_redo"] = "Refaire";
$lang["site_page_strategy_board_help_save"] = "Enregistrez l'image de toile actuelle et ouvrir dans un nouvel onglet";
$lang["site_page_strategy_board_help_clear"] = "Effacer la zone de la carte (réinitialisation complète)";
$lang["site_page_strategy_board_help_copy"] = "Copier";
$lang["site_page_strategy_board_help_paste"] = "Coller";
$lang["site_page_strategy_board_help_cut"] = "Couper";
$lang["site_page_strategy_board_help_duplicate"] = "Dupliquer (cloner)";
$lang["site_page_strategy_board_copied"] = "Copié";
$lang["site_page_strategy_board_get_link"] = "Générer un lien";
$lang["site_page_strategy_board_generating"] = "Générant...";
$lang["site_page_strategy_board_edit_code_not_generated"] = "Clé non générée";
$lang["site_page_strategy_board_edit_code_desc"] = "Gardez cette clé pour déverrouiller l'édition plus tard.";
$lang["site_page_strategy_board_edit_code_header"] = "Modifier la clé";
$lang["site_page_strategy_board_delete_modal_header"] = "Supprimer";
$lang["site_page_strategy_board_delete_modal_content"] = "Cela va supprimer votre travail de façon permanente. <br> êtes-vous sûr?";
$lang["site_page_strategy_board_delete_modal_cancel_button"] = "Annuler";
$lang["site_page_strategy_board_delete_modal_delete_button"] = "Supprimer";
$lang["site_page_strategy_board_link_get_success"] = "Toile chargée!";
$lang["site_page_strategy_board_link_get_error"] = "Chargement de toile impossible !";
$lang["site_page_strategy_board_link_save"] = "Toile sauvegardée et lien généré!";
$lang["site_page_strategy_board_link_save_error"] = "La toile n'a pas pu être enregistrée!";
$lang["site_page_strategy_board_link_delete"] = "Le canevas a été supprimé définitivement!";
$lang["site_page_strategy_board_link_delete_error"] = "Echec de suppression!";
$lang["site_page_strategy_board_edit_code_error"] = "Erreur ou une clé d'édition incorrecte!";
$lang["site_page_strategy_board_link_get_limit"] = "Vous ne pouvez générer que 20 liens par heure.";
$lang['site_page_strategy_board_get_link_not_yet'] = "Cliquez pour générer un lien pour partager votre tactique.";
$lang["site_page_strategy_board_mobile_warning"] = "Cette fonctionnalité n'est pas disponible pour les appareils mobiles.";

$lang["site_page_recent_header"] = "Jeux récents"; // Recent Games page header
$lang["site_page_recent_region_filter"] = "Toutes les régions"; // Recent games region filter
$lang["site_page_recent_not_found"] = "Aucun match trouvé dans cette région."; // Recent games not found matches text

$lang["site_page_live_main_champs_not_notice"] = "Pas joué de jeux pendant deux semaines";
$lang["site_page_live_main_champs_notice"] = "2 dernières semaines";
$lang["site_page_live_main_champ_tag_tooltip"] = "Champion principal";
$lang["site_page_live_champion_rank_info"] = "Tableau de classement des champions";
$lang["site_page_live_fetching"] = "Récupération des données du match"; // Live Match page loading text
$lang["site_page_live_btn_refresh"] = "Rafraîchir"; // Live Match page refresh button
$lang["site_page_live_btn_share"] = "Partager"; // Live Match page share button
$lang["site_page_live_btn_spectate"] = "Assister"; // Live Match page spectate button
$lang["site_page_live_head_champion"] = "Champion"; // Live Match page champion table header
$lang["site_page_live_head_summoner"] = "Invocateur"; // Live Match page summoner table header
$lang["site_page_live_head_ranked_wins"] = "Victoires classées"; // Live Match page ranked wins table header
$lang["site_page_live_head_solo_duo"] = "Solo / Duo"; // Live Match page solo/duo table header
$lang["site_page_live_head_flex"] = "Fléchir"; // Live Match page flex table header
$lang["site_page_live_head_main_champ"] = "Meilleur joueur";
$lang["site_page_live_head_kda"] = "KDA";
$lang["site_page_live_head_wins"] = "Gagne";
$lang["site_page_live_tooltip_champion_games"] = "Le nombre de matchs joués par ce champion.";
$lang["site_page_live_tooltip_mastery_points"] = "Points de maîtrise:"; // Live Match page champion stars tooltip mastery points text
$lang["site_page_live_tooltip_mastery_level"] = "Niveau de maîtrise:"; // Live Match page champion stars tooltip mastery level text
$lang["site_page_live_tooltip_champion_points"] = "Meilleur score :"; // Live Match page champion start tooltip champion points
$lang["site_page_live_tooltip_runes"] = "Cliquez sur la page des runes"; // Live Match page rune button tooltip
$lang["site_page_live_tooltip_masteries"] = "Retour au menu principal"; // Live Match page mastery button tooltip
$lang["site_page_live_banned_champions"] = "Champions exclus"; // Live Match page table footer banned champions text
$lang["site_page_live_no_data"] = "L'invocateur n'est pas actuellement en jeu."; // Live Match page player is not currently in a game text
$lang["site_page_live_text_now_playing"] = "Jouer maintenant"; // Live Match page social share text.
$lang["site_page_live_spectate_text_1"] = "Si vous utilisez BaronReplays, sélectionnez <b> fichier → analyser la commande </b> et copier/coller dans la boîte."; // Live Match page spectate modal text 1
$lang["site_page_live_spectate_text_2"] = "Pour visualiser ce jeu, appuyez sur <b> Win + R </b> (ou ouvrez la fenêtre Exécuter) et collez-le dans:"; // Live Match page spectate modal text 2
$lang["site_page_live_spectate_text_3"] = "Vous devrez peut-être mettre à jour l'emplacement de l'exécutable du jeu de la Ligue des Legendes."; // Live Match page spectate modal text 3
$lang["site_page_live_spectate_text_4"] = "Après avoir exécuté <b> Terminal </b>, copiez et collez le message ci-dessous."; // Live Match page spectate modal text 4

$lang["site_page_summary_fetching"] = "Recherche de données récapitulatives"; // Summary page loading text
$lang["site_page_summary_no_league_data"] = "Aucune donnée de la ligue"; // Summary page no league data text
$lang["site_page_summary_header_played_with"] = "Joué avec (dans les 20 derniers matchs)"; // Summary page played with block header text
$lang["site_page_summary_no_match_data"] = "Aucune donnée de correspondance"; // Summary page no match data text
$lang["site_page_summary_header_summoner"] = "Invocateur"; // Summary page played with block table header summoner text
$lang["site_page_summary_header_pwl"] = "P / W / L"; // Summary page played with block table header play/win/lose text
$lang["site_page_summary_header_ratio"] = "Rapport"; // Summary page played with block table header ratio text
$lang["site_page_summary_no_pw_data"] = "Non joué avec les données"; // Summary page no played with data text
$lang["site_page_summary_all_queues"] = "Toutes les files d'attente"; // Summary page top all queues button.
$lang["site_page_summary_no_champion_data"] = "Aucune donnée de champion"; // Summary page no champion data (max: 20 characters)
$lang["site_page_summary_header_wins"] = "Gagne"; // Summary page wins block header
$lang["site_page_summary_details_overview"] = "Aperçu"; // Summary page match details overview tab header
$lang["site_page_summary_details_builds"] = "Construit"; // Summary page match details build tab header
$lang["site_page_summary_no_summary_data"] = "Aucune donnée récapitulative."; // Summary page no summary data
$lang["site_page_summary_overview_tooltip_total_damage_2_champ"] = "Dommages aux champions:"; // Summary page details overview damage tooltip damage dealt
$lang["site_page_summary_overview_tooltip_total_damage"] = "Dégâts totaux:"; // Summary page details overview damage tooltip total damage
$lang["site_page_summary_overview_tooltip_total_damage_ratio"] = "Rapport de dégâts:"; //
$lang["site_page_summary_overview_tooltip_control_ward"] = "Salle de contrôle:"; // Summary page details overview wards tooltip control ward
$lang["site_page_summary_overview_tooltip_wards_placed"] = "Endroit cadré :"; // Summary page details overview wards tooltip wards placed
$lang["site_page_summary_overview_tooltip_wards_killed"] = "Dégâts engendrés:"; // Summary page details overview wards tooltip wards killed
$lang["site_page_summary_builds_final_build"] = "Construction finale"; // Summary page details builds section final build text
$lang["site_page_summary_matches_updated"] = "Matchs mis à jour"; // Summary page refresh data success status text
$lang["site_page_summary_load_more_loaded"] = "10 autres matchs chargés"; // Summary page load more button succes status text
$lang["site_page_summary_load_more_no_data"] = "Plus de données de correspondance"; // Summary page load more button no more match status text
$lang["site_page_summary_pkill_tooltip"] = "Taux de participation de l'invocateur sur le nombre total de tueurs de l'équipe. (Tuer et assister)"; // Summary page handshake icon tooltip text
$lang["site_page_summary_get_your_profile_card"] = "Obtenir votre carte de profil";
$lang["site_page_summary_create_profile_card"] = "Créer une carte de profil";

$lang["site_page_champions_fetching"] = "Récupération des données des champions";
$lang["site_page_champions_total_mastery_score"] = "Score de maîtrise total";
$lang["site_page_champions_search_champion"] = "Rechercher le champion";
$lang["site_page_champions_filter_role_all"] = "Tous les rôles";
$lang["site_page_champions_filter_role_assassin"] = "Meurtrier";
$lang["site_page_champions_filter_role_fighter"] = "Combattant";
$lang["site_page_champions_filter_role_mage"] = "Wizard";
$lang["site_page_champions_filter_role_marksman"] = "Tireur";
$lang["site_page_champions_filter_role_support"] = "Soutien";
$lang["site_page_champions_filter_role_tank"] = "Blindé";
$lang["site_page_champions_filter_mastery_points"] = "Points d'influence";
$lang["site_page_champions_filter_mastery_level"] = "Niveau de maîtrise";
$lang["site_page_champions_filter_last_played"] = "Joué dernier";
$lang["site_page_champions_filter_alphabetic"] = "Alphabétique";
$lang["site_page_champions_filter_chest_granted"] = "Accorder la respiration";
$lang["site_page_champions_filter_no_champions"] = "Pas de défenseurs correspondants";
$lang["site_page_champions_tooltip_chest_granted"] = "Accorder la respiration";
$lang["site_page_champions_tooltip_tokens_earned"] = "Jetons gagnés:";
$lang["site_page_champions_tooltip_last_played"] = "Enfin joué";
$lang["site_page_champions_no_data"] = "Aucune donnée du champion.";

$lang["site_page_leagues_fetching"] = "Récupération des données de la Ligue";
$lang["site_page_leagues_header_summoners"] = "Invocateurs";
$lang["site_page_leagues_header_emblems"] = "Emblèmes";
$lang["site_page_leagues_header_games"] = "Jeux";
$lang["site_page_leagues_header_victory"] = "Victoires";
$lang["site_page_leagues_header_defeats"] = "Defaites";
$lang["site_page_leagues_header_series_lp"] = "Série / LP";
$lang["site_page_leagues_tooltip_veteran"] = "Veteran: a joué 100 ou plus de jeux dans cette ligue.";
$lang["site_page_leagues_tooltip_hot_streak"] = "Hot Streak: a remporté 3 jeux ou plus dans une rangée.";
$lang["site_page_leagues_tooltip_fresh_blood"] = "Sang frais: récemment rejoint cette ligue.";
$lang["site_page_leagues_tooltip_inactive_player"] = "Joueur inactif";
$lang["site_page_leagues_no_data"] = "Aucune donnée de la Ligue.";

$lang["site_page_runes_fetching"] = "Extraction des données de runes";
$lang["site_page_runes_rune_pieces"] = "Pièces de rune";
$lang["site_page_runes_rune_effects"] = "Effets de la rune";
$lang["site_page_runes_page_cost"] = "Charge la page";
$lang["site_page_runes_no_data"] = "Pas de données sur les runes.";

$lang["site_page_masteries_fetching"] = "Récupération des données des dominations";
$lang["site_page_masteries_ferocity"] = "Férocité";
$lang["site_page_masteries_cunning"] = "Astuce";
$lang["site_page_masteries_resolve"] = "Résoudre";
$lang["site_page_masteries_no_data"] = "Aucune donnée de maîtrise.";

$lang["site_page_lboards_search_summoner"] = "Rechercher l'invocateur";
$lang["site_page_lboards_tab_champion_stats"] = "Statistiques de champion";
$lang["site_page_lboards_tab_champion_masteries"] = "Maîtrise de champion";
$lang["site_page_lboards_h_champion_name"] = "Nom du vainqueur";
$lang["site_page_lboards_h_summoner"] = "Invocateur";
$lang["site_page_lboards_h_ranked_tier"] = "Niveau classé";
$lang["site_page_lboards_h_team"] = "Equipe";
$lang["site_page_lboards_h_lp"] = "Points d'influences";
$lang["site_page_lboards_h_win_lose"] = "Gagner / Perdre";
$lang["site_page_lboards_h_win_ratio"] = "Pourcentage de victoires";
$lang["site_page_lboards_h_nodata"] = "Pas de données plus récentes";
$lang["site_page_lboards_h_kda"] = "KDA";
$lang["site_page_lboards_h_played"] = "Joué";
$lang["site_page_lboards_h_win"] = "Gagner";
$lang["site_page_lboards_h_lose"] = "Perdre";
$lang["site_page_lboards_h_most_kill"] = "La plupart des tueries";
$lang["site_page_lboards_h_pentakills"] = "Pentakills";
$lang["site_page_lboards_h_most_death"] = "La plupart des tueries";
$lang["site_page_lboards_h_avg_cs"] = "Moy.CS";
$lang["site_page_lboards_h_avg_gold"] = "Moy.Or";
$lang["site_page_lboards_h_avg_death"] = "Moy. Décès";
$lang["site_page_lboards_h_avg_kill"] = "Moy. Tuer";
$lang["site_page_lboards_h_avg_assists"] = "Moy. Aides";
$lang["site_page_lboards_h_avg_dmg_dealt"] = "Moy.Dmg.Gérer";
$lang["site_page_lboards_h_avg_dmg_taken"] = "Moy.Dmg.pris";
$lang["site_page_lboards_h_level"] = "Niveau";
$lang["site_page_lboards_h_points"] = "Points";
$lang["site_page_lboards_w_stat_selected"] = "statistiques sélectionnées";
$lang["site_page_lboards_w_stats_selected"] = "statistiques sélectionnées";
$lang["site_page_lboards_w_select_stats"] = "Sélectionner les statistiques";
$lang["site_page_lboards_all_champions"] = "Tous les champions";
$lang["site_page_lboards_max_filter"] = "7 Filtres au maximum";
$lang["site_page_lboards_min_played"] = "Joué au minimum";

$lang["site_per_level"] = "par niveau";
$lang["site_as_ally"] = "En tant qu'allié";
$lang["site_as_enemy"] = "En tant qu'ennemi";
$lang["site_champion_kill"] = "Tuer les champions";
$lang["site_gold_earned"] = "Or gagné";
$lang["site_damage_dealt_to_champions"] = "Dégâts infligés aux champions";
$lang["site_wards_placed"] = "Pupilles placées";
$lang["site_damage_taken"] = "Les dégâts subis";
$lang["site_farsight_alteration"] = "Altération de la pyrite";
$lang["site_sightstone"] = "Spectacle miraculeux";
$lang["site_vision_ward"] = "Vision de pupille";
$lang["site_warding_totem_trinket"] = "Totem baliseur (bijou)";
$lang["site_warding totem_upgrade_trinket"] = "Mise à niveau de totem baliseur (bijou)";
$lang["site_winner_team"] = "Équipe victorieuse";
$lang["site_loser_team"] = "Équipe vaincue";
$lang["site_p_kill"] = "Participation aux tueries:";
$lang["site_tower"] = "Tour";
$lang["site_inhibitor"] = "Inhibiteur";
$lang["site_minions"] = "Sbires";
$lang["site_level"] = "Niveau";
$lang["site_year"] = "Année";
$lang["site_month"] = "Mois";
$lang["site_week"] = "Semaine";
$lang["site_day"] = "Jour";
$lang["site_hour"] = "Heure";
$lang["site_min"] = "Minute";
$lang["site_sec"] = "Seconde";
$lang["site_years"] = "Années";
$lang["site_months"] = "Mois";
$lang["site_weeks"] = "Semaines";
$lang["site_days"] = "Jours";
$lang["site_hours"] = "Heures";
$lang["site_mins"] = "Minutes";
$lang["site_secs"] = "Secondes";
