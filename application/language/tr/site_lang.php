<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang["site_meta_default_title"] = "Lol Profiller, Canlı Maç, Sihirdar İstatistikleri, Karşılaşma Geçmişi";
$lang["site_meta_default_description"] = "League of Legends Gerçek Zamanlı Canlı Maç, Sihirdar Arama, Karşılaşma Geçmişi, Şampiyonlar, Ligler, Rünler, Kabiliyetler ve MMR. Güncel Dizilimler ve İstatistikler.";

$lang["site_meta_default_about_title"] = "Hakkımızda - LoL Profiller, Canlı Maç, Sihirdar İstatistikleri, Karşılaşma Geçmişi";
$lang["site_meta_default_about_description"] = "Lols.gg, Canlı Maç Analizi ve Sihirdar Arama için kullanabileceğiniz en hızlı ve en doğru bilgiyi sunan kaynaktır. Özellikle ‘analiz’ kelimesini kullanmayı tercih ediyoruz, çünkü Lols.gg karmaşık istatistiksel verileri işleyerek size ihtiyacınız olan en basit halini sunar. Diğerlerinden farklı olarak biz gerçekten size en son teknolojiyi kullanarak en hızlı hizmeti sunarken en doğru ve en güncel bilgiyi ulaştırmayı hedefliyoruz.";

$lang["site_meta_default_recent_title"] = "Son Maçlar – LoL Profiller, Canlı Maç, Sihirdar İstatistikleri, Karşılaşma Geçmişi";
$lang["site_meta_default_recent_description"] = "Lols.gg League of Legends Canlı Maçlar için Son Maçlar Sayfası, Aktif Oyuncular ve Sihirdarlar, Karşılaşma Geçmişi ve Karşılaşma Arşivi.";

$lang["site_meta_default_strategy_board_title"] = "Strateji Tahtası - LoL Strateji Tahtası, Taktikler, Harita, Sihirdar Vadisi";
$lang["site_meta_default_strategy_board_description"] = "Lols.gg Strateji Tahtası - Çizim Yapıp Taktik Oluşturun, Resmi Kaydedin ve E-Spor Takımlarınız ile Paylaşın, Yeni Taktikler, Yeni Meta, Sihirdar Vadisi, Uğursuz Koruluk, ARAM, LoL Strateji Tahtası ve Tasarım.";

$lang["site_meta_default_live_title"] = "Canlı Maç Arama";
$lang["site_meta_default_live_description"] = "Güncelle, Oyunu İzle, Paylaş, Canlı Maç, Özet, Şampiyonlar, Ligler, Rünler, Kabiliyetler.";

$lang["site_meta_default_stream_badges_title"] = "Stream Badges";
$lang["site_meta_default_stream_badges_description"] = "Stream Badges, Twitch Streamers, Summoner Info, Live Match Info";

$lang["site_meta_default_masteries_title"] = "Kabiliyet Sayfaları ve Anahtar Kabiliyetler";
$lang["site_meta_default_masteries_description"] = "Kabiliyet Sayfası ve Anahtar Kabiliyetler – Kabiliyet Dizilimi – Hırs, Maharet, Azim, Anahtar Kabiliyet, Kabiliyet Ağacı, Kumandanın Kan Hırsı, Savaş Şevki, Ölümateşi Dokunuşu, Fırtına Yağmacısı'nın Cinneti, Yıldırımlar Efendisi'nin Hükmü, Rüzgara Fısıldayan'ın Hediyesi, Hortlağın Pençesi, Heyulanın Cesareti, Kayadoğan Akdi, League of Legends, Efsaneler Ligi";

$lang["site_meta_default_runes_title"] = "Rünler, Rün Sayfaları & Rüne Sayfası Maliyeti";
$lang["site_meta_default_runes_description"] = "Rünler, Rün Sayfaları, Rün Sayfası Mailiyetleri ve Her Rün Parçasının Fiyatı, Toplam Rün Etkileri, Rün Parçaları, Gelişmiş, Cevher, Damga, Nişan, Mühür, Rün Maliyeti, Rün Fiyatı, IP, League of Legends.";

$lang["site_meta_default_leagues_title"] = "Dereceli Lig Sayfası";
$lang["site_meta_default_leagues_description"] = "League of Legends Dereceli Lig – Solo / Duo – Esnek – Şampiyonluk, Ustalık, Elmas, Platin, Altın, Gümüş, Bronz, Yerleştirme, Derecesiz, Aşama, Küme, V, IV, III, II, I – Kulağı Kesik, Kesintisiz Başarı, Taze Kan, İnaktiflik, İnaktif Oyuncu – Sihirdar İstatistikleri";

$lang["site_meta_default_champions_title"] = "Şampiyonlar & Şampiyon Ustalığı";
$lang["site_meta_default_champions_description"] = "Şampiyonlar & Şampiyon Ustalığı Sayfası – Şampiyon Ustalık Seviyesi, Şampiyon Ustalık Skoru, Hextech Sandığı, Şampiyon Simgesi, Karakterler, Kahramanlar, LoL, League of Legends.";

$lang["site_meta_default_summary_title"] = "Profil ve Özet – Sihirdar İstatistikleri";
$lang["site_meta_default_summary_description"] = "Sihirdar Profil ve Özeti. Bilmek istediğiniz herşey. İstatistikler, Grafikler, Ligler, Dereceli, Tekli / Çiftli, Esnek, 5v5, 3v3, Karşılaşma Geçmişi, Maç Detayları, Oyun İzleme, Özet, Şampiyonlar, Ligler, Rünler, Kabiliyetler, Lolking, Lolskill, League of Legends.";

$lang["site_meta_champions_title"] = "{{summoner}} - {{region}} - Şampiyonlar & Şampiyon Ustalığı";
$lang["site_meta_champions_description"] = "{{summoner}} - {{region}} - Toplam Ustalık Skoru: {{totalMasteryPoints}} – Şampiyonlar & Şampiyon Ustalığı Sayfası – Şampiyon Ustalık Seviyesi, Şampiyon Ustalık Puanı, Hextech Sandığı, Şampiyon Simgesi, Karakterler, Kahramanlar, LoL, League of Legends";

$lang["site_meta_leagues_title"] = "{{summoner}} - {{region}} - Dereceli Lig Sayfası";
$lang["site_meta_leagues_description"] = "{{summoner}} - {{region}} – League of Legends Dereceli Lig – Solo / Duo – Esnek – Şampiyonluk, Ustalık, Elmas, Platin, Altın, Gümüş, Bronz, Yerleştirme, Derecesiz, Aşama, Küme, V, IV, III, II, I – Kulağı Kesik, Kesintisiz Başarı, Taze Kan, İnaktiflik, İnaktif Oyuncu – Sihirdar İstatistikleri";

$lang["site_meta_runes_title"] = "{{summoner}} - {{region}} - Rünler, Rün Sayfaları & Rün Sayfası Maliyeti";
$lang["site_meta_runes_description"] = "{{summoner}} - {{region}} – Rünler, Rün Sayfaları, Rün Sayfası Mailiyetleri ve Her Rün Parçasının Fiyatı, Toplam Rün Etkileri, Rün Parçaları, Gelişmiş, Cevher, Damga, Nişan, Mühür, Rün Maliyeti, Rün Fiyatı, IP, League of Legends.";

$lang["site_meta_masteries_title"] = "{{summoner}} - {{region}} - Kabiliyet Sayfası & Anahtar Kabiliyetler";
$lang["site_meta_masteries_description"] = "{{summoner}} - {{region}} – Kabiliyet Sayfası ve Anahtar Kabiliyetler – Kabiliyet Dizilimi – Hırs, Maharet, Azim, Anahtar Kabiliyet, Kabiliyet Ağacı, Kumandanın Kan Hırsı, Savaş Şevki, Ölümateşi Dokunuşu, Fırtına Yağmacısı'nın Cinneti, Yıldırımlar Efendisi'nin Hükmü, Rüzgara Fısıldayan'ın Hediyesi, Hortlağın Pençesi, Heyulanın Cesareti, Kayadoğan Akdi, League of Legends, Efsaneler Ligi";

$lang["site_meta_live_title"] = "{{summoner}} - {{region}} - Canlı Maç Arama";
$lang["site_meta_live_description"] = "{{region}} - {{gameType}} - {{summoner}} - {{mapName}} – Güncelle, Oyunu İzle, Paylaş, Canlı Maç, Özet, Şampiyonlar, Ligler, Rünler, Kabiliyetler.";

$lang["site_meta_summary_title"] = "{{summoner}} - {{region}} - Profil ve Özet – Sihirdar İstatistikleri";
$lang["site_meta_summary_description"] = "{{summoner}} - {{region}} – Sihirdar Profil ve Özeti. Bilmek istediğiniz herşey. İstatistikler, Grafikler, Ligler, Dereceli, Tekli / Çiftli, Esnek, 5v5, 3v3, Karşılaşma Geçmişi, Maç Detayları, Oyun İzleme, Özet, Şampiyonlar, Ligler, Rünler, Kabiliyetler, Lolking, Lolskill, League of Legends.";

$lang["site_meta_default_leaderboards_title"] = "Sıralamalar - LoL Profiller, Canlı Maç, Sihirdar İstatistikleri, Karşılaşma Geçmişi - Lols.gg";
$lang["site_meta_default_leaderboards_description"] = "Sıralamalar - Lols.gg Oyuncu ve Şampiyon Sıralamaları. Şampiyonlar, Şampiyon Ustalığı, KDA, Puanlama, Lig Puanı, LP Sıralaması, Dünya Sıralaması.";

$lang["site_meta_be_calculator_title"] = "Mavi Öz Hesaplayıcı - LoL Profiller, Canlı Maç, Sihirdar İstatistikleri, Karşılaşma Geçmişi - Lols.gg";
$lang["site_meta_be_calculator_description"] = "Mavi Öz Hesaplayıcı - Lols.gg Oyuncu ve Şampiyon Sıralamaları. Şampiyonlar, Şampiyon Ustalığı, KDA, Puanlama, Lig Puanı, LP Sıralaması, Dünya Sıralaması.";

$lang["site_meta_builds_title"] = "Eşya Dizilimleri - LoL Profiller, Canlı Maç, Sihirdar İstatistikleri, Karşılaşma Geçmişi - Lols.gg";
$lang["site_meta_builds_description"] = "Eşya Dizilimleri - Lols.gg Oyuncu ve Şampiyon Sıralamaları. Şampiyonlar, Şampiyon Ustalığı, KDA, Puanlama, Lig Puanı, LP Sıralaması, Dünya Sıralaması.";

// Cookies law description
$lang["site_cookies_law"] = "Sitemizi en iyi şekilde kullanabilmeniz için çerezleri kullanıyoruz. Çerezler tarayıcınızda saklanan dosyalardır ve internet sitelerinin çoğu web tecrübenizi kişiselleştirebilmek için bunları kullanır. Sitemizi çerez ayarlarınızı değiştirmeden kullanarak, çerezleri kullanmamızı kabul etmiş sayılıyorsunuz.";


$lang["site_under_construction"] = "Bakım yapılıyor.";
$lang["site_menu_home"] = "Ana Sayfa"; // Home menu
$lang["site_menu_about"] = "Hakkımızda"; // About Us menu
$lang["site_menu_builds"] = "Eşya Dizilimleri";
$lang["site_menu_leaderboards"] = "Sıralamalar";
$lang["site_menu_champions"] = "Şampiyonlar"; // Champions menu
$lang["site_menu_recent_games"] = "Son Maçlar"; // Recent Games menu
$lang["site_menu_tools"] = "Araçlar"; // Tools menu
$lang["site_menu_strategy_board"] = "Strateji Tahtası"; // Strategy Board Menu
$lang["site_menu_stream_badges"] = "Yayıncı Kartları";
$lang["site_menu_theory_crafting"] = "Theory Crafting";
$lang["site_menu_be_calculator"] = "Mavi Öz Hesaplayıcı";
$lang["site_menu_summoner_live"] = "Canlı Maç"; // Live Match menu
$lang["site_menu_summoner_summary"] = "Özet"; // Summary menu
$lang["site_menu_summoner_champions"] = "Şampiyonlar"; // Champions menu
$lang["site_menu_summoner_leagues"] = "Ligler"; // Leagues menu
$lang["site_menu_summoner_runes"] = "Rünler"; // Runes menu
$lang["site_menu_summoner_masteries"] = "Kabiliyetler"; // Masteries menu
$lang["site_search_summoner"] = "Sunucu seç, sihirdar ara."; // Search summoner input placeholder
$lang["site_search_summoner_not_found"] = "{{summonerName}} geçerli bir isim değil!"; // Player name is not a valid name message.
$lang["site_search_languages"] = "Dil Ara..."; // Language selector placeholder
$lang["site_search_languages_not_found"] = "Eşleşme Yok."; // Language selector filter no matching language.
$lang["site_summoner_data_not_found"] = "Sihirdar bilgisi alınamadı.<br> Lütfen tekrar deneyin."; // Player cant get server
$lang["site_cannot_retrieve"] = "'{{page}}' verisi alınamadı. <br> Lütfen tekrar deneyin."; // Page data cannot retrieved
$lang["site_recent_header"] = "Son Aramalar"; // Recent search list header
$lang["site_no_recent"] = "Son Arama Yok."; // Recent search list empty text
$lang["site_favorite_header"] = "Favoriler"; // Favorite list header
$lang["site_no_favorite"] = "Henüz Favoriniz Yok."; // Favorite list empty text
$lang["site_text_ago"] = "önce"; // 1 days ago
$lang["site_text_live_match"] = "Canlı Maç"; // Recent games match list live match button.
$lang["site_text_short_lp"] = "LP"; // Short league points
$lang["site_text_short_win"] = "Z"; // Short 'Win' text. exp: 10W 5L 3P
$lang["site_text_short_lose"] = "B"; // Short 'Lose' text. exp: 10W 5L 3P
$lang["site_text_short_played"] = "O"; // Short 'Played' text. exp: 10W 5L 3P
$lang["site_text_top"] = "Üst";
$lang["site_text_middle"] = "Orta";
$lang["site_text_bot"] = "Alt";
$lang["site_text_support"] = "Destek";
$lang["site_text_jungle"] = "Orman";
$lang["site_text_fill"] = "Doldur";
$lang["site_text_win"] = "Zafer";
$lang["site_text_lose"] = "Bozgun";
$lang["site_text_played"] = "Oynama";
$lang["site_text_short_kda"] = "KDA"; // Kill Death Assist short text.
$lang["site_text_add_favorite"] = "Favoriye Ekle"; // Add Favorite button text
$lang["site_text_unranked"] = "Unranked"; // Unranked text
$lang["site_text_refreshing"] = "Güncelleniyor"; // Some page (summary..) data refreshing state button.
$lang["site_text_refresh_data"] = "Güncelle"; // Some page (summary..) data refresh button.
$lang["site_text_loading"] = "Yükleniyor"; // Loading text
$lang["site_text_load_more"] = "Daha Fazla Yükle"; // Load More text (summary, match list)
$lang["site_text_gold"] = "Altın"; // Gold text
$lang["site_text_damage"] = "Hasar"; // Damage text
$lang["site_text_wards"] = "Totemler"; // Wards text
$lang["site_text_short_cs"] = "CS"; // Minions and Jungle monsters short text
$lang["site_text_team_1"] = "Takım 1"; // Team 1 text
$lang["site_text_team_2"] = "Takım 2"; // Team 2 text
$lang["site_text_victory"] = "Zafer"; // Victory text
$lang["site_text_defeat"] = "Bozgun"; // Defeat text
$lang["site_text_remake"] = "Tekrar";
$lang["site_text_items"] = "Eşyalar"; // Items text
$lang["site_text_skills"] = "Yetenekler"; // Skills text
$lang["site_text_cooldown"] = "Bekleme Süresi"; // Cooldown text
$lang["site_text_cost"] = "Maliyet"; // Cost text
$lang["site_text_range"] = "Menzil"; // Range text
$lang["site_text_series"] = "Seri"; // Series text
$lang["site_text_free"] = "Ücretsiz";
$lang["site_text_finished"] = "Tamamlandı";
$lang["site_text_vilemaw"] = "Zehirçene";
$lang["site_text_rift_herald"] = "Vadi'nin Alameti";
$lang["site_text_baron"] = "Baron Nashor";
$lang["site_text_dragon"] = "Ejderha";
$lang["site_text_inhibitor"] = "İnhibitör";
$lang["site_text_turret"] = "Kule ";
$lang["site_text_other"] = "Diğer";
$lang["site_text_rank"] = "Sıralama";
$lang["site_text_cr_maintenance"] = "Şampiyon Sıralamalarında Bakım Çalışması Yapılıyor";
$lang["site_text_maintenance"] = "Bakım Çalışması";
$lang["site_text_league_maintenance"] = "Bu sunucudaki lig sıralamalarını güncelliyoruz. Çalışmamız bittikten sonra tekrar kullanıma açılacaktır.";
$lang["site_loading_summoner_header"] = "Sihirdar Yükleniyor"; // Summoner loading bar header text
$lang["site_loading_summoner_content"] = "Sihirdar Verisi İşleniyor"; // Summoner loading bar description
$lang["site_game_type_solo_duo"] = "Tekli / Çiftli"; // Solo / Duo game type text
$lang["site_game_type_flex_5v5"] = "Esnek"; // Flex 5v5 Game Type Text
$lang["site_game_type_flex_3v3"] = "Esnek 3v3"; // Flex 3v3 game type text
$lang["site_game_type_normal"] = "Normal";
$lang["site_game_type_aram"] = "ARAM";
$lang["site_game_type_custom"] = "Özel";
$lang["site_game_type_event"] = "Etkinlik";
$lang["site_color_red"] = "Kırmızı";
$lang["site_color_green"] = "Yeşil";
$lang["site_color_blue"] = "Mavi";
$lang["site_color_cyan"] = "Açık Mavi";
$lang["site_color_yellow"] = "Sarı";
$lang["site_color_white"] = "Beyaz";
$lang["site_color_orange"] = "Turuncu";
$lang["site_color_black"] = "Siyah";
$lang["site_top_live_stream"] = "Top Yayınlar";
$lang["site_social_follow_text"] = "Bizi sosyal ağlardan takip etmeyi unutmayın.";
$lang["site_text_direct_link"] = "Direkt Link";
$lang["site_text_copy"] = "Kopyala";
$lang["site_text_calculate"] = "Hesapla";
$lang["site_text_free_champion_rotation"] = "Ücretsiz Şampiyon Rotasyonu";

$lang["site_feedback_notice"] = "Site ile ilgili görüşlerinizi bekliyoruz!";
$lang["site_feedback_header"] = "Geri Bildirim Formu";
$lang["site_feedback_message_text"] = "Mesaj";
$lang["site_feedback_summoner_text"] = "Sihirdar Adı";
$lang["site_feedback_send"] = "Bildirimi Gönder";
$lang["site_feedback_success_header"] = "Bildiriminiz Gönderildi!";
$lang["site_feedback_success_text"] = "Teşekkür ederiz! Geri dönüşleriniz bizim için çok önemli! En yakın zamanda size geri dönüş yapacağız.";
$lang["site_feedback_error_header"] = "Bildirim gönderilemedi!";
$lang["site_feedback_error_text"] = "Üzgünüz :( Görünüşe göre geri bildirim sistemimiz bozulmuş. Daha sonra tekrar deneyebilir misiniz? Ve ya e-posta gönderebilir misiniz? social@lols.gg";
$lang["site_feedback_validate_message"] = "Lütfen bir mesaj yazınız";
$lang["site_feedback_validate_email"] = "Lütfen geçerli bir e-posta adresi giriniz";
$lang["site_feedback_validate_captcha"] = "Captcha Kısmını Doldurunuz";

$lang["site_summoner_not_in_league_header"] = "Henüz 30 Seviye Değilsin ve Hiç Dereceli Maçın Yok :(";
$lang["site_summoner_not_in_league_content"] = "Riot Games (Dostumuz Rito) normal maç bilgilerini artık göndermiyor. (Diğer analiz siteleri 24 Temmuz 2017 tarihine kadar bu bilgileri sunmaya devam edebilecekler.) Bu veriler geçici olduğu ve silinecekleri için
üzerlerinde çalışmamaya karar verdik. Yine de Canlı Maç, Şampiyon, Rün ve Kabiliyet sayfalarınızı inceleyebilirsiniz.";
$lang["site_summoner_not_in_league_end_content"] = "Dereceli maç oynadıktan sonra bu sayfayı tekrar incelemeni can-ı gönülden isteriz.";

// Home page bottom content 1 header
$lang["site_page_home_header_1"] = "Tutarlılık ve Gerçeklik";

// Home page bottom content 1
$lang["site_page_home_content_1"] = "League of Legends’ta yani LoL’da neredeyse her şey her an değişiyor. Sadece siz için değil, rakipleriniz için de geçerli bu durum. Size şu anda geçerli olan bilgileri sunabilmek için oldukça akıllı bir sistem geliştirdik. Bu sayede size günler belki de aylar öncesinden kalma çöpleri değil, ne görmeye ihtiyacınız var ise onu sunuyoruz. Kısacası ‘diğerlerinin aksine’ Lols.gg de arama yaptığınızda sadece gerçeklerle karşılaşırsınız.";

// Home page bottom content 2 header
$lang["site_page_home_header_2"] = "Işık Kadar Hızlı, Tüy Kadar Hafif";

// Home page bottom content 2
$lang["site_page_home_content_2"] = "Oyuncular olarak vaktinizin ne kadar değerli olduğunu biliyoruz ve buna saygı duyuyoruz. Bu yüzden size ihtiyacınız olanı göz açıp kapayana kadar getirebilmek için son teknolojiyi ve gerçekten en iyilerini kullanıyoruz. Sitede dolaşırken sayfaların yüklendiğini fark edemeyeceksiniz bile. Cidden, yukarıdaki Strateji Tahtası butonuna tıklayın mesela.";

// Home page bottom content 3 header
$lang["site_page_home_header_3"] = "Daha Yüksek Başarı Şansı";

// Home page bottom content 3
$lang["site_page_home_content_3"] = "Ayrım olmadan her birinizin oyundan zevk almanızı istiyoruz. Bizim sistemimizi kullandığınızda, kendiniz, takım arkadaşlarınız ve özellikle rakipleriniz hakkında önemli bilgiler edinirsiniz. Bu sayede oynatışınıza yön vererek başarıya giden yolda rakiplerinizden bir adım önde başlarsınız. Ne kadar sık oynuyorlar, ne kadar iyiler, öldürmeye mi yoksa görevlere mi odaklanıyorlar, tecrübeliler mi yoksa henüz yeni mi başlamışlar, geride (safe) mi yoksa agresif mi oynuyorlar? Tüm bunları ve daha fazlasını size sunuyoruz. E gerisi size kalmış tabii. Oyun sonu GG yazmayı unutmazsınız artık.";

// Home page bottom content 4 header
$lang["site_page_home_header_4"] = "Durun Daha Yeni Başlıyoruz";

// Home page bottom content 4
$lang["site_page_home_content_4"] = "Şimdiye dek sadece elimizde olanlar ile ortaya çalışma çıkarmaya çalıştık. Eklemeyi planladığımız daha birçok fikir var. Bu fikirlerin arasında kesin gerekli olanlar da var, eklesek çok eğlenceli olacaklar da var. Bizim amacımız sitemizi sizin istek ve ihtiyaçlarınıza göre düzenlemek. Bunu yapabilmek için sizin fikir ve geri bildirimlerinize ihtiyacımız var. Bizimle konuşmaktan çekinmeyin. Öğrenmek istediğiniz bir bilgi, veya mevcut olanları başka türlü mü görmek istiyorsunuz? Her ne olursa… Lütfen bize söyleyin. social@lols.gg (Veya sağ alttaki baloncuk)";

// About Us page header
$lang["site_page_about_header"] = "Hakkımızda";

/* About Us Page Content */
$lang["site_page_about_content"] = "<p>Lols.gg, Canlı Maç Analizi ve Sihirdar Arama için kullanabileceğiniz <b>en hızlı</b> ve <b>en doğru bilgiyi</b> sunan kaynaktır. Özellikle ‘analiz’ kelimesini kullanmayı tercih ediyoruz, çünkü Lols.gg karmaşık istatistiksel verileri işleyerek size ihtiyacınız olan en <b>basit</b> halini sunar. Diğerlerinden farklı olarak biz gerçekten size en son teknolojiyi kullanarak en hızlı hizmeti sunarken en doğru ve en güncel bilgiyi ulaştırmayı hedefliyoruz.</p><p>Şu anda Lols.gg ’de yapabilecekleriniz;</p><ul> <li>17 farklı dilde ve 11 farklı LoL bölgesinde (sunucusunda) kullanabilirsiniz. </li> <li>Canlı Maç Arama özelliğini kullanabilir ve… Bir sihirdarın aktif bir maçta olup olmadığını kontrol edebilirsiniz. </li> <li>Oynanmakta olan maç hakkındaki oyun türü, harita vb. temel bilgileri öğrenebilirsiniz. </li> <li>Maçtaki tüm sihirdarları ve seçili şampiyonları ve onların dereceli başarılarını, rünlerini, kabiliyetlerini ve daha fazlasını görebilirsiniz. </li> <li>Canlı Maçtaki rakip şampiyonlar hakkında tavsiyeler alabilirsiniz. </li> <li>League of Legends istemcisi aracılığı ile Oyunu İzle özelliğinden yararlanabilirsiniz. </li> <li>Sihirdar Profillerinde dolaşabilir ve… Özet sayfasında göz gezdirip sihirdar hakkında hızlı bilgilere ulaşabilirsiniz. </li> <li>Detaylı Karşılaşma Geçmişi bilgisine ulaşabilir hatta sosyal medya üzerinden bu maçları paylaşabilirsiniz. Kişinin birlikte oynadığı kişilerin listesine ulaşabilirsiniz. </li> <li>Kişinin şampiyonlarını ve şampiyon ustalıklarını istediğiniz gibi görüntüleyebilirsiniz. </li> <li>Kişinin lig bilgilerine ulaşabilir ve aynı ligde olan diğer kişiler ile tablo aracılığı ile karşılaştırma yapabilirsiniz. </li> <li>Rün sayfalarına ve Kabiliyet sayfalarına modern yeni tasarım ile göz atabilir ve görüntülediğiniz Rün sayfalarının maliyetini görebilirsiniz. (Sadece Lols.gg’de) </li> <li>Son Maçlar sayfasını kullanarak kendinize bir aktif maç bulabilir ve isterseniz oyunu izleyebilirsiniz. </li></ul><p>Oyuncular olarak, oyuncuların ne istediğini ve nelere ihtiyaç duyduğunu biliyoruz.</p><p>Sistemimizi oyuncuların istek ve ihtiyaçlarına göre şekillendirmeye özen gösteriyoruz. </p><p>(Bu aynı zamanda sizlerden geri dönüş beklediğimiz anlamına da geliyor. Bizle iletişime geçin ve istediğinizi sorun. Paylaşılan tüm fikirleri göz önünde bulunduracağız ve size cevap vereceğiz. social@lols.gg) </p><h2 class='py-3'>Sırada Ne Var? </h2><p>Eklemek istediğimiz bir sürü bekleyen fikrimiz var. Bazıları eklenmesi gerekli olanlar bazıları ise hoşumuza gittiği için eklemek istediklerimiz. Bu fikirlerin sırasını sık sık değişiyoruz. Bu yüzden sıradaki özelliğimiz ‘bu’ olacak deme şansımız pek yok. Yine de, detaylı şampiyon istatistikleri ve eşya dizilimleri (build) pek yakında. 😊 </p><h2 class='py-3'>Peki Gelecekte Ne Olacak?</h2><p>Lols.gg ‘nin yakın geleceğinde çoğumuzun YZ yani Yapay Zeka olarak bildiği sistem bulunmakta. Ekibimizdeki uzman yazılımcılar hayal bile edilmesi güç bir Otomatik Makine Öğrenimi (Machine Learning) sistemi geliştiriyorlar. Bu sistem kendini birçok istatistiksel veri ve değişken ile besliyor ve bunları analiz ederek akıl almaz sonuçlar ortaya çıkarıyor. </p><p>Bu sistemin sonucunda ise sizlere… </p><ul> <li>Oyun tecrübenizi nasıl iyileştirebileceğinizi söyleyebilecek. </li> <li>Diğerlerinden daha iyi olabilmek için ne yapmanız gerektiğinizi açıklayabilecek. </li> <li>Ne yapMAmanız gerektiği hakkında tavsiyelerde bulunabilecek. </li> <li>Ve en önemlisi, sizleri bir adım daha öteye taşıyabileceğiz… Zafere Doğru! </li></ul><h4 class='type-font text-italic text-right'>Karmaşık işleri bize bırakın… siz kolayınıza bakın. </h4><div class='type-font text-italic text-right'>- Lols.gg Ekibi</div>";

$lang["site_page_stream_badges_header"] = "Yayıncı Kartları";
$lang["site_page_stream_badges_login_header"] = "Yayıncı Girişi";
$lang["site_page_stream_badges_login_description"] = "Merhaba! Eğer Yayıncı Kartı özelliğimizi siz de kullanmak istiyor iseniz lütfen aşağıdaki bilgiler ile birlikte bize bir e-posta gönderin!";
$lang["site_page_stream_badges_login_error"] = "Kullanıcı adı veya şifre hatalı.";
$lang["site_page_stream_badges_login_username"] = "Kullanıcı Adı";
$lang["site_page_stream_badges_login_password"] = "Şifre";
$lang["site_page_stream_badges_login_sign_in"] = "Giriş Yap";
$lang["site_page_stream_badges_save"] = "Kaydet";
$lang["site_page_stream_badges_sign_out"] = "Çıkış Yap";
$lang["site_page_stream_badges_new_badge"] = "Yeni kart";
$lang["site_page_stream_badges_summoner_data"] = "Sihirdar Bilgileri";
$lang["site_page_stream_badges_animation_align"] = "Animasyon & Hizalama";
$lang["site_page_stream_badges_show_hide"] = "Göster/Gizle";
$lang["site_page_stream_badges_background"] = "Arkaplan";
$lang["site_page_stream_badges_background_shadow"] = "Arkaplan Gölgesi";
$lang["site_page_stream_badges_champion"] = "Şampiyon";
$lang["site_page_stream_badges_text_colors"] = "Yazı Rengi";
$lang["site_page_stream_badges_text_shadow"] = "Yazı Gölgesi";
$lang["site_page_stream_badges_summoner_name"] = "Sihirdar Adı";
$lang["site_page_stream_badges_region"] = "Bölge";
$lang["site_page_stream_badges_animation"] = "Animasyon";
$lang["site_page_stream_badges_text_align"] = "Yazı Hizası";
$lang["site_page_stream_badges_show_champion_image"] = "Şampiyon Resmi Göster";
$lang["site_page_stream_badges_show_league_tier"] = "Lig Bilgisi Göster";
$lang["site_page_stream_badges_show_winrate"] = "Kazanma Oranı Göster";
$lang["site_page_stream_badges_live_match_check"] = "Canlı Maç Kontrolü Yap";
$lang["site_page_stream_badges_border"] = "Kenarlık";
$lang["site_page_stream_badges_radius"] = "Yarıçap";
$lang["site_page_stream_badges_horizontal"] = "Yatay";
$lang["site_page_stream_badges_vertical"] = "Dikey";
$lang["site_page_stream_badges_blur"] = "Bulanıklık";
$lang["site_page_stream_badges_color"] = "Renk";
$lang["site_page_stream_badges_border_color"] = "Kenar Rengi";
$lang["site_page_stream_badges_league"] = "Lig";
$lang["site_page_stream_badges_summoner"] = "Sihirdar";
$lang["site_page_stream_badges_winrate"] = "Kaz. Oranı";
$lang["site_page_stream_badges_link"] = "Link";
$lang["site_page_stream_badges_left"] = "Sol";
$lang["site_page_stream_badges_right"] = "Sağ";
$lang["site_page_stream_badges_center"] = "Orta";

$lang["site_page_strategy_board_header"] = "Strateji Tahtası";
$lang["site_page_strategy_board_how_to_save"] = "Kaydet";
$lang["site_page_strategy_board_clear"] = "Temizle";
$lang["site_page_strategy_board_map"] = "Harita";
$lang["site_page_strategy_board_tools"] = "Araçlar";
$lang["site_page_strategy_board_remove"] = "Sil";
$lang["site_page_strategy_board_edit"] = "Düzen";
$lang["site_page_strategy_board_share"] = "Ekran Paylaş";
$lang["site_page_strategy_board_share_end"] = "Paylaşımı Durdur";
$lang["site_page_strategy_board_actions"] = "Eylemler";
$lang["site_page_strategy_board_upload"] = "Yükle";
$lang["site_page_strategy_board_note"] = "Not";
$lang["site_page_strategy_board_reset_zoom"] = "Sıfırla";
$lang["site_page_strategy_board_add"] = "Ekle";
$lang["site_page_strategy_board_help"] = "Yardım";
$lang["site_page_strategy_board_text_placeholder"] = "Yazı ekle";
$lang["site_page_strategy_board_utilities"] = "Yardımcılar";
$lang["site_page_strategy_board_champions"] = "Şampiyonlar";
$lang["site_page_strategy_board_ward"] = "Görüş Totemi";
$lang["site_page_strategy_board_control_ward"] = "Kontrol Totemi";
$lang["site_page_strategy_board_minion_caster_blue"] = "Büyücü Minyon - Mavi";
$lang["site_page_strategy_board_minion_caster_red"] = "Büyücü Minyon - Kırmızı";
$lang["site_page_strategy_board_minion_melee_blue"] = "Dövüşcü Minyon - Mavi";
$lang["site_page_strategy_board_minion_melee_red"] = "Dövüşcü Minyon - Kırmızı";
$lang["site_page_strategy_board_minion_siege_blue"] = "Kuşatma Minyonu - Mavi";
$lang["site_page_strategy_board_minion_siege_red"] = "Kuşatma Minyonu - Kırmızı";
$lang["site_page_strategy_board_minion_super_blue"] = "Süper Minyon - Mavi";
$lang["site_page_strategy_board_minion_super_red"] = "Süper Minyon - Kırmızı";
$lang["site_page_strategy_board_blank"] = "Boş";
$lang["site_page_strategy_board_sr"] = "Sihirdar Vadisi";
$lang["site_page_strategy_board_ha"] = "Sonsuz Uçurum";
$lang["site_page_strategy_board_tt"] = "Uğursuz Koruluk";
$lang["site_page_strategy_board_help_drawing"] = "Çizim Aracı";
$lang["site_page_strategy_board_help_move"] = "Nesne Taşı";
$lang["site_page_strategy_board_help_drag"] = "Tuvali Sürükle";
$lang["site_page_strategy_board_help_reset"] = "Yakınlaştırmayı Sıfırla";
$lang["site_page_strategy_board_help_delete"] = "Seçili Objeyi Sil";
$lang["site_page_strategy_board_help_undo"] = "Geri Al";
$lang["site_page_strategy_board_help_redo"] = "İleri Al";
$lang["site_page_strategy_board_help_save"] = "Mevcut Tuval Görüntüsünü Kaydet ve Yeni Sekmede Aç";
$lang["site_page_strategy_board_help_clear"] = "Tuvali Temizle (Tamamen Sıfırlar)";
$lang["site_page_strategy_board_help_copy"] = "Kopyala";
$lang["site_page_strategy_board_help_paste"] = "Yapıştır";
$lang["site_page_strategy_board_help_cut"] = "Kes";
$lang["site_page_strategy_board_help_duplicate"] = "Kopyasını Oluştur (Çoğalt)";
$lang["site_page_strategy_board_copied"] = "Kopyalandı";
$lang["site_page_strategy_board_get_link"] = "Bağlantı Oluştur";
$lang["site_page_strategy_board_generating"] = "Oluşturuluyor...";
$lang["site_page_strategy_board_edit_code_not_generated"] = "Anahtar Oluşturulamadı";
$lang["site_page_strategy_board_edit_code_desc"] = "Daha sonra düzenleme yapabilmek için bu anahtarı saklayın.";
$lang["site_page_strategy_board_edit_code_header"] = "Düzenleme Anahtarı";
$lang["site_page_strategy_board_delete_modal_header"] = "Kalıcı Olarak Sil";
$lang["site_page_strategy_board_delete_modal_content"] = "Bu seçenek çalışmanızı kalıcı olarak silecektir.<br> Emin misiniz?";
$lang["site_page_strategy_board_delete_modal_cancel_button"] = "İptal Et";
$lang["site_page_strategy_board_delete_modal_delete_button"] = "Evet, Sil";
$lang["site_page_strategy_board_link_get_success"] = "Tuval Yüklendi!";
$lang["site_page_strategy_board_link_get_error"] = "Tuval Yüklemesi Başarısız!";
$lang["site_page_strategy_board_link_save"] = "Tuval Kaydedildi ve Bağlantı Oluşturuldu!";
$lang["site_page_strategy_board_link_save_error"] = "Tuval Kaydedilemedi!";
$lang["site_page_strategy_board_link_delete"] = "Çalışmanız Kalıcı Olarak Silindi!";
$lang["site_page_strategy_board_link_delete_error"] = "Silme İşlemi Başarısız";
$lang["site_page_strategy_board_edit_code_error"] = "Düzenleme Anahtarı Girilmedi veya Hatalı!";
$lang["site_page_strategy_board_link_get_limit"] = "Saatte YALNIZCA 20 yeni bağlantı oluşturabilirsiniz!";
$lang['site_page_strategy_board_get_link_not_yet'] = "Henüz bağlantı oluşturulmamış.";
$lang["site_page_strategy_board_mobile_warning"] = "Bu özellik ne yazık ki mobil cihazlarda kullanılamıyor.";

$lang["site_page_recent_header"] = "Son Oyunlar"; // Recent Games page header
$lang["site_page_recent_region_filter"] = "Tüm Sunucular"; // Recent games region filter
$lang["site_page_recent_not_found"] = "Bu sunucuda mevcut maç bulunamadı."; // Recent games not found matches text

$lang["site_page_live_main_champs_not_notice"] = "Son 2 hafta içerisinde hiç oyun oynamamış.";
$lang["site_page_live_main_champs_notice"] = "Son 2 hafta içerisinde oynadığı şampiyonlar";
$lang["site_page_live_main_champ_tag_tooltip"] = "Favori Şampiyonu";
$lang["site_page_live_champion_rank_info"] = "Şampiyon Sıralaması Derecesi";
$lang["site_page_live_fetching"] = "Maç Verileri İşleniyor"; // Live Match page loading text
$lang["site_page_live_btn_refresh"] = "Güncelle"; // Live Match page refresh button
$lang["site_page_live_btn_share"] = "Paylaş"; // Live Match page share button
$lang["site_page_live_btn_spectate"] = "Oyunu İzle"; // Live Match page spectate button
$lang["site_page_live_head_champion"] = "Şampiyon"; // Live Match page champion table header
$lang["site_page_live_head_summoner"] = "Sihirdar"; // Live Match page summoner table header
$lang["site_page_live_head_ranked_wins"] = "Dereceli Maçlar"; // Live Match page ranked wins table header
$lang["site_page_live_head_solo_duo"] = "Tekli / Çiftli"; // Live Match page solo/duo table header
$lang["site_page_live_head_flex"] = "Esnek"; // Live Match page flex table header
$lang["site_page_live_head_main_champ"] = "Favori Şampiyonlar";
$lang["site_page_live_head_kda"] = "KDA";
$lang["site_page_live_head_wins"] = "Zafer";
$lang["site_page_live_tooltip_champion_games"] = "Bu şampiyonla oynanan maç sayısı";
$lang["site_page_live_tooltip_mastery_points"] = "Ustalık Skoru:"; // Live Match page champion stars tooltip mastery points text
$lang["site_page_live_tooltip_mastery_level"] = "Ustalık Seviyesi:"; // Live Match page champion stars tooltip mastery level text
$lang["site_page_live_tooltip_champion_points"] = "Şampiyon Puanı:"; // Live Match page champion start tooltip champion points
$lang["site_page_live_tooltip_runes"] = "Rün Sayfası için Tıklayın"; // Live Match page rune button tooltip
$lang["site_page_live_tooltip_masteries"] = "Kabiliyet Sayfası için Tıklayın"; // Live Match page mastery button tooltip
$lang["site_page_live_banned_champions"] = "Yasaklı Şampiyonlar"; // Live Match page table footer banned champions text
$lang["site_page_live_no_data"] = "Bu sihirdar şu an aktif bir maçta değil."; // Live Match page player is not currently in a game text
$lang["site_page_live_text_now_playing"] = "Şu an oynuyor."; // Live Match page social share text.
$lang["site_page_live_spectate_text_1"] = "Eğer BaronReplays kullanıyor iseniz, <b>FILE → ANALYZE COMMAND</b> seçeneğine gidin ve oradaki kutucuğa kopyalayıp yapıştırın."; // Live Match page spectate modal text 1
$lang["site_page_live_spectate_text_2"] = "Bu oyunu izlemek için <b>Win+R</b> tuşlarına basın (yada kendiniz Çalıştır programını açın) ve bu komutu yapıştırın:"; // Live Match page spectate modal text 2
$lang["site_page_live_spectate_text_3"] = "Dosyanın içinden League of Legends'in kurulu olduğu dizini düzeltmeniz gerekebilir."; // Live Match page spectate modal text 3
$lang["site_page_live_spectate_text_4"] = "<b>Terminal</b>i açtıktan sonra, aşağıdakini kopyalayıp yapıştrın."; // Live Match page spectate modal text 4

$lang["site_page_summary_fetching"] = "Özet Veriler İşleniyor"; // Summary page loading text
$lang["site_page_summary_no_league_data"] = "Lig Verisi Bulunamadı"; // Summary page no league data text
$lang["site_page_summary_header_played_with"] = "Son 20 Maçta Birlikte Oynananlar"; // Summary page played with block header text
$lang["site_page_summary_no_match_data"] = "Karşılaşma Verisi Bulunamadı"; // Summary page no match data text
$lang["site_page_summary_header_summoner"] = "Sihirdar"; // Summary page played with block table header summoner text
$lang["site_page_summary_header_pwl"] = "O / Z / B"; // Summary page played with block table header play/win/lose text
$lang["site_page_summary_header_ratio"] = "Kazanma"; // Summary page played with block table header ratio text
$lang["site_page_summary_no_pw_data"] = "Oynananlar Verisi Bulunamadı"; // Summary page no played with data text
$lang["site_page_summary_all_queues"] = "Tüm Sıralar"; // Summary page top all queues button.
$lang["site_page_summary_no_champion_data"] = "Veri Bulunamadı"; // Summary page no champion data (max: 20 characters)
$lang["site_page_summary_header_wins"] = "Dereceli Maçlar"; // Summary page wins block header
$lang["site_page_summary_details_overview"] = "Genel Bakış"; // Summary page match details overview tab header
$lang["site_page_summary_details_builds"] = "Dizilimler"; // Summary page match details build tab header
$lang["site_page_summary_no_summary_data"] = "Özet Verisi Bulunamadı."; // Summary page no summary data
$lang["site_page_summary_overview_tooltip_total_damage_2_champ"] = "Şampiyonlara Verilen Toplam Hasar:"; // Summary page details overview damage tooltip damage dealt
$lang["site_page_summary_overview_tooltip_total_damage"] = "Toplam Hasar:"; // Summary page details overview damage tooltip total damage
$lang["site_page_summary_overview_tooltip_total_damage_ratio"] = "Hasar Oranı:"; //
$lang["site_page_summary_overview_tooltip_control_ward"] = "Kontrol Totemi:"; // Summary page details overview wards tooltip control ward
$lang["site_page_summary_overview_tooltip_wards_placed"] = "Yerleştirilen Totemler:"; // Summary page details overview wards tooltip wards placed
$lang["site_page_summary_overview_tooltip_wards_killed"] = "Yokedilen Totemler:"; // Summary page details overview wards tooltip wards killed
$lang["site_page_summary_builds_final_build"] = "Final Eşya Dizilimi"; // Summary page details builds section final build text
$lang["site_page_summary_matches_updated"] = "Maçlar Güncellendi"; // Summary page refresh data success status text
$lang["site_page_summary_load_more_loaded"] = "10 Maç Daha Yüklendi"; // Summary page load more button succes status text
$lang["site_page_summary_load_more_no_data"] = "Daha Fazla Maç Verisi Bulunamadı"; // Summary page load more button no more match status text
$lang["site_page_summary_pkill_tooltip"] = "Sihirdar'ın Öldürme ve Asist Sayısının Takımın Toplam Öldürme Sayısına Olan Katkı Oranı"; // Summary page handshake icon tooltip text
$lang["site_page_summary_get_your_profile_card"] = "Profil Kartı";
$lang["site_page_summary_create_profile_card"] = "Kartınızı Oluşturun";
$lang["site_page_summary_mmr_heading"] = "Solo MMR";
$lang["site_page_summary_update_warning"] = "Profiliniz güncel değil gibi görünüyor. Yenilemek için 'Güncelle' butonuna tıklayınız.";

$lang["site_page_champions_fetching"] = "Şampiyon Verisi İşleniyor";
$lang["site_page_champions_total_mastery_score"] = "Toplam Ustalık Skoru";
$lang["site_page_champions_search_champion"] = "Şampiyon Ara";
$lang["site_page_champions_filter_role_all"] = "Tüm Roller";
$lang["site_page_champions_filter_role_assassin"] = "Suikastçi";
$lang["site_page_champions_filter_role_fighter"] = "Dövüşçü";
$lang["site_page_champions_filter_role_mage"] = "Büyücü";
$lang["site_page_champions_filter_role_marksman"] = "Nişancı";
$lang["site_page_champions_filter_role_support"] = "Destek";
$lang["site_page_champions_filter_role_tank"] = "Tank";
$lang["site_page_champions_filter_mastery_points"] = "Ustalık Puanı";
$lang["site_page_champions_filter_mastery_level"] = "Ustalık Seviyesi";
$lang["site_page_champions_filter_last_played"] = "Son Oynananlar";
$lang["site_page_champions_filter_alphabetic"] = "Alfabetik";
$lang["site_page_champions_filter_chest_granted"] = "Sandık Alınanlar";
$lang["site_page_champions_filter_no_champions"] = "Eşleşen Şampiyon Bulunamadı";
$lang["site_page_champions_tooltip_chest_granted"] = "Sandık Alındı";
$lang["site_page_champions_tooltip_tokens_earned"] = "Alınan Simgeler:";
$lang["site_page_champions_tooltip_last_played"] = "En Son Oynama:";
$lang["site_page_champions_no_data"] = "Şampiyon Verisi Bulunamadı.";

$lang["site_page_leagues_fetching"] = "Lig Verisi İşleniyor";
$lang["site_page_leagues_header_summoners"] = "Sihirdarlar";
$lang["site_page_leagues_header_emblems"] = "Armalar";
$lang["site_page_leagues_header_games"] = "Maçlar";
$lang["site_page_leagues_header_victory"] = "Zaferler";
$lang["site_page_leagues_header_defeats"] = "Bozgunlar";
$lang["site_page_leagues_header_series_lp"] = "Seri / LP";
$lang["site_page_leagues_tooltip_veteran"] = "Kulağı Kesik: Bu ligde 100 veya daha fazla oyun oynadı.";
$lang["site_page_leagues_tooltip_hot_streak"] = "Kesintisiz Başarı: Peşpeşe 3 veya daha fazla oyun kazandı.";
$lang["site_page_leagues_tooltip_fresh_blood"] = "Taze Kan: Bu lige yeni katıldı.";
$lang["site_page_leagues_tooltip_inactive_player"] = "İnaktif Oyuncu";
$lang["site_page_leagues_no_data"] = "Lig Verisi Bulunamadı";

$lang["site_page_runes_fetching"] = "Rün Verileri İşleniyor";
$lang["site_page_runes_rune_pieces"] = "Rün Parçaları";
$lang["site_page_runes_rune_effects"] = "Rün Etkileri";
$lang["site_page_runes_page_cost"] = "Sayfa Maliyeti";
$lang["site_page_runes_no_data"] = "Yeni rün sistemi ile yapılan değişikliklerden dolayı, rün sayfalarının gösterimi Riot Games tarafından kaldırıldı.<br><br>Şu anda sihirdarların rün sayfalarını gösterebileceğimiz yeni bir sistem üzerine çalışmaktayız.<br><br>O vakte kadar bu sayfa pek bir işe yaramayacak :(<br><br>Takipte Kalın!";

$lang["site_page_masteries_fetching"] = "Kabiliyet Verisi İşleniyor";
$lang["site_page_masteries_ferocity"] = "Hırs";
$lang["site_page_masteries_cunning"] = "Maharet";
$lang["site_page_masteries_resolve"] = "Azim";
$lang["site_page_masteries_no_data"] = "Kabiliyet Verisi Bulunamadı.";

$lang["site_page_lboards_search_summoner"] = "Sihirdar ara";
$lang["site_page_lboards_tab_champion_stats"] = "Şampiyon Sıralama";
$lang["site_page_lboards_tab_champion_masteries"] = "Şampiyon Ustalığı";
$lang["site_page_lboards_h_champion_name"] = "Şampiyon";
$lang["site_page_lboards_h_summoner"] = "Sihirdar";
$lang["site_page_lboards_h_ranked_tier"] = "Lig Aşaması";
$lang["site_page_lboards_h_team"] = "Takım";
$lang["site_page_lboards_h_lp"] = "Lig Puanı";
$lang["site_page_lboards_h_win_lose"] = "Zafer/Bozgun";
$lang["site_page_lboards_h_win_ratio"] = "Kazanma Oranı";
$lang["site_page_lboards_h_nodata"] = "Yeni veri bulunamadı.";
$lang["site_page_lboards_h_kda"] = "KDA";
$lang["site_page_lboards_h_played"] = "Oynama";
$lang["site_page_lboards_h_win"] = "Zafer";
$lang["site_page_lboards_h_lose"] = "Bozgun";
$lang["site_page_lboards_h_most_kill"] = "En Çok Öldürme";
$lang["site_page_lboards_h_pentakills"] = "BeşteBeşler";
$lang["site_page_lboards_h_most_death"] = "En Çok Ölüm";
$lang["site_page_lboards_h_avg_cs"] = "Ort. Minyon";
$lang["site_page_lboards_h_avg_gold"] = "Ort. Altın";
$lang["site_page_lboards_h_avg_death"] = "Ort. Ölüm";
$lang["site_page_lboards_h_avg_kill"] = "Ort. Öldürme";
$lang["site_page_lboards_h_avg_assists"] = "Ort. Asist";
$lang["site_page_lboards_h_avg_dmg_dealt"] = "Ort. Verilen Hasar";
$lang["site_page_lboards_h_avg_dmg_taken"] = "Ort. Alınan Hasar";
$lang["site_page_lboards_h_level"] = "Seviye";
$lang["site_page_lboards_h_points"] = "Puan";
$lang["site_page_lboards_w_stat_selected"] = "filtre seçildi";
$lang["site_page_lboards_w_stats_selected"] = "filtre seçildi";
$lang["site_page_lboards_w_select_stats"] = "Filtre Seç";
$lang["site_page_lboards_all_champions"] = "Tüm Şampiyonlar";
$lang["site_page_lboards_max_filter"] = "Maksimum 7 Filtre";
$lang["site_page_lboards_min_played"] = "Min oynama";

$lang["site_page_be_calculator_blue_essence"] = "Mavi Öz";
$lang["site_page_be_calculator_ip"] = "IP";
$lang["site_page_be_calculator_rune_pages"] = "Rün Sayfaları (IP)";
$lang["site_page_be_calculator_mark"] = "Damgalar";
$lang["site_page_be_calculator_seal"] = "Mühürler";
$lang["site_page_be_calculator_glyph"] = "Nişanlar";
$lang["site_page_be_calculator_quintessence"] = "Cevherler";
$lang["site_page_be_calculator_in_account"] = "Hesaptakiler";
$lang["site_page_be_calculator_be_received"] = "Mavi Öz Karşılıkları";
$lang["site_page_be_calculator_total_be_received"] = "Toplam Alınacak Mavi Öz";

$lang["site_builds_live_header"] = "Önerilen Eşya Dizilimi";
$lang["site_builds_live_alternatives"] = "Alternatifler:";
$lang["site_builds_live_against"] = "Karşı:";
$lang["site_builds_back_to_champions"] = "Şampiyon listesine dön";

$lang["site_per_level"] = "Seviye başına";
$lang["site_as_ally"] = "Dost Olduğunda";
$lang["site_as_enemy"] = "Rakip Olduğunda";
$lang["site_champion_kill"] = "Şampiyon Öldürme";
$lang["site_gold_earned"] = "Kazanılan Altın";
$lang["site_damage_dealt_to_champions"] = "Şampiyonlara Verilen Hasar";
$lang["site_wards_placed"] = "Yerleştirilen Totemler";
$lang["site_damage_taken"] = "Alınan Hasar";
$lang["site_farsight_alteration"] = "Uzak Görüş Dönüşümü";
$lang["site_sightstone"] = "Keşiftaşı";
$lang["site_vision_ward"] = "Görüş Totemi";
$lang["site_warding_totem_trinket"] = "Totem Uğuru (Ziynet)";
$lang["site_warding totem_upgrade_trinket"] = "Totem Uğuru Yükseltmesi (Ziynet)";
$lang["site_winner_team"] = "Kazanan Takım";
$lang["site_loser_team"] = "Kaybeden Takım";
$lang["site_p_kill"] = "Öldürme Katkısı:";
$lang["site_tower"] = "Kule";
$lang["site_inhibitor"] = "İnhibitor";
$lang["site_minions"] = "Minyonlar";
$lang["site_level"] = "Seviye:";
$lang["site_year"] = "Yıl";
$lang["site_month"] = "Ay";
$lang["site_week"] = "Hafta";
$lang["site_day"] = "Gün";
$lang["site_hour"] = "Saat";
$lang["site_min"] = "Dakika";
$lang["site_sec"] = "Saniye";
$lang["site_years"] = "Yıl";
$lang["site_months"] = "Ay";
$lang["site_weeks"] = "Hafta";
$lang["site_days"] = "Gün";
$lang["site_hours"] = "Saat";
$lang["site_mins"] = "Dakika";
$lang["site_secs"] = "Saniye";
