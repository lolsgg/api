<?php

class Jobs extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);
    }

    public function hourly($custom_hour = 0)
    {
        set_time_limit(0);
        $starttime = microtime(true);
        $message = '';
        $hour = (int)date('H');

        if ($custom_hour > 0) {
            $hour = (int)$custom_hour;
        }

        $region_update_list = array(
            1 => array('ru'),
            2 => array('tr'),
            5 => array('euw'),
            6 => array('eune'),
            11 => array('lan', 'las'),
            12 => array('na'),
            13 => array('br'),
            20 => array('jp', 'oce'),
            21 => array('kr')
        );


        if (isset($region_update_list[$hour])) {
            $regions = $region_update_list[$hour];
            $message .= 'REGIONS: ' . implode(', ', $regions);
            $message .= "<br>";

            $starttime_league = microtime(true);

            $this->Ranking->updateLeagueRanking($regions);

            $message .= 'League Update Time: ' . (microtime(true) - $starttime_league);
            $message .= "<br>";

            $starttime_champion = microtime(true);

            $this->Ranking->updateChampionMasteryRanking($regions);

            $message .= 'Champion Update Time: ' . (microtime(true) - $starttime_champion);
            $message .= "<br>";
        }


        if ($hour == 22) {
            $regions = array('ru', 'tr', 'euw', 'eune', 'lan', 'las', 'na', 'br', 'kr', 'jp', 'oce');
            $this->Ranking->updateChampionMasteryRankingLeaderboard($regions);
            $message .= 'leaderboard ranking updated: ru, tr, euw, eune, lan, las, na, br, kr, jp, oce<br>';
        }

        //if ($hour == 2) {
        // $log_count = $this->profilerUpload();
        // $message .= 'profiler logs transferred to aws s3 bucket (count: '.$log_count.')<br>';
        //}

        if (strlen($message)) {
            $message .= ' <br> ' . (microtime(true) - $starttime);
            if (defined('BASE_URL_CONF')) {
                $message .= ' <br> ' . BASE_URL_CONF;
            }
            $this->Request->sendEmailFromNoreply('eyupcanakman@gmail.com', 'Lols.gg Cronjob', $message);
        }
    }

    public function updateLolFiles()
    {
        //header('Content-Type: application/json');
        $this->lol->setRegion("TR");
        //$latest_lol_version_api = $this->lol->api("lol/static-data/v3/versions", '', 3600);

        $champions_data = $this->lol->api("lol/platform/v3/champions", '&freeToPlay=true', 86400);

        if (isset($champions_data->champions)) {
            $data = new stdClass();
            $data->status = true;
            $data->champions = array();
            foreach ($champions_data->champions as $_champ) {
                $champ = $this->lol->getChampionById2($_champ->id);

                $champ_data = new StdClass();
                $champ_data->championName = $champ->name;
                $champ_data->championKey = $champ->key;
                $champ_data->championId = $champ->id;

                $data->champions[] = $champ_data;
            }
            $free_champions = json_encode($data);

            if ($free_champions != file_get_contents("https://cdn.lols.gg/json/freeChampions.json")) {
                if ($this->Request->uploadFileToCdn77('/www/json/freeChampions.json', $free_champions)) {
                    echo "free champions uploaded<br>";
                    if ($this->Request->cdn77Purge(array('/json/freeChampions.json'))) {
                        echo "free champions purged<br>";
                    } else {
                        echo "free champions purge error<br>";
                    }
                } else {
                    echo 'free champions upload error<br>';
                }
            } else {
                echo 'free champions are already up to date<br>';
            }

        }


        //return false;
    }


    /**
     * Gets the profilers from assets/profiler folder and uploads to the S3 Bucket
     */
    public function profilerUpload()
    {
        $log_count = 0;
        $files = array_diff(scandir(FCPATH . 'assets/profiler/'), array('..', '.', '.htaccess'));
        unset($files[0]);
        unset($files[1]);
        unset($files[2]);
        //$promises = array();
        try {
            $sharedConfig = [
                'region' => 'us-east-2',
                'version' => 'latest',
                'credentials' => [
                    'key' => 'AKIAJQJBVX232WFPD77A',
                    'secret' => '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',
                ]
            ];
            $sdk = new Aws\Sdk($sharedConfig);
            $s3Client = $sdk->createS3();

            foreach ($files as $key => $filename) {
                $source_file = FCPATH . 'assets/profiler/' . $filename;
                $target_file = 'ci-profiler/' . date('Y-m-d') . '/' . $filename;
                $bucket = 'lolsgg-logs';
                try {
                    $s3Client->putObject(array(
                        'Bucket' => $bucket,
                        'Key' => $target_file,
                        'SourceFile' => $source_file,
                        'ContentType' => 'text/html'
                    ));
                    unlink($source_file);
                    $log_count++;
                } catch (Aws\S3\Exception\S3Exception $e) {

                } catch (Aws\Exception\AwsException $e) {

                }
            }
        } catch (Aws\Exception\AwsException $e) {

        }
        return $log_count;
    }
}
