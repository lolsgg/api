<?php

class Bot extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        include(APPPATH . 'libraries/simple_html_dom.php');
    }

    public function roleTest()
    {
        $this->db->order_by('percent', 'desc');
        $roles_query = $this->db->get('championgg');
        $roles = $roles_query->result();

        $team1 = array("Vel'Koz" => array('Flash', 'Exhaust'), 'Olaf' => array('Smite', 'Ghost'), 'Ryze' => array('Flash', 'Teleport'), 'Lux' => array('Flash', 'Barrier'), 'Jhin' => array('Heal', 'Flash'));
        // $team1 = array('Alistar'=>array('Exhaust','Flash'),'Malphite'=>array('Teleport','Flash'),'Zac'=>array('Flash','Smite'),'Caitlyn'=>array('Heal','Flash'),'Orianna'=>array('Flash','Ghost'));
        // $team1 = array();
        // $team1 = array(mn);


        foreach ($roles as $role) {
            if (isset($team1[$role->name])) {
                if (!isset($team1[$role->name]['role'])) {
                    $team1[$role->name]['role'] = array();
                }
                $team1[$role->name]['role'][] = $role;
            }
        }

        // $this->Request->pr($team1);
        // $this->Request->pr($team2);
    }


    public function testdata($region = 'tr', $account_id = '25577778')
    {
        echo '(Tüm kurallar kalanlar arasındaki kişilere uygulanacak)<ul>
		<li>1-Takımda 1 kişide çarp varsa o kişi Jungle+</li>
		<li>2-Takımda 1 ADC Varsa o kişi ADC+</li>
		<li>3-Eğer 2 ADC Varsa ve Işınlanı olan varsa ışınlanı olan Top diğeri ADC+</li>
		<li>4-Eğer 2 ADC varsa ve ışınlan hiç yoksa şifa olanı ADC+</li>
		<li>5-Eğer 2 ADC varsa ve ışınlanı yoksa ve şifa olanı yoksa yüksek oranlı olanı ADC+</li>
		<li>6-Rolünde Top olanların arasında Işınlan olan 1 kişi varsa ve middle rolüne uygun en az 1 kişi varsa o kişi Top+</li>
		<li>7-Rolünde Support olan kişilerden Bitkinlik olan 1 kişi varsa o kişi Support+</li>
		<li>8-Eğer rolünde support olan 1 kişi varsa o kişi supporttur+</li>
		<li>9-Eğer support yok ise ve takımda bitkinliği olan 1 kişi varsa o kişi Support+</li>
		<li>10-Supporttan başka rolü yoksa ve en yüksek Support oranına sahipse Support</li>
		<li>11-Middle rolüne uygun 1 den fazla kişi varsa ve bu kişiler arasından 1 kişide bitkinlik varsa o kişi Support+</li>
		<li>12-Eğer rolünde Middle olan varsa en yüksek oranlısı Middle+</li>
		<li>13-Eğer rolünde Top olan varsa en yüksek oranlısı Top+</li>
		<li>14-(Özel Kural) Son kalan kişiyi son kalan role yerleştir+</li></ul>';
        $roles_query = $this->db->get('championgg');
        $roles_ = $roles_query->result();
        $roles = array();

        foreach ($roles_ as $role) {
            if (!isset($roles[$role->name])) {
                $roles[$role->name] = array();
            }
            $roles[$role->name][] = $role;
        }

        $total_team = 0;
        $total_time = 0;
        $warning_count = 0;
        $placement_count_total[0] = 0;
        $placement_count_total[1] = 0;
        $placement_count_total[2] = 0;
        $placement_count_total[3] = 0;
        $placement_count_total[4] = 0;
        $placement_count_total[5] = 0;

        $this->lol->setRegion($region);
        $this->lol->setLang('en_US');
        $matches = array();



        // echo '<div style="border:1px dotted gray;padding:30px;margin-top:50px;">';
        foreach ($matches->matches as $match_) {
            // echo '<br>';
            $match_id = $match_->matchId;
            // echo $match_id.' - team1';
            // echo '<br>';
            $match = $this->lol->api("api/lol/$region/v2.2/match/$match_id", "", 1000000);
            // echo '<table style="border:1px solid gray;">';
            $t = false;

            $process_start = microtime(true);

            // $this->Request->pr($match);

            $teams = new StdClass();
            $teams->team1 = array();
            $teams->team2 = array();

            if (!isset($match->participants)) {
                continue;
            }

            foreach ($match->participants as $participant) {


                $team_id = $participant->teamId;
                $role = $participant->timeline->role;
                $lane = $participant->timeline->lane;
                $champ = $this->lol->getChampionById2($participant->championId);
                $spell1 = $this->lol->getSpellById($participant->spell1Id);
                $spell2 = $this->lol->getSpellById($participant->spell2Id);
                if ($champ->name == 'Graves') {
                    if (in_array('Marksman', $champ->tags)) {
                        // echo 'Nişancı';
                    }
                    // $this->Request->pr($champ);
                    // exit;
                }
                $summoner = new StdClass();
                $summoner->champion = $champ->name;
                $summoner->spell1 = $spell1->name;
                $summoner->spell2 = $spell2->name;
                $summoner->role = $role;
                $summoner->lane = $lane;
                $summoner->championgg = $roles[$champ->name];
                $summoner->prediction = '';
                $summoner->reason = array();

                $teams->{$team_id == 100 ? 'team1' : 'team2'}[] = $summoner;


                if ($team_id == 200 && !$t) {
                    // echo '</table>';
                    // echo '<br>'.$match_id.' - team2';
                    // echo '<table style="margin-bottom:20px;border:1px solid gray;">';
                }
                // echo '<tr>';
                // echo '<td style="padding:3px;border:1px solid gray;"><b>'.$champ->key.'</b></td>';
                // echo '<td style="padding:3px;border:1px solid gray;">'.$spell1->name.'</td>';
                // echo '<td style="padding:3px;border:1px solid gray;">'.$spell2->name.'</td>';
                // echo '<td style="padding:3px;border:1px solid gray;">'.$role.'</td>';
                // echo '<td style="padding:3px;border:1px solid gray;">'.$lane.'</td>';
                // echo '<td style="padding:3px;border:1px solid gray;width:50px;"></td>';
                // echo '<td style="padding:3px;border:1px solid gray;">'.$prediction.'</td>';
                // echo '</tr>';
                if ($team_id == 200) {
                    $t = true;
                }
            }

            $top_placed = false;
            $support_placed = false;
            $adc_placed = false;
            $middle_placed = false;
            $jungle_placed = false;

            $smite_summoner_key = array();
            foreach ($teams->team1 as $summoner_key => $summoner) {
                if ($summoner->spell1 == 'Smite' || $summoner->spell2 == 'Smite') {
                    $smite_summoner_key[] = $summoner_key;
                }
            }

            echo '<span style="margin:5px;border:1px solid gray;padding:2px;">' . count($smite_summoner_key) . ' kisi çarp</span>';
            if (count($smite_summoner_key) == 1) {//Takımda 1 kişide çarp varsa o kişi Jungle
                $teams->team1[$smite_summoner_key[0]]->prediction = 'Jungle';
                $jungle_placed = true;
                $teams->team1[$smite_summoner_key[0]]->reason[] = '[1] Takımda 1 kişide çarp varsa o kişi Jungle';
            }

            $adc_summoner_key = array();
            foreach ($teams->team1 as $summoner_key => $summoner) {
                foreach ($summoner->championgg as $gg_row) {
                    if ($gg_row->role == 'ADC') {
                        if (!$teams->team1[$summoner_key]->prediction) {
                            $adc_summoner_key[] = $summoner_key;
                            $teams->team1[$summoner_key]->adc_percent = $gg_row->percent;
                        }
                    }
                }
            }

            echo '<span style="margin:5px;border:1px solid gray;padding:2px;">' . count($adc_summoner_key) . ' kisi adc</span>';
            if (count($adc_summoner_key) == 1) {//Takımda 1 ADC Varsa o kişi ADC

                $teams->team1[$adc_summoner_key[0]]->prediction = 'ADC';
                $adc_placed = true;
                $teams->team1[$adc_summoner_key[0]]->reason[] = '[2] Takımda 1 ADC Varsa o kişi ADC';
            } else if (count($adc_summoner_key) == 2) {//Eğer 2 ADC Varsa
                $one = $teams->team1[$adc_summoner_key[0]];
                $two = $teams->team1[$adc_summoner_key[1]];
                if ($one->spell1 == 'Teleport' || $one->spell2 == 'Teleport') {//Işınlanı olan varsa ışınlanı olan Top diğeri ADC
                    $teams->team1[$adc_summoner_key[0]]->prediction = 'Top';
                    $teams->team1[$adc_summoner_key[1]]->prediction = 'ADC';
                    $adc_placed = true;
                    $top_placed = true;
                    $teams->team1[$adc_summoner_key[0]]->reason[] = '[3] Işınlanı olan varsa ışınlanı olan Top diğeri ADC';
                    $teams->team1[$adc_summoner_key[1]]->reason[] = '[3] Işınlanı olan varsa ışınlanı olan Top diğeri ADC';
                } else if ($two->spell1 == 'Teleport' || $two->spell2 == 'Teleport') {//Işınlanı olan varsa ışınlanı olan Top diğeri ADC
                    $teams->team1[$adc_summoner_key[0]]->prediction = 'ADC';
                    $teams->team1[$adc_summoner_key[1]]->prediction = 'Top';
                    $adc_placed = true;
                    $top_placed = true;
                    $teams->team1[$adc_summoner_key[0]]->reason[] = '[3] Işınlanı olan varsa ışınlanı olan Top diğeri ADC';
                    $teams->team1[$adc_summoner_key[1]]->reason[] = '[3] Işınlanı olan varsa ışınlanı olan Top diğeri ADC';
                } else if ($one->spell1 == 'Heal' || $one->spell2 == 'Heal') {//Işınlan yoksa şifa olanı ADC
                    $teams->team1[$adc_summoner_key[0]]->prediction = 'ADC';
                    $adc_placed = true;
                    $teams->team1[$adc_summoner_key[0]]->reason[] = '[4] Işınlan yoksa şifa olanı ADC';
                } else if ($two->spell1 == 'Heal' || $two->spell2 == 'Heal') {//Işınlan yoksa şifa olanı ADC
                    $teams->team1[$adc_summoner_key[1]]->prediction = 'ADC';
                    $adc_placed = true;
                    $teams->team1[$adc_summoner_key[1]]->reason[] = '[4] Işınlan yoksa şifa olanı ADC';
                } else if ($one->adc_percent >= $two->adc_percent) {//Şifa yoksa yüksek oranlı olanı ADC
                    $teams->team1[$adc_summoner_key[0]]->prediction = 'ADC';
                    $adc_placed = true;
                    $teams->team1[$adc_summoner_key[0]]->reason[] = '[5] Şifa yoksa yüksek oranlı olanı ADC';
                } else if ($one->adc_percent < $two->adc_percent) {//Şifa yoksa yüksek oranlı olanı ADC
                    $teams->team1[$adc_summoner_key[1]]->prediction = 'ADC';
                    $adc_placed = true;
                    $teams->team1[$adc_summoner_key[1]]->reason[] = '[5] Şifa yoksa yüksek oranlı olanı ADC';
                }
            }

            $top_summoner_key = array();
            foreach ($teams->team1 as $summoner_key => $summoner) {
                foreach ($summoner->championgg as $gg_row) {
                    if ($gg_row->role == 'Top') {
                        if (!$teams->team1[$summoner_key]->prediction) {
                            $top_summoner_key[] = $summoner_key;
                        }
                    }
                }
            }


            if (count($top_summoner_key) > 0) {//Rolünde top olan kişiler varsa
                echo '<span style="margin:5px;border:1px solid gray;padding:2px;">' . count($top_summoner_key) . ' top var</span>';
                $teleport_summoner_key_tmp = array();
                foreach ($top_summoner_key as $summoner_key) {
                    $participant = $teams->team1[$summoner_key];
                    if ($participant->spell1 == 'Teleport' || $participant->spell2 == 'Teleport') {//Rolünde ışınlan olanlar
                        $teleport_summoner_key_tmp[] = $summoner_key;
                    }
                }

                if (count($teleport_summoner_key_tmp) == 1) { //eğer rolünde ışınlan olan 1 top varsa
                    $middle_summoner_key_ = array();
                    foreach ($teams->team1 as $summoner_key_ => $summoner) {
                        foreach ($summoner->championgg as $gg_row) {
                            if ($gg_row->role == 'Middle') {
                                if (!$teams->team1[$summoner_key_]->prediction && $summoner_key_ != $teleport_summoner_key_tmp[0]) {
                                    $middle_summoner_key_[] = $summoner_key_;
                                }
                            }
                        }
                    }
                    if (count($middle_summoner_key_) > 0) {//rolünde middle olan en az 1 kişi varsa
                        $teams->team1[$teleport_summoner_key_tmp[0]]->prediction = 'Top';
                        $top_placed = true;
                        $teams->team1[$teleport_summoner_key_tmp[0]]->reason[] = '[6] Rolü top olanlar arasında 1 ışınlan varsa ve middle rolüne uygun en az 1 kişi varsa o kişi Top';
                    }
                }
            }

            $support_summoner_key = array();
            foreach ($teams->team1 as $summoner_key => $summoner) {
                foreach ($summoner->championgg as $gg_row) {
                    if ($gg_row->role == 'Support') {
                        if (!$teams->team1[$summoner_key]->prediction) {
                            $support_summoner_key[] = $summoner_key;
                        }
                    }
                }
            }

            if (count($support_summoner_key) > 0) {//Rolünde support olan varsa
                echo '<span style="margin:5px;border:1px solid gray;padding:2px;">' . count($support_summoner_key) . ' support var</span>';
                $exhaust_summoner_key_tmp = array();
                foreach ($support_summoner_key as $summoner_key) {
                    $participant = $teams->team1[$summoner_key];
                    if ($participant->spell1 == 'Exhaust' || $participant->spell2 == 'Exhaust') {//Rolünde bitkinlik olanlar
                        $exhaust_summoner_key_tmp[] = $summoner_key;
                    }
                }
                if (count($exhaust_summoner_key_tmp) == 1 && false) { //eğer rolünde bitkinlik olan 1 support varsa
                    $teams->team1[$exhaust_summoner_key_tmp[0]]->prediction = 'Support';
                    $support_placed = true;
                    $teams->team1[$exhaust_summoner_key_tmp[0]]->reason[] = '[7] Rolü support olanlardan bitkinliği olan 1 kişi varsa Support';
                } else if (count($support_summoner_key) == 1) { //eğer rolünde support olan 1 kişi varsa
                    $teams->team1[$support_summoner_key[0]]->prediction = 'Support';
                    $support_placed = true;
                    $teams->team1[$support_summoner_key[0]]->reason[] = '[8] Rolü support olan 1 kişi varsa o kişi Support';
                }
            } else if (count($support_summoner_key) == 0) { // eğer rolünde support olan hiç şampiyon yoksa
                $exhaust_summoner_key_tmp = array();
                foreach ($teams->team1 as $summoner_key => $summoner) {
                    if ($summoner->spell1 == 'Exhaust' || $summoner->spell2 == 'Exhaust') {
                        $exhaust_summoner_key_tmp[] = $summoner_key;
                    }
                }
                if (count($exhaust_summoner_key_tmp) == 1) { //eğer rolünde bitkinlik olan 1 şampiyon varsa
                    $teams->team1[$exhaust_summoner_key_tmp[0]]->prediction = 'Support';
                    $support_placed = true;
                    $teams->team1[$exhaust_summoner_key_tmp[0]]->reason[] = '[9] Takımda hiç support yoksa ve bitkinliği olan 1 kişi varsa o kişi Support';
                }
            }

            //Sadece support rolü olan var ise ve kalan kişiler arasında support rolü oranı en yüksek olan o ise Support
            if (!$support_placed) {
                $highest_support_percent_tmp = 0;
                $support_highest_summoner_key = 0;
                foreach ($teams->team1 as $summoner_key => $summoner) {
                    foreach ($summoner->championgg as $gg_row) {
                        if ($gg_row->role == 'Support') { // Rolünde support olanlar
                            if (!$teams->team1[$summoner_key]->prediction) {
                                if ($gg_row->percent > $highest_support_percent_tmp) {
                                    $highest_support_percent_tmp = $gg_row->percent;
                                    $support_highest_summoner_key = $summoner_key;
                                }
                            }
                        }
                    }
                }
                if ($highest_support_percent_tmp > 0) { // Support oranı en yüksek olan
                    $participant = $teams->team1[$support_highest_summoner_key];
                    if (count($participant->championgg) == 1) { // Sadece support rolü varsa
                        $teams->team1[$support_highest_summoner_key]->prediction = 'Support';
                        $support_placed = true;
                        $teams->team1[$support_highest_summoner_key]->reason[] = '[10] Supporttan başka rolü yoksa ve en yüksek Support oranına sahipse Support';
                    }
                }
            }

            if (!$support_placed) {
                $middle_exhaust_summoner_key = array();
                foreach ($teams->team1 as $summoner_key => $summoner) {
                    foreach ($summoner->championgg as $gg_row) {
                        if ($gg_row->role == 'Middle') { // Rolünde middle olanlar
                            if (!$teams->team1[$summoner_key]->prediction) {
                                if ($summoner->spell1 == 'Exhaust' || $summoner->spell2 == 'Exhaust') {//Rolünde bitkinlik olanlar
                                    $middle_exhaust_summoner_key[] = $summoner_key;
                                }
                            }
                        }
                    }
                }

                if (count($middle_exhaust_summoner_key) == 1) { // eğer hem middle olan hem bitkinliği olan 1 kişi varsa o kişi Support
                    $teams->team1[$middle_exhaust_summoner_key[0]]->prediction = 'Support';
                    $support_placed = true;
                    $teams->team1[$middle_exhaust_summoner_key[0]]->reason[] = '[11] Eğer hem Middle olan hem Bitkinliği olan 1 kişi varsa o kişi Support';
                }
            }

            $middle_summoner_key = array();
            $middle_highest_summoner_key = 0;
            $middle_highest_percent = 0;
            foreach ($teams->team1 as $summoner_key => $summoner) {
                foreach ($summoner->championgg as $gg_row) {
                    if ($gg_row->role == 'Middle') {
                        if (!$teams->team1[$summoner_key]->prediction) {
                            if ($gg_row->percent > $middle_highest_percent) {
                                $middle_highest_percent = $gg_row->percent;
                                $middle_highest_summoner_key = $summoner_key;
                            }
                            $middle_summoner_key[] = $summoner_key;
                        }
                    }
                }
            }

            if (count($middle_summoner_key) > 0) { //Rolünde middle olan varsa
                echo '<span style="margin:5px;border:1px solid gray;padding:2px;">' . count($middle_summoner_key) . ' middle var</span>';
                $participant = $teams->team1[$middle_highest_summoner_key]; //en yüksek oranlı middle
                $teams->team1[$middle_highest_summoner_key]->prediction = 'Middle';
                $middle_placed = true;
                $teams->team1[$middle_highest_summoner_key]->reason[] = '[12] Takımda Middle varsa en yüksek oranlısı Middle';
            }


            if (!$top_placed) {
                $top_summoner_key = array();
                $top_highest_summoner_key = 0;
                $top_highest_percent = 0;
                foreach ($teams->team1 as $summoner_key => $summoner) {
                    foreach ($summoner->championgg as $gg_row) {
                        if ($gg_row->role == 'Top') {
                            if (!$teams->team1[$summoner_key]->prediction) {
                                if ($gg_row->percent > $top_highest_percent) {
                                    $top_highest_percent = $gg_row->percent;
                                    $top_highest_summoner_key = $summoner_key;
                                }
                                $top_summoner_key[] = $summoner_key;
                            }
                        }
                    }
                }

                if (count($top_summoner_key) > 0) { //Rolünde top olan varsa
                    $participant = $teams->team1[$top_highest_summoner_key]; //en yüksek oranlı top
                    $teams->team1[$top_highest_summoner_key]->prediction = 'Top';
                    $top_placed = true;
                    $teams->team1[$top_highest_summoner_key]->reason[] = '[13] Takımda Top varsa en yüksek oranlısı Top';
                }

            }

            $not_placed_roles = array('Top', 'ADC', 'Support', 'Jungle', 'Middle');
            $placement_count = 0;
            $last_summoner_key = -1;
            foreach ($teams->team1 as $summoner_key => $participant) {
                if (!empty($participant->prediction)) {
                    $not_placed_roles = array_diff($not_placed_roles, array($participant->prediction));
                    $placement_count++;
                } else {
                    $last_summoner_key = $summoner_key;
                }
            }
            sort($not_placed_roles);

            if (count($not_placed_roles) == 1 && $last_summoner_key >= 0) { // eğer yerleştirilmeyen 1 rol varsa
                $teams->team1[$last_summoner_key]->prediction = $not_placed_roles[0];
                $teams->team1[$last_summoner_key]->reason[] = '[F] Sona kalan dona kalır';
                $placement_count++;
            } else {
                // echo $last_summoner_key.'_'.$not_placed_roles[0].'_'.count($not_placed_roles);
            }

            if ($placement_count >= 0 && $placement_count < 6) {
                $placement_count_total[$placement_count]++;
            }

            echo '<table style="margin-bottom:15px;margin-top:3px;border:1px solid black;padding:5px;">';
            $total_team++;
            foreach ($teams->team1 as $summoner_key => $participant) {
                $warning = '';
                if ($participant->prediction == 'Jungle' && $participant->lane != 'JUNGLE') {
                    $warning = '!!JUNGLE!';
                }
                if ($participant->prediction == 'ADC' && $participant->role != 'DUO_CARRY') {
                    if ($participant->role != 'DUO') {
                        $warning = '!!ADC!';
                    }
                }
                if ($participant->prediction == 'Top' && $participant->lane != 'TOP') {
                    $warning = '!!TOP!';
                }
                if ($participant->prediction == 'Middle' && $participant->lane != 'MIDDLE') {
                    $warning = '!!MIDDLE!';
                }
                if ($participant->prediction == 'Support' && $participant->role != 'DUO_SUPPORT') {
                    if ($participant->role != 'DUO') {
                        $warning = '!!SUPPORT!';
                    }
                }
                if ($warning) {
                    $warning_count++;
                }

                $reason = '';
                $reasoncount = count($participant->reason) - 1;
                foreach ($participant->reason as $reason_key => $reason_) {
                    $reason .= '<div style="color:' . ($reason_key == $reasoncount ? 'black' : 'red') . ';margin-right:10px;">' . $reason_ . ($reason_key == $reasoncount ? '' : ' (İptal)') . '</div>';
                }
                echo '<tr>';
                echo '<td style="padding:3px;border:1px solid gray;"><b>' . $participant->champion . '</b></td>';
                echo '<td style="padding:3px;border:1px solid gray;">' . $participant->spell1 . '</td>';
                echo '<td style="padding:3px;border:1px solid gray;">' . $participant->spell2 . '</td>';
                echo '<td style="padding:3px;border:1px solid gray;">' . $participant->role . '</td>';
                echo '<td style="padding:3px;border:1px solid gray;">' . $participant->lane . '</td>';
                echo '<td style="padding:3px;border:1px solid gray;width:50px;"></td>';
                echo '<td style="padding:3px;border:1px solid gray;">' . $participant->prediction . '</td>';
                echo '<td style="padding:3px;border:1px solid gray;">' . $reason . '</td>';
                echo '<td style="padding:3px;border:1px solid gray;">' . $warning . '</td>';
                echo '</tr>';
            }

            $process_sec = microtime(true) - $process_start;
            $total_time += $process_sec;
            echo '<tr><td colspan="7">Toplam Belirlenen Rol: ' . $placement_count . '</td><td>' . $match->queueType . '</td><td>' . $match->matchId . '</td></tr>';
            echo '<tr><td colspan="9" style="text-align:right;">İşlem Süresi:<b>' . number_format($process_sec, 4, '.', ',') . ' saniye</b></td></tr>';
            echo '</table>';

            // $this->Request->pr($teams->team1);
            // echo '</table>';
        }
        echo '<div style="position:absolute;right:20px;top:20px;font-size:30px;">';
        echo '<div>Total Team: ' . $total_team . '</div>';
        echo '<div>5 Role: ' . $placement_count_total[5] . ' (' . number_format(100 / $total_team * $placement_count_total[5], 1) . '%)' . '</div>';
        echo '<div>4 Role: ' . $placement_count_total[4] . ' (' . number_format(100 / $total_team * $placement_count_total[4], 1) . '%)' . '</div>';
        echo '<div>3 Role: ' . $placement_count_total[3] . ' (' . number_format(100 / $total_team * $placement_count_total[3], 1) . '%)' . '</div>';
        echo '<div>2 Role: ' . $placement_count_total[2] . ' (' . number_format(100 / $total_team * $placement_count_total[2], 1) . '%)' . '</div>';
        echo '<div>1 Role: ' . $placement_count_total[1] . ' (' . number_format(100 / $total_team * $placement_count_total[1], 1) . '%)' . '</div>';
        echo '<div>0 Role: ' . $placement_count_total[0] . ' (' . number_format(100 / $total_team * $placement_count_total[0], 1) . '%)' . '</div>';
        echo '<div>Warning: ' . $warning_count . ' (' . number_format(100 / ($total_team * 5) * $warning_count, 1) . '%)' . '</div>';
        echo '<div>Time Avg:  ' . number_format(($total_time / $total_team), 4, '.', ',') . ' sec</div>';


        echo '</div>';
        // echo '</div>';
    }

    public function championgg()
    {
        /*
            şampiyon isimleri html decode yapılacak
        */
        exit;
        set_time_limit(0);
        $url = 'http://champion.gg';

        $str = $this->Request->curlWithCache($url);

        $html = new simple_html_dom();
        $html->load($str);

        $s = 0;

        foreach ($html->find('.champ-height a[style*="display:block"]') as $element) {
            $champ_str = $this->Request->curlWithCache($url . $element->href);
            $champ_html = new simple_html_dom();
            $champ_html->load($champ_str);

            $name = trim($champ_html->find('.champion-profile h1', 0)->plaintext);
            $role = trim($champ_html->find('.champion-profile .selected-role h3', 0)->plaintext);
            $percent = trim(str_replace('Role Rate', '', $champ_html->find('.champion-profile .selected-role small', 0)->plaintext));
            $spell1 = trim($champ_html->find('.summoner-wrapper img', 0)->tooltip);
            $spell2 = trim($champ_html->find('.summoner-wrapper img', 1)->tooltip);
            echo $name . ' ' . $role . ' (' . $percent . ') ' . $spell1 . ',' . $spell2;
            echo '<br>';
            // $this->db->insert('championgg',array('name'=>$name,'role'=>$role,'percent'=>$percent,'spell1'=>$spell1,'spell2'=>$spell2));
        }
    }

    public function lolnexusrecentgames()
    {
        set_time_limit(0);
        $url = 'http://www.lolnexus.com/recent-games?cookieTest=1';//&page=2

        $str = $this->Request->curlWithCache($url, 30);

        $html = new simple_html_dom();
        $html->load($str);

        foreach ($html->find('.recent-game') as $element) {
            $region = $element->find('small', 0)->innertext;
            $player = $element->find('.player h4', 0)->innertext;
            $time = (int)str_replace(':', '', $element->find('.elapsed', 0)->innertext);
            if ($time > 300 && $time < 20000) {
                echo $region;
                echo '<br>';
                echo $player;
                echo '<br>';
                echo '<br>';
            }

        }
    }

    public function lolnexuslive($region, $summoner_name)
    {
        set_time_limit(0);
        $url = 'http://www.lolnexus.com/ajax/get-game-info/' . $region . '.json?cookieTest=1&name=' . urlencode($summoner_name);

        $query = $this->Request->curlWithCache($url, 30);
        if ($query) {
            $result = json_decode($query);
            echo $result->html;
        }
    }
}