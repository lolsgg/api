<?php
class Search extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function index($term = "")
	{
        echo "Redirecting...";
		$this->load->helper('cookie');

		$lang2region = array(
			'en' => 'NA',
			'de' => 'EUW',
			'ru' => 'RU',
			'tr' => 'TR',
			'es' => 'EUW',
			'fr' => 'EUW',
			'pt' => 'BR',
			'it' => 'EUW',
			'ro' => 'EUNE',
			'pl' => 'EUNE',
			'hu' => 'EUNE',
			'id' => 'OCE',
			'ko' => 'KR',
			'ja' => 'JP',
			'zh' => 'KR',
			'ms' => 'OCE',
			'th' => 'OCE'
		);

		/*if(strpos($term, '%20'))
		{
			$term = str_replace('%20','+', $term);
			redirect("search/$term", 'refresh');
		}*/

		$region = get_cookie('region');

		if(!$region)
		{
			$user_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
			$region = (isset($lang2region[$user_lang]) ? $lang2region[$user_lang] : 'NA');
		}

		if($term){
            redirect("https://lols.gg/summoner/live/$region/$term", 'refresh');
        } else {
            redirect("https://lols.gg", 'refresh');
        }
	}
}
?>
