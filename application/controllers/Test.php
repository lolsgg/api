<?php

class Test extends CI_Controller
{
    public function index()
    {

    }

    public function asd()
    {
        $status = true;
        $this->cache->redis->save('maintenance:league:na', $status, 86400);
        $this->cache->redis->save('maintenance:league:euw', $status, 86400);
        $this->cache->redis->save('maintenance:league:eune', $status, 86400);
        $this->cache->redis->save('maintenance:league:br', $status, 86400);
        $this->cache->redis->save('maintenance:league:tr', $status, 86400);
        $this->cache->redis->save('maintenance:league:ru', $status, 86400);
        $this->cache->redis->save('maintenance:league:lan', $status, 86400);
        $this->cache->redis->save('maintenance:league:las', $status, 86400);
        $this->cache->redis->save('maintenance:league:oce', $status, 86400);
        $this->cache->redis->save('maintenance:league:kr', $status, 86400);
        $this->cache->redis->save('maintenance:league:jp', $status, 86400);
    }

    public function asdf()
    {
        $status = true;
        $this->cache->redis->save('maintenance:champmastery:na', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:euw', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:eune', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:br', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:tr', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:ru', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:lan', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:las', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:oce', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:kr', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:jp', $status, 86400);
    }

    public function asdfg()
    {
        $status = false;
        $this->cache->redis->save('maintenance:league:na', $status, 86400);
        $this->cache->redis->save('maintenance:league:euw', $status, 86400);
        $this->cache->redis->save('maintenance:league:eune', $status, 86400);
        $this->cache->redis->save('maintenance:league:br', $status, 86400);
        $this->cache->redis->save('maintenance:league:tr', $status, 86400);
        $this->cache->redis->save('maintenance:league:ru', $status, 86400);
        $this->cache->redis->save('maintenance:league:lan', $status, 86400);
        $this->cache->redis->save('maintenance:league:las', $status, 86400);
        $this->cache->redis->save('maintenance:league:oce', $status, 86400);
        $this->cache->redis->save('maintenance:league:kr', $status, 86400);
        $this->cache->redis->save('maintenance:league:jp', $status, 86400);
    }

    public function asdfgh()
    {
        $status = false;
        $this->cache->redis->save('maintenance:champmastery:na', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:euw', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:eune', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:br', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:tr', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:ru', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:lan', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:las', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:oce', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:kr', $status, 86400);
        $this->cache->redis->save('maintenance:champmastery:jp', $status, 86400);
    }

    public function mongotest()
    {

        if (ENVIRONMENT == 'development') {
            $dns = "mongodb://192.168.1.113:27017";
            //$dns = "mongodb://lolsgg:mongo*18711565*aws@18.218.103.55:27017";
        } else {
            $dns = "mongodb://lolsgg:mongo*18711565*aws@172.31.26.234:27017";
        }

        try {

            $client = new MongoDB\Client($dns);

            $collection = $client->match_history->tr;

            try {

                $collection->find(array('_id' => array('$in' => array(657996524))), array('projection' => array('timeline' => false)));
                echo "ok 1<br>";
            } catch (Exception $e) {
                echo "bad 1<br>";
            }
        } catch (Exception $e) {
            echo "bad 2<br>";
        }
        echo "ok 2<br>";
    }

    public function redistest()
    {

    }

    public function publictest()
    {
        echo "ok";
    }

    public function deneme()
    {
        $this->lol->setRegion("tr");
        $this->lol->setLang("tr");


    }

    public function guncelle($reg_)
    {
        if (true) {
            die();
        }

        set_time_limit(0);
        $starttime = microtime(true);
        $regions = array($reg_);
        $champions = $this->lol->getChampions();
        foreach ($regions as $region) {
            $this->cache->redis->save('maintenance:champmastery:' . $region, true, 86400);
            foreach ($champions as $champion) {
                if ($champion->key >= 0) {
                    $champion_id = (int)$champion->key;
                    $this->db->query("SET @r_counter = 0");
                    $this->db->query("UPDATE champ_mastery_" . strtolower($region) . " SET position = (@r_counter := @r_counter + 1) WHERE champion_id = " . (int)$champion_id . " ORDER BY points DESC;");
                }
            }
            $this->cache->redis->save('maintenance:champmastery:' . $region, false, 86400);
        }
        $time = microtime(true) - $starttime;
        $message = $reg_ . " Bitti:" . $time;
        $this->Request->sendEmailFromNoreply('eyupcanakman@gmail.com', 'Lols.gg Test', $message);
    }

    public function log()
    {
        $key = "lol_request_log";
        $result = $this->cache->redis->lRange($key, 0, -1);
        echo count($result);
        $this->Request->pr($result);
    }


    public function ram()
    {
        echo "Using ", (memory_get_peak_usage(1) / 1024 / 1024), " MB of ram.";
    }

    public function summarysignature()
    {
        $summoner = array();
        $summoner['region'] = 'TR';
        $summoner['summoner_name'] = 'RVA Nakamura';
        $summoner['league'] = 'SILVER I';
        $summoner['lp'] = '86';
        $summoner['series'] = 'WLNNN';
        $summoner['win_ratio'] = '54%';
        $summoner['profile_icon_id'] = '580';
        $summoner['kills'] = '13.7';
        $summoner['deaths'] = '16.6';
        $summoner['assists'] = '13.2';
        $summoner['kda'] = '2.56';
        $summoner['kill_participant'] = '57.7%';

        $summoner['stat_1_name'] = 'Braum';
        $summoner['stat_1_key'] = 'Braum';
        $summoner['stat_1_win'] = '433';
        $summoner['stat_1_lose'] = '534';
        $summoner['stat_1_kills'] = '15';
        $summoner['stat_1_deaths'] = '41';
        $summoner['stat_1_assists'] = '136';
        $summoner['stat_1_kda'] = '3.68';
        $summoner['stat_1_winratio'] = '54.4%';
        $summoner['stat_1_kill_participant'] = '64.5%';

        $summoner['stat_2_name'] = 'Lulu';
        $summoner['stat_2_key'] = 'Lulu';
        $summoner['stat_2_win'] = '2';
        $summoner['stat_2_lose'] = '0';
        $summoner['stat_2_kills'] = '2';
        $summoner['stat_2_deaths'] = '12';
        $summoner['stat_2_assists'] = '23';
        $summoner['stat_2_kda'] = '2.08';
        $summoner['stat_2_winratio'] = '100%';
        $summoner['stat_2_kill_participant'] = '43.9%';

        $summoner['champ_1_key'] = 'Braum';
        $summoner['champ_1_level'] = '5';
        $summoner['champ_1_points'] = '238,167';

        $summoner['champ_2_key'] = 'Nidalee';
        $summoner['champ_2_level'] = '7';
        $summoner['champ_2_points'] = '538,256';

        $summoner['champ_3_key'] = 'Gnar';
        $summoner['champ_3_level'] = '3';
        $summoner['champ_3_points'] = '38,167';

        //$this->lolimage->summarySignature($summoner);
    }
}