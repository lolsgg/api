<?php

class Home extends CI_Controller
{
    public function index()
    {
        echo '
			<html>
				<head>
					<style>
					html, body {
						height: 100%;
						display: flex;
						justify-content: center;
						align-items: center;
						}
						body {
						background: url(https://media.giphy.com/media/3oFyCZm9jPjlzsAwXS/giphy.gif);
						background-size: cover;
						background-position: center;
						}
						h1 {
						font-family: monospace;
						text-align:center;
						text-transform: uppercase;
						border: 5px solid white;
						padding: 0.5em;
						color: #fff;
						cursor: default;
						-webkit-user-select: none;
						-moz-user-select: none;
						user-select: none;
						}
					</style>
				</head>
				<body>
					<h1>Lols.gg API <br> Access Denied!</h1>
				</body>
			</html>
		';
    }
}

?>
