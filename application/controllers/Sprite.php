<?php
class Sprite extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}

	public function skins()
	{
		set_time_limit(0);

		$results = $this->lol->getChampions();
		$names = array();
		foreach($results as $key => $result)
		{
			$champ_id = $result->id;
			$champ = $this->lol->getChampionById($result->key,'skins');
			foreach ($champ->skins as $skin_id => $skin) {
				$names[] = array('name'=>$champ_id.'_'.$skin_id,'filename'=>$champ_id.'_'.$skin_id.'.jpg');
			}
		}

		$foldername = 'loading-jpg';

		foreach($names as $name)
		{
			if (!is_dir(FCPATH.'assets/lol/'.$foldername.'/'))
			{
			    mkdir(FCPATH.'assets/lol/'.$foldername.'/');
			}
			$local = FCPATH.'assets/lol/'.$foldername.'/'.$name['filename'];
			if(!file_exists($local))
			{
				// $remote_url = $this->lol->getDragonImgUrl().$foldername.'/'.$name['filename'];
				$remote_url = 'http://ddragon.leagueoflegends.com/cdn/img/champion/loading/'.$name['filename'];
				file_put_contents($local, file_get_contents($remote_url));
				// echo $remote_url.'<br>';
				// echo '<img src="'.$remote_url.'" width="50"/>';
				echo $name['filename'].' downloaded<br>';
			}
		}

	}

	public function csstest()
	{
		// echo '<link href="http://localhost/lols///media.lols.gg/img/champions48x48.css" rel="stylesheet">';
		echo '<link href="http://localhost/lols/assets/lol/spells20x20.css" rel="stylesheet">';
		$champions = $this->lol->getSummonerSpells();
		$count=0;
		foreach($champions as $champion)
		{
			$name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$champion->name));
			$count++;
			echo '<img class="spells-20 '.$name.'"/>';
			if($count%10==0)
			{
				echo '<br>';
			}
		}
	}


	public function info()
	{
		$data = new StdClass();
		$data->page_title = "Sprite";

		$this->lol->setRegion('NA');
		$champions = $this->lol->getProfileIcons();
		$count=0;
		foreach($champions as $champion)
		{
			$count++;
			$this->Request->pr($champion);
		}
		echo $count;
	}


	public function makeSprite()
	{
		set_time_limit(0);

		$names = array();
		$type = 'champion';
		$foldername = 'champion';
		// $type = 'spells';
		$size = 32;
		$imgperline = 10;
		$this->lol->setRegion('NA');

		$results = $this->lol->getChampions();
		// $results = $this->lol->getSummonerSpells();
		foreach($results as $key => $result)
		{
			$name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$key));
			// $name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$result->id));
			$names[] = array('name'=>$name,'filename'=>$result->image->full);
		}

		$this->Request->pr($names);
		exit;

		$sprite_url = $this->imageCreate($type,$foldername,$names,$size,$imgperline);
		echo $sprite_url.'<br>';
		echo $this->cssCreate($type,$names,$sprite_url,$size,$imgperline);
	}

	public function makeChampionSprite()
	{
		set_time_limit(0);

		$names = array();
		$type = 'champion';
		$foldername = 'champion';
		// $type = 'spells';
		$size = 100;
		$imgperline = 10;
		$this->lol->setRegion('NA');

		$results = $this->lol->getChampions();
		// $results = $this->lol->getSummonerSpells();
		foreach($results as $key => $result)
		{
			$name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$key));
			// $name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$result->id));
			$names[] = array('name'=>$name,'filename'=>$result->image->full);
		}


		$sprite_url = $this->imageCreate($type,$foldername,$names,$size,$imgperline);
		echo $sprite_url.'<br>';
		echo $this->cssCreate($type,$names,$sprite_url,$size,$imgperline);
	}

	public function makeSpellSprite()
	{
		set_time_limit(0);

		$names = array();
		$type = 'spells';
		$foldername = 'spell';
		// $type = 'spells';
		$size = 16;
		$imgperline = 10;
		$this->lol->setRegion('NA');

		// $results = $this->lol->getChampions();
		$results = $this->lol->getSummonerSpells();
		foreach($results as $key => $result)
		{
			$name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$key));
			// $name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$result->id));
			$names[] = array('name'=>$name,'filename'=>$result->image->full);
		}

		// $this->Request->pr($names);
		// exit;
		$sprite_url = $this->imageCreate($type,$foldername,$names,$size,$imgperline);
		echo $sprite_url.'<br>';
		echo $this->cssCreate($type,$names,$sprite_url,$size,$imgperline);
	}

	public function vtest()
	{
		$list = ["6.24.1","6.23.1","6.22.1","6.21.1","6.20.1","6.19.1","6.18.1","6.17.1","6.16.2","6.16.1","6.15.1","6.14.2","6.14.1","6.13.1","6.12.1","6.11.1","6.10.1","6.9.1","6.8.1","6.7.1","6.6.1","6.5.1","6.4.2","6.4.1","6.3.1","6.2.1","6.1.1","5.24.2","5.24.1","5.23.1","5.22.3","5.22.2","5.22.1","5.21.1","5.20.1","5.19.1","5.18.1","5.17.1","5.16.1","5.15.1","5.14.1","5.13.1","5.12.1","5.11.1","5.10.1","5.9.1","5.8.1","5.7.2","5.7.1","5.6.2","5.6.1","5.5.3","5.5.2","5.5.1","5.4.1","5.3.1","5.2.2","5.2.1","5.1.2","5.1.1","4.21.5","4.21.4","4.21.3","4.21.1","4.20.2","4.20.1","4.19.3","4.19.2","4.18.1","4.17.1","4.16.1","4.15.1","4.14.2","4.13.1","4.12.2","4.12.1","4.11.3","4.10.7","4.10.2","4.9.1","4.8.3","4.8.2","4.8.1","4.7.16","4.7.9","4.7.8","4.6.3","4.5.4","4.4.3","4.4.2","4.3.18","4.3.12","4.3.10","4.3.4","4.3.2","4.2.6","4.2.5","4.2.1","4.1.43","4.1.41","4.1.13","4.1.9","4.1.2","3.15.5","3.15.4","3.15.2","3.14.41","3.14.23","3.14.22","3.14.20","3.14.19","3.14.16","3.14.13","3.14.12","3.13.24","3.13.8","3.13.6","3.13.1","3.12.37","3.12.36","3.12.34","3.12.33","3.12.26","3.12.24","3.12.2","3.11.4","3.11.2","3.10.6","3.10.3","3.10.2","3.9.7","3.9.5","3.9.4","3.8.5","3.8.3","3.8.1","3.7.9","3.7.2","3.7.1","3.6.15","3.6.14","0.154.3","0.154.2","0.153.2","0.152.115","0.152.108","0.152.107","0.152.55","0.151.101","0.151.2"];
		foreach ($list as $version) {
			echo '<img src="http://ddragon.leagueoflegends.com/cdn/'.$version.'/img/profileicon/36.png"><br>';
		}
	}

	public function imageDownload()
	{
		set_time_limit(0);

		$names = array();
		$foldername = 'profileicon';
		$this->lol->setRegion('NA');

		$results = $this->lol->getProfileIcons();
		// $results = $this->lol->getSummonerSpells();
		foreach($results as $key => $result)
		{
			$name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$key));
			// $name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$result->id));
			$names[] = array('name'=>$name,'filename'=>$result->image->full);
		}
		foreach($names as $name)
		{
			if (!is_dir(FCPATH.'assets/'.$foldername.'/'))
			{
			    mkdir(FCPATH.'assets/'.$foldername.'/');
			}
			$local = FCPATH.'assets/'.$foldername.'/'.$name['filename'];
			if(!file_exists($local))
			{
				// $remote_url = $this->lol->getDragonImgUrl().$foldername.'/'.$name['filename'];
				$remote_url = $this->lol->getDragonImgUrl().'profileicon/'.$name['filename'];
				file_put_contents($local, file_get_contents($remote_url));
				echo $name['filename'].' downloaded<br>';
			}
		}
		echo "<br>done<br>";
	}

	public function profileIconDownload()
	{
		set_time_limit(0);

		$names = array();
		$foldername = 'profileicon';
		$this->lol->setRegion('NA');

		$results = $this->lol->getProfileIcons();
		// $results = $this->lol->getSummonerSpells();
		foreach($results as $key => $result)
		{
			$name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$key));
			// $name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$result->id));
			$names[] = array('name'=>$name,'filename'=>$result->image->full);
		}
		foreach($names as $name)
		{
			if (!is_dir(FCPATH.'assets/'.$foldername.'/'))
			{
			    mkdir(FCPATH.'assets/'.$foldername.'/');
			}
			$local = FCPATH.'assets/'.$foldername.'/'.$name['filename'];
			if(!file_exists($local))
			{
				// $remote_url = $this->lol->getDragonImgUrl().$foldername.'/'.$name['filename'];
				$remote_url = $this->lol->getDragonImgUrl().'profileicon/'.$name['filename'];
				file_put_contents($local, file_get_contents($remote_url));
				echo $name['filename'].' downloaded<br>';
			}
		}
		echo "<br>done<br>";
	}

	public function itemDownload()
	{
		set_time_limit(0);

		$names = array();
		$foldername = 'items';
		$this->lol->setRegion('NA');

		$results = $this->lol->getItems();
		// $results = $this->lol->getSummonerSpells();
		foreach($results as $key => $result)
		{
			$name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$key));
			// $name = mb_strtolower(str_replace(array(' ',"'"),array('-',''),$result->id));
			$names[] = array('name'=>$name,'filename'=>$result->image->full);
		}

		foreach($names as $name)
		{
			if (!is_dir(FCPATH.'assets/'.$foldername.'/'))
			{
			    mkdir(FCPATH.'assets/'.$foldername.'/');
			}
			$local = FCPATH.'assets/'.$foldername.'/'.$name['filename'];
			if(!file_exists($local))
			{
				// $remote_url = $this->lol->getDragonImgUrl().$foldername.'/'.$name['filename'];
				$remote_url = $this->lol->getDragonImgUrl().'item/'.$name['filename'];
				file_put_contents($local, file_get_contents($remote_url));
				echo $name['filename'].' downloaded<br>';
			}
		}
		echo "<br>done<br>";
	}

	public function quoteSoundDownload()
	{
		$this->load->database();
		$query = $this->db->get('champion_quote');
		foreach ($query->result() as $quote) {
			$filename = $this->lol->cleanText($quote->champion_name).'-'.$quote->champion_quote_id;
			$local = FCPATH.'assets/lol/sound/'.$filename.'.ogg';
			if(!file_exists($local))
			{
				$url = $quote->remote_url;
				file_put_contents($local, file_get_contents($url));
				echo $quote->champion_name.' downloaded<br>';
			}
			$this->db->where('champion_quote_id',$quote->champion_quote_id);
			$this->db->update('champion_quote',array('sound_file'=>$filename.'.ogg'));
		}
	}


	public function makeSpriteWithList()
	{
		set_time_limit(0);

		$spritename = 'tiers';
		$foldername = 'tier';

		$size = 128;
		$imgperline = 5;

		$names = array();
		$names[] = array("name"=>'bronze_i','filename'=>'bronze_i.png');
		$names[] = array("name"=>'bronze_ii','filename'=>'bronze_ii.png');
		$names[] = array("name"=>'bronze_iii','filename'=>'bronze_iii.png');
		$names[] = array("name"=>'bronze_iv','filename'=>'bronze_iv.png');
		$names[] = array("name"=>'bronze_v','filename'=>'bronze_v.png');
		$names[] = array("name"=>'silver_i','filename'=>'silver_i.png');
		$names[] = array("name"=>'silver_ii','filename'=>'silver_ii.png');
		$names[] = array("name"=>'silver_iii','filename'=>'silver_iii.png');
		$names[] = array("name"=>'silver_iv','filename'=>'silver_iv.png');
		$names[] = array("name"=>'silver_v','filename'=>'silver_v.png');
		$names[] = array("name"=>'gold_i','filename'=>'gold_i.png');
		$names[] = array("name"=>'gold_ii','filename'=>'gold_ii.png');
		$names[] = array("name"=>'gold_iii','filename'=>'gold_iii.png');
		$names[] = array("name"=>'gold_iv','filename'=>'gold_iv.png');
		$names[] = array("name"=>'gold_v','filename'=>'gold_v.png');
		$names[] = array("name"=>'platinum_i','filename'=>'platinum_i.png');
		$names[] = array("name"=>'platinum_ii','filename'=>'platinum_ii.png');
		$names[] = array("name"=>'platinum_iii','filename'=>'platinum_iii.png');
		$names[] = array("name"=>'platinum_iv','filename'=>'platinum_iv.png');
		$names[] = array("name"=>'platinum_v','filename'=>'platinum_v.png');
		$names[] = array("name"=>'diamond_i','filename'=>'diamond_i.png');
		$names[] = array("name"=>'diamond_ii','filename'=>'diamond_ii.png');
		$names[] = array("name"=>'diamond_iii','filename'=>'diamond_iii.png');
		$names[] = array("name"=>'diamond_iv','filename'=>'diamond_iv.png');
		$names[] = array("name"=>'diamond_v','filename'=>'diamond_v.png');
		$names[] = array("name"=>'master','filename'=>'master.png');
		$names[] = array("name"=>'challenger','filename'=>'challenger.png');
		$names[] = array("name"=>'provisional','filename'=>'provisional.png');
		$names[] = array("name"=>'unranked','filename'=>'unranked.png');

		$sprite_url = $this->imageCreate($spritename,$foldername,$names,$size,$imgperline);
		echo $sprite_url.'<br>';
		echo $this->cssCreate($spritename,$names,$sprite_url,$size,$imgperline);
	}

	public function skincss()
	{
		set_time_limit(0);
		ini_set('memory_limit', '-1');

		$results = $this->lol->getChampions();
		$names = array();
		$s = 0;
		foreach($results as $key => $result)
		{
			$s++;
			if ($s>25) {
				// continue;
			}
			$champ_id = $result->id;
			$champ = $this->lol->getChampionById($result->key,'skins');
			foreach ($champ->skins as $skin_id => $skin) {
				if (file_exists(FCPATH.'assets/lol/loading/'.$champ_id.'_'.$skin_id.'.png')) {
					$names[] = array('name'=>$champ_id.'_'.$skin_id,'filename'=>$champ_id.'_'.$skin_id.'.png');
				}
			}
		}

		$foldername = 'loading';
		$type = 'loading';
		$size = 308;
		$imgperline = 71;

		$this->imageCreate($type,$foldername,$names,$size,$imgperline);
	}

	private function imageCreate($type,$foldername,$names,$size,$imgperline)
	{
		$line = 0;
		$count = 0;
		$orj_size_w = 0;
		$lines = array();
		$s = 0;
		foreach($names as $name)
		{
			$s++;
			if (!is_dir(FCPATH.'assets/lol/'.$foldername.'/'))
			{
			    mkdir(FCPATH.'assets/lol/'.$foldername.'/');
			}
			$local = FCPATH.'assets/lol/'.$foldername.'/'.$name['filename'];
			if(!file_exists($local))
			{
				// continue;
				$remote_url = $this->lol->getDragonImgUrl().$foldername.'/'.$name['filename'];
				file_put_contents($local, file_get_contents($remote_url));
			}



			$count++;
			$oldline = $line;
			$line = ceil($count/$imgperline);



			$sprite = FCPATH.'assets/lol/'.$foldername.$line.'.png';
			if($line==$oldline)
			{
				$this->merge($sprite, $local, $sprite);
			}
			else
			{
				$this->firstImage($local,$sprite);
				list($orj_size_w, $orj_size_h) = getimagesize($local);
				$lines[] = $sprite;
			}
		}
		$names = NULL;

		$sprite = FCPATH.'assets/lol/'.$type.$size.'x'.$size.'.png';

		$line_counter = 0;
		foreach($lines as $line)
		{
			$line_counter++;
			echo $line_counter.'-';
			if($line_counter==1)
			{
				$this->firstImage($line,$sprite);
			}
			else
			{
				$this->merge($sprite, $line, $sprite,true);
			}
			unlink($line);
		}

		$this->resizeImage($sprite,$orj_size_w,$size);


		return base_url().'assets/lol/'.$type.$size.'x'.$size.'.png';
	}

	private function cssCreate($type,$names,$url,$size,$imgperline)
	{
		$line = 0;
		$row = 0;

		$css = '.'.$type.'-'.$size.'{width: '.$size.'px;height:'.$size.'px;background-image: url('.$url.');}';

		$lines = array();

		$champions = $this->lol->getChampions();
		$count=0;
		foreach($names as $name)
		{
			$count++;
			$oldline = $line;
			$line = ceil($count/$imgperline);
			$row = $oldline!=$line?1:$row+1;



			$css .= '.'.$type.'-'.$size.'.'.$name['name'].'{background-position:'.(($row-1)*$size*-1).'px '.(($line-1)*$size*-1).'px;}';



		}
		$css_location = FCPATH.'assets/lol/'.$type.$size.'x'.$size.'.css';
		file_put_contents($css_location, $css);
		return '../lol/sprite/'.$type.$size.'x'.$size.'.png';
	}

	private function merge($filename_x, $filename_y, $filename_result,$vertical=false)
	{
		// Get dimensions for specified images

		list($width_x, $height_x) = getimagesize($filename_x);
		list($width_y, $height_y) = getimagesize($filename_y);

		// Create new image with desired dimensions
		if($vertical==false)
		{
			$image = imagecreatetruecolor($width_x + $width_y, $height_x);
		}
		else
		{
			$image = imagecreatetruecolor($width_x, $height_x + $height_y);
		}

		$color = imagecolorallocatealpha($image, 0, 0, 0, 127);
		imagefill($image, 0, 0, $color);


		imagesavealpha($image, true);

		// Load images and then copy to destination image

		$image_x = imagecreatefrompng($filename_x);
		$image_y = imagecreatefrompng($filename_y);

		imagesavealpha($image_x, true);
		imagesavealpha($image_y, true);


		if($vertical==false)
		{
			imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);
			imagecopy($image, $image_y, $width_x, 0, 0, 0, $width_y, $height_y);
		}
		else
		{
			imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);
			imagecopy($image, $image_y, 0, $height_x, 0, 0, $width_y, $height_y);

		}
		// Save the resulting image to disk (as JPEG)

		imagepng($image, $filename_result);

		// Clean up

		imagedestroy($image);
		imagedestroy($image_x);
		imagedestroy($image_y);

	}

	private function firstImage($filename,$target)
	{
		$image = imagecreatefrompng($filename);
		imagesavealpha($image, true);
		imagepng($image, $target);
		imagedestroy($image);
	}

	private function resizeImage($image,$oldsize,$newsize)
	{
		list($width, $height) = getimagesize($image);



		$newheight = $height/$oldsize*$newsize;
		$newwidth = $width/$oldsize*$newsize;


		$src = imagecreatefrompng($image);
		imagesavealpha($src, true);



		$dst = imagecreatetruecolor($newwidth, $newheight);
		$color = imagecolorallocatealpha($dst, 0, 0, 0, 127);
		imagefill($dst, 0, 0, $color);


		imagesavealpha($dst, true);




		imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

		imagepng($dst, $image);
	}

}

?>
