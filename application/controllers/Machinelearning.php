<?php

class Machinelearning extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        echo "machine";
    }

    public function listSummoner()
    {
        set_time_limit(0);
        $summoners = array();
        $region = 'tr';
        $platform = $this->lol->getPlatform();
        $this->lol->setRegion($region);
        foreach ($summoners as $summoner_name_) {
            $summoner_name = trim(str_replace(' ', '', mb_strtolower($summoner_name_)));
            $summoner_search = $this->lol->api("api/lol/$region/v1.4/summoner/by-name/$summoner_name");
            if (isset($summoner_search->{$summoner_name})) {
                $summoner = $summoner_search->{$summoner_name};
                $summoner_id = $summoner->id;
                $match = $this->lol->api("observer-mode/rest/consumer/getSpectatorGameInfo/$platform/$summoner_id");
                if (isset($match) && isset($match->participants)) {
                    foreach ($match->participants as $participant_key => $participant) {
                        echo $participant->summonerId . '<br>';
                    }
                }
            } else {
                echo 'summoner_yok<br>';
            }
        }
    }

    public function clearduplicatematches()
    {
        $matches_q = $this->db->query("SELECT match_list_id FROM match_list GROUP BY match_id HAVING COUNT(*)>1");
        $matches = $matches_q->result();
        foreach ($matches as $match) {
            $this->db->where("match_list_id", $match->match_list_id);
            $this->db->delete("match_list");
        }
    }


    public function collectMatch()
    {
        $region = 'tr';
        $this->lol->setRegion($region);
        set_time_limit(0);

        $summoner_list = array();
        $summoner_list[] = '';
        $total = 0;
        foreach ($summoner_list as $summoner_id) {
            $match_list = $this->lol->api("api/lol/$region/v2.2/matchlist/by-summoner/" . $summoner_id);
            if (!isset($match_list->matches)) {
                echo $summoner_id . ' Match Not Found<br>';
                continue;
            }
            $total += count($match_list->matches);
            echo $summoner_id . ' Total Matches:' . count($match_list->matches) . '<br>';
            foreach ($match_list->matches as $match) {
                $this->db->insert('match_list', array('match_id' => $match->matchId));
            }
        }
    }

    public function collectData()
    {

    }


    public function statNormalize()
    {
        // $this->db->query("UPDATE match_stat SET stat = stat*-1 WHERE stat<0");
    }


    public function turnreset()
    {
        $this->session->set_userdata('refrest_counter', 0);
        $this->session->set_userdata('turn_start_sec', time());
    }


    public function calculate()
    {
        $turn = $this->session->userdata('refrest_counter');
        $turn_sec = number_format($turn / (time() - $this->session->userdata('turn_start_sec')), 1, '.', '');
        if ($turn < 2000000) {
            // header("Refresh: 1;url='http://localhost/lols/machinelearning/calculate'");
        }
        for ($i = 0; $i < 1; $i++) {
            echo '<div style="color:#848484;font-size:14px;">';
            echo 'Turn:' . number_format($turn, 0, ',', '.') . ' (' . $turn_sec . '/s)' . '<br><br>';

            set_time_limit(0);

            $match_list_q = $this->db->get("match_list");
            $match_list = $match_list_q->result();

            $match_stat_q_w = $this->db->get_where("match_stat", array('winner' => 1));
            $match_stat_w = $match_stat_q_w->result();

            $match_stat_q_l = $this->db->get_where("match_stat", array('winner' => 0));
            $match_stat_l = $match_stat_q_l->result();

            $total_match = $match_list_q->num_rows();

            $winner = 0;
            $loser = 0;

            $winner_new = 0;
            $loser_new = 0;

            $first = 0;
            $last = 0;

            foreach ($match_stat_w as $match) {
                $winner += $match->stat * $match->multiplier / $total_match;
            }
            foreach ($match_stat_l as $match) {
                $loser += $match->stat * $match->multiplier / $total_match;
            }

            $first = ($winner - $loser);
            $first_percent = (100 / ($winner + $loser)) * $first;

            echo 'OLD:<br>';
            //echo $winner.'<br>';
            //echo $loser.'<br>';
            //echo '<b>'.$first.'</b><br>';
            echo '%' . $first_percent;
            echo '<br>';
            echo '<br>';

            $this->db->limit(0, 1);
            $this->db->order_by("turn", "asc");
            $stat_select_q = $this->db->get("match_stat");

            $stat_select = $stat_select_q->row();
            $selected_stat_title = $stat_select->title;

            $this->db->where('title', $stat_select->title);
            $this->db->update('match_stat', array('turn' => $stat_select->turn + 1));


            foreach ($match_stat_w as $match) {
                if ($match->title == $selected_stat_title) {
                    $match->multiplier -= 1;
                }
                $winner_new += $match->stat * $match->multiplier / $total_match;
            }
            foreach ($match_stat_l as $match) {
                if ($match->title == $selected_stat_title) {
                    $match->multiplier -= 1;
                }
                $loser_new += $match->stat * $match->multiplier / $total_match;
            }

            $last = ($winner_new - $loser_new);
            $last_percent = (100 / ($winner_new + $loser_new)) * $last;

            if ($last_percent > $first_percent) {
                $this->session->set_userdata('update_turn', $turn);
                $this->db->where('title', $stat_select->title);
                $this->db->update('match_stat', array('multiplier' => $stat_select->multiplier - 1));
            }
            $update_turn = $turn - $this->session->userdata('update_turn');

            echo 'NEW:<br>';
            //echo $winner_new.'<br>';
            //echo $loser_new.'<br>';
            //echo '<b>'.$last.'</b><br>';
            echo '%' . $last_percent;
            echo '<br>';
            echo '<br>';

            echo '<span style="font-size:10px;">' . $selected_stat_title . '</span>';
            echo '<br>';
            echo '<br>';

            echo($update_turn == 0 ? ' <span style="color:green;"><b>UPDATED</b></span>' : 'Last Update:' . $update_turn . ' turn');


            $this->session->set_userdata('refrest_counter', $turn + 1);
            echo '</div>';
        }
    }

    public function frame()
    {
        $this->load->view('frame.php');
    }
}