<?php

class RiotLimit extends CI_Controller
{
    public function index()
    {
        $this->load->library('session');
        if ($this->session->userdata('login')) {
            $data = array();
            $data['page_title'] = 'RiotLimit';


            $riot_limits_ = $this->lol->api_limits;
            $riot_limits = $this->lol->api_limits;

            foreach ($riot_limits_ as $region => $region_limits) {
                $riot_limits[$region] = array();
                $app_limit_usage = (int)$this->cache->redis->get("limit:v1:c:app:" . $region);
                $riot_limits[$region]['app'] = array($this->lol->api_app_limits[0], $this->lol->api_app_limits[1], $app_limit_usage, $this->lol->api_app_limits[1]);
                foreach ($region_limits as $limit_url => $limits) {
                    $limits[2] = (int)$this->cache->redis->get("limit:v1:c:" . $region . ':' . $limit_url);
                    $riot_limits[$region][$limit_url] = $limits;
                }
            }

            $data['riot_limits'] = $riot_limits;

            $this->load->view('panel/header', $data);
            $this->load->view('panel/riotlimit', $data);
            $this->load->view('panel/footer', $data);
        } else {
            redirect('Panel/Login');
        }
    }
}