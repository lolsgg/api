<?php

class AutoLive extends CI_Controller
{
    public function index()
    {
        $this->load->library('session');
        if ($this->session->userdata('login')) {
            $data = array();
            $data['page_title'] = 'AutoLive';


            if ($this->input->post()) {
                $this->form_validation->set_rules('player_name', 'Player Name', 'required|max_length[250]');
                $this->form_validation->set_rules('region', 'Region', 'required|max_length[250]');
                $this->form_validation->set_rules('twitter', 'Twitter', 'required|max_length[250]');
                $this->form_validation->set_rules('twitch', 'Twitch', 'required|max_length[250]');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->insert('autolive', array(
                        'player_name' => $this->input->post('player_name'),
                        'region' => $this->input->post('region'),
                        'twitter' => $this->input->post('twitter'),
                        'twitch' => $this->input->post('twitch')
                    ));
                    $this->session->set_flashdata('success', 'Player added');
                    redirect(site_url('Panel/AutoLive'));
                }
            }

            $query = $this->db->get('autolive');
            $data['results'] = $query->result();

            $this->load->view('panel/header', $data);
            $this->load->view('panel/autolive', $data);
            $this->load->view('panel/footer', $data);
        } else {
            redirect('Panel/Login');
        }
    }

    public function delete($autolive_id)
    {
        $this->load->library('session');
        if ($this->session->userdata('login')) {
            $this->db->where('autolive_id', $autolive_id);
            $this->db->delete('autolive');
            $this->session->set_flashdata('success', 'Player deleted');
            redirect(site_url('Panel/AutoLive'));
        } else {
            redirect('Panel/Login');
        }
    }

}