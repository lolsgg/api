<?php

class CacheCreate extends CI_Controller
{
    public function index()
    {
        $this->load->library('session');
        if ($this->session->userdata('login')) {
            $data = array();
            $data['page_title'] = 'CacheCreate';
            $data['current_version'] = $this->lol->getVersion();

            ob_start();

            if ($this->input->post('create_champion_cache') && $this->input->post('version')) {
                $version = $this->input->post('version');
                $lang = $this->input->post('lang') ? $this->input->post('lang') : 'tr';
                $this->lol->setVersion($version);
                $this->lol->setRegion('tr');
                $this->lol->setLang($lang);

                $champions = $this->lol->getChampions();
                foreach ($champions as $champion_name => $champ) {
                    echo $champion_name . '<br>';
                    echo $champ->key . '<br>';

                    $champ_details = $this->lol->getChampionById2($champ->key);

                    if (isset($champ_details->id) && (int)$champ->key == (int)$champ_details->id) {
                        echo 'OK';
                    } else {
                        echo 'BAD';
                    }
                    echo '<hr>';
                }
            }

            if ($this->input->post('clear_all_champion_cache') && $this->input->post('version')) {
                $version = $this->input->post('version');
                $lang = $this->input->post('lang') ? $this->input->post('lang') : 'tr';
                $this->lol->setVersion($version);
                $this->lol->setRegion('tr');
                $this->lol->setLang($lang);
                $lang_ = $this->lol->getLang();

                $champions = $this->lol->getChampions();

                foreach ($champions as $champion_name => $champ) {
                    echo $champion_name . '<br>';
                    echo $champ->key . '<br>';

                    $cache_key = 'getchampionbyid2_v2_' . md5($version . $lang_ . '_' . $champ->key . 'all');

                    if ($this->cache->redis->delete($cache_key)) {
                        echo 'CLEANED';
                    } else {
                        echo 'NOT CLEANED';
                    }
                    echo '<hr>';
                }
            }

            if ($this->input->post('clear_champion_cache') && $this->input->post('version') && $this->input->post('champion_id')) {
                $version = $this->input->post('version');
                $champion_id = $this->input->post('champion_id');
                $lang = $this->input->post('lang') ? $this->input->post('lang') : 'tr';

                $this->lol->setVersion($version);
                $this->lol->setRegion('tr');
                $this->lol->setLang($lang);
                $lang_ = $this->lol->getLang();
                $cache_key = 'getchampionbyid2_v2_' . md5($version . $lang_ . '_' . $champion_id . 'all');
                echo 'CACHE DELETED<br>';
                echo '<div onclick="$(\'#old_cache_data\').toggle();">Old Cache Data Show/Hide</div><div style="display:none;color:blue;min-height:100px;min-width:100px;border:1px solid gray;" id="old_cache_data">';
                echo "<pre>";
                print_r($this->cache->redis->get($cache_key));
                echo "</pre>";
                echo '</div>';
                echo '<hr>';
                $this->cache->redis->delete($cache_key);

            }


            if ($this->input->post('create_build_cache') && $this->input->post('build_version')) {
                $this->load->model('Build_model');
                $build_version = $this->input->post('build_version');
                $lang = $this->input->post('lang') ? $this->input->post('lang') : 'tr';
                $this->lol->setRegion('tr');
                $this->lol->setLang($lang);

                echo '<br>created<br>';
                $champions = $this->lol->getChampions();
                foreach ($champions as $champion_name => $champ) {
                    echo $champion_name . '<br>';
                    echo $champ->key . '<br>';

                    $lanes = $this->Build_model->getChampionLanes($build_version, $champ->key);

                    foreach ($lanes as $lane => $ratio) {
                        if ($ratio > 6) {
                            $this->Build_model->items($build_version, $champ->key, $lane);
                            $this->Build_model->runes($build_version, $champ->key, $lane);
                            $this->Build_model->spells($build_version, $champ->key, $lane);
                        }

                    }

                    echo '<hr>';
                }
            }

            if ($this->input->post('clear_build_cache') && $this->input->post('build_version')) {
                $this->load->model('Build_model');
                $build_version = $this->input->post('build_version');
                $lang = $this->input->post('lang') ? $this->input->post('lang') : 'tr';
                $this->lol->setRegion('tr');
                $this->lol->setLang($lang);

                echo '<br>cleaned<br>';
                $champions = $this->lol->getChampions();
                foreach ($champions as $champion_name => $champ) {
                    echo $champion_name . '<br>';
                    echo $champ->key . '<br>';

                    $lanes = $this->Build_model->getChampionLanes($build_version, $champ->key);

                    foreach ($lanes as $lane => $ratio) {
                        $cache_key = 'build_items_v5_' . $build_version . '_TR_' . $this->lol->getLang() . '_' . $champ->key . '_' . $lane;
                        if ($this->cache->redis->delete($cache_key)) {
                            echo $lane.' Items CLEANED<br>';
                        } else {
                            echo $lane.' Items NOT CLEANED<br>';
                        }
                        $cache_key = 'build_runes_v4_' . $build_version . '_TR_' . $this->lol->getLang() . '_' . $champ->key . '_' . $lane;
                        if ($this->cache->redis->delete($cache_key)) {
                            echo $lane.' Runes CLEANED<br>';
                        } else {
                            echo $lane.' Runes NOT CLEANED<br>';
                        }
                        $cache_key = 'build_spells_v4_' . $build_version . '_TR_' . $this->lol->getLang() . '_' . $champ->key . '_' . $lane;
                        if ($this->cache->redis->delete($cache_key)) {
                            echo $lane.' Spells CLEANED<br>';
                        } else {
                            echo $lane.' Spells NOT CLEANED<br>';
                        }
                    }

                    echo '<hr>';
                }
            }


            $data['log'] = ob_get_contents();
            ob_end_clean();
            // $champ = $this->lol->getChampions($banned_champion->championId,'all');

            $this->load->view('panel/header', $data);
            $this->load->view('panel/cachecreate', $data);
            $this->load->view('panel/footer', $data);
        } else {
            redirect('Panel/Login');
        }
    }

}