<?php

class Feedback extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('user_agent');
        $this->load->library('pagination');
        $this->load->helper('text');
        $this->load->helper('string');
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $data = array();
            $data['page_title'] = 'Feedback';

            $data['is_mobile'] = $this->agent->is_mobile();

            $full_query = $this->db->get('feedback');

            $config['base_url'] = site_url('Panel/Feedback/index');
            $config['total_rows'] = $full_query->num_rows();
            $config['per_page'] = 50;
            $config["uri_segment"] = 4;
            $config['attributes'] = array('class' => 'page-link');

            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['first_link'] = 'First';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = 'Last';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';

            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';

            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';

            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';

            $this->pagination->initialize($config);

            $page = ($this->uri->segment($config["uri_segment"])) ? $this->uri->segment($config["uri_segment"]) : 0;

            $this->db->select('f.*, fr.feedback_reply_id');
            $this->db->order_by('f.feedback_id', 'desc');
            $this->db->limit($config["per_page"], $page);
            $this->db->join('(SELECT feedback_reply_id,feedback_id FROM feedback_reply GROUP BY feedback_id) fr', "f.feedback_id = fr.feedback_id", "left");
            $query = $this->db->get('feedback f');
            $results = $query->result();

            $data['pagination'] = $this->pagination->create_links();

            $data['results'] = array();

            foreach ($results as $result) {
                $result->date = date('M d H:i', strtotime($result->date_added));
                $result->message_short = word_limiter($result->message, 5);

                $data['results'][] = $result;
            }


            $draft_query = $this->db->get("feedback_draft");
            if ($draft_query->num_rows()) {
                $data['drafts'] = $draft_query->result();
            }


            $this->load->view('panel/header', $data);
            $this->load->view('panel/feedback', $data);
            $this->load->view('panel/footer', $data);
        } else {
            redirect('Panel/Login');
        }
    }

    public function getFeedbackJson($feedback_id = 0)
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        if ($this->session->userdata('login')) {
            $this->db->where("feedback_id", $feedback_id);
            $feedback_query = $this->db->get("feedback");
            if ($feedback_query->num_rows()) {
                $data->status = true;
                $feedback = $feedback_query->row();

                $feedback->date = date('M d H:i', strtotime($feedback->date_added));
                $feedback->message_short = word_limiter($feedback->message, 5);

                $this->agent->parse($feedback->browser);
                $feedback->browser = $this->agent->browser() . ' ' . $this->agent->version();
                $feedback->os = $this->agent->platform();
                $data->feedback = $feedback;

                $this->db->where("feedback_id", $feedback_id);
                $this->db->order_by("feedback_reply_id", "asc");
                $replies_query = $this->db->get("feedback_reply");
                if ($replies_query->num_rows()) {
                    $replies = $replies_query->result();
                    $data->replies = array();
                    foreach ($replies as $reply) {
                        $data->replies[] = array(
                            'message' => $reply->message,
                            'date' => date('M d H:i', strtotime($reply->date_added))
                        );
                    }
                }
            }
        }

        echo json_encode($data);
    }

    public function replyFeedback($feedback_id = 0)
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;
        $data->error = false;

        if ($this->session->userdata('login')) {
            $this->form_validation->set_rules('reply_text', 'Reply Text', 'required|min_length[5]|max_length[2500]');


            if ($this->form_validation->run() == TRUE) {
                $this->db->where("feedback_id", $feedback_id);
                $feedback_query = $this->db->get("feedback");
                if ($feedback_query->num_rows()) {
                    $feedback = $feedback_query->row();
                    $reply_text = $this->input->post("reply_text", true);

                    $email_body = $this->load->view('email/feedback-reply', array(
                        'message' => $feedback->message,
                        'reply' => $reply_text
                    ), true);

                    $result = $this->Request->sendEmailAwsSes(
                        'feedback@lols.gg',
                        [$feedback->email],
                        "Lols.gg Feedback",
                        $email_body);
                    if ($result) {
                        $data->status = true;
                        $data->message = $reply_text;
                        $data->date = date('M d H:i');
                        $this->db->insert('feedback_reply', array(
                            'feedback_id' => $feedback_id,
                            'message' => $reply_text
                        ));
                    } else {
                        $data->error = 'Email not sent';
                    }

                } else {
                    $data->error = 'Feedback Not Found';
                }
            } else {
                $data->error = validation_errors(' ', ' ');
            }

        } else {
            $data->error = 'Permission Denied';
        }

        echo json_encode($data);
    }

    public function sendFeedbackToTrello($feedback_id = 0)
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        if ($this->session->userdata('login')) {
            $this->db->where("feedback_id", $feedback_id);
            $feedback_query = $this->db->get("feedback");
            if ($feedback_query->num_rows()) {
                $feedback = $feedback_query->row();

                $feedback->date = date('M d H:i', strtotime($feedback->date_added));

                $this->agent->parse($feedback->browser);
                $feedback->browser = $this->agent->browser() . ' ' . $this->agent->version();
                $feedback->os = $this->agent->platform();
                $data->feedback = $feedback;

                $email_body = '';

                $email_body .= '<b>Feedback</b><br>' . $feedback->message;
                $email_body .= '<br>';
                $email_body .= '<b>Note</b><br>' . $this->input->post('note', true);
                $email_body .= '<br><br><br>';
                $email_body .= '<b>Summoner:</b> ' . $feedback->summoner_name;
                $email_body .= '<br>';
                $email_body .= '<b>Region:</b> ' . $feedback->region;
                $email_body .= '<br>';
                $email_body .= '<b>Email:</b> ' . $feedback->email;
                $email_body .= '<br>';
                $email_body .= '<b>Feedback ID:</b> ' . $feedback_id;
                $email_body .= '<br>';
                $email_body .= '<b>Browser:</b> ' . $feedback->browser;
                $email_body .= '<br>';
                $email_body .= '<b>OS:</b> ' . $feedback->os;
                $email_body .= '<br>';
                $email_body .= '<b>IP:</b> ' . $feedback->ip;
                $email_body .= '<br>';
                $email_body .= '<b>Referrer:</b> ' . $feedback->referrer;
                $email_body .= '<br>';
                $email_body .= '<b>Feedback Date:</b> ' . $feedback->date_added;

                $result = $this->Request->sendEmailAwsSes(
                    'feedback@lols.gg',
                    ['eyupakman+rppaf3niay3yb32vnchl@boards.trello.com'],
                    "Feedback: " . word_limiter($feedback->message, 5, ''),
                    $email_body);
                if ($result) {
                    $data->status = true;
                    $this->db->where('feedback_id', $feedback_id);
                    $this->db->update('feedback', array(
                        'trello_status' => 1
                    ));
                } else {
                    $data->error = 'Error..';
                }

            }
        }

        echo json_encode($data);
    }

    public function markFeedbackImportant($feedback_id = 0)
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        if ($this->session->userdata('login')) {
            $this->db->where("feedback_id", $feedback_id);
            $feedback_query = $this->db->get("feedback");
            if ($feedback_query->num_rows()) {
                $data->status = true;
                $important_status = (int)$this->input->post('important_status', true);
                $data->important_status = $important_status;
                $this->db->where('feedback_id', $feedback_id);
                $this->db->update('feedback', array(
                    'important_status' => $important_status
                ));
            }
        }

        echo json_encode($data);
    }

    public function addDraft()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        if ($this->session->userdata('login')) {

            $this->form_validation->set_rules('name', 'Name', 'required|min_length[2]|max_length[200]');
            $this->form_validation->set_rules('draft', 'Message', 'required|min_length[3]|max_length[2500]');
            if ($this->form_validation->run() == TRUE) {
                $insert = $this->db->insert('feedback_draft', array(
                    'name' => $this->input->post('name', true),
                    'draft' => $this->input->post('draft', true)
                ));
                if ($insert) {
                    $data->status = true;
                    $data->feedback_draft_id = $this->db->insert_id();
                    $data->draft = quotes_to_entities($this->input->post('draft', true));
                    $data->name = quotes_to_entities($this->input->post('name', true));
                } else {
                    $data->error = 'Hata.';
                }
            } else {
                $data->error = validation_errors(' ', ' ');
            }


        }


        echo json_encode($data);
    }

    public function deleteDraft($feedback_draft_id = 0)
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        if ($this->session->userdata('login')) {
            $this->db->where("feedback_draft_id", $feedback_draft_id);
            $feedback_draft = $this->db->get("feedback_draft");
            if ($feedback_draft->num_rows()) {

                $this->db->where("feedback_draft_id", $feedback_draft_id);
                $delete = $this->db->delete("feedback_draft");
                if ($delete) {
                    $data->status = true;
                    $data->feedback_draft_id = $feedback_draft_id;
                } else {
                    $data->error = 'Error..';
                }

            }
        }

        echo json_encode($data);
    }

}