<?php

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            redirect('Panel/Feedback');
        } else {
            $data = array();
            if ($this->input->post()) {
                if ($this->input->post('username') == 'lolsgg' && $this->input->post('password') == 'panel18711565panel') {
                    $this->session->set_userdata('login', true);
                    redirect('Panel/Feedback');
                } else {
                    $data['error'] = 'Wrong Username or Password';
                }
            }
            $this->load->view('panel/login', $data);
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('login');
        redirect('Panel/Login');
    }
}