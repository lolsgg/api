<?php

class RedisStats extends CI_Controller
{
    public function index()
    {
        $this->load->library('session');
        if ($this->session->userdata('login')) {
            $data = array();
            $data['page_title'] = 'RedisStats';

            $data['cache_info'] = $this->cache->redis->cache_info();

            // $redis = new Redis();
            // $redis->connect($this->config->item('host','redis'), 6379);
            // $allKeys = $redis->keys('*');
            // $this->Request->pr($allKeys);

            $this->load->view('panel/header', $data);
            $this->load->view('panel/redistats', $data);
            $this->load->view('panel/footer', $data);
        } else {
            redirect('Panel/Login');
        }
    }
}