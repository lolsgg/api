<?php

class RiotApiStats extends CI_Controller
{
    public function index()
    {
        $this->load->library('session');
        if ($this->session->userdata('login')) {
            $data = array();
            $data['page_title'] = 'RiotApiStats';
            $logs = array_reverse($this->cache->redis->lRange("lol_request_log", 0, -1));
            $data['logs'] = array();
            $replace = array(
                'api_key=' . $this->config->item('lol_api_key')
            );
            foreach ($logs as $log_) {
                $log_exp = explode("**", $log_);
                $data['logs'][] = array(
                    'req' => trim(str_replace($replace, '', $log_exp[4]), '?&'),
                    'region' => $log_exp[3],
                    'href' => $log_exp[4],
                    'http_code' => $log_exp[2],
                    'exp' => (float)number_format(($log_exp[1] / 60), 2, '.', '') . ' min',
                    'date' => date('H:i:s', $log_exp[0])
                );
            }
            $data['logs'] = array_reverse($data['logs']);
            $this->load->view('panel/header', $data);
            $this->load->view('panel/riotapistats', $data);
            $this->load->view('panel/footer', $data);
        } else {
            redirect('Panel/Login');
        }
    }

    public function resetLogs()
    {
        $this->load->library('session');
        if ($this->session->userdata('login')) {
            $this->cache->redis->save('api_request_log', array(), 7200);
        }
        redirect('Panel/RiotApiStats');
    }
}