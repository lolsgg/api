<?php

class Language extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

    }

    public function test()
    {
        $this->Lang->setLang('tr_TR');
        echo $this->Lang->get('lol', 'Range_');
    }

    public function create()
    {
        set_time_limit(0);
        $lang_google['en_US'] = 'en';
        $lang_google['ko_KR'] = 'ko';
        $lang_google['ja_JP'] = 'ja';
        $lang_google['pl_PL'] = 'pl';
        $lang_google['fr_FR'] = 'fr';
        $lang_google['de_DE'] = 'de';
        $lang_google['es_ES'] = 'es';
        $lang_google['ru_RU'] = 'ru';
        $lang_google['hu_HU'] = 'hu';
        $lang_google['tr_TR'] = 'tr';
        $lang_google['ro_RO'] = 'ro';
        $lang_google['pt_BR'] = 'pt';
        $lang_google['zh_CN'] = 'zh';
        $lang_google['it_IT'] = 'it';
        $lang_google['id_ID'] = 'id';
        $lang_google['ms_MY'] = 'ms';
        $lang_google['th_TH'] = 'th';
        foreach ($lang_google as $long => $short) {
            $this->Lang->createLangFile($long);
            echo $long . ' ok<br>';
        }
    }

    public function lolLangFile()
    {
        $this->lol->setLang("fr");
        $results = $this->lol->getLanguageString();
        echo "<?php" . PHP_EOL . "defined('BASEPATH') OR exit('No direct script access allowed');" . PHP_EOL;
        foreach ($results as $key => $value) {
            echo '$lang["lol_' . $key . '"] = "' . $value . '";' . PHP_EOL;
        }
    }
}