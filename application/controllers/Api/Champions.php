<?php

class Champions extends CI_Controller
{
    private $db_read;

    function __construct()
    {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);

        if (false && rand(0, 1) === 1) {
            $this->db_read = $this->load->database('read', TRUE, TRUE);
        } else {
            $this->db_read = $this->db;
        }
        // $this->Throttle->set('api:global:'.$this->Request->getClientIp(),15,3);
    }


    public function test()
    {
        $_POST['summonerName'] = 'NeroZeds1';
        $_POST['region'] = 'TR';
        $_POST['lang'] = 'en';
        $_POST['championId'] = 34;
        $this->summonerMasteryRanking();
    }

    /**
     * Gets summoner's champions' game details and  masteries
     * @uses CI_Input::post($summonerId) Summoner ID
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($lang) Language
     */
    public function summonerChampions()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->summonerId = $this->input->post('summonerId');
        $request->region = $this->input->post('region');
        $request->lang = $this->input->post('lang');

        if ($request->summonerId && $request->region && $request->lang) {
            $summoner_id = $request->summonerId;
            $region = mb_strtolower($request->region);
            $this->lol->setRegion($region);
            $this->lol->setLang($request->lang);
            //$platform = $this->lol->getPlatform();

            if ($this->Request->isLanguageExist($request->lang)) {
                $this->lang->load('site', mb_strtolower($request->lang));
            } else {
                $this->lang->load('site', "en");
            }

            $data->maintenanceStatus = $maintenance_status = (bool)$this->cache->redis->get('maintenance:champmastery:' . $region);


            $champion_masteries_ = $this->lol->api("lol/champion-mastery/v3/champion-masteries/by-summoner/" . $summoner_id, '', 7200);

            if ($update_champ_mastery_db = (!$this->cache->redis->get('l_cmastery_upd:' . $region . ':' . $summoner_id) AND !$maintenance_status)) {
                $this->cache->redis->save('l_cmastery_upd:' . $region . ':' . $summoner_id, TRUE, 7200);
                $summoner = $this->lol->api("lol/summoner/v3/summoners/" . $summoner_id, '', 86400);
            }

            if ($champion_masteries_ && count($champion_masteries_)) {
                $data->status = true;
                $data->totalMasteryPoints = 0;

                $champ_mastery_updates = array();
                $champ_mastery_update_list = array();
                $champ_mastery_insert_list = array();

                if (!$maintenance_status) {
                    $this->db_read->where(array('summoner_id' => (int)$summoner_id));
                    $ranking_q = $this->db_read->get('champ_mastery_' . $this->lol->getRegion());
                    if ($ranking_q->num_rows()) {
                        $ranking = $ranking_q->result();
                        foreach ($ranking as $row) {
                            $champ_mastery_updates[$row->champion_id] = $row->summoner_champion_id;
                        }
                    }
                }

                foreach ($champion_masteries_ as $champ_) {
                    $champ = $this->lol->getChampionById2($champ_->championId);
                    $champ_->name = isset($champ->name) ? $champ->name : '';
                    $search_time = $this->Request->secondToTime(time() - (int)($champ_->lastPlayTime / 1000));
                    $time_since = $search_time[0] . ' ' . lang('site_' . $search_time[1]);
                    $champ_->lastPlayTimeAgo = isset($champ_->lastPlayTime) ? $time_since : '';
                    $champ_->key = isset($champ->key) ? $champ->key : '';
                    $champ_->tags = isset($champ->tags) ? $champ->tags : '';
                    $champ_->ranking = 0;
                    $champion_masteries[$champ_->championId] = (array)$champ_;
                    $data->totalMasteryPoints += $champ_->championLevel;
                    if (!$maintenance_status && isset($update_champ_mastery_db) && $update_champ_mastery_db) {
                        if (isset($champ_mastery_updates[$champ_->championId])) {
                            if (isset($summoner)) {
                                $champ_mastery_update_list[] = array(
                                    'summoner_champion_id' => $champ_mastery_updates[$champ_->championId],
                                    'summoner_name' => $summoner->name,
                                    'level' => $champ_->championLevel,
                                    'points' => $champ_->championPoints,
                                    'last_played' => date('Y-m-d H:i:s', $champ_->lastPlayTime / 1000),
                                    'date_updated' => date('Y-m-d H:i:s')
                                );
                            }
                        } else {
                            if (isset($summoner)) {
                                $champ_mastery_insert_list[] = array(
                                    'summoner_id' => $summoner_id,
                                    'champion_id' => $champ_->championId,
                                    'summoner_name' => $summoner->name,
                                    'level' => $champ_->championLevel,
                                    'points' => $champ_->championPoints,
                                    'last_played' => date('Y-m-d H:i:s', $champ_->lastPlayTime / 1000)
                                );
                            }
                        }
                    }
                }

                if (!$maintenance_status && isset($ranking) && count($ranking)) {
                    foreach ($ranking as $row) {
                        if (isset($champion_masteries[$row->champion_id])) {
                            $champion_masteries[$row->champion_id]['ranking'] = $row->position > 100000 ? 0 : (int)$row->position;
                        }
                    }
                }

                if (count($champ_mastery_update_list)) {
                    $this->db->update_batch('champ_mastery_' . $region, $champ_mastery_update_list, 'summoner_champion_id');
                }
                if (count($champ_mastery_insert_list)) {
                    $this->db->insert_batch('champ_mastery_' . $region, $champ_mastery_insert_list);
                }

                $data->championsMasteries = false;
                if (isset($champion_masteries)) {
                    $data->championsMasteries = $this->Request->championSort($champion_masteries, 'championLevel', SORT_DESC);
                }
            }


        }
        echo json_encode($data);
    }

    /**
     * Gets champion mastery ranking
     *
     * @param @uses CI_Input::post($region) Region
     * @param @uses CI_Input::post($page) Page
     * @param @uses CI_Input::post($championId) Champion ID
     *
     */

    public function masteryRanking()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;


        if (false) {
            $data->maintenanceStatus = true;
            echo json_encode($data);
            die();
        }

        $request = new stdClass();
        $request->region = $this->input->post('region');
        $request->page = $this->input->post('page');
        $request->championId = $this->input->post('championId');

        if ($request->region) {
            $this->lol->setRegion(mb_strtolower($request->region));
            $region = $this->lol->getRegion();

            $max_page = 150;
            $page = 1;
            $per_page = 40;
            if ($request->page > 1 && $request->page < $max_page) {
                $page = $request->page;
            }

            $champion_id = false;
            if ($request->championId > 0 && $request->championId < 10000) {
                $champion_id = $request->championId;
            }

            $cache_key = 'leaderboard_champ_c1_v4_' . $region . '_' . $page . '_' . (int)$champion_id;
            if ($request->page < 15 && $request->page > 0 && $result = $this->cache->redis->get($cache_key)) {
                echo $result;
                die();
            }


            $data->maintenanceStatus = $maintenance_status = (bool)$this->cache->redis->get('maintenance:champmastery:' . $region);
            if ($maintenance_status == true) {
                echo json_encode($data);
                die();
            }


            $data->lastUpdate = $this->cache->redis->get('leaderboard:champ_mastery:update:' . $region);
            if ($data->lastUpdate) {
                $data->lastUpdate = date('d.m.Y H:i:s', $data->lastUpdate);
            }

            $this->db_read->from('leaderboard_champ_mastery_' . $region);
            if ($champion_id) {
                $this->db_read->where('champion_id', (int)$champion_id);
            }
            $this->db_read->limit($per_page, ($page - 1) * $per_page);
            $this->db_read->order_by('position', 'asc');
            $mastery_query = $this->db_read->get();
            $summoners = array();
            if ($mastery_query->num_rows()) {
                $result = $mastery_query->result();
                foreach ($result as $summoner) {
                    unset($summoner->date_added);

                    $_summoner = new stdClass();
                    if ($summoner->position) {
                        $_summoner->position = $summoner->position;
                        $_summoner->summoner_id = $summoner->summoner_id;
                        $_summoner->summoner_name = $summoner->summoner_name ? $summoner->summoner_name : '';
                        $_summoner->champion_id = $summoner->champion_id;
                        $_summoner->points = $summoner->points ? $summoner->points : 0;
                        $_summoner->level = $summoner->level ? $summoner->level : 0;
                    }


                    $summoners[] = $_summoner;
                }
                $data->status = true;
                $data->summoners = $summoners;

                if ($request->page < 15 && $request->page > 0) {
                    $this->cache->redis->save($cache_key, json_encode($data), 21600);
                }
            }


        }
        echo json_encode($data);
    }

    /**
     * Gets given summoner's champion mastery ranking. If Champion ID not exists or given 0, it gets all champions of the summoner.
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($summonerName) Summoner Name
     * @uses CI_Input::post($championId) Champion ID
     */
    public function summonerMasteryRanking()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->region = $this->input->post('region');
        $request->summonerName = $this->input->post('summonerName');
        $request->championId = $this->input->post('championId');


        if ($request->region) {
            $region = mb_strtolower($request->region);
            $this->lol->setRegion($region);

            $summoner_name = urldecode(trim(str_replace(array(' ', '+'), '', mb_strtolower($request->summonerName))));
            $summoner_search = $this->lol->api("lol/summoner/v3/summoners/by-name/" . $summoner_name, '', 10800);
            if (isset($summoner_search->id)) {
                $summoner = $summoner_search;


                $champion_id = false;
                if ($request->championId > 0 && $request->championId < 10000) {
                    $champion_id = $request->championId;
                }

                $this->db_read->where('summoner_id', (int)$summoner->id);
                $this->db_read->where('champion_id', (int)$champion_id);
                $this->db_read->limit(1);


                $this->db_read->order_by('position', 'asc');
                $ranking_q = $this->db_read->get('leaderboard_champ_mastery_' . $this->lol->getRegion());
                if ($ranking_q->num_rows()) {
                    $data->status = true;
                    $data->region = mb_strtoupper($region);
                    $data->summonerName = $summoner->name;
                    $data->summonerId = $summoner->id;
                    $data->accountId = $summoner->accountId;
                    $data->summonerLevel = $summoner->summonerLevel;
                    $data->profileIconId = $summoner->profileIconId;
                    $ranking = $ranking_q->row();
                    $data->position = (int)$ranking->position;
                }
            }

        }
        echo json_encode($data);
    }
}
