<?php

class Main extends CI_Controller
{
    //private $db_read;
    function __construct()
    {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);
        // $this->Throttle->set('api:global:'.$this->Request->getClientIp(),15,3);
    }

    /**
     * Rastgele şampiyon sözü getirir
     */
    public function championQuote()
    {
        header('Content-Type: application/json');

        $data = new stdClass();
        echo json_encode($data);
        die();
        /*
        if (false && rand(0,1) === 1) {
            $this->db_read = $this->load->database( 'read', TRUE, TRUE );
        } else {
            $this->db_read = $this->db;
        }

        $CI =& get_instance();
        $CI->db_read =& $this->db_read;

		$data->quote = 'Rules are made to be broken... like buildings... and people';
		$data->champion = 'Jinx';
		$data->sound = site_url().'assets/lol/sound/jhin-116.ogg';

		$this->db_read->order_by('champion_quote_id','RANDOM');
		$this->db_read->limit(1);
		$query = $this->db_read->get('champion_quote');
		if ($query->num_rows()) {
			$quote = $query->row();
			$data->quote = $quote->quote;
			$data->sound = 'assets/lol/sound/'.$quote->sound_file;
			$data->champion = $quote->champion_name;
		}
		echo json_encode($data);
		*/
    }

    /**
     * Verilen regiona ait ücretsiz şampiyon rotasyonu getirir
     *
     * @uses CI_Input::post($region) Region
     *
     */
    public function freeChampions()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $this->lol->setRegion("tr");
        $this->lol->setLang("tr");

        $champions_data = $this->lol->api("lol/platform/v3/champions", '&freeToPlay=true', 86400);

        if (isset($champions_data->champions)) {
            $data->status = true;
            $data->champions = array();
            foreach ($champions_data->champions as $_champ) {
                $champ = $this->lol->getChampionById2($_champ->id);

                $champ_data = new StdClass();
                $champ_data->championName = $champ->name;
                $champ_data->championKey = $champ->key;
                $champ_data->championId = $champ->id;

                $data->champions[] = $champ_data;
            }
        }

        echo json_encode($data);
    }

    /**
     * Verilen sihirdar adına ait sihirdar ID bilgisini getirir
     *
     * @uses CI_Input::post($summonerName) Summoner name
     * @uses CI_Input::post($region) Region
     *
     */
    public function summonerId()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->summonerName = $this->input->post("summonerName");
        $request->region = $this->input->post("region");

        if (isset($request->summonerName) && isset($request->region)) {
            $summoner_name = urldecode(trim(str_replace(array(' ', '+'), '', mb_strtolower($request->summonerName))));
            $region = mb_strtolower($request->region);
            $this->lol->setRegion($region);
            $this->lol->setLang('tr_TR');
            $summoner_search = $this->lol->api("lol/summoner/v3/summoners/by-name/" . $summoner_name, '', 10800);
            if (isset($summoner_search->id)) {
                $summoner = $summoner_search;
                $data->status = true;
                $data->region = mb_strtoupper($region);
                $data->summonerName = $summoner->name;
                $data->summonerId = $summoner->id;
                $data->accountId = $summoner->accountId;
                $data->summonerLevel = $summoner->summonerLevel;
                $data->profileIconId = $summoner->profileIconId;
            }
        }

        echo json_encode($data);
    }

    /**
     * Verilen bilgileri veritabanına kaydeder
     *
     * @uses CI_Input::post($email) User E-Mail
     * @uses CI_Input::post($message) Message
     * @uses CI_Input::post($summonername) Summoner Name
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($captcha) reCapcha code
     *
     */
    public function sendFeedback()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->email = $this->input->post("email", true);
        $request->message = $this->input->post("message", true);
        $request->summonername = $this->input->post("summonername", true);
        $request->region = $this->input->post("region", true);
        $request->captcha = $this->input->post("captcha");

        if ($request->email && $request->message && $request->captcha) {
            $this->load->library('user_agent');
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'E-Mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('message', 'Message', 'trim|required|min_length[5]|max_length[5000]');
            if ($this->form_validation->run() !== FALSE) {
                if ($this->Request->reCaptchaCheck($request->captcha)) {

                    $insert = $this->db->insert("feedback", array(
                        'email' => $request->email,
                        'region' => $request->region,
                        'summoner_name' => $request->summonername,
                        'message' => $request->message,
                        'browser' => $this->agent->agent_string(),
                        'referrer' => $this->agent->referrer(),
                        'ip' => $this->Request->getClientIp()
                    ));
                    if ($insert) {
                        $data->status = true;

                        $email_body = '<b>Region</b><br>' . $request->region . '<br><br>';
                        $email_body .= '<b>Summoner Name</b><br>' . $request->summonername . '<br><br>';
                        $email_body .= '<b>Message</b><br>' . $request->message . '<br>';
                        $this->Request->sendEmailAwsSes(
                            'feedback@lols.gg',
                            ['ahmetmobile@gmail.com'],
                            "Lols.gg Feedback",
                            $email_body);
                    }
                }
            }
        }

        echo json_encode($data);
    }

    /**
     * Verilen hash koduna göre şampiyonun live maç kartını getirir
     *
     * @uses CI_Input::post($hash) Hash
     *
     */
    public function streamer()
    {
        if (true) {
            die();
        }
        $this->load->model('matchhistory');
        $hash_list = array(
            "re4df2" => array('region' => 'tr', 'summoner_name' => 'Zeonnn'),
            "gfr43v" => array('region' => 'tr', 'summoner_name' => 'lllllllIllllll'),
            "y53g3r" => array('region' => 'euw', 'summoner_name' => 'Nnnoez'),
            "y32fe2" => array('region' => 'tr', 'summoner_name' => 'Bestvayne'),
            "s53ggc" => array('region' => 'tr', 'summoner_name' => 'Zeonnnv2'),
            "rs4gh3" => array('region' => 'tr', 'summoner_name' => 'Zeonnnisthatu'),
            "lv1gh2" => array('region' => 'euw', 'summoner_name' => 'GOD TIER LEVO'),
            'rre53r' => array('region' => 'na', 'summoner_name' => 'PunishedDestiny')
        );
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->hash = $this->input->post("hash");

        if ($request->hash && isset($hash_list[$request->hash])) {
            $region = $hash_list[$request->hash]['region'];
            $summoner_name = $hash_list[$request->hash]['summoner_name'];
            $summoner_name = urldecode(trim(str_replace(array(' ', '+'), '', mb_strtolower($summoner_name))));
            $this->lol->setRegion($region);
            $this->lol->setLang('tr_TR');
            $summoner_search = $this->lol->api("lol/summoner/v3/summoners/by-name/" . $summoner_name, '', 86400);
            if (isset($summoner_search->id)) {
                $summoner = $summoner_search;
                $data->status = true;
                $data->summonerName = $summoner->name;
                $data->summonerId = $summoner->id;
                $data->region = $region;
                $summoner_id = $summoner->id;
                $account_id = $summoner->accountId;
                $champion_masteries_ = $this->lol->api("lol/champion-mastery/v3/champion-masteries/by-summoner/" . $summoner_id, '', 3600);
                if (count($champion_masteries_)) {
                    $champs = array();
                    foreach ($champion_masteries_ as $champ_) {
                        $champ = $this->lol->getChampionById2($champ_->championId);
                        $champs[] = array(
                            'key' => $champ->key,
                            'level' => $champ_->championLevel,
                            'points' => $this->Request->pointsToK($champ_->championPoints)
                        );
                    }
                    $data->champs = array_slice($champs, 0, 3);
                }

                $league_data = $this->lol->api("lol/league/v3/leagues/by-summoner/" . $summoner_id, '', 1800);
                if ($league_data) {
                    $data->league = new stdClass();

                    foreach ($league_data as $league) {
                        if ($league->queue == 'RANKED_SOLO_5x5') {
                            $data->league->tier = $league->tier;
                            foreach ($league->entries as $entry) {
                                if ($entry->playerOrTeamId == $summoner_id) {
                                    $data->league->division = $entry->rank;
                                    $data->league->leaguePoints = !(isset($entry->miniSeries)) ? $entry->leaguePoints : str_replace(array('W', 'L', 'N'), array('✔', '✖', '–'), $entry->miniSeries->progress);
                                    $data->league->leagueTD = $this->lol->divisionTierCombine($entry->rank, $league->tier);
                                    $data->league->wins = $entry->wins;
                                    $data->league->losses = $entry->losses;
                                    $data->league->winRatio = 0;
                                    if ($entry->wins > 0) {
                                        $data->league->winRatio = number_format(100 / ($entry->wins + $entry->losses) * $entry->wins, 2, '.', '');
                                    }
                                }
                            }
                        }
                    }
                }

                $update = $this->lol->updateSummonerMatches($account_id);
                if ($update) {
                    $matches = $this->lol->getSummonerMatches($account_id, 20, 0, false);
                    if ($matches) {
                        $played = 0;
                        $kills = 0;
                        $deaths = 0;
                        $assists = 0;
                        foreach ($matches as $match_) {
                            if (isset($match_->match)) {
                                $match = $match_->match;
                                foreach ($match->participants as $participant) {
                                    if (isset($participant->identity) && $participant->identity->summonerId == $summoner_id && isset($participant->stats)) {
                                        $played++;
                                        $kills += $participant->stats->kills;
                                        $deaths += $participant->stats->deaths;
                                        $assists += $participant->stats->assists;
                                    }
                                }
                            }
                        }
                        $data->kills = $kills / $played;
                        $data->deaths = $deaths / $played;
                        $data->assists = $assists / $played;
                        $data->kdaRatio = $data->kills + $data->deaths;
                        if ($data->deaths > 0) {
                            $data->kdaRatio = $data->kdaRatio / $data->deaths;
                        }
                        $data->kills = number_format($data->kills, 1, '.', '');
                        $data->deaths = number_format($data->deaths, 1, '.', '');
                        $data->assists = number_format($data->assists, 1, '.', '');
                        $data->kdaRatio = number_format($data->kdaRatio, 1, '.', '');
                    }
                }
            }
        }

        echo json_encode($data);
    }
}
