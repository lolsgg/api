<?php

class Language extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Gets strings of the given language
     * @uses CI_Input::get($lang_code)
     * @param $lang_code
     */
    public function get($lang_code)
    {
        header('Content-Type: application/json');
        if (!file_exists(APPPATH . "language/" . $lang_code . "/" . 'site_lang.php')) {
            $lang_code = 'en';
        }
        $lang_code = $this->lol->languageCheckShort($lang_code);
        $result = new stdClass();
        $language_orj = $this->lang->load('site', 'en', true);
        $language = $this->lang->load('site', $lang_code, true);
        foreach ($language_orj as $key => $value) {
            $result->{substr($key, 5)} = $value;
        }
        foreach ($language as $key => $value) {
            $result->{substr($key, 5)} = $value;
        }

        echo json_encode($result);
    }
}
