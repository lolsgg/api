<?php

class Build extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Build_model');
    }

    /**
     * Gets a champion's items builds
     * @uses CI_Input::post(championId) Champion ID
     * @uses CI_Input::post(lang) Language
     */
    public function items()
    {
        header('Content-Type: application/json');
        $champion_id = (int)$this->input->post('championId');
        $lang = $this->input->post('lang');

        $build_version = 1528182845;

        if ($lang && $this->Request->isLanguageExist($lang)) {
            $this->lol->setLang($lang);
        } else {
            $this->lol->setLang("en");
        }

        $data = new stdClass();
        $data->status = false;
        $data->results = array();

        if ($champion_id && $champion_id > 0 && $champion_id < 99999) {

            $this->lol->setRegion("TR");

            $lanes = $this->Build_model->getChampionLanes($build_version, $champion_id);

            foreach ($lanes as $lane => $ratio) {
                $data->results[$lane] = $this->Build_model->items($build_version, $champion_id, $lane);
                $data->results[$lane]['ratio'] = number_format($ratio, 2);
            }

            if (count($data->results)) {
                $data->status = true;
            }
        }

        echo json_encode($data);
    }

    /**
     * Gets a champion's runes builds
     * @uses CI_Input::post(championId) Champion ID
     * @uses CI_Input::post(lang) Language
     */
    public function runes()
    {
        header('Content-Type: application/json');
        $champion_id = (int)$this->input->post('championId');
        $lang = $this->input->post('lang');

        $build_version = 1528182845;

        if ($lang && $this->Request->isLanguageExist($lang)) {
            $this->lol->setLang($lang);
        } else {
            $this->lol->setLang("en");
        }



        $data = new stdClass();
        $data->status = false;
        $data->results = array();

        if ($champion_id && $champion_id > 0 && $champion_id < 99999) {
            $this->lol->setRegion("TR");

            $lanes = $this->Build_model->getChampionLanes($build_version, $champion_id);

            foreach ($lanes as $lane => $ratio) {
                $data->results[$lane] = $this->Build_model->runes($build_version, $champion_id, $lane);
                $data->results[$lane]['ratio'] = number_format($ratio, 2);
            }

            if (count($data->results)) {
                $data->status = true;
            }
        }

        echo json_encode($data);
    }

    /**
     * Gets a champion's runes builds
     * @uses CI_Input::post(championId) Champion ID
     * @uses CI_Input::post(lang) Language
     */
    public function spells()
    {
        header('Content-Type: application/json');
        $champion_id = (int)$this->input->post('championId');
        $lang = $this->input->post('lang');

        $build_version = 1528182845;

        if ($lang && $this->Request->isLanguageExist($lang)) {
            $this->lol->setLang($lang);
        } else {
            $this->lol->setLang("en");
        }

        $data = new stdClass();
        $data->status = false;
        $data->results = array();

        if ($champion_id && $champion_id > 0 && $champion_id < 99999) {


            $this->lol->setRegion("TR");

            $lanes = $this->Build_model->getChampionLanes($build_version, $champion_id);

            foreach ($lanes as $lane => $ratio) {
                $data->results[$lane] = $this->Build_model->spells($build_version, $champion_id, $lane);
                $data->results[$lane]['ratio'] = number_format($ratio, 2);
            }

            if (count($data->results)) {
                $data->status = true;
            }

        }

        echo json_encode($data);
    }

    /**
     * Gets a champion's items, runes and spells builds
     * @uses CI_Input::post(championId) Champion ID
     * @uses CI_Input::post(lang) Language
     */
    public function all()
    {
        header('Content-Type: application/json');
        $champion_id = (int)$this->input->post('championId');
        $lang = $this->input->post('lang');

        $build_version = 1528182845;

        if ($lang && $this->Request->isLanguageExist($lang)) {
            $this->lol->setLang($lang);
            $this->lang->load('lol', mb_strtolower($lang));
            $this->lang->load('site', mb_strtolower($lang));

        } else {
            $this->lol->setLang("en");
            $this->lang->load('lol', "en");
            $this->lang->load('site', "en");
        }

        $data = new stdClass();
        $data->status = false;
        $data->results = array();

        if ($champion_id && $champion_id > 0 && $champion_id < 99999) {
            $this->lol->setRegion("TR");


            $champ = $this->lol->getChampionById2($champion_id);

            $data->champion = new stdClass();

            $data->champion->id = $champion_id;
            $data->champion->name = $champ->name;

            $data->champion->info = array();
            $data->champion->info[0] = array('name' => lang('lol_statAttack'), 'value' => $champ->info->attack);
            $data->champion->info[1] = array('name' => lang('lol_statDefense'), 'value' => $champ->info->defense);
            $data->champion->info[2] = array('name' => lang('lol_statDifficulty'), 'value' => $champ->info->difficulty);
            $data->champion->info[3] = array('name' => lang('lol_Magic'), 'value' => $champ->info->magic);

            $data->champion->tags = array();
            foreach ($champ->tags as $champ_tag) {
                $data->champion->tags[] = lang('lol_' . $champ_tag);
            }

            $chstat = $champ->stats;
            $chattackspeed = number_format((0.625 / (1 + $chstat->attackspeedoffset)), 3, '.', '.');

            $per_level_lang = lang('site_per_level');
            $spell_keys = array(0 => 'Q', 1 => 'W', 2 => 'E', 3 => 'R');

            if (isset($champ->spells)) {
                foreach ($champ->spells as $spell_key => $spell) {
                    $champ->spells[$spell_key]->key = $spell_keys[$spell_key];
                }
            }

            $data->champion->stats = array();
            $data->champion->stats[] = array('key' => 'armor', 'name' => lang('lol_Armor'), 'description' => $chstat->armor . ' (+' . $chstat->armorperlevel . ' ' . $per_level_lang . ')');
            $data->champion->stats[] = array('key' => 'attackdamage', 'name' => lang('lol_Damage'), 'description' => $chstat->attackdamage . ' (+' . $chstat->attackdamageperlevel . ' ' . $per_level_lang . ')');
            $data->champion->stats[] = array('key' => 'attackspeed', 'name' => lang('lol_AttackSpeed'), 'description' => $chattackspeed . ' (+' . $chstat->attackspeedperlevel . '% ' . $per_level_lang . ')');
            $data->champion->stats[] = array('key' => 'hp', 'name' => lang('lol_Health'), 'description' => $chstat->hp . ' (+' . $chstat->hpperlevel . ' ' . $per_level_lang . ')');
            $data->champion->stats[] = array('key' => 'hpregen', 'name' => lang('lol_HealthRegen'), 'description' => $chstat->hpregen . ' (+' . $chstat->hpregenperlevel . ' ' . $per_level_lang . ')');
            $data->champion->stats[] = array('key' => 'spellblock', 'name' => lang('lol_SpellBlock'), 'description' => $chstat->spellblock . ' (+' . $chstat->spellblockperlevel . ' ' . $per_level_lang . ')');
            $data->champion->stats[] = array('key' => 'mp', 'name' => lang('lol_Mana'), 'description' => $chstat->mp . ' (+' . $chstat->mpperlevel . ' ' . $per_level_lang . ')');
            $data->champion->stats[] = array('key' => 'mpregen', 'name' => lang('lol_ManaRegen'), 'description' => $chstat->mpregen . ' (+' . $chstat->mpregenperlevel . ' ' . $per_level_lang . ')');
            $data->champion->stats[] = array('key' => 'movespeed', 'name' => lang('lol_Movement'), 'description' => $chstat->movespeed);

            $data->champion->tips = new stdClass();
            $data->champion->tips->langAlly = lang('site_as_ally');
            $data->champion->tips->langEnemy = lang('site_as_enemy');
            $data->champion->tips->ally = array();
            $data->champion->tips->enemy = array();
            if (isset($champ->allytips) && count($champ->allytips) > 0) {
                $data->champion->tips->ally = array();
                foreach ($champ->allytips as $allytip) {
                    $allytip_ = $allytip;
                    foreach ($champ->spells as $spell) {
                        $allytip_ = str_replace($spell->name, $spell->name . '(' . $spell->key . ')', $allytip_);

                    }
                    $data->champion->tips->ally[] = $allytip_;
                }
            }

            if (isset($champ->enemytips) && count($champ->enemytips) > 0) {
                $data->champion->tips->enemy = array();
                foreach ($champ->enemytips as $enemytip) {
                    $enemytip_ = $enemytip;
                    foreach ($champ->spells as $spell) {
                        $enemytip_ = str_replace($spell->name, $spell->name . '(' . $spell->key . ')', $enemytip_);
                    }
                    $data->champion->tips->enemy[] = $enemytip_;
                }
            }

            $lanes = $this->Build_model->getChampionLanes($build_version, $champion_id);

            $orders = array('TOP' => 0, 'JUNGLE' => 1, 'MIDDLE' => 2, 'BOTTOM' => 3);
            $lower_names = array('TOP' => 'top', 'JUNGLE' => 'jungle', 'MIDDLE' => 'middle', 'BOTTOM' => 'bot');
            $max = -1;
            $max_lane = '';

            foreach ($lanes as $lane => $ratio) {
                if (isset($orders[$lane])) {
                    $data->results[$lane] = array();
                    $data->results[$lane]['items'] = $this->Build_model->items($build_version, $champion_id, $lane);
                    $data->results[$lane]['runes'] = $this->Build_model->runes($build_version, $champion_id, $lane);
                    $data->results[$lane]['spells'] = $this->Build_model->spells($build_version, $champion_id, $lane);
                    $data->results[$lane]['ratio'] = number_format($ratio, 2);
                    $data->results[$lane]['order'] = $orders[$lane];
                    $data->results[$lane]['lower'] = $lower_names[$lane];
                    $data->results[$lane]['active'] = false;
                    if ($max < $ratio) {
                        $max = $ratio;
                        $max_lane = $lane;
                    }
                }
            }
            $data->results[$max_lane]['active'] = true;

            if (count($data->results)) {
                $data->status = true;
            }

        }

        echo json_encode($data);
    }

    public function debug_items($champion_id = 1)
    {
        $build_version = 1528182845;

        $this->lol->setRegion("TR");
        $this->lol->setLang('tr');
        $champions = $this->lol->getChampions();

        $cdn_url = 'https://code.jquery.com/jquery-3.2.1.min.js';
        echo '<head><style>body{font-family: Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;} html{overflow-y:scroll;}</style>';
        echo '<script src="' . $cdn_url . '"></script>';
        echo '</head>';
        echo '<body>';

        echo '<div style="position: absolute; right: 10px; top:15px;">';

        echo '<select id="champion_select" style="margin-right:10px;font-size:1.5em;" onchange="window.location=\'' . base_url() . 'Api/Build/debug_items/\'+this.value">';
        foreach ($champions as $champion) {
            echo '<option value="' . $champion->key . '" ' . ($champion->key == $champion_id ? 'selected="selected"' : '') . '>' . $champion->name . '</option>';
        }

        echo '</select>';
        echo '</div>';

        echo '<div style="position: absolute; right: 20px; top:50px;">';


        echo '<a onclick="$(\'#champion_select > option:selected\').prev().prop(\'selected\', true);$(\'#champion_select\').change();" href="#" style="font-size:2em;">prev</a>';
        echo '<a onclick="$(\'#champion_select > option:selected\').next().prop(\'selected\', true);$(\'#champion_select\').change();" href="#" style="font-size:2em;margin-left:10px;">next</a>';

        echo '<div style="font-size:1.5em; margin-top:10px;text-align:right;">';
        echo '<a href="' . site_url('Api/Build/debug_spells/' . $champion_id) . '">Spells</a> ';
        echo '<a href="' . site_url('Api/Build/debug_runes/' . $champion_id) . '">Runes</a>';
        echo '</div>';

        echo '<div style="font-size:1.5em; margin-top:10px;text-align:right;color: darkblue; cursor: pointer;" onclick="$(\'.details\').toggle();">Toggle</div>';
        echo '</div>';


        echo '<div style="border:1px solid black; width:800px; margin:10px; padding:15px;float: left;">';
        $this->Build_model->items($build_version, $champion_id, 'TOP', true);
        echo '</div>';
        echo '<div style="border:1px solid black; width:800px; margin:10px; padding:15px;float: left;">';
        $this->Build_model->items($build_version, $champion_id, 'MIDDLE', true);
        echo '</div>';
        echo '<div style="clear:left;"></div>';
        echo '<div style="border:1px solid black; width:800px; margin:10px; padding:15px;float: left;">';
        $this->Build_model->items($build_version, $champion_id, 'BOTTOM', true);
        echo '</div>';
        echo '<div style="border:1px solid black; width:800px; margin:10px; padding:15px;float: left;">';
        $this->Build_model->items($build_version, $champion_id, 'JUNGLE', true);

        echo '</div>';
        echo '<div style="clear:left;"></div>';
        echo '</body>';
    }

    public function debug_runes($champion_id)
    {
        $build_version = 1528182845;

        $this->lol->setRegion("TR");
        $this->lol->setLang('tr');
        $champions = $this->lol->getChampions();

        $cdn_url = 'https://code.jquery.com/jquery-3.2.1.min.js';
        echo '<head><style>body{font-family: Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif; background-color: #ffffff;} html{overflow-y:scroll;}</style>';
        echo '<script src="' . $cdn_url . '"></script>';
        echo '</head>';
        echo '<body>';

        echo '<div style="position: absolute; right: 10px; top:15px;">';

        echo '<select id="champion_select" style="margin-right:10px;font-size:1.5em;" onchange="window.location=\'' . base_url() . 'Api/Build/debug_runes/\'+this.value">';
        foreach ($champions as $champion) {
            echo '<option value="' . $champion->key . '" ' . ($champion->key == $champion_id ? 'selected="selected"' : '') . '>' . $champion->name . '</option>';
        }

        echo '</select>';
        echo '</div>';

        echo '<div style="position: absolute; right: 20px; top:50px;">';


        echo '<a onclick="$(\'#champion_select > option:selected\').prev().prop(\'selected\', true);$(\'#champion_select\').change();" href="#" style="font-size:2em;">prev</a>';
        echo '<a onclick="$(\'#champion_select > option:selected\').next().prop(\'selected\', true);$(\'#champion_select\').change();" href="#" style="font-size:2em;margin-left:10px;">next</a>';

        echo '<div style="font-size:1.5em; margin-top:10px;text-align:right;">';
        echo '<a href="' . site_url('Api/Build/debug_items/' . $champion_id) . '">Items</a> ';
        echo '<a href="' . site_url('Api/Build/debug_spells/' . $champion_id) . '">Spells</a>';
        echo '</div>';

        echo '</div>';


        echo '<div style="width:300px; margin:10px; padding:15px;float: left; background-color: lightgray;">';
        $this->Build_model->runes($build_version, $champion_id, 'TOP', true);
        echo '</div>';
        echo '<div style="width:300px; margin:10px; padding:15px;float: left; background-color: lightgray;">';
        $this->Build_model->runes($build_version, $champion_id, 'MIDDLE', true);
        echo '</div>';
        echo '<div style="width:300px; margin:10px; padding:15px;float: left; background-color: lightgray;">';
        $this->Build_model->runes($build_version, $champion_id, 'BOTTOM', true);
        echo '</div>';
        echo '<div style="width:300px; margin:10px; padding:15px;float: left; background-color: lightgray;">';
        $this->Build_model->runes($build_version, $champion_id, 'JUNGLE', true);
        echo '</div>';

        echo '<div style="clear:left;"></div>';
        echo '</body>';
    }

    public function debug_spells($champion_id)
    {
        $build_version = 1528182845;

        $this->lol->setRegion("TR");
        $this->lol->setLang('tr');
        $champions = $this->lol->getChampions();

        $cdn_url = 'https://code.jquery.com/jquery-3.2.1.min.js';
        echo '<head><style>body{font-family: Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif; background-color: #ffffff;} html{overflow-y:scroll;}</style>';
        echo '<script src="' . $cdn_url . '"></script>';
        echo '</head>';
        echo '<body>';

        echo '<div style="position: absolute; right: 10px; top:15px;">';

        echo '<select id="champion_select" style="margin-right:10px;font-size:1.5em;" onchange="window.location=\'' . base_url() . 'Api/Build/debug_spells/\'+this.value">';
        foreach ($champions as $champion) {
            echo '<option value="' . $champion->key . '" ' . ($champion->key == $champion_id ? 'selected="selected"' : '') . '>' . $champion->name . '</option>';
        }

        echo '</select>';
        echo '</div>';

        echo '<div style="position: absolute; right: 20px; top:50px;">';


        echo '<a onclick="$(\'#champion_select > option:selected\').prev().prop(\'selected\', true);$(\'#champion_select\').change();" href="#" style="font-size:2em;">prev</a>';
        echo '<a onclick="$(\'#champion_select > option:selected\').next().prop(\'selected\', true);$(\'#champion_select\').change();" href="#" style="font-size:2em;margin-left:10px;">next</a>';

        echo '<div style="font-size:1.5em; margin-top:10px;text-align:right;">';
        echo '<a href="' . site_url('Api/Build/debug_items/' . $champion_id) . '">Items</a> ';
        echo '<a href="' . site_url('Api/Build/debug_runes/' . $champion_id) . '">Runes</a>';
        echo '</div>';


        echo '</div>';


        echo '<div style="width:300px; margin:10px; padding:15px;float: left; background-color: lightgray;">';
        $this->Build_model->spells($build_version, $champion_id, 'TOP', true);
        echo '</div>';
        echo '<div style="width:300px; margin:10px; padding:15px;float: left; background-color: lightgray;">';
        $this->Build_model->spells($build_version, $champion_id, 'MIDDLE', true);
        echo '</div>';
        echo '<div style="width:300px; margin:10px; padding:15px;float: left; background-color: lightgray;">';
        $this->Build_model->spells($build_version, $champion_id, 'BOTTOM', true);
        echo '</div>';
        echo '<div style="width:300px; margin:10px; padding:15px;float: left; background-color: lightgray;">';
        $this->Build_model->spells($build_version, $champion_id, 'JUNGLE', true);
        echo '</div>';

        echo '<div style="clear:left;"></div>';
        echo '</body>';
    }

    public function make($region = 'tr', $limit = 10)
    {
        $this->Build_model->make($region, $limit);
    }

    public function championLanes()
    {
        $lanes = $this->Build_model->getChampionLanes(1528182845, 157);
        echo "<pre>";
        print_r($lanes);
        echo "</pre>";
    }

    public function test()
    {
        $this->lol->setRegion("TR");
        $this->lol->setLang('tr');
        $items_lol = $this->lol->getItems();

        foreach ($items_lol as $item_id => $item) {
            if (isset($item->requiredAlly) && 'Ornn' == $item->requiredAlly) {
                //echo '<img style="width: 30px;height:30px;padding:3px;" src="https://media.lols.gg/img/item/' . $item_id . '.png"/>';
                //echo $item->name;
                //echo '<br>';
                echo 'array(' . $item_id . ', ' . $item->from[0] . '),';
                echo '<br>';
            }

        }
    }

    public function laneTest()
    {
        ini_set('memory_limit', '500M');
        $region = 'TR';
        $account_id = 941667;//200155286;

        $build_version = 1528182845;

        $data = array();

        $this->lol->setRegion($region);
        $this->lol->setLang('tr');

        /*echo "<pre>";
        print_r($this->lol->getSummonerSpells());
        echo "</pre>";
        die();*/

        $ok_count = 0;


        $results = $this->Build_model->getLaneMatches($account_id);
        $teams = array();
        foreach ($results as $game_id => $result) {
            $match = $result->match;
            $teams_tmp = array(100 => array(), 200 => array());
            foreach ($match->participants as $participant) {
                $participant->queueId = $match->queueId;
                $teams_tmp[$participant->teamId][] = $participant;
            }
            $teams[] = $teams_tmp[100];
            $teams[] = $teams_tmp[200];
        }


        foreach ($teams as &$team) {
            $jungle_count = 0;
            $sup_count = 0;
            $adc_count = 0;
            $top_count = 0;
            $mid_count = 0;
            $exhaust_count = 0;
            $only_sup_count = 0; // %95

            $jungle_cursor = 0;
            $exhaust_cursor = 0;
            $only_sup_cursor = 0;

            $adc_cursors = array();
            $top_cursors = array();
            $mid_cursors = array();
            $sup_cursors = array();


            foreach ($team as &$champion) {

                /*echo "<pre>";
                print_r($champion);
                echo "</pre>";
                die();*/
                $champ = $this->lol->getChampionById2($champion->championId);
                $lanes = $this->Build_model->getChampionLanes($build_version, $champion->championId);

                /*echo "<pre>";
                print_r($lanes);
                echo "</pre>";
                die();*/

                $champion->lanes = $lanes;

                if ($champion->spell1Id == 11 || $champion->spell2Id == 11) { // Kural 1
                    $jungle_count++;
                    $jungle_cursor = &$champion;
                } else if (isset($lanes['BOTTOM']) && $lanes['BOTTOM'] > 60 && isset($champ->tags) && in_array('Marksman', $champ->tags)) { // Kural 2
                    $champion->bottom_ratio = $lanes['BOTTOM'];
                    $adc_count++;
                    $adc_cursors[] = &$champion;
                }

                if (isset($lanes['TOP']) && $lanes['TOP'] > 25) {
                    $top_count++;
                    $top_cursors[] = &$champion;
                }
                if (isset($lanes['MIDDLE']) && $lanes['MIDDLE'] > 25) {
                    $mid_count++;
                    $mid_cursors[] = &$champion;
                }
                if (isset($lanes['BOTTOM']) && $lanes['BOTTOM'] > 25) {
                    $sup_count++;
                    $sup_cursors[] = &$champion;
                    if ($lanes['BOTTOM'] > 95) {
                        $only_sup_count++;
                        $only_sup_cursor = &$champion;
                    }
                }

                if ($champion->spell1Id == 3 || $champion->spell2Id == 3) {  // Exhaust
                    $exhaust_count++;
                    $exhaust_cursor = &$champion;
                }


                $champion->champDetails = $champ;
                $champion->spell1 = $this->lol->getSpellById($champion->spell1Id);
                $champion->spell2 = $this->lol->getSpellById($champion->spell2Id);
                $champion->position = '';
                $champion->status = '';
                $champion->test = '';
                $champion->point = 0;

                $champion->game_position = $champion->timeline->lane . ' ' . $champion->timeline->role;
            }
            if ($jungle_count == 1) { // Kural 1
                $jungle_cursor->position = 'JUNGLE';
                $jungle_cursor->point = 1;
            }
            unset($jungle_cursor);
            if ($adc_count == 1) { // Kural 2
                $adc_cursors[0]->position = 'ADC';
                $adc_cursors[0]->point = 2;
            } else if ($adc_count == 2) {
                if ($adc_cursors[0]->spell1Id == 12 || $adc_cursors[0]->spell2Id == 12) { // Kural 3
                    $adc_cursors[0]->position = "TOP";
                    $adc_cursors[1]->position = "ADC";
                    $adc_cursors[0]->point = 3;
                    $adc_cursors[1]->point = 3;
                } else if ($adc_cursors[1]->spell1Id == 12 || $adc_cursors[1]->spell2Id == 12) { // Kural 3
                    $adc_cursors[0]->position = "ADC";
                    $adc_cursors[1]->position = "TOP";
                    $adc_cursors[0]->point = 4;
                    $adc_cursors[1]->point = 4;
                } else {
                    if ($adc_cursors[0]->spell1Id == 7 || $adc_cursors[0]->spell2Id == 7) { // Kural 4
                        $adc_cursors[0]->position = "ADC";
                        $adc_cursors[0]->point = 5;
                    } else if ($adc_cursors[1]->spell1Id == 7 || $adc_cursors[1]->spell2Id == 7) { // Kural 4
                        $adc_cursors[1]->position = "ADC";
                        $adc_cursors[1]->point = 6;
                    } else {
                        if ($adc_cursors[0]->bottom_ratio > $adc_cursors[1]->bottom_ratio) { // Kural 5
                            $adc_cursors[0]->position = "ADC";
                            $adc_cursors[0]->point = 7;
                        } else { // Kural 5
                            $adc_cursors[1]->position = "ADC";
                            $adc_cursors[1]->point = 8;
                        }
                    }
                }
            }
            unset($adc_cursors);


            if ($top_count > 0) { // Kural 6
                $tmp_teleport_count = 0;
                $tmp_top_cursor = false;
                $best_top_cursor = false;
                $best_top_ratio = 0;
                foreach ($top_cursors as &$top_cursor) {
                    if ($top_cursor->spell1Id == 12 || $top_cursor->spell2Id == 12) { // Kural 6
                        $tmp_teleport_count++;
                        $tmp_top_cursor = &$top_cursor;
                    }
                    if (isset($top_cursor->lanes['TOP']) && $top_cursor->lanes['TOP'] > 25 && $top_cursor->lanes['TOP'] > $best_top_ratio) {
                        $best_top_ratio = $top_cursor->lanes['TOP'];
                        $best_top_cursor = &$top_cursor;
                    }
                }
                if ($tmp_teleport_count == 1 && $mid_count > 0) { // Kural 6
                    if (!$tmp_top_cursor->position) {
                        $tmp_top_cursor->position = "TOP";
                        $tmp_top_cursor->point = 9;
                    }
                }
                unset($tmp_top_cursor);
                unset($tmp_teleport_count);
            }

            if ($sup_count == 1) { // Kural 8
                if (!$sup_cursors[0]->position) {
                    $sup_cursors[0]->position = "SUPPORT";
                    $sup_cursors[0]->point = 10;
                }
            } else if ($sup_count > 0) {
                $tmp_exhausut_count = 0;
                $tmp_sup_cursor = false;
                foreach ($sup_cursors as &$sup_cursor) {
                    if ($sup_cursor->spell1Id == 3 || $sup_cursor->spell2Id == 3) { // Kural 7
                        $tmp_exhausut_count++;
                        $tmp_sup_cursor = &$sup_cursor;
                    }
                }
                if ($tmp_exhausut_count) { // Kural 7
                    if (!$tmp_sup_cursor->position) {
                        $tmp_sup_cursor->position = "SUPPORT";
                        $tmp_sup_cursor->point = 11;
                    }
                }
                unset($tmp_exhausut_count);
                unset($tmp_sup_cursor);
            } else if ($sup_count == 0 && $exhaust_count == 1) { // Kural 9
                if (!$exhaust_cursor->position) {
                    $exhaust_cursor->position = "SUPPORT";
                    $exhaust_cursor->point = 12;
                }

            }

            if ($only_sup_count == 1) { // Kural 10
                if (!$only_sup_cursor->position) {
                    $only_sup_cursor->position = "SUPPORT";
                    $only_sup_cursor->point = 13;
                }
            }

            if ($mid_count > 0) {
                $tmp_exhausut_count = 0;
                $tmp_mid_cursor = false;
                $tmp_best_mid_cursor = false;
                foreach ($mid_cursors as &$mid_cursor) {
                    if ($mid_count > 1) {
                        if ($mid_cursor->spell1Id == 3 || $mid_cursor->spell2Id == 3) { // Kural 11
                            $tmp_exhausut_count++;
                            $tmp_mid_cursor = &$mid_cursor;
                        }
                    }
                    if ($tmp_best_mid_cursor === false || (isset($tmp_best_mid_cursor->midRatio) && $tmp_best_mid_cursor->midRatio < $mid_cursor->midRatio)) { // Kural 12
                        $tmp_best_mid_cursor = &$mid_cursor;

                    }
                }
                if ($tmp_exhausut_count) { // Kural 11
                    if (!$tmp_mid_cursor->position) {
                        $tmp_mid_cursor->position = "SUPPORT";
                        $tmp_mid_cursor->point = 14;
                    }
                }

                if ($tmp_best_mid_cursor !== false) { // Kural 12
                    if (!$tmp_best_mid_cursor->position) {
                        $tmp_best_mid_cursor->position = "MIDDLE";
                        $tmp_best_mid_cursor->point = 15;
                    }

                }

                unset($tmp_exhausut_count);
                unset($tmp_mid_cursor);
                unset($tmp_best_mid_cursor);
            }

            if (isset($best_top_cursor)) { // Kural 13
                if (!$best_top_cursor->position) {
                    $best_top_cursor->position = "TOP";
                    $best_top_cursor->point = 16;
                }
            }

            unset($best_top_cursor);
            unset($top_cursors);
            unset($sup_cursor);
            unset($only_sup_cursor);
            unset($exhaust_cursor);
            unset($mid_cursors);
            unset($sup_cursors);
            unset($jungle_count);
            unset($adc_count);
            unset($top_count);
            unset($mid_count);
            unset($sup_count);
            unset($only_sup_count);
            unset($exhaust_count);

        }

        foreach ($teams as &$team) {
            $list = array("TOP" => 0, "JUNGLE" => 0, "MIDDLE" => 0, "SUPPORT" => 0, "ADC" => 0);
            foreach ($team as $champion) {
                if (isset($champion->position) && isset($list[$champion->position])) {
                    unset($list[$champion->position]);
                }
            }
            if (count($list) == 1) {
                $list = array_flip($list);
                foreach ($team as &$champion) {
                    if (empty($champion->position)) {
                        $champion->position = $list[0];
                        $champion->point = 17;
                    }
                }
            }
        }


        foreach ($teams as &$team) {
            foreach ($team as &$champion) {
                if ($champion->position == 'JUNGLE' && $champion->timeline->lane == 'JUNGLE') {
                    $champion->status = "OK";
                    $ok_count++;
                }
                if ($champion->position == 'ADC' && $champion->timeline->lane == 'BOTTOM' && $champion->timeline->role == 'DUO_CARRY') {
                    $champion->status = "OK";
                    $ok_count++;
                }
                if ($champion->position == 'SUPPORT' && $champion->timeline->lane == 'BOTTOM' && $champion->timeline->role == 'DUO_SUPPORT') {
                    $champion->status = "OK";
                    $ok_count++;
                }
                if ($champion->position == 'TOP' && $champion->timeline->lane == 'TOP' && $champion->timeline->role == 'SOLO') {
                    $champion->status = "OK";
                    $ok_count++;
                }
                if ($champion->position == 'MIDDLE' && $champion->timeline->lane == 'MIDDLE') {
                    $champion->status = "OK";
                    $ok_count++;
                }
            }
        }

        $data['ok_count'] = $ok_count;

        $data['teams'] = $teams;
        $data['ram'] = memory_get_peak_usage(true) / 1024 / 1024;
        $this->load->view('lanetest', $data);
    }

}