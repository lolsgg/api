<?php

class Leagues extends CI_Controller
{
    private $db_read;

    function __construct()
    {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);

        if (false && rand(0, 1) === 1) {
            $this->db_read = $this->load->database('read', TRUE, TRUE);
        } else {
            $this->db_read = $this->db;
        }
        // $this->Throttle->set('api:global:'.$this->Request->getClientIp(),15,3);
    }

    public function test()
    {
        $_POST['summonerId'] = '1893409';
        $_POST['region'] = 'TR';
        $_POST['lang'] = 'en';

        $this->summonerLeagues();
    }

    /**
     * Verilen sihirdara ait lig bilgilerini getirir
     *
     * @uses CI_Input::post($summonerId) Summoner ID
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($lang) Language
     * @uses CI_Input::post($summary) optional Gets only summary page data
     *
     */
    public function summonerLeagues()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();

        /*$request->summonerId = 4018805;
        $request->region = 'TR';
        $request->lang = 'tr';
        $request->summary = false;*/

        $request->summonerId = $this->input->post('summonerId');
        $request->region = $this->input->post('region');
        $request->lang = $this->input->post('lang');
        $request->summary = $this->input->post('summary');

        if (isset($request->summonerId) && isset($request->region) && isset($request->lang)) {

            $summoner_id = $request->summonerId;
            $this->lol->setRegion($request->region);
            $this->lol->setLang($request->lang);
            $summary = false;
            $region_lower = $this->lol->getRegion();

            $data->maintenanceStatus = $maintenance_status = (bool)$this->cache->redis->get('maintenance:league:' . $region_lower);

            $division_list = array('I' => 1, 'II' => 2, 'III' => 3, 'IV' => 4, 'V' => 5);

            $league_name_db_names = array();
            $league_name_db_names['RANKED_SOLO_5x5'] = '1';
            $league_name_db_names['RANKED_FLEX_SR'] = '2';
            $league_name_db_names['RANKED_FLEX_TT'] = '3';


            if (isset($request->summary) && $request->summary == 'true') {
                $summary = true;
            }

            $data->league = new stdClass();

            $league_info = array();
            $league_info['RANKED_SOLO_5x5'] = new stdClass();
            $league_info['RANKED_SOLO_5x5']->queue = "Ranked Solo/Duo";
            $league_info['RANKED_SOLO_5x5']->queueLong = "RANKED_SOLO_5x5";
            $league_info['RANKED_SOLO_5x5']->summoner = new stdClass();
            $league_info['RANKED_SOLO_5x5']->entries = array();
            $league_info['RANKED_SOLO_5x5']->active = false;
            $league_info['RANKED_SOLO_5x5']->title = $this->lol->getGameTypeByType('RANKED_SOLO_5x5');
            $league_info['RANKED_FLEX_SR'] = new stdClass();
            $league_info['RANKED_FLEX_SR']->queue = "Ranked Flex 5v5";
            $league_info['RANKED_FLEX_SR']->queueLong = "RANKED_FLEX_SR";
            $league_info['RANKED_FLEX_SR']->summoner = new stdClass();
            $league_info['RANKED_FLEX_SR']->entries = array();
            $league_info['RANKED_FLEX_SR']->active = false;
            $league_info['RANKED_FLEX_SR']->title = $this->lol->getGameTypeByType('RANKED_FLEX_SR');
            $league_info['RANKED_FLEX_TT'] = new stdClass();
            $league_info['RANKED_FLEX_TT']->queue = "Ranked Flex 3v3";
            $league_info['RANKED_FLEX_TT']->queueLong = "RANKED_FLEX_TT";
            $league_info['RANKED_FLEX_TT']->summoner = new stdClass();
            $league_info['RANKED_FLEX_TT']->entries = array();
            $league_info['RANKED_FLEX_TT']->active = false;
            $league_info['RANKED_FLEX_TT']->title = $this->lol->getGameTypeByType('RANKED_FLEX_TT');

            $update_statuses = array(
                'RANKED_SOLO_5x5' => true,
                'RANKED_FLEX_SR' => true,
                'RANKED_FLEX_TT' => true
            );

            $expre_times = array(
                'RANKED_SOLO_5x5' => 3600,
                'RANKED_FLEX_SR' => 3600,
                'RANKED_FLEX_TT' => 21600
            );

            if (!$maintenance_status) {
                foreach ($league_name_db_names as $queue_name => $league_key) {
                    $this->db_read->where('summoner_id', $summoner_id);
                    $league_query = $this->db_read->get('summoner_league_' . $region_lower . '_' . $league_key);
                    if ($league_query->num_rows()) {
                        $league_data = $league_query->row();
                        if (strtotime($league_data->date_added) > time() - ($expre_times[$queue_name])) {
                            $update_statuses[$queue_name] = false;
                        }

                        if ($league_data->league_id != 'Unranked') {
                            if (!isset($data->league->{$queue_name})) {
                                $data->league->{$queue_name} = $league_info[$queue_name];
                            }

                            $data->status = true;
                            $data->league->{$queue_name}->active = true;
                            $data->league->{$queue_name}->active_division = $this->lol->rankToNumber($league_data->rank, true);
                            $data->league->{$queue_name}->tier = $this->lol->tierToNumber($league_data->tier, true);
                            $data->league->{$queue_name}->name = $league_data->name;
                            $data->league->{$queue_name}->summoner = new stdClass();
                            $data->league->{$queue_name}->entries = array();
                            $data->league->{$queue_name}->summoner->division = $this->lol->rankToNumber($league_data->rank, true);
                            $data->league->{$queue_name}->summoner->id = $league_data->summoner_id;
                            $data->league->{$queue_name}->summoner->isFreshBlood = (bool)$league_data->freshblood;
                            $data->league->{$queue_name}->summoner->isHotStreak = (bool)$league_data->hotstreak;
                            $data->league->{$queue_name}->summoner->isInactive = (bool)$league_data->inactive;
                            $data->league->{$queue_name}->summoner->isVeteran = (bool)$league_data->veteran;
                            $data->league->{$queue_name}->summoner->leagueName = $league_data->name;
                            $data->league->{$queue_name}->summoner->leaguePoints = (int)$league_data->points;
                            $data->league->{$queue_name}->summoner->losses = (int)$league_data->losses;
                            $data->league->{$queue_name}->summoner->miniSeries = $league_data->series ? unserialize($league_data->series) : false;
                            $data->league->{$queue_name}->summoner->name = $league_data->summoner_name;
                            $data->league->{$queue_name}->summoner->ranking = (int)$league_data->position;
                            $data->league->{$queue_name}->summoner->ratio = 0;
                            $data->league->{$queue_name}->summoner->td = $this->lol->divisionTierCombine($data->league->{$queue_name}->summoner->division, $data->league->{$queue_name}->tier);
                            $data->league->{$queue_name}->summoner->tier = $this->lol->tierToNumber($league_data->tier, true);
                            $data->league->{$queue_name}->summoner->wins = (int)$league_data->wins;

                            if ($league_data->wins + $league_data->losses > 0) {
                                $data->league->{$queue_name}->summoner->ratio = number_format(100 / ($league_data->wins + $league_data->losses) * $league_data->wins, 0);
                            }

                            if ($summary == false) {
                                foreach ($division_list as $division_name => $division_num) {
                                    $data->league->{$queue_name}->entries[$division_num] = array(
                                        'division' => $division_name,
                                        'order' => $division_num,
                                        'normal' => array(),
                                        'series' => array()
                                    );
                                }

                                $this->db_read->where('league_id', $league_data->league_id);
                                $league_full_q = $this->db_read->get('summoner_league_' . $region_lower . '_' . $league_key);
                                $league_full = $league_full_q->result();
                                foreach ($league_full as $league_summoner) {
                                    $rank = $this->lol->rankToNumber($league_summoner->rank, true);
                                    $tmp_league_summoner = new stdClass();
                                    $tmp_league_summoner->id = $league_summoner->summoner_id;
                                    $tmp_league_summoner->isFreshBlood = (bool)$league_summoner->freshblood;
                                    $tmp_league_summoner->isHotStreak = (bool)$league_summoner->hotstreak;
                                    $tmp_league_summoner->isInactive = (bool)$league_summoner->inactive;
                                    $tmp_league_summoner->isVeteran = (bool)$league_summoner->veteran;
                                    $tmp_league_summoner->leaguePoints = (int)$league_summoner->points;
                                    $tmp_league_summoner->losses = (int)$league_summoner->losses;
                                    $tmp_league_summoner->miniSeries = $league_summoner->series ? unserialize($league_summoner->series) : false;
                                    $tmp_league_summoner->name = $league_summoner->summoner_name;
                                    $tmp_league_summoner->ratio = 0;
                                    $tmp_league_summoner->wins = (int)$league_summoner->wins;
                                    if ($tmp_league_summoner->wins + $tmp_league_summoner->losses > 0) {
                                        $tmp_league_summoner->ratio = number_format(100 / ($tmp_league_summoner->wins + $tmp_league_summoner->losses) * $tmp_league_summoner->wins, 0);
                                    }
                                    $data->league->{$queue_name}->entries[$division_list[$rank]][$league_summoner->series ? 'series' : 'normal'][] = $tmp_league_summoner;
                                }
                            }
                        }
                    }
                }
            }

            $api_status = false;
            foreach ($league_name_db_names as $queue_name => $league_key) {
                if (isset($update_statuses[$queue_name]) && $update_statuses[$queue_name] == true) {
                    $api_status = true;
                }
            }
            if ($api_status) {
                $data->api_status = true;
                $league_ids = array();
                $positions = $this->lol->api('lol/league/v3/positions/by-summoner/' . $summoner_id, '', 3600);
                if ($positions) {
                    foreach ($positions as $position) {
                        if (isset($position->queueType)) {
                            $data->status = true;
                            if (!isset($data->league->{$position->queueType})) {
                                $data->league->{$position->queueType} = $league_info[$position->queueType];
                            }
                            $data->league->{$position->queueType}->active = true;
                            $data->league->{$position->queueType}->active_division = $position->rank;
                            $data->league->{$position->queueType}->tier = $position->tier;
                            $data->league->{$position->queueType}->name = $position->leagueName;
                            if (!isset($data->league->{$position->queueType}->summoner)) {
                                $data->league->{$position->queueType}->summoner = new stdClass();
                            }
                            if (!isset($data->league->{$position->queueType}->entries)) {
                                $data->league->{$position->queueType}->entries = array();
                            }
                            $data->league->{$position->queueType}->summoner->division = $position->rank;
                            $data->league->{$position->queueType}->summoner->id = $position->playerOrTeamId;
                            $data->league->{$position->queueType}->summoner->isFreshBlood = (bool)$position->freshBlood;
                            $data->league->{$position->queueType}->summoner->isHotStreak = (bool)$position->hotStreak;
                            $data->league->{$position->queueType}->summoner->isInactive = (bool)$position->inactive;
                            $data->league->{$position->queueType}->summoner->isVeteran = (bool)$position->veteran;
                            $data->league->{$position->queueType}->summoner->leagueName = $position->leagueName;
                            $data->league->{$position->queueType}->summoner->leaguePoints = (int)$position->leaguePoints;
                            $data->league->{$position->queueType}->summoner->losses = (int)$position->losses;
                            $data->league->{$position->queueType}->summoner->miniSeries = (isset($position->miniSeries) && $position->miniSeries) ? $position->miniSeries : false;
                            $data->league->{$position->queueType}->summoner->name = $position->playerOrTeamName;
                            $data->league->{$position->queueType}->summoner->ratio = 0;
                            $data->league->{$position->queueType}->summoner->td = $this->lol->divisionTierCombine($position->rank, $position->tier);
                            $data->league->{$position->queueType}->summoner->tier = $position->tier;
                            $data->league->{$position->queueType}->summoner->wins = (int)$position->wins;

                            if ($position->wins + $position->losses > 0) {
                                $data->league->{$position->queueType}->summoner->ratio = number_format(100 / ($position->wins + $position->losses) * $position->wins, 0);
                            }

                            $league_preolad_urls[] = array('url' => 'lol/league/v3/leagues/' . $position->leagueId, 'parameters' => '', 'expire' => 3600);
                            $league_ids[$position->leagueId] = $position->leagueId;
                        }
                    }
                }
                if (count($league_ids) && isset($league_preolad_urls)) {
                    $this->lol->apiPreload($league_preolad_urls, count($league_preolad_urls) + 1);

                    foreach ($league_ids as $league_id) {
                        $league_data = $this->lol->apiOnlyCache('lol/league/v3/leagues/' . $league_id);
                        if (isset($league_data->entries)) {
                            $update_league_db_status = false;
                            if (!$maintenance_status && !$this->cache->redis->get('update_league_v1_id:' . $region_lower . ':' . $league_id)) { // update league every 2 hours
                                $this->cache->redis->save('update_league_v1_id:' . $region_lower . ':' . $league_id, TRUE, 3600);
                                $update_league_db_status = true;
                            }
                            if ($update_league_db_status && !isset($update_league_data_tmp[$league_data->queue])) {
                                $update_league_data_tmp[$league_data->queue] = array();
                            }
                            if ($summary == false) {
                                foreach ($division_list as $division_name => $division_num) {
                                    $data->league->{$league_data->queue}->entries[$division_num] = array(
                                        'division' => $division_name,
                                        'order' => $division_num,
                                        'normal' => array(),
                                        'series' => array()
                                    );
                                }
                            }

                            $tier_number = $this->lol->tierToNumber($league_data->tier);
                            foreach ($league_data->entries as $entry) {
                                if ($summary == false) {
                                    $tmp_league_summoner = new stdClass();
                                    $tmp_league_summoner->id = $entry->playerOrTeamId;
                                    $tmp_league_summoner->isFreshBlood = (bool)$entry->freshBlood;
                                    $tmp_league_summoner->isHotStreak = (bool)$entry->hotStreak;
                                    $tmp_league_summoner->isInactive = (bool)$entry->inactive;
                                    $tmp_league_summoner->isVeteran = (bool)$entry->veteran;
                                    $tmp_league_summoner->leaguePoints = (int)$entry->leaguePoints;
                                    $tmp_league_summoner->losses = (int)$entry->losses;
                                    $tmp_league_summoner->miniSeries = (isset($entry->miniSeries) && $entry->miniSeries) ? $entry->miniSeries : false;
                                    $tmp_league_summoner->name = $entry->playerOrTeamName;
                                    $tmp_league_summoner->ratio = 0;
                                    $tmp_league_summoner->wins = (int)$entry->wins;
                                    if ($tmp_league_summoner->wins + $tmp_league_summoner->losses > 0) {
                                        $tmp_league_summoner->ratio = number_format(100 / ($tmp_league_summoner->wins + $tmp_league_summoner->losses) * $tmp_league_summoner->wins, 0);
                                    }
                                    $data->league->{$league_data->queue}->entries[$division_list[$entry->rank]][$tmp_league_summoner->miniSeries ? 'series' : 'normal'][] = $tmp_league_summoner;
                                }
                                $rank_number = $this->lol->rankToNumber($entry->rank);
                                if ($update_league_db_status) {
                                    $update_league_data_tmp[$league_data->queue][] = array(
                                        'summoner_id' => (int)$entry->playerOrTeamId,
                                        'league_id' => $league_id,
                                        'summoner_name' => $entry->playerOrTeamName,
                                        'tier' => $tier_number,
                                        'rank' => $rank_number,
                                        'name' => $league_data->name,
                                        'points' => $entry->leaguePoints,
                                        'wins' => $entry->wins,
                                        'losses' => $entry->losses,
                                        'veteran' => $entry->veteran,
                                        'inactive' => $entry->inactive,
                                        'freshblood' => $entry->freshBlood,
                                        'hotstreak' => $entry->hotStreak,
                                        'ranking' => (int)((6 - $tier_number) . (5 - $rank_number) . str_pad($entry->leaguePoints, 4, '0', STR_PAD_LEFT) . str_pad($entry->wins, 4, '0', STR_PAD_LEFT)),
                                        'series' => isset($entry->miniSeries) ? serialize($entry->miniSeries) : '',
                                        'date_added' => date('Y-m-d H:i:s')
                                    );
                                }
                            }
                        }
                        unset($league_data);
                    }
                }
            }

            if (isset($update_league_data_tmp)) {
                if (count($update_league_data_tmp)) {
                    foreach ($data->league as $queue_type => $league) {
                        if (!$league->active) {
                            $update_league_data_tmp[$queue_type][] = array(
                                'summoner_id' => $summoner_id, 'league_id' => 'Unranked',
                                'summoner_name' => NULL, 'tier' => NULL, 'rank' => NULL,
                                'name' => NULL, 'points' => NULL, 'wins' => NULL, 'losses' => NULL, 'veteran' => NULL,
                                'inactive' => NULL, 'freshblood' => NULL, 'hotstreak' => NULL, 'ranking' => NULL, 'series' => '', 'date_added' => date('Y-m-d H:i:s')
                            );
                        }
                    }
                    $update_keys = array('league_id', 'summoner_name', 'tier', 'rank', 'name', 'points', 'wins', 'losses', 'veteran', 'inactive', 'freshblood', 'hotstreak', 'ranking', 'series', 'date_added');
                    if (isset($update_league_data_tmp['RANKED_SOLO_5x5'])) {
                        $this->db->insert_on_duplicate_update_batch('summoner_league_' . $region_lower . '_1', $update_league_data_tmp['RANKED_SOLO_5x5'], true, 100, $update_keys);
                        unset($update_league_data_tmp['RANKED_SOLO_5x5']);
                    }
                    if (isset($update_league_data_tmp['RANKED_FLEX_SR'])) {
                        $this->db->insert_on_duplicate_update_batch('summoner_league_' . $region_lower . '_2', $update_league_data_tmp['RANKED_FLEX_SR'], true, 100, $update_keys);
                        unset($update_league_data_tmp['RANKED_FLEX_SR']);
                    }
                    if (isset($update_league_data_tmp['RANKED_FLEX_TT'])) {
                        $this->db->insert_on_duplicate_update_batch('summoner_league_' . $region_lower . '_3', $update_league_data_tmp['RANKED_FLEX_TT'], true, 100, $update_keys);
                        unset($update_league_data_tmp['RANKED_FLEX_TT']);
                    }
                    unset($update_league_data_tmp);
                }
            }

            foreach ($league_info as $queue => $league) {
                if (!isset($data->league->{$queue})) {
                    $data->league->{$queue} = $league;
                }
            }

        }


        echo json_encode($data);
    }

    /**
     * Lig sıralamasını getirir
     *
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($page) Language
     * @uses CI_Input::post($league) League Filter
     *
     */
    public function ranking()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->region = $this->input->post('region');
        $request->page = $this->input->post('page');
        $request->league = $this->input->post('league');

        if ($request->region) {
            $this->lol->setRegion($request->region);
            $region = $this->lol->getRegion();

            $cache_key = 'leaderboard_leaguage_c1_v4_' . md5($region . $request->page . $request->league);
            if ($request->page < 15 && $request->page > 0 && $result = $this->cache->redis->get($cache_key)) {
                echo $result;
                die();
            }

            $data->maintenanceStatus = $maintenance_status = (bool)$this->cache->redis->get('maintenance:league:' . $region);
            if ($maintenance_status == true) {
                echo json_encode($data);
                die();
            }
            $max_page = 1000000;
            $page = 1;
            $per_page = 40;
            if ($request->page > 1 && $request->page < $max_page) {
                $page = $request->page;
            }
            $league_num = 1;
            if ($request->league == 2 || $request->league == 3) {
                $league_num = $request->league;
            }

            $data->lastUpdate = date('d.m.Y H:i:s', strtotime('2018-05-23 05:00:00'));
            $data->totalSummoner = 1000000;

            $this->db_read->from('summoner_league_' . $region . '_' . $league_num);
            $this->db_read->limit($per_page, ($page - 1) * $per_page);
            $this->db_read->where('position is NOT NULL', NULL, FALSE);
            $this->db_read->order_by('position', 'asc');
            $league_query = $this->db_read->get();
            $summoners = array();
            if ($league_query->num_rows()) {
                $result = $league_query->result();
                foreach ($result as $summoner) {
                    unset($summoner->date_added);
                    unset($summoner->series);

                    $_summoner = new stdClass();
                    $_summoner->summoner_id = $summoner->summoner_id;
                    $_summoner->summoner_name = $summoner->summoner_name;
                    $_summoner->tier = $this->lol->tierToNumber($summoner->tier, true);
                    $_summoner->rank = ($summoner->tier <= 1) ? '' : $this->lol->rankToNumber($summoner->rank, true);
                    $_summoner->td = $this->lol->divisionTierCombine($_summoner->rank, $_summoner->tier);
                    $_summoner->points = $summoner->points;
                    $_summoner->wins = $summoner->wins;
                    $_summoner->losses = $summoner->losses;
                    $_summoner->team = $summoner->name;
                    $_summoner->position = $summoner->position;
                    $_summoner->winRatio = $summoner->wins > 0 ? (100 / ($summoner->wins + $summoner->losses) * $summoner->wins) : 0;

                    $summoners[] = $_summoner;
                }
                $data->status = true;
                $data->summoners = $summoners;
            }
            if ($request->page < 15 && $request->page > 0) {
                $this->cache->redis->save($cache_key, json_encode($data), 3600);
            }
        }
        echo json_encode($data);
    }

    /**
     * Belirli bir sihirdarın lig sırasını getirir
     *
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($summonerId) Summoner ID
     * @uses CI_Input::post($league) League Filter
     *
     */
    public function summonerRanking()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->region = $this->input->post('region');
        $request->summonerId = $this->input->post('summonerId');
        $request->league = $this->input->post('league');

        if ($request->region) {
            $summoner_id = $request->summonerId;
            $region = $request->region;
            $this->lol->setRegion($region);
            //$region = $this->lol->getRegion();
            $league_num = 1;
            if ($request->league == 2 || $request->league == 3) {
                $league_num = $request->league;
            }
            $region_lower = $this->lol->getRegion();
            $ranking_q = $this->db_read->get_where('summoner_league_' . $region_lower . '_' . $league_num, array('summoner_id' => (int)$summoner_id));
            if ($ranking_q->num_rows()) {
                $ranking = $ranking_q->row();
                $data->status = true;
                $data->ranking = (int)$ranking->position;
            }
        }
        echo json_encode($data);
    }
}
