<?php

class Summoner extends CI_Controller
{
    private $db_read;

    function __construct()
    {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);

        if (false && rand(0, 1) === 1) {
            $this->db_read = $this->load->database('read', TRUE, TRUE);
        } else {
            $this->db_read = $this->db;
        }
        // $this->Throttle->set('api:global:'.$this->Request->getClientIp(),15,3);
    }

    public function test()
    {
        $_POST['summonerId'] = '1893409';
        $_POST['region'] = 'TR';
        $_POST['lang'] = 'en';

        $this->ranking();
    }



    /**
     * Seviye sıralamasını getirir
     *
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($page) Language
     *
     */
    public function ranking()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->region = $this->input->post('region');
        $request->page = $this->input->post('page');

        if ($request->region) {
            $this->lol->setRegion($request->region);
            $region = $this->lol->getRegion();

            $cache_key = 'leaderboard_level_c1_v4_' . md5($region . $request->page);
            if ($request->page < 15 && $request->page > 0 && $result = $this->cache->redis->get($cache_key)) {
                echo $result;
                die();
            }

            $data->maintenanceStatus = $maintenance_status = (bool)$this->cache->redis->get('maintenance:level:' . $region);
            if ($maintenance_status == true) {
                echo json_encode($data);
                die();
            }
            $max_page = 1000000;
            $page = 1;
            $per_page = 40;
            if ($request->page > 1 && $request->page < $max_page) {
                $page = $request->page;
            }

            $data->lastUpdate = date('d.m.Y H:i:s', strtotime('2018-05-23 05:00:00'));
            $data->totalSummoner = 1000000;

            $this->db_read->from('summoner_league_' . $region . '_1');
            $this->db_read->limit($per_page, ($page - 1) * $per_page);
            $this->db_read->where('position is NOT NULL', NULL, FALSE);
            $this->db_read->order_by('position', 'asc');
            $league_query = $this->db_read->get();
            $summoners = array();
            if ($league_query->num_rows()) {
                $result = $league_query->result();
                foreach ($result as $summoner) {
                    unset($summoner->date_added);
                    unset($summoner->series);

                    $_summoner = new stdClass();
                    $_summoner->summoner_id = $summoner->summoner_id;
                    $_summoner->summoner_name = $summoner->summoner_name;
                    $_summoner->level = rand(1, 5000);
                    $_summoner->position = $summoner->position;

                    $summoners[] = $_summoner;
                }
                $data->status = true;
                $data->summoners = $summoners;
            }
            if ($request->page < 15 && $request->page > 0) {
                $this->cache->redis->save($cache_key, json_encode($data), 3600);
            }
        }
        echo json_encode($data);
    }

    /**
     * Belirli bir sihirdarın seviye sırasını getirir
     *
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($summonerId) Summoner ID
     *
     */
    public function summonerRanking()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->region = $this->input->post('region');
        $request->summonerId = $this->input->post('summonerId');

        if ($request->region) {
            $summoner_id = $request->summonerId;
            $region = $request->region;
            $this->lol->setRegion($region);
            $region_lower = $this->lol->getRegion();
            $ranking_q = $this->db_read->get_where('summoner_league_' . $region_lower . '_1', array('summoner_id' => (int)$summoner_id));
            if ($ranking_q->num_rows()) {
                $ranking = $ranking_q->row();
                $data->status = true;
                $data->level = rand(1, 5000);
                $data->ranking = (int)$ranking->position;
            }
        }
        echo json_encode($data);
    }
}
