<?php

class StrategyBoard extends CI_Controller
{
    private $db_read;

    function __construct()
    {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);

        if (false && rand(0, 1) === 1) {
            $this->db_read = $this->load->database('read', TRUE, TRUE);
        } else {
            $this->db_read = $this->db;
        }
    }

    /**
     * Adds the $canvas data to the database. Returns share and edit codes.
     * If $edit code is given, it updates the existing canvas data. Returns only share code.
     * @uses CI_Input::post($canvas) Canvas Data
     * @uses CI_Input::post($edit) Edit Code
     */
    public function save()
    {
        $data = new stdClass();
        $data->status = false;
        $client_ip = $this->Request->getClientIp();
        $this->db_read->where('date_added>', date('Y-m-d H:i:s', time() - (60 * 60)));
        $this->db_read->where('ip_added', $client_ip);
        $this->db_read->or_where('ip_updated', $client_ip);
        $ip_check = $this->db_read->get('strategy_board');
        if ($ip_check->num_rows() < 20) {
            $canvas = $this->input->post('canvas');
            $edit_code = preg_replace("/[^a-z0-9]/", '', trim(strtolower($this->input->post('edit'))));
            if (strlen($canvas) > 0 && strlen($canvas) < 5000000) {
                if ($edit_code) {
                    if (strlen($edit_code) == 34) {
                        $strategy_q = $this->db_read->get_where('strategy_board', array('edit_code' => $edit_code));
                        if ($strategy_q->num_rows() == 1) {
                            $strategy = $strategy_q->row();
                            $this->db->where('strategy_board_id', $strategy->strategy_board_id);
                            $this->db->update('strategy_board', array('canvas' => base64_encode($canvas), 'ip_updated' => $client_ip));
                            $data->status = true;
                            $data->shareCode = $strategy->share_code;
                        }
                    }
                } else {
                    list($edit_code, $share_code) = $this->generateShareCodes();
                    $this->db->insert('strategy_board', array(
                        'ip_added' => $client_ip,
                        'edit_code' => $edit_code,
                        'share_code' => $share_code,
                        'canvas' => base64_encode($canvas)
                    ));
                    $data->status = true;
                    $data->editCode = $edit_code;
                    $data->shareCode = $share_code;
                }
            }
        } else {
            $data->count = $ip_check->num_rows();
        }
        echo json_encode($data);
    }

    /**
     * Deletes the canvas
     * @uses CI_Input::post($edit) Edit Code
     */
    public function delete()
    {
        $data = new stdClass();
        $data->status = false;
        $client_ip = $this->Request->getClientIp();
        $this->db_read->where('date_added>', date('Y-m-d H:i:s', time() - (60 * 60)));
        $this->db_read->where('ip_added', $client_ip);
        $this->db_read->or_where('ip_updated', $client_ip);
        $ip_check = $this->db_read->get('strategy_board');
        if ($ip_check->num_rows() < 20) {
            $edit_code = preg_replace("/[^a-z0-9]/", '', trim(strtolower($this->input->post('edit'))));
            if (strlen($edit_code) == 34) {
                $strategy_q = $this->db_read->get_where('strategy_board', array('edit_code' => $edit_code));
                if ($strategy_q->num_rows() == 1) {
                    $strategy = $strategy_q->row();
                    $this->db->delete('strategy_board', array('strategy_board_id' => $strategy->strategy_board_id));
                    $data->status = true;
                }
            }
        } else {
            $data->count = $ip_check->num_rows();
        }
        echo json_encode($data);
    }

    /**
     * Paylaşım kodu belirtilen kanvas verilerini getirir
     *
     * @param share paylaşım kodu
     *
     */
    public function get()
    {
        $data = new stdClass();
        $data->status = false;
        $share_code = preg_replace("/[^a-z0-9]/", '', trim(strtolower($this->input->post('share'))));
        if (strlen($share_code) == 8) {
            $strategy_q = $this->db_read->get_where('strategy_board', array('share_code' => $share_code));
            if ($strategy_q->num_rows() == 1) {
                $strategy = $strategy_q->row();
                $data->status = true;
                $data->canvas = base64_decode($strategy->canvas);
            }
        }
        echo json_encode($data);
    }

    private function generateShareCodes()
    {
        $share = substr(base_convert(microtime(false), 8, 36) . md5(microtime(false) . 'lolsgg.lols.gg'), 0, 8);
        $edit = substr(base_convert(microtime(false), 15, 36) . rand(1000, 9999) . md5(microtime(false) . 'lolsgg.lols.gg'), 0, 34);
        for ($i = 0; $i < 15; $i++) {
            $this->db_read->where('share_code', $share);
            $this->db_read->or_where('edit_code', $edit);
            $result = $this->db_read->get('strategy_board');
            if ($result->num_rows() == 0) {
                break;
            }
        }
        return array($edit, $share);
    }
}
