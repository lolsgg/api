<?php

class Twitch extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Aktif olan yayınları getirir
     */
    public function getLiveStreams()
    {
        header('Content-Type: application/json');

        $data = new stdClass();
        $data->status = false;

        $data->streams = array();

        $promoted = array();

        $twitchApi = new \TwitchApi\TwitchApi(array('client_id' => 'f5wjzhvos3rzgrcy9m1fernwwrm7rf'));

        $streams = $twitchApi->getLiveStreams(NULL, "League of Legends", "", 'live', 8);

        if ($promoted) {
            foreach ($promoted as $userName) {
                $_user = $twitchApi->getUserByUsername($userName);
                if ($_user) {
                    $_stream = $twitchApi->getStreamByUser($_user['users'][0]['_id'], 'live');
                    if (isset($_stream['stream']) && $_stream['stream']) {
                        array_unshift($streams['streams'], $_stream['stream']);
                        array_pop($streams['streams']);
                    }
                }
            }
        }

        if ($streams) {
            foreach ($streams['streams'] as $stream) {
                $data->streams[] = array(
                    'channel' => $stream['channel']['name'],
                    'name' => $stream['channel']['display_name'],
                    'url' => $stream['channel']['url'],
                    'viewers' => $stream['viewers'],
                    'image' => str_replace('{width}', '235', str_replace('{height}', '132', $stream['preview']['template'])),
                    'status' => $stream['channel']['status'],
                    'language' => strtoupper($stream['channel']['broadcaster_language']),
                    'status' => $stream['channel']['status'],
                    'promoted' => in_array($stream['channel']['name'], $promoted) ? true : false
                );
            }

            $data->status = true;
        }

        echo json_encode($data);
    }
}
