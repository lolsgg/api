<?php

class Matches extends CI_Controller
{
    private $db_read;

    /**
     * Matches constructor.
     */
    function __construct()
    {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);
        $this->load->model('matchhistory');

        if (false && rand(0, 1) === 1) {
            $this->db_read = $this->load->database('read', TRUE, TRUE);
        } else {
            $this->db_read = $this->db;
        }
        // $this->Throttle->set('api:global:'.$this->Request->getClientIp(),15,3);
    }

    /**
     * updates summoner match history and gets latest 20 matches
     * @uses CI_Input::post($summonerId) Summoner ID
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($lang) Language
     */
    public function updateSummonerMatchHistory()
    {
        $request = new stdClass();
        $request->summonerId = $this->input->post('summonerId');
        $request->accountId = $this->input->post('accountId');
        $request->region = $this->input->post('region');
        $request->lang = $this->input->post('lang');
        $request->gameType = $this->input->post('gameType');
        // echo $request->accountId;
        $update = false;

        if (isset($request->accountId) && isset($request->region)) {
            $account_id = (int)$request->accountId;
            if ($account_id > 0) {
                $region = mb_strtoupper($request->region);
                $this->lol->setRegion($region);

                $update = $this->lol->updateSummonerMatches($account_id);

                if($update) {
                    $this->db->insert('summoner_history_update_' . $this->lol->getRegion(), array('account_id' => (int)$account_id));
                }
            }
        }
        if ($update) {
            $this->matchHistory();
        } else {
            header('Content-Type: application/json');
            $data = new stdClass();
            $data->status = false;
            echo json_encode($data);
        }
    }


    /**
     * Gets summoner match history
     * If first page requested, gets 20 matches. After first page it gets 10 matches per page.
     * If first page requested, gets champion stats and the summoners who played together.
     *
     * @uses CI_Input::post($summonerId) Summoner ID
     * @uses CI_Input::post($accountId) Account ID
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($lang) Language
     * @uses CI_Input::post($page) Page, Default: 1
     * @uses CI_Input::post($gameType) Game Type
     */
    public function matchHistory()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;


        $request = new stdClass();
        $request->accountId = (int)$this->input->post('accountId');
        $request->summonerId = $this->input->post('summonerId');
        $request->region = $this->input->post('region');
        $request->lang = $this->input->post('lang');
        $request->page = $this->input->post('page');
        $request->gameType = $this->input->post('gameType');


        if (isset($request->summonerId) && $request->accountId > 0 && isset($request->region) && isset($request->lang)) {

            $game_type_filter = false;
            if (isset($request->gameType)) {
                $game_type_filter = $request->gameType;
            }

            $data->matches = new stdClass();
            $data->matchesTotal = 0;
            $summoner_id = $request->summonerId;
            $account_id = $request->accountId;
            $region = mb_strtoupper($request->region);
            $this->lol->setRegion($region);
            $this->lol->setLang($request->lang);

            $this->load->model('Runes');

            if ($this->Request->isLanguageExist($request->lang)) {
                $this->lang->load('site', mb_strtolower($request->lang));
            } else {
                $this->lang->load('site', "en");
            }

            if (!isset($request->page)) {
                $request->page = 1;
            }

            $data->role = array();


            $playedWithStatus = false;
            $champStats = false;
            $match_per_page = 10;
            $page_start = $request->page * 10;
            if ($request->page == 1) {
                $match_per_page += 10;
                $page_start = 0;
                $playedWithStatus = true;
                $champStats = true;
            }

            $match_statuses = array('win' => lang('site_text_win'), 'lose' => lang('site_text_lose'), 'remake' => lang('site_text_remake'));

            $matches = $this->lol->getSummonerMatches($account_id, $match_per_page, $page_start, $game_type_filter);

            $maps = $this->lol->getMaps();
            //$masteries = $this->lol->getMasteries();
            $items = $results = $this->lol->getItems();
            $played_with = new stdClass();
            $champions = array();
            $tags = array('Fighter' => 0, 'Assassin' => 0, 'Marksman' => 0, 'Tank' => 0, 'Support' => 0, 'Mage' => 0);
            $totalTags = 0;

            $totalAllSameTeamSummonerKills = 0;
            $totalAllSameTeamSummonerKillsChamps = array();

            if ($matches) {

                $this->db_read->where('account_id', $account_id);
                $this->db_read->limit(1);
                $this->db_read->order_by("summoner_history_update_id", "desc");
                $history_update_q = $this->db_read->get('summoner_history_update_' . $this->lol->getRegion());
                $history_update_num = $history_update_q->num_rows();
                if ($history_update_num > 0) {
                    $history_update = $history_update_q->row();
                    $data->lastUpdate = $history_update->date_added;
                    $search_time = $this->Request->secondToTime(time() - strtotime($history_update->date_added));
                    $data->lastUpdateText = $search_time[0] . ' ' . lang('site_' . $search_time[1]) . ' ' . lang('site_text_ago');
                }


                foreach ($matches as $match_) {
                    if (!isset($match_->match)) {
                        continue;
                    }

                    $data->status = true;
                    $match = $match_->match;
                    $matchdata = new stdClass();
                    $matchdata->gameId = $match->gameId;
                    $matchdata->gameLength = $match->gameDuration;
                    $matchdata->startTime = $match->gameCreation;
                    $search_time = $this->Request->secondToTime(time() - ($match->gameCreation / 1000));
                    $matchdata->ago = $search_time[0] . ' ' . lang('site_' . $search_time[1]) . ' ' . lang('site_text_ago');
                    $matchdata->timestamp = $match->gameCreation;
                    $matchdata->gameType = $this->lol->getGameTypeById($match->queueId);
                    $search_time = $this->Request->secondToTime($match->gameDuration);
                    $matchdata->matchTime = $search_time[0] . ' ' . lang('site_' . $search_time[1]);

                    $matchdata->mapName = '';
                    $matchdata->mapId = $match->mapId;

                    if (isset($match->mapId) && isset($maps->{$match->mapId})) {
                        $matchdata->mapName = $this->lol->getMapName($maps->{$match->mapId}->MapName);
                    }

                    $matchdata->team1 = new stdClass();
                    $matchdata->team2 = new stdClass();
                    $matchdata->team1->summoners = new stdClass();
                    $matchdata->team2->summoners = new stdClass();

                    $matchdata->team1->totalKills = 0;
                    $matchdata->team2->totalKills = 0;

                    foreach ($match->participants as $participant_key => $participant) {
                        $summoner = new stdClass();
                        if (!isset($participant->identity->summonerId)) {
                            $summoner->summonerId = $participant_key;
                            $summoner->summonerName = '';
                        } else {
                            $summoner->summonerId = $participant->identity->summonerId;
                            $summoner->summonerName = $participant->identity->summonerName;
                        }


                        $champ = $this->lol->getChampionById2($participant->championId);
                        if (isset($champ->key)) {
                            $summoner->championKey = $champ->key;
                            $summoner->championName = $champ->name;
                        } else {
                            $summoner->championKey = '';
                            $summoner->championName = '';
                        }

                        $summoner->team = "team" . ($participant->teamId == '100' ? 1 : 2);

                        $matchdata->{$summoner->team}->summoners->{$summoner->summonerId} = $summoner;

                        $matchdata->{$summoner->team}->totalKills += $participant->stats->kills;

                        if ($summoner->summonerId == $summoner_id) {
                            $msummoner = new stdClass();
                            $msummoner->summonerId = $participant->identity->summonerId;
                            $msummoner->summonerName = $participant->identity->summonerName;

                            $champ = $this->lol->getChampionById2($participant->championId);
                            $msummoner->championKey = $champ->key;
                            $msummoner->championName = $champ->name;

                            if (isset($champ->tags) && count($champ->tags) > 0) {
                                if ($champ->tags[0] == 'Tank,melee') {
                                    $champ->tags = array('Tank', 'Fighter');
                                }

                                //$this->Request->pr($champ->tags);

                                if (isset($tags[$champ->tags[0]])) {
                                    $tags[$champ->tags[0]]++;
                                } else {
                                    $tags[$champ->tags[0]] = 1;
                                }
                                $totalTags++;

                                /*foreach ($champ->tags as $champ_tag) {
                                    if (isset($tags[$champ_tag])) {
                                        $tags[$champ_tag]++;
                                    } else {
                                        $tags[$champ_tag] = 1;
                                    }
                                    $totalTags++;
                                }*/
                            }

                            $matchdata->matchStatus = $participant->stats->win == 'Win' ? 'win' : 'lose';
                            if ($matchdata->gameLength < 300 && $match->teams[0]->towerKills == 0 && $match->teams[1]->towerKills == 0) {
                                $matchdata->matchStatus = 'remake';
                            }

                            $matchdata->matchStatusText = $match_statuses[$matchdata->matchStatus];


                            if ($champStats) {
                                if (!isset($champions[$champ->id])) {
                                    $champions[$champ->id] = array();
                                    $champions[$champ->id]['kills'] = 0;
                                    $champions[$champ->id]['deaths'] = 0;
                                    $champions[$champ->id]['assists'] = 0;
                                    $champions[$champ->id]['win'] = 0;
                                    $champions[$champ->id]['lose'] = 0;
                                    $champions[$champ->id]['championKey'] = $msummoner->championKey;
                                    $champions[$champ->id]['championName'] = $msummoner->championName;
                                }

                                if ($matchdata->matchStatus != 'remake') {
                                    $champions[$champ->id]['kills'] += $participant->stats->kills;
                                    $champions[$champ->id]['deaths'] += $participant->stats->deaths;
                                    $champions[$champ->id]['assists'] += $participant->stats->assists;
                                    $champions[$champ->id]['win'] += $participant->stats->win == 'Win' ? 1 : 0;
                                    $champions[$champ->id]['lose'] += $participant->stats->win == 'Win' ? 0 : 1;
                                }


                            }

                            $msummoner->team = "team" . ($participant->teamId == '100' ? 1 : 2);
                            $msummoner->championLevel = $participant->stats->champLevel;
                            $msummoner->championLevelText = lang('site_level') . ' ' . $msummoner->championLevel;
                            $msummoner->largestKillingSpree = $participant->stats->largestKillingSpree;
                            $msummoner->largestMultiKill = $participant->stats->largestMultiKill;
                            $msummoner->goldEarned = $participant->stats->goldEarned;
                            $msummoner->assists = $participant->stats->assists;
                            $msummoner->deaths = $participant->stats->deaths;
                            $msummoner->kills = $participant->stats->kills;
                            $msummoner->kdaRatio = $msummoner->kills + $msummoner->assists;
                            if ($msummoner->deaths > 0) {
                                $msummoner->kdaRatio = $msummoner->kdaRatio / $msummoner->deaths;
                            }
                            $msummoner->kdaRatio = number_format($msummoner->kdaRatio, 1, '.', '');
                            $msummoner->minionKills = $participant->stats->totalMinionsKilled + $participant->stats->neutralMinionsKilled;
                            $msummoner->neutralMinionKills = $participant->stats->neutralMinionsKilled;
                            if ($matchdata->gameLength > 0) {
                                $msummoner->minionKillsPerMin = number_format(($msummoner->minionKills) / ($matchdata->gameLength / 60), 1, '.', '');
                            } else {
                                $msummoner->minionKillsPerMin = number_format($msummoner->minionKills, 1, '.', '');
                            }

                            $msummoner->perkStyleId = false;
                            $msummoner->perkStyleName = false;
                            $msummoner->perkStyleDescription = false;
                            $msummoner->perkSubStyleId = false;
                            $msummoner->perkSubStyleName = false;
                            $msummoner->perkSubStyleDescription = false;
                            $msummoner->perkSubStyleBonus = false;

                            if (isset($participant->stats->perkPrimaryStyle)) {
                                if (mb_strtolower(trim($request->lang)) == 'tr') {
                                    $perk_style = $this->Runes->perk_styles_tr[$participant->stats->perkPrimaryStyle];
                                    $perk_sub_style = $this->Runes->perk_styles_tr[$participant->stats->perkSubStyle];
                                } else {
                                    $perk_style = $this->Runes->perk_styles_en[$participant->stats->perkPrimaryStyle];
                                    $perk_sub_style = $this->Runes->perk_styles_en[$participant->stats->perkSubStyle];
                                }
                                $msummoner->perkStyleId = $participant->stats->perkPrimaryStyle;
                                $msummoner->perkStyleName = $perk_style['name'];
                                $msummoner->perkStyleDescription = $perk_style['description'];
                                $msummoner->perkSubStyleId = $participant->stats->perkSubStyle;
                                $msummoner->perkSubStyleName = $perk_sub_style['name'];
                                $msummoner->perkSubStyleDescription = $perk_sub_style['description'];
                                $msummoner->perkSubStyleBonus = $perk_style['combinations'][$participant->stats->perkSubStyle];
                            }


                            if (isset($participant->timeline) && isset($participant->timeline->lane)) {
                                $role = $participant->timeline->lane;
                                if ($role == 'BOTTOM' && isset($participant->timeline->role)) {
                                    $role = $participant->timeline->role;
                                }
                                if ($role == "DUO_SUPPORT") {
                                    $role = "SUPPORT";
                                }
                                if ($role == "SOLO") {
                                    $role = "TOP";
                                }
                                if ($role == "NONE") {
                                    $role = "FILL";
                                }
                                if ($role == "DUO" || $role == "DUO_CARRY") {
                                    $role = "BOT";
                                }
                                $role = strtolower($role);
                                if (!isset($data->role[$role])) {
                                    $data->role[$role] = 0;
                                }
                                $data->role[$role] += 1;
                            }

                            $spell1 = $this->lol->getSpellById($participant->spell1Id);
                            $spell2 = $this->lol->getSpellById($participant->spell2Id);
                            if (isset($spell1->name)) {
                                $msummoner->spell1Name = $spell1->name;
                                $msummoner->spell1Description = $spell1->description;
                                $msummoner->spell1Key = $this->lol->cleanText($spell1->key);
                            }
                            if (isset($spell2->name)) {
                                $msummoner->spell2Name = $spell2->name;
                                $msummoner->spell2Description = $spell2->description;
                                $msummoner->spell2Key = $this->lol->cleanText($spell2->key);
                            }


                            $msummoner->items = array();
                            for ($i = 0; $i <= 6; $i++) { // 6 iteme de aynı işlemi yap
                                $msummoner->items[$i] = $participant->stats->{'item' . $i};
                                if (isset($items->{$msummoner->items[$i]})) {
                                    $item = $items->{$msummoner->items[$i]};
                                    $msummoner->items[$i] = new stdClass();
                                    $msummoner->items[$i]->name = $item->name;
                                    $msummoner->items[$i]->description = $item->description;
                                    $msummoner->items[$i]->image = $item->image->full;
                                    $msummoner->items[$i]->gold = $item->gold;
                                } else {
                                    $msummoner->items[$i] = 0;
                                }
                            }


                            /*
                            $msummoner->masteryKey = '';
                            $msummoner->masteryName = '';
                            $msummoner->masteryDescription = '';
                            if (isset($participant->masteries) && count($participant->masteries) > 0) {
                                foreach ($participant->masteries as $masteries_key => $mastery) {
                                    $selected_masteries[$mastery->masteryId] = $mastery->rank;
                                    if (isset($masteries->{$mastery->masteryId})) {
                                        $mastery_details = $masteries->{$mastery->masteryId};
                                        if ($mastery_details->last) {
                                            $msummoner->masteryKey = "mastery-" . $mastery->masteryId;
                                            $msummoner->masteryName = $mastery_details->name;
                                            $msummoner->masteryDescription = $mastery_details->description[$mastery_details->ranks - 1];
                                        }
                                    }
                                }
                            }
                            */


                        }
                    }

                    foreach ($match->participants as $participant) {
                        if ($playedWithStatus && isset($msummoner) && "team" . ($participant->teamId == '100' ? 1 : 2) == $msummoner->team) {
                            $s_id = $participant->identity->summonerId;
                            if (!isset($played_with->{$s_id})) {
                                $played_with->{$s_id} = new stdClass();
                                $played_with->{$s_id}->summonerName = $participant->identity->summonerName;
                                $played_with->{$s_id}->played = 0;
                                $played_with->{$s_id}->win = 0;
                            }
                            $played_with->{$s_id}->played += 1;
                            $played_with->{$s_id}->win += $participant->stats->win == 'Win' ? 1 : 0;
                        }
                    }

                    if (isset($msummoner)) {
                        $msummoner->killParticipationRatio = 0;
                        if (($msummoner->kills + $msummoner->assists) > 0 && $matchdata->{$msummoner->team}->totalKills > 0) {
                            $msummoner->killParticipationRatio = number_format((($msummoner->kills + $msummoner->assists) * 100) / $matchdata->{$msummoner->team}->totalKills, 1, '.', '');
                            $totalAllSameTeamSummonerKills += $matchdata->{$msummoner->team}->totalKills;
                            if (!isset($totalAllSameTeamSummonerKillsChamps[$msummoner->championKey])) {
                                $totalAllSameTeamSummonerKillsChamps[$msummoner->championKey] = 0;
                            }
                            $totalAllSameTeamSummonerKillsChamps[$msummoner->championKey] += $matchdata->{$msummoner->team}->totalKills;
                        }
                        $msummoner->killParticipationRatioText = lang('site_p_kill') . ' ' . $msummoner->killParticipationRatio;

                        $matchdata->summoner = $msummoner;
                    }
                    $data->matches->{$matchdata->gameId} = $matchdata;
                }

                $data->matchesTotal = count((array)$data->matches);

                if ($playedWithStatus && isset($played_with)) {
                    foreach ($played_with as $s_id => $stats) {
                        if ($stats->played > 1 && $summoner_id != $s_id) {
                            $played_with->{$s_id}->ratio = (int)number_format(100 / $stats->played * $stats->win, 0, '', '');
                            $played_with->{$s_id}->lose = $stats->played - $stats->win;
                        } else {
                            unset($played_with->{$s_id});
                        }
                    }
                    $data->playedWith = $played_with;
                }

                if ($champStats) {
                    $totalStats = new stdClass();
                    $totalStats->win = 0;
                    $totalStats->lose = 0;
                    $totalStats->played = 0;
                    $totalStats->kills = 0;
                    $totalStats->deaths = 0;
                    $totalStats->assists = 0;
                    foreach ($champions as $champ_key => $champion) {
                        $champions[$champ_key]['kda'] = $champion['kills'] + $champion['assists'];
                        $champions[$champ_key]['winRatio'] = 100;
                        $champions[$champ_key]['played'] = $champion['win'] + $champion['lose'];
                        $totalStats->played += $champions[$champ_key]['played'];
                        $totalStats->win += $champion['win'];
                        $totalStats->lose += $champion['lose'];
                        $totalStats->kills += $champion['kills'];
                        $totalStats->assists += $champion['assists'];
                        $totalStats->deaths += $champion['deaths'];
                        if ($champion['deaths'] > 0) {
                            $champions[$champ_key]['kda'] = number_format($champions[$champ_key]['kda'] / $champion['deaths'], 2, '.', '');
                        }
                        if (($champion['lose'] + $champion['win']) > 0) {
                            $champions[$champ_key]['winRatio'] = number_format(100 / ($champion['lose'] + $champion['win']) * $champion['win'], 2, '.', '');
                        }
                    }
                    $champions = $this->Request->championSort($champions, 'played', SORT_DESC);
                    $totalStats->kda = $totalStats->kills + $totalStats->assists;
                    if ($totalStats->deaths > 0) {
                        $totalStats->kda = number_format($totalStats->kda / $totalStats->deaths, 2, '.', '');
                    }
                    $totalStats->winRatio = 100;
                    if ($totalStats->lose > 0) {
                        $totalStats->winRatio = number_format(100 / ($totalStats->played) * $totalStats->win, 2, '.', '');
                    }


                    $totalStats->killParticipationRatio = 0;
                    if (($totalStats->kills + $totalStats->assists) > 0 && $totalAllSameTeamSummonerKills > 0) {
                        $totalStats->killParticipationRatio = number_format((($totalStats->kills + $totalStats->assists) * 100) / $totalAllSameTeamSummonerKills, 1, '.', '');
                    }
                    $totalStats->killParticipationRatioText = lang('site_p_kill') . ' ' . $totalStats->killParticipationRatio;

                    $totalStats->kills = $totalStats->kills > 0 ? $totalStats->kills / $totalStats->played : 0;
                    $totalStats->assists = $totalStats->assists > 0 ? $totalStats->assists / $totalStats->played : 0;
                    $totalStats->deaths = $totalStats->deaths > 0 ? $totalStats->deaths / $totalStats->played : 0;
                    $s = 0;
                    $_s = 0;
                    $championstmp = new stdClass();
                    foreach ($champions as $key => $value) {
                        $s++;
                        if ($s <= 4) {
                            $_s++;
                            $championstmp->{$key} = $value;
                            if ($value->played > 0) {
                                $championstmp->{$key}->killsAvg = number_format(($value->kills > 0 ? $value->kills / $value->played : 0), 2);
                                $championstmp->{$key}->deathsAvg = number_format(($value->deaths > 0 ? $value->deaths / $value->played : 0), 2);
                                $championstmp->{$key}->assistsAvg = number_format(($value->assists > 0 ? $value->assists / $value->played : 0), 2);
                                if (isset($totalAllSameTeamSummonerKillsChamps[$value->championKey])) {
                                    $championstmp->{$key}->killParticipationRatio = number_format((($value->kills + $value->assists) * 100) / $totalAllSameTeamSummonerKillsChamps[$value->championKey], 1, '.', '');
                                    $championstmp->{$key}->killParticipationRatioText = lang('site_p_kill') . ' ' . $championstmp->{$key}->killParticipationRatio;
                                }

                            }

                        }
                    }

                    arsort($data->role);
                    $data->role = array_slice($data->role, 0, 3, true);
                    $data->roleCount = count($data->role);

                    $data->championStats = $championstmp;
                    $data->championStatsCount = $_s;
                    $data->totalStats = $totalStats;
                    $data->tags = $tags;
                    $data->tagsTotal = $totalTags;
                    arsort($data->tags);

                    $this->db_read->where('region', strtolower($region));
                    $this->db_read->where('summoner_id', $summoner_id);
                    $signature_query = $this->db_read->get('signature_summary');
                    $data->signature = new stdClass();
                    $data->signature->status = false;
                    if ($signature_query->num_rows()) {
                        $signature = $signature_query->row();
                        $data->signature->status = true;
                        $data->signature->src = SIGNATURE_URL . 's/' . $signature->image_name . '.png';
                        $data->signature->lastUpdate = $signature->date_added;
                        $data->signature->lastUpdateSec = time() - strtotime($data->signature->lastUpdate);
                    }
                }
            } else { // hiç maç bulunamadıysa
                if (FALSE && $request->page == 1) {
                    $this->load->library('user_agent');
                    if ($this->agent->is_browser()) { // botlar işlem yapmasın
                        $this->db_read->where('date_added>', date('Y-m-d H:i:s', time() - (60 * 2)));
                        $total_check = $this->db_read->get('summoner_history_update_' . $this->lol->getRegion());
                        if ($total_check->num_rows() < 40) { // eğer 2 dakikada 40 maçtan az maç geçmişi getiriliyorsa
                            if (isset($history_update_num) && $history_update_num == 0) {
                                $this->updateSummonerMatchHistory();
                                die();
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }

    /**
     * Gets match history details of a match
     * @uses CI_Input::post($matchId) Match ID
     * @uses CI_Input::post($summonerId) Summoner ID
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($lang) Language
     */
    public function details()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        //$request = new stdClass();
        //$request->matchId = 521892655;
        //$request->summonerId = 4070743;
        //$request->region = 'tr';
        //$request->lang = 'en_US';

        $request = new stdClass();
        $request->matchId = $this->input->post('matchId');
        $request->summonerId = $this->input->post('summonerId');
        $request->region = $this->input->post('region');
        $request->lang = $this->input->post('lang');

        if (isset($request->matchId) && isset($request->region) && isset($request->lang) && isset($request->summonerId)) {
            $result = $this->matchhistory->getMatchHistory($request->region, array((int)$request->matchId), true);
            if (isset($result->{$request->matchId})) {
                $this->lol->setRegion($request->region);
                $this->lol->setLang($request->lang);

                if ($this->Request->isLanguageExist($request->lang)) {
                    $this->lang->load('site', mb_strtolower($request->lang));
                } else {
                    $this->lang->load('site', "en");
                }

                $this->load->model('Runes');

                $runes = $this->lol->getRunes();

                $match = $result->{$request->matchId};
                $data->status = true;

                $match_summoner_id = $request->summonerId;

                //$masteries = $this->lol->getMasteries();
                //$runes = $this->lol->getRunes();
                $items = $results = $this->lol->getItems();

                $stat_list = array(
                    'kills' => array('title' => lang('site_champion_kill')),
                    'goldEarned' => array('title' => lang('site_gold_earned')),
                    'totalDamageDealtToChampions' => array('title' => lang('site_damage_dealt_to_champions')),
                    'wardsPlaced' => array('title' => lang('site_wards_placed')),
                    'totalDamageTaken' => array('title' => lang('site_damage_taken')),
                    'totalMinionsKilled' => array('title' => 'CS', 'add' => 'neutralMinionsKilled')
                );

                $killmap_colors = array(
                    1 => 'red',
                    2 => 'white',
                    3 => 'blue',
                    4 => 'orange',
                    5 => 'pink',
                    6 => 'purple',
                    7 => 'green',
                    8 => 'gray',
                    9 => 'black',
                    10 => 'yellow'
                );

                //$runeTypeOrder = array('black' => 1,'red' => 2,'yellow' => 3,'blue' => 4);

                $ward_types = array(
                    'BLUE_TRINKET' => lang('site_farsight_alteration'),
                    'SIGHT_WARD' => lang('site_sightstone'),
                    'VISION_WARD' => lang('site_vision_ward'),
                    'YELLOW_TRINKET' => lang('site_warding_totem_trinket'),
                    'YELLOW_TRINKET_UPGRADE' => lang('site_warding totem_upgrade_trinket')
                );

                $skill_keys = array(1 => 'Q', 2 => 'W', 3 => 'E', 4 => 'R');

                $analysis = new stdClass();
                foreach ($stat_list as $stat_key => $stat) {
                    $analysis->{$stat_key} = new stdClass();
                    $analysis->{$stat_key}->title = $stat['title'];
                    $analysis->{$stat_key}->team1 = new stdClass();
                    $analysis->{$stat_key}->team2 = new stdClass();
                    $analysis->{$stat_key}->team1->summoners = new stdClass();
                    $analysis->{$stat_key}->team2->summoners = new stdClass();
                    $analysis->{$stat_key}->team1->percent = 0;
                    $analysis->{$stat_key}->team2->percent = 0;
                    $analysis->{$stat_key}->team1->total = 0;
                    $analysis->{$stat_key}->team2->total = 0;
                }

                $overview = new stdClass();
                $overview->team1 = new stdClass();
                $overview->team2 = new stdClass();
                $overview->team1->summoners = new stdClass();
                $overview->team2->summoners = new stdClass();

                $overview->team1->totalGold = 0;
                $overview->team2->totalGold = 0;
                $overview->team1->totalKills = 0;
                $overview->team2->totalKills = 0;
                $overview->team1->totalDeaths = 0;
                $overview->team2->totalDeaths = 0;
                $overview->team1->totalAssists = 0;
                $overview->team2->totalAssists = 0;

                $killmap = new stdClass();
                $match_timeline = array();
                $builds = new stdClass();
                $builds->items = new stdClass();
                $builds->skills = array();
                $builds->skillDetails = array();
                $builds->masteries = new stdClass();
                $builds->masteries->tree = new stdClass();
                $builds->masteries->cunning = 0;
                $builds->masteries->ferocity = 0;
                $builds->masteries->resolve = 0;
                $builds->runePage = array();
                //$builds->runes = array();
                $builds->perks = new stdClass();
                $builds->perks->perkStyle = new stdClass();
                $builds->perks->perkStyle->runes = array();
                $builds->perks->perkSubStyle = new stdClass();
                $builds->perks->perkSubStyle->runes = array();

                $timeline_team_stats = new stdClass();
                $timeline_team_stats->totalGolds = new stdClass();
                $timeline_team_stats->totalGolds->team1 = new stdClass();
                $timeline_team_stats->totalGolds->team2 = new stdClass();

                $participant_id_summoner_id = array();

                $overview->team1->mostDamageDealtToChampions = 0;
                $overview->team2->mostDamageDealtToChampions = 0;

                foreach ($match->participants as $participant_key => $participant) {
                    $summoner = new stdClass();

                    if (!isset($participant->identity->summonerId)) {
                        $summoner->summonerId = $participant_key;
                        $summoner->summonerName = '';
                    } else {
                        $summoner->summonerId = $participant->identity->summonerId;
                        $summoner->summonerName = $participant->identity->summonerName;
                    }

                    $participant_id_summoner_id[$participant->participantId] = $summoner->summonerId;

                    $champ = $this->lol->getChampionById2($participant->championId);
                    $summoner->championKey = $champ->key;
                    $summoner->championName = $champ->name;

                    $summoner->team = "team" . ($participant->teamId == '100' ? 1 : 2);
                    $summoner->tier = '';//isset($participant->highestAchievedSeasonTier) ? $participant->highestAchievedSeasonTier : '';
                    $summoner->championLevel = $participant->stats->champLevel;
                    $summoner->championLevelText = lang('site_level') . ' ' . $summoner->championLevel;
                    $summoner->largestKillingSpree = $participant->stats->largestKillingSpree;
                    $summoner->largestMultiKill = $participant->stats->largestMultiKill;
                    $summoner->assists = $participant->stats->assists;
                    $summoner->visionWardsBought = $participant->stats->visionWardsBoughtInGame;
                    $summoner->sightWardsBought = $participant->stats->sightWardsBoughtInGame;
                    $summoner->wardsPlaced = isset($participant->stats->wardsPlaced) ? $participant->stats->wardsPlaced : 0;
                    $summoner->wardsKilled = isset($participant->stats->wardsKilled) ? $participant->stats->wardsKilled : 0;
                    $summoner->goldEarned = $participant->stats->goldEarned;
                    $summoner->totalDamageDealt = $participant->stats->totalDamageDealt;
                    $summoner->totalDamageDealtToChampions = $participant->stats->totalDamageDealtToChampions;
                    $overview->{$summoner->team}->mostDamageDealtToChampions = ($participant->stats->totalDamageDealtToChampions > $overview->{$summoner->team}->mostDamageDealtToChampions) ? $participant->stats->totalDamageDealtToChampions : $overview->{$summoner->team}->mostDamageDealtToChampions;
                    $summoner->deaths = $participant->stats->deaths;
                    $summoner->kills = $participant->stats->kills;
                    $summoner->kdaRatio = $summoner->kills + $summoner->assists;
                    if ($summoner->deaths > 0) {
                        $summoner->kdaRatio = $summoner->kdaRatio / $summoner->deaths;
                    }
                    $summoner->kdaRatio = number_format($summoner->kdaRatio, 1, '.', '');
                    $summoner->minionKills = $participant->stats->totalMinionsKilled + $participant->stats->neutralMinionsKilled;
                    $summoner->neutralMinionKills = $participant->stats->neutralMinionsKilled;
                    if ($match->gameDuration > 0) {
                        $summoner->minionKillsPerMin = number_format(($summoner->minionKills) / ($match->gameDuration / 60), 1, '.', '');
                    } else {
                        $summoner->minionKillsPerMin = number_format($summoner->minionKills, 1, '.', '');
                    }

                    $overview->{$summoner->team}->totalGold += $participant->stats->goldEarned;
                    $overview->{$summoner->team}->totalKills += $participant->stats->kills;
                    $overview->{$summoner->team}->totalAssists += $participant->stats->assists;
                    $overview->{$summoner->team}->totalDeaths += $participant->stats->deaths;

                    $summoner->perkStyleId = false;
                    $summoner->perkStyleName = false;
                    $summoner->perkStyleDescription = false;
                    $summoner->perkSubStyleId = false;
                    $summoner->perkSubStyleName = false;
                    $summoner->perkSubStyleDescription = false;
                    $summoner->perkSubStyleBonus = false;

                    $summoner->perks = new stdClass();
                    $summoner->perks->perkStyle = new stdClass();
                    $summoner->perks->perkStyle->id = false;
                    $summoner->perks->perkStyle->name = false;
                    $summoner->perks->perkStyle->description = false;
                    $summoner->perks->perkStyle->runes = array();
                    $summoner->perks->perkSubStyle = new stdClass();
                    $summoner->perks->perkSubStyle->id = false;
                    $summoner->perks->perkSubStyle->name = false;
                    $summoner->perks->perkSubStyle->description = false;
                    $summoner->perks->perkSubStyle->bonus = false;
                    $summoner->perks->perkSubStyle->runes = array();

                    if (isset($participant->stats) && isset($participant->stats->perkPrimaryStyle)) {

                        if (mb_strtolower(trim($request->lang)) == 'tr') {
                            $perk_style = $this->Runes->perk_styles_tr[$participant->stats->perkPrimaryStyle];
                            $perk_sub_style = $this->Runes->perk_styles_tr[$participant->stats->perkSubStyle];
                        } else {
                            $perk_style = $this->Runes->perk_styles_en[$participant->stats->perkPrimaryStyle];
                            $perk_sub_style = $this->Runes->perk_styles_en[$participant->stats->perkSubStyle];
                        }

                        $summoner->perks->perkStyle->id = $participant->stats->perkPrimaryStyle;
                        $summoner->perks->perkStyle->name = $perk_style['name'];
                        $summoner->perks->perkStyle->description = $perk_style['description'];
                        $summoner->perks->perkSubStyle->id = $participant->stats->perkSubStyle;
                        $summoner->perks->perkSubStyle->name = $perk_sub_style['name'];
                        $summoner->perks->perkSubStyle->description = $perk_sub_style['description'];
                        $summoner->perks->perkSubStyle->bonus = $perk_style['combinations'][$participant->stats->perkSubStyle];

                        if ($summoner->summonerId == $match_summoner_id) {
                            $builds->perks->perkStyle->id = $participant->stats->perkPrimaryStyle;
                            $builds->perks->perkStyle->name = $perk_style['name'];
                            $builds->perks->perkStyle->description = $perk_style['description'];
                            $builds->perks->perkSubStyle->id = $participant->stats->perkSubStyle;
                            $builds->perks->perkSubStyle->name = $perk_sub_style['name'];
                            $builds->perks->perkSubStyle->description = $perk_sub_style['description'];
                            $builds->perks->perkSubStyle->bonus = $perk_style['combinations'][$participant->stats->perkSubStyle];
                        }


                        for ($perk_i = 0; $perk_i <= 5; $perk_i++) {
                            if (isset($participant->stats->{'perk' . $perk_i})) {
                                $perk_id = $participant->stats->{'perk' . $perk_i};
                                if (isset($runes[$perk_id])) {
                                    $perk = $runes[$perk_id];
                                    if ($summoner->summonerId == $match_summoner_id) {
                                        if ($perk['style_id'] == $participant->stats->perkPrimaryStyle) {
                                            $builds->perks->perkStyle->runes[] = array(
                                                'details' => $perk,
                                                'score' => $participant->stats->{'perk' . $perk_i . 'Var1'}
                                            );
                                        } else {
                                            $builds->perks->perkSubStyle->runes[] = array(
                                                'details' => $perk,
                                                'score' => $participant->stats->{'perk' . $perk_i . 'Var1'}
                                            );
                                        }
                                    }
                                    if ($perk['style_id'] == $participant->stats->perkPrimaryStyle) {
                                        $summoner->perks->perkStyle->runes[] = $perk;
                                    } else {
                                        $summoner->perks->perkSubStyle->runes[] = $perk;
                                    }
                                }
                            }
                        }

                    }


                    if ($participant->spell1Id > 0) {
                        $spell1 = $this->lol->getSpellById($participant->spell1Id);
                        $summoner->spell1Name = $spell1->name;
                        $summoner->spell1Description = $spell1->description;
                        $summoner->spell1Key = $this->lol->cleanText($spell1->key);
                    } else {
                        $summoner->spell1Name = '';
                        $summoner->spell1Description = '';
                        $summoner->spell1Key = '';
                    }

                    if ($participant->spell2Id > 0) {
                        $spell2 = $this->lol->getSpellById($participant->spell2Id);
                        $summoner->spell2Name = $spell2->name;
                        $summoner->spell2Description = $spell2->description;
                        $summoner->spell2Key = $this->lol->cleanText($spell2->key);
                    } else {
                        $summoner->spell2Name = '';
                        $summoner->spell2Description = '';
                        $summoner->spell2Key = '';
                    }

                    $summoner->items = array();
                    for ($i = 0; $i <= 6; $i++) { // 6 iteme de aynı işlemi yap
                        $summoner->items[$i] = $participant->stats->{'item' . $i};
                        if (isset($items->{$summoner->items[$i]})) {
                            $item = $items->{$summoner->items[$i]};
                            $summoner->items[$i] = new stdClass();
                            $summoner->items[$i]->name = $item->name;
                            $summoner->items[$i]->description = $item->description;
                            $summoner->items[$i]->image = $item->image->full;
                            $summoner->items[$i]->gold = $item->gold;
                        } else {
                            $summoner->items[$i] = 0;
                        }
                    }


                    /*
                    $summoner->masteryKey = '';
                    $summoner->masteryName = '';
                    $summoner->masteryDescription = '';
                    if (isset($participant->masteries) && count($participant->masteries) > 0) {
                        foreach ($participant->masteries as $masteries_key => $mastery) {
                            $selected_masteries[$mastery->masteryId] = $mastery->rank;
                            if ($summoner->summonerId == $match_summoner_id) {
                                $selected_masteries_main[$mastery->masteryId] = $mastery->rank;
                            }
                            if (isset($masteries->{$mastery->masteryId})) {
                                $mastery_details = $masteries->{$mastery->masteryId};
                                if ($match_summoner_id == $summoner->summonerId) { // Sadece belirtilen summoner için mastery puanları
                                    if ($mastery_details->tree == 'Ferocity') {
                                        $builds->masteries->ferocity += $mastery->rank;
                                    } elseif ($mastery_details->tree == 'Cunning') {
                                        $builds->masteries->cunning += $mastery->rank;
                                    } elseif ($mastery_details->tree == 'Resolve') {
                                        $builds->masteries->resolve += $mastery->rank;
                                    }
                                }
                                if ($mastery_details->last) {
                                    $summoner->masteryKey = "mastery-" . $mastery->masteryId;
                                    $summoner->masteryName = $mastery_details->name;
                                    $summoner->masteryDescription = $mastery_details->description[$mastery_details->ranks - 1];
                                }
                            }
                        }
                    }
                    */


                    // Team analysis
                    foreach ($stat_list as $stat_key => $stat) {
                        $add = 0;
                        if (isset($stat['add'])) {
                            $add = $participant->stats->{$stat['add']};
                        }
                        if (isset($participant->stats->{$stat_key})) {
                            $analysis->{$stat_key}->{$summoner->team}->summoners->{$summoner->summonerId} = ($participant->stats->{$stat_key} + $add);
                            $analysis->{$stat_key}->{$summoner->team}->total += ($participant->stats->{$stat_key} + $add);
                        }
                    }

                    $overview->{$summoner->team}->summoners->{$summoner->summonerId} = $summoner;

                    // Kill Map
                    $killmap->{$summoner->summonerId} = new stdClass();
                    $killmap->{$summoner->summonerId}->summoner = new stdClass();
                    $killmap->{$summoner->summonerId}->kills = array();
                    $killmap->{$summoner->summonerId}->deaths = array();
                    $killmap->{$summoner->summonerId}->summoner->color = $killmap_colors[$participant->participantId];
                    $killmap->{$summoner->summonerId}->summoner->team = $summoner->team;
                    $killmap->{$summoner->summonerId}->summoner->status = $participant->stats->win == "Win" ? lang('site_winner_team') : lang('site_loser_team');
                    $killmap->{$summoner->summonerId}->summoner->winner = $participant->stats->win ? true : false;
                    $killmap->{$summoner->summonerId}->summoner->summonerName = $summoner->summonerName;
                    $killmap->{$summoner->summonerId}->summoner->championKey = $summoner->championKey;

                    // Builds > Skills
                    if ($summoner->summonerId == $match_summoner_id) {
                        $match_summoner_champ = $champ;
                    }


                    /*
                    // Summoner Masteries ve Runes
                    if ($summoner->summonerId == $match_summoner_id) {
                        if (isset($participant->masteries) && count($participant->masteries) > 0) {
                            foreach ($masteries->tree as $tree_title => $tree) {
                                $builds->masteries->tree->{$tree_title} = array();
                                foreach ($tree as $tree_row => $mastery_col) {
                                    $builds->masteries->tree->{$tree_title}[$tree_row] = array();
                                    foreach ($mastery_col as $mastery_row => $mastery) {
                                        if (isset($masteries->{$mastery->masteryId})) {
                                            $mastery_details_ = $masteries->{$mastery->masteryId};
                                            $mastery_ = new stdClass();
                                            $mastery_->key = "mastery-" . $mastery->masteryId;
                                            $mastery_->name = $mastery_details_->name;
                                            $mastery_->description = $mastery_details_->description[(isset($selected_masteries_main[$mastery->masteryId]) ? ($selected_masteries_main[$mastery->masteryId] - 1) : 0)];
                                            $mastery_->ranks = $mastery_details_->ranks;
                                            $mastery_->selectedRank = (isset($selected_masteries_main[$mastery->masteryId]) ? ($selected_masteries_main[$mastery->masteryId]) : 0);
                                            $builds->masteries->tree->{$tree_title}[$tree_row][$mastery_row] = $mastery_;
                                        }
                                    }
                                }
                            }
                        }
                        if (isset($participant->runes) && count($participant->runes) > 0) {
                            $runepagetotaltmp = array();
                            foreach ($participant->runes as $rune_key => $rune) {
                                $rune_details = $runes->{$rune->runeId};
                                //$builds->runes[$rune->runeId] = $rune_details->description.' (x'.$rune->rank.')';
                                $runepagetotaltmp[$rune->runeId] = array('description' => $rune_details->description, 'count' => $rune->rank);
                                $builds->runePage[$rune->runeId] = new stdClass();
                                $builds->runePage[$rune->runeId]->count = $rune->rank;
                                $builds->runePage[$rune->runeId]->description = $rune_details->description;
                                $builds->runePage[$rune->runeId]->name = $rune_details->name;
                                $builds->runePage[$rune->runeId]->tier = $rune_details->rune->tier;
                                $builds->runePage[$rune->runeId]->type = $rune_details->rune->type;
                                $builds->runePage[$rune->runeId]->order = $runeTypeOrder[$rune_details->rune->type];
                                $builds->runePage[$rune->runeId]->image = $rune_details->image->full;
                                $builds->runePage[$rune->runeId]->runeId = $rune->runeId;
                            }
                            $builds->runesTotal = $this->lol->runeStatsTotalFromDescription($runepagetotaltmp);
                            unset($runepagetotaltmp);
                        }
                    }
                    */

                }

                foreach ($match->teams as $team) {
                    $teamkey = "team" . ($team->teamId == '100' ? 1 : 2);
                    $overview->{$teamkey}->status = $team->win == 'Win' ? lang('site_winner_team') : lang('site_loser_team');
                    $overview->{$teamkey}->teamKey = $teamkey;
                    $overview->{$teamkey}->winner = $team->win == 'Win' ? true : false;
                    $overview->{$teamkey}->inhibitorKills = $team->inhibitorKills;
                    $overview->{$teamkey}->towerKills = $team->towerKills;
                    $overview->{$teamkey}->baronKills = $team->baronKills;
                    $overview->{$teamkey}->dragonKills = $team->dragonKills;
                    $overview->{$teamkey}->riftHeraldKills = isset($team->riftHeraldKills) ? $team->riftHeraldKills : 0;
                    $overview->{$teamkey}->vilemawKills = $team->vilemawKills;
                }

                foreach ($overview as $team_key => $team) {
                    foreach ($team->summoners as $summoner) {
                        $ratio = 0;
                        if (($summoner->kills + $summoner->assists) > 0 && $overview->{$summoner->team}->totalKills > 0) {
                            $ratio = number_format((($summoner->kills + $summoner->assists) * 100) / $overview->{$summoner->team}->totalKills, 0, '.', '');
                        }
                        $overview->{$summoner->team}->summoners->{$summoner->summonerId}->killParticipationRatio = $ratio;
                        $overview->{$summoner->team}->summoners->{$summoner->summonerId}->killParticipationRatioText = lang('site_p_kill') . $ratio;
                    }
                }

                foreach ($analysis as $stat_key => $analysis_) {
                    $total = $analysis_->team1->total + $analysis_->team2->total;
                    if ($total > 0) {
                        $team1percent = 100 / $total * $analysis_->team1->total;
                        $analysis->{$stat_key}->team1->percent = $team1percent;
                        $analysis->{$stat_key}->team2->percent = 100 - $team1percent;
                    }
                }

                $skill_counter = 1;

                $firstSkills[1] = false;
                $firstSkills[2] = false;
                $firstSkills[3] = false;
                $firstSkills[4] = false;


                if (!isset($match->timeline)) { //timeline yoksa ve api kontrol edilmemişse kontrol et
                    $region_lower = $this->lol->getRegion();
                    $timeline_q = $this->db_read->get_where('match_history_' . $region_lower, array('game_id' => (int)$match->gameId, 'timeline_status' => 0));
                    if ($timeline_q->num_rows()) {
                        $timeline_req = $this->lol->api('lol/match/v3/timelines/by-match/' . $match->gameId, '', 5);
                        if (isset($timeline_req->frames)) {
                            $match->timeline = $timeline_req;
                            $this->matchhistory->addMatchHistoryTimeline($request->region, $match->gameId, $timeline_req);
                            $this->db->where('game_id', (int)$match->gameId);
                            $this->db->update('match_history_' . $region_lower, array('timeline_status' => 1));
                        } else if (isset($timeline_req->status) && isset($timeline_req->status->status_code) && $timeline_req->status->status_code == 404) {
                            $this->db->where('game_id', (int)$match->gameId);
                            $this->db->update('match_history_' . $region_lower, array('timeline_status' => 1));
                        }

                    }
                }

                if (isset($match->timeline) && isset($match->timeline->frames)) { // Timeline varsa
                    $first_blood = true;
                    $first_tower = true;
                    foreach ($match->timeline->frames as $timeline) {
                        $pframe = $timeline->participantFrames;
                        $min = $this->Request->timestampToMin($timeline->timestamp);
                        foreach ($pframe as $frame) {
                            $summoner_id_ = $participant_id_summoner_id[$frame->participantId];
                            $team_ = $killmap->{$summoner_id_}->summoner->team;

                            if (!isset($timeline_team_stats->totalGolds->{$team_}->{$min})) {
                                $timeline_team_stats->totalGolds->{$team_}->{$min} = 0;
                            }
                            $timeline_team_stats->totalGolds->{$team_}->{$min} += $frame->totalGold;
                        }
                        if (isset($timeline->events)) {
                            $events = $timeline->events;
                            foreach ($events as $event) {
                                if ($event->type == 'CHAMPION_KILL') {//şampiyon öldürme
                                    $timeline_entry = new stdClass();

                                    $victim_summoner_id = $participant_id_summoner_id[$event->victimId];
                                    $killmap->{$victim_summoner_id}->deaths[] = $event->position;
                                    $timeline_entry->victimName = $killmap->{$victim_summoner_id}->summoner->summonerName;
                                    $timeline_entry->victimChampionKey = $killmap->{$victim_summoner_id}->summoner->championKey;

                                    if ($event->killerId > 0) {
                                        $killer_summoner_id = $participant_id_summoner_id[$event->killerId];
                                        $killmap->{$killer_summoner_id}->kills[] = $event->position;
                                        $timeline_entry->killerTeam = $killmap->{$killer_summoner_id}->summoner->team;
                                        $timeline_entry->killerName = $killmap->{$killer_summoner_id}->summoner->summonerName;
                                        $timeline_entry->killerChampionKey = $killmap->{$killer_summoner_id}->summoner->championKey;
                                    } else {
                                        $timeline_entry->killerName = ($killmap->{$victim_summoner_id}->summoner->winner ? lang('site_winner_team') : lang('site_loser_team')) . ' Tower';
                                    }

                                    $timeline_entry->time = $this->Request->timestampToMinSec($event->timestamp);
                                    $timeline_entry->type = 'Kill';
                                    $timeline_entry->firstBlood = false;
                                    if ($first_blood == true) {
                                        $timeline_entry->firstBlood = true;
                                        $first_blood = false;
                                    }
                                    $match_timeline[] = $timeline_entry;
                                } else if ($event->type == 'WARD_KILL' && $event->wardType != 'UNDEFINED' && $event->wardType != 'TEEMO_MUSHROOM') { // Ward öldürme
                                    $killer_summoner_id = $participant_id_summoner_id[$event->killerId];
                                    $timeline_entry = new stdClass();
                                    $timeline_entry->time = $this->Request->timestampToMinSec($event->timestamp);
                                    $timeline_entry->type = 'Wards';
                                    $timeline_entry->team = $killmap->{$killer_summoner_id}->summoner->team;
                                    $timeline_entry->killerName = $killmap->{$killer_summoner_id}->summoner->summonerName;
                                    $timeline_entry->killerChampionKey = $killmap->{$killer_summoner_id}->summoner->championKey;
                                    $timeline_entry->wardType = isset($ward_types[$event->wardType]) ? $ward_types[$event->wardType] : $event->wardType;
                                    $match_timeline[] = $timeline_entry;
                                } else if ($event->type == 'BUILDING_KILL') { // Kule öldürme
                                    $timeline_entry = new stdClass();
                                    $timeline_entry->time = $this->Request->timestampToMinSec($event->timestamp);
                                    $timeline_entry->type = lang('site_tower');
                                    $timeline_entry->towerTeam = $event->teamId == 100 ? 'team1' : 'team2';
                                    $timeline_entry->buildingType = $event->buildingType == 'TOWER_BUILDING' ? lang('site_tower') : lang('site_inhibitor');
                                    $timeline_entry->description = $overview->{$timeline_entry->towerTeam}->status . ' ' . $timeline_entry->buildingType;
                                    if ($event->killerId > 0) {
                                        $killer_summoner_id = $participant_id_summoner_id[$event->killerId];
                                        $timeline_entry->killerTeam = $killmap->{$killer_summoner_id}->summoner->team;
                                        $timeline_entry->killerName = $killmap->{$killer_summoner_id}->summoner->summonerName;
                                        $timeline_entry->killerChampionKey = $killmap->{$killer_summoner_id}->summoner->championKey;
                                    } else {
                                        $timeline_entry->killerName = $overview->{$timeline_entry->towerTeam}->status . lang('site_minions');
                                    }
                                    $timeline_entry->firstBlood = false;
                                    if ($first_tower == true) {
                                        $timeline_entry->firstTower = true;
                                        $first_tower = false;
                                    }
                                    $match_timeline[] = $timeline_entry;
                                } else if ($event->type == 'ITEM_PURCHASED' || $event->type == 'ITEM_SOLD') { // Item alım satım
                                    $summoner_id_ = $participant_id_summoner_id[$event->participantId];
                                    if ($summoner_id_ == $match_summoner_id) {
                                        $time = $this->Request->timestampToMin($event->timestamp);
                                        if (!isset($builds->items->{$time})) {
                                            $builds->items->{$time} = new stdClass();
                                        }
                                        if (!isset($builds->items->{$time}->list)) {
                                            $builds->items->{$time}->time = $time;
                                            $builds->items->{$time}->list = array();
                                        }
                                        if (isset($builds->items->{$time}->list[$event->itemId . '_' . $event->type])) {
                                            $builds->items->{$time}->list[$event->itemId . '_' . $event->type]->count++;
                                        } else {
                                            if (isset($items->{$event->itemId})) {
                                                $item_ = $items->{$event->itemId};
                                                $item = new stdClass();
                                                $item->type = $event->type;
                                                $item->count = 1;
                                                $item->timestamp = $event->timestamp;
                                                $item->name = $item_->name;
                                                $item->description = $item_->description;
                                                $item->image = $item_->image;
                                                $item->gold = $item_->gold;
                                                $builds->items->{$time}->list[$event->itemId . '_' . $event->type] = $item;
                                            }
                                        }
                                    }
                                } else if ($event->type == 'SKILL_LEVEL_UP') { //Skill seviye yükseltme
                                    $summoner_id_ = $participant_id_summoner_id[$event->participantId];
                                    if ($summoner_id_ == $match_summoner_id) {
                                        if ($event->levelUpType == 'NORMAL') {
                                            $builds->skills[$skill_counter] = array('slot' => $event->skillSlot, 'key' => $skill_keys[$event->skillSlot]);
                                            if ($firstSkills[$event->skillSlot] == false && isset($match_summoner_champ)) {
                                                $firstSkills[$event->skillSlot] = array();
                                                $skill_details = $match_summoner_champ->spells[$event->skillSlot - 1];
                                                $firstSkills[$event->skillSlot]['key'] = $skill_keys[$event->skillSlot];
                                                $firstSkills[$event->skillSlot]['name'] = $skill_details->name;
                                                $firstSkills[$event->skillSlot]['description'] = $skill_details->description;
                                                $firstSkills[$event->skillSlot]['cooldown'] = $skill_details->cooldownBurn;
                                                if (isset($skill_details->resource)) {
                                                    $firstSkills[$event->skillSlot]['cost'] = str_replace('{{ cost }}', $skill_details->costBurn, $skill_details->resource);
                                                } else {
                                                    $firstSkills[$event->skillSlot]['cost'] = '';
                                                }
                                                $firstSkills[$event->skillSlot]['range'] = $skill_details->rangeBurn;
                                                $firstSkills[$event->skillSlot]['image'] = $skill_details->image->full;
                                                $builds->skillDetails[$skill_keys[$event->skillSlot]] = $firstSkills[$event->skillSlot];
                                            }
                                            $skill_counter++;
                                        } else {
                                            $builds->skills[$skill_counter - 1]['evolve_slot'] = $event->skillSlot;
                                            $builds->skills[$skill_counter - 1]['evolve_key'] = $skill_keys[$event->skillSlot];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //$data->test = $match;
                $data->teamStats = false;//$timeline_team_stats;
                $data->builds = $builds;
                $data->overview = $overview;
                $data->analysis = false;//$analysis;
                $data->killmap = false;//$killmap;
                $data->timeline = false;//$match_timeline;
            }
        }

        echo json_encode($data);
    }

    /**
     * Deletes old matches
     * @param int $limit
     */
    public function cronclear($limit = 10)
    {
        // Eski
        if (true) {
            die();
        }
        $this->db->from('match_history');
        $this->db->where('status', 1);
        $this->db->where('start_time<', (time() - (60 * 60 * 24 * 30)));
        $this->db->limit((int)$limit);
        $query = $this->db->get();
        $delete_count = 0;
        if ($query->num_rows()) {
            echo 'Limit: ' . $limit;
            echo '<br>';
            echo 'Found: ' . $query->num_rows();
            echo '<br>';
            $results = $query->result();
            foreach ($results as $result) {
                $game_id = (int)$result->game_id;
                $region = $result->region;
                $deleted = $this->matchhistory->deleteMatchHistory($region, $game_id);
                if ($deleted) {
                    $this->db->where('region', $region);
                    $this->db->where('game_id', $game_id);
                    $this->db->update('match_history', array('status' => 0));
                    $delete_count++;
                    if ($delete_count >= $limit) {
                        break;
                    }
                }

                usleep(1000);
            }
            echo 'Removed: ' . $delete_count;
        }
    }


    /**
     * Crates summoner signature
     * @uses CI_Input::post($summonerId) Summoner ID
     * @uses CI_Input::post($accountId) Account ID
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($lang) Language
     */
    public function signature()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data2 = new stdClass();
        $data->status = false;
        $data2->status = false;

        $request = new stdClass();
        $request->accountId = $this->input->post('accountId');
        $request->summonerId = $this->input->post('summonerId');
        $request->region = $this->input->post('region');
        $request->lang = $this->input->post('lang');


        if (isset($request->summonerId) && isset($request->accountId) && isset($request->region) && isset($request->lang)) {
            $summoner_id = $request->summonerId;
            $account_id = $request->accountId;
            $region = mb_strtoupper($request->region);
            $this->lol->setRegion($region);
            $this->lol->setLang($request->lang);
            $reg = $this->lol->getRegion();

            $this->db_read->where('region', $reg);
            $this->db_read->where('summoner_id', $summoner_id);
            $signature_query = $this->db_read->get('signature_summary');
            if ($signature_query->num_rows()) {
                $signature = $signature_query->row();
            }
            if (isset($signature) && (time() - strtotime($signature->date_added)) < 3600) {
                $data2->status = true;
                $data2->src = SIGNATURE_URL . 's/' . $signature->image_name . '.png';
                $data2->lastUpdate = $signature->date_added;
                $data2->lastUpdateSec = time() - strtotime($data2->lastUpdate);
            } else {
                $summoner_signature = array();

                if ($this->Request->isLanguageExist($request->lang)) {
                    $this->lang->load('site', mb_strtolower($request->lang));
                } else {
                    $this->lang->load('site', "en");
                }

                $game_type_filter = false;
                $match_per_page = 20;
                $page_start = 0;

                $matches = $this->lol->getSummonerMatches($account_id, $match_per_page, $page_start, $game_type_filter);
                $champions = array();

                $totalAllSameTeamSummonerKills = 0;
                $totalAllSameTeamSummonerKillsChamps = array();

                if ($matches) {
                    foreach ($matches as $match_) {
                        if (!isset($match_->match)) {
                            continue;
                        }
                        $data->status = true;
                        $match = $match_->match;
                        $matchdata = new stdClass();

                        $summoner_details = $this->lol->api("lol/summoner/v3/summoners/" . $summoner_id, '', 86400);


                        $matchdata->team1 = new stdClass();
                        $matchdata->team2 = new stdClass();
                        $matchdata->team1->summoners = new stdClass();
                        $matchdata->team2->summoners = new stdClass();

                        $matchdata->team1->totalKills = 0;
                        $matchdata->team2->totalKills = 0;

                        foreach ($match->participants as $participant_key => $participant) {
                            $summoner = new stdClass();
                            if (!isset($participant->identity->summonerId)) {
                                $summoner->summonerId = $participant_key;
                                $summoner->summonerName = '';
                            } else {
                                $summoner->summonerId = $participant->identity->summonerId;
                                $summoner->summonerName = $participant->identity->summonerName;
                            }

                            $champ = $this->lol->getChampionById2($participant->championId);
                            $summoner->championKey = $champ->key;
                            $summoner->championName = $champ->name;
                            $summoner->team = "team" . ($participant->teamId == '100' ? 1 : 2);

                            $matchdata->{$summoner->team}->summoners->{$summoner->summonerId} = $summoner;

                            $matchdata->{$summoner->team}->totalKills += $participant->stats->kills;

                            if ($summoner->summonerId == $summoner_id) {
                                $msummoner = new stdClass();

                                $champ = $this->lol->getChampionById2($participant->championId);

                                if (!isset($champions[$champ->id])) {
                                    $champions[$champ->id] = array();
                                    $champions[$champ->id]['kills'] = 0;
                                    $champions[$champ->id]['deaths'] = 0;
                                    $champions[$champ->id]['assists'] = 0;
                                    $champions[$champ->id]['win'] = 0;
                                    $champions[$champ->id]['lose'] = 0;
                                    $champions[$champ->id]['championKey'] = $champ->key;
                                    $champions[$champ->id]['championName'] = $champ->name;
                                }

                                $champions[$champ->id]['kills'] += $participant->stats->kills;
                                $champions[$champ->id]['deaths'] += $participant->stats->deaths;
                                $champions[$champ->id]['assists'] += $participant->stats->assists;
                                $champions[$champ->id]['win'] += $participant->stats->win == 'Win' ? 1 : 0;
                                $champions[$champ->id]['lose'] += $participant->stats->win == 'Win' ? 0 : 1;

                                $msummoner->team = "team" . ($participant->teamId == '100' ? 1 : 2);
                                $msummoner->assists = $participant->stats->assists;
                                $msummoner->deaths = $participant->stats->deaths;
                                $msummoner->kills = $participant->stats->kills;
                                $msummoner->championKey = $champ->key;
                            }
                        }
                        if (isset($msummoner)) {
                            $msummoner->killParticipationRatio = 0;
                            if (($msummoner->kills + $msummoner->assists) > 0 && $matchdata->{$msummoner->team}->totalKills > 0) {
                                $msummoner->killParticipationRatio = number_format((($msummoner->kills + $msummoner->assists) * 100) / $matchdata->{$msummoner->team}->totalKills, 1, '.', '');
                                $totalAllSameTeamSummonerKills += $matchdata->{$msummoner->team}->totalKills;
                                if (!isset($totalAllSameTeamSummonerKillsChamps[$msummoner->championKey])) {
                                    $totalAllSameTeamSummonerKillsChamps[$msummoner->championKey] = 0;
                                }
                                $totalAllSameTeamSummonerKillsChamps[$msummoner->championKey] += $matchdata->{$msummoner->team}->totalKills;
                            }
                        }

                    }

                    $totalStats = new stdClass();
                    $totalStats->win = 0;
                    $totalStats->lose = 0;
                    $totalStats->played = 0;
                    $totalStats->kills = 0;
                    $totalStats->deaths = 0;
                    $totalStats->assists = 0;
                    foreach ($champions as $champ_key => $champion) {
                        $champions[$champ_key]['kda'] = $champion['kills'] + $champion['assists'];
                        $champions[$champ_key]['winRatio'] = 100;
                        $champions[$champ_key]['played'] = $champion['win'] + $champion['lose'];
                        $totalStats->played += $champions[$champ_key]['played'];
                        $totalStats->win += $champion['win'];
                        $totalStats->lose += $champion['lose'];
                        $totalStats->kills += $champion['kills'];
                        $totalStats->assists += $champion['assists'];
                        $totalStats->deaths += $champion['deaths'];
                        if ($champion['deaths'] > 0) {
                            $champions[$champ_key]['kda'] = number_format($champions[$champ_key]['kda'] / $champion['deaths'], 2, '.', '');
                        }
                        if (($champion['lose'] + $champion['win']) > 0) {
                            $champions[$champ_key]['winRatio'] = number_format(100 / ($champion['lose'] + $champion['win']) * $champion['win'], 2, '.', '');
                        }
                    }
                    $champions = $this->Request->championSort($champions, 'played', SORT_DESC);
                    $totalStats->kda = $totalStats->kills + $totalStats->assists;
                    if ($totalStats->deaths > 0) {
                        $totalStats->kda = number_format($totalStats->kda / $totalStats->deaths, 2, '.', '');
                    }
                    $totalStats->winRatio = 100;
                    if ($totalStats->lose > 0) {
                        $totalStats->winRatio = number_format(100 / ($totalStats->played) * $totalStats->win, 2, '.', '');
                    }


                    $totalStats->killParticipationRatio = 0;
                    if (($totalStats->kills + $totalStats->assists) > 0 && $totalAllSameTeamSummonerKills > 0) {
                        $totalStats->killParticipationRatio = number_format((($totalStats->kills + $totalStats->assists) * 100) / $totalAllSameTeamSummonerKills, 1, '.', '');
                    }

                    $totalStats->kills = $totalStats->kills / $totalStats->played;
                    $totalStats->assists = $totalStats->assists / $totalStats->played;
                    $totalStats->deaths = $totalStats->deaths / $totalStats->played;
                    $s = 0;
                    $championstmp = array();
                    foreach ($champions as $key => $value) {
                        $s++;
                        if ($s <= 2) {
                            $championstmp[$key] = $value;
                            if ($value->played > 0) {
                                $championstmp[$key]->killsAvg = number_format(($value->kills > 0 ? $value->kills / $value->played : 0), 2);
                                $championstmp[$key]->deathsAvg = number_format(($value->deaths > 0 ? $value->deaths / $value->played : 0), 2);
                                $championstmp[$key]->assistsAvg = number_format(($value->assists > 0 ? $value->assists / $value->played : 0), 2);
                                if (isset($totalAllSameTeamSummonerKillsChamps[$value->championKey])) {
                                    $championstmp[$key]->killParticipationRatio = number_format((($value->kills + $value->assists) * 100) / $totalAllSameTeamSummonerKillsChamps[$value->championKey], 1, '.', '');
                                } else {
                                    $championstmp[$key]->killParticipationRatio = 0;
                                }

                            }

                        }
                    }

                    $data->championStats = $championstmp;
                    $data->totalStats = $totalStats;

                }
            }
        }
        if ($data->status == true && isset($reg) && isset($summoner_id) && isset($summoner_details) && isset($region)) {


            $league_query = $this->db_read->query("SELECT * FROM summoner_league_" . $reg . "_1 WHERE summoner_id = '" . (int)$summoner_id . "'");
            if ($league_query->num_rows()) {
                $league = $league_query->row();
                if ($league->tier >= 0 && $league->tier <= 6) {
                    $summoner_signature['league'] = $this->lol->tierToNumber($league->tier, true) . ' ' . $this->lol->rankToNumber($league->rank, true);
                } else {
                    $summoner_signature['league'] = $league->tier . ' ' . $league->rank;
                }
                $summoner_signature['lp'] = $league->points;
                $series = unserialize($league->series);
                // $this->Request->pr($series);
                $summoner_signature['series'] = isset($series->progress) ? $series->progress : '';
            } else {
                $summoner_signature['league'] = $summoner_details->summonerLevel . ' Level';
                $summoner_signature['lp'] = '0';
                $summoner_signature['series'] = '';
            }


            $summoner_signature['region'] = strtoupper($reg);
            $summoner_signature['summoner_name'] = $summoner_details->name;
            $summoner_signature['profile_icon_id'] = $summoner_details->profileIconId;
            $summoner_signature['win_ratio'] = ((float)$data->totalStats->winRatio) . '%';
            $summoner_signature['kills'] = (float)number_format($data->totalStats->kills, 1, '.', '');
            $summoner_signature['deaths'] = (float)number_format($data->totalStats->deaths, 1, '.', '');
            $summoner_signature['assists'] = (float)number_format($data->totalStats->assists, 1, '.', '');
            $summoner_signature['kda'] = (float)$data->totalStats->kda;
            $summoner_signature['kill_participant'] = ((float)$data->totalStats->killParticipationRatio) . '%';

            if (isset($data->championStats['1'])) {
                $champ = $data->championStats['1'];
                $summoner_signature['stat_1_name'] = $champ->championName;
                $summoner_signature['stat_1_key'] = $champ->championKey;
                $summoner_signature['stat_1_win'] = $champ->win;
                $summoner_signature['stat_1_lose'] = $champ->lose;
                $summoner_signature['stat_1_kills'] = (float)number_format($champ->killsAvg, 1, '.', '');
                $summoner_signature['stat_1_deaths'] = (float)number_format($champ->deathsAvg, 1, '.', '');
                $summoner_signature['stat_1_assists'] = (float)number_format($champ->assistsAvg, 1, '.', '');
                $summoner_signature['stat_1_kda'] = (float)$champ->kda;
                $summoner_signature['stat_1_winratio'] = ((float)$champ->winRatio) . '%';
                $summoner_signature['stat_1_kill_participant'] = ((float)$champ->killParticipationRatio) . '%';
            }
            if (isset($data->championStats['2'])) {
                $champ = $data->championStats['2'];
                $summoner_signature['stat_2_name'] = $champ->championName;
                $summoner_signature['stat_2_key'] = $champ->championKey;
                $summoner_signature['stat_2_win'] = $champ->win;
                $summoner_signature['stat_2_lose'] = $champ->lose;
                $summoner_signature['stat_2_kills'] = (float)number_format($champ->killsAvg, 1, '.', '');
                $summoner_signature['stat_2_deaths'] = (float)number_format($champ->deathsAvg, 1, '.', '');
                $summoner_signature['stat_2_assists'] = (float)number_format($champ->assistsAvg, 1, '.', '');
                $summoner_signature['stat_2_kda'] = (float)$champ->kda;
                $summoner_signature['stat_2_winratio'] = ((float)$champ->winRatio) . '%';
                $summoner_signature['stat_2_kill_participant'] = ((float)$champ->killParticipationRatio) . '%';
            }

            $champ_maintenance_status = (bool)$this->cache->redis->get('maintenance:champmastery:' . $region);
            if (!$champ_maintenance_status) {
                $this->db_read->where('summoner_id', $summoner_id);
                $this->db_read->order_by('points', 'desc');
                $mastery_query = $this->db_read->get('champ_mastery_' . $this->lol->getRegion());
            }

            if (!$champ_maintenance_status && isset($mastery_query) && $mastery_query->num_rows() > 0) { // eğer veritabanında kayıtlı champion mastery bilgisi varsa
                $champion_masteries = $mastery_query->result();
            } else { // kayıtlı champion mastery bilgisi yoksa getir
                $champion_masteries_ = $this->lol->api("lol/champion-mastery/v3/champion-masteries/by-summoner/$summoner_id", '', 3600);
                if (count($champion_masteries_)) {
                    //$update_summoner_champions = array();
                    $champion_masteries = array();
                    foreach ($champion_masteries_ as $champ_) {
                        $champion_masteries[$champ_->championId] = new stdClass();
                        $champion_masteries[$champ_->championId]->level = $champ_->championLevel;
                        $champion_masteries[$champ_->championId]->points = $champ_->championPoints;
                        $champion_masteries[$champ_->championId]->champion_id = $champ_->championId;

                        //if ($champ_->championLevel > 0) {
                        //if (!$champ_maintenance_status) {
                        // @TODO burası yapılacak
                        //}

                        //}
                    }
                    $champion_masteries = $this->Request->championSort($champion_masteries, 'championPoints', SORT_DESC);
                    //if (count($update_summoner_champions) > 0) {
                    //
                    //}
                }
            }

            if (isset($champion_masteries) && $champion_masteries) {
                $s = 0;
                foreach ($champion_masteries as $mastery) {
                    if ($s <= 2) {
                        $s++;
                        $champ = $this->lol->getChampionById2($mastery->champion_id);
                        $summoner_signature['champ_' . $s . '_key'] = $champ->key;
                        $summoner_signature['champ_' . $s . '_level'] = $mastery->level;
                        $summoner_signature['champ_' . $s . '_points'] = number_format($mastery->points, 0, '', ',');
                    }
                }
            }

            if (isset($signature)) {
                $image_name = $signature->image_name;
                $this->db->where('region', $reg);
                $this->db->where('summoner_id', $summoner_id);
                $this->db->delete('signature_summary');
            } else {
                $signature_count_query = $this->db_read->query("SELECT COUNT(*) AS total FROM signature_summary");
                $signature_count = $signature_count_query->row();
                $image_name = $this->Request->generateRandomString(6) . $signature_count->total;
            }

            $signature_result = $this->lolimage->summarySignature($summoner_signature, 's/' . $image_name . '.png');
            if ($signature_result) {
                $this->db->insert('signature_summary', array(
                    'summoner_id' => $summoner_id,
                    'region' => $reg,
                    'summoner_name' => $summoner_details->name,
                    'image_name' => $image_name
                ));
                $data2->status = true;
                $data2->src = SIGNATURE_URL . 's/' . $image_name . '.png';
                $data2->lastUpdate = date('Y-m-d H:i:s');
                $data2->lastUpdateSec = 0;
            }
        }
        echo json_encode($data2);
    }

    /**
     * Calculates the MMR of the summoner
     * @uses CI_Input::post($summonerId) Summoner ID
     * @uses CI_Input::post($accountId) Account ID
     * @uses CI_Input::post($region) Region
     */
    public function mmr()
    {
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->accountId = $this->input->post('accountId'); //$accountId;//
        $request->summonerId = $this->input->post('summonerId');//$summonerId;//
        $request->region = $this->input->post('region');
        $request->lang = 'en';
        if (isset($request->summonerId) && isset($request->accountId) && isset($request->region) && isset($request->lang)) {

            $cache_key = 'mmr:v3:' . $request->region . ':' . md5($request->accountId . $request->summonerId);

            if ($result = $this->cache->redis->get($cache_key)) {
                echo json_encode($result);
                die();
            }

            $this->lol->updateSummonerMatches($request->accountId);

            $distribute_point = 69;//$this->input->post('distribute_point');//69;
            $streak_percent = 15;//$this->input->post('streak_percent');//15;
            $streak_base = 1.5;//$this->input->post('streak_base');//1.5;
            $new_match_percent = 25;//$this->input->post('new_match_percent');//25;


            $mmr_list = array(
                0 => array(
                    1 => array('min' => 2900, 'max' => 6500)
                ),
                1 => array(
                    1 => array('min' => 2550, 'max' => 2899)
                ),
                2 => array(
                    1 => array('min' => 2480, 'max' => 2549),
                    2 => array('min' => 2410, 'max' => 2479),
                    3 => array('min' => 2340, 'max' => 2409),
                    4 => array('min' => 2270, 'max' => 2339),
                    5 => array('min' => 2200, 'max' => 2269),
                ),
                3 => array(
                    1 => array('min' => 2130, 'max' => 2199),
                    2 => array('min' => 2060, 'max' => 2129),
                    3 => array('min' => 1990, 'max' => 2059),
                    4 => array('min' => 1920, 'max' => 1989),
                    5 => array('min' => 1850, 'max' => 1919),
                ),
                4 => array(
                    1 => array('min' => 1780, 'max' => 1849),
                    2 => array('min' => 1710, 'max' => 1779),
                    3 => array('min' => 1640, 'max' => 1709),
                    4 => array('min' => 1570, 'max' => 1639),
                    5 => array('min' => 1500, 'max' => 1569),
                ),
                5 => array(
                    1 => array('min' => 1430, 'max' => 1499),
                    2 => array('min' => 1360, 'max' => 1429),
                    3 => array('min' => 1290, 'max' => 1359),
                    4 => array('min' => 1220, 'max' => 1289),
                    5 => array('min' => 1150, 'max' => 1219),
                ),
                6 => array(
                    1 => array('min' => 1080, 'max' => 1149),
                    2 => array('min' => 1010, 'max' => 1079),
                    3 => array('min' => 940, 'max' => 1009),
                    4 => array('min' => 870, 'max' => 939),
                    5 => array('min' => 800, 'max' => 869),
                )
            );


            $summoner_id = $request->summonerId;
            $account_id = $request->accountId;
            $region = mb_strtoupper($request->region);
            $this->lol->setRegion($region);
            $this->lol->setLang($request->lang);
            $region_lower = $this->lol->getRegion();


            //$this->db_read->where('summoner_id', $summoner_id);
            //$this->db_read->where('date_added>', date('Y-m-d H:i:s', time() - (36000)));
            //$league_query = $this->db_read->get('league_l1_' . $reg);

            $league_maintenance_status = (bool)$this->cache->redis->get('maintenance:league:' . $region_lower);


            $league_add_point = 0;
            $mmr_min = 0;
            $rank_list = array();
            $total_players_list = array();

            $league_from_db = false;
            $league_status = false;
            if (!$league_maintenance_status) {
                $this->db_read->where('summoner_id', $summoner_id);
                $this->db_read->where('date_added>', date('Y-m-d H:i:s', time() - (3600)));
                $league1_query = $this->db_read->get('summoner_league_' . $region_lower . '_1');
                if ($league1_query->num_rows()) {
                    $league_l1 = $league1_query->row();
                    $league_from_db = true;
                    if ($league_l1->league_id != 'Unranked') {
                        $league_status = true;
                        $tier = $this->lol->tierToNumber($league_l1->tier, true);
                        $tier_number = $league_l1->tier;
                        $rank = $this->lol->rankToNumber($league_l1->rank, true);
                        $rank_number = $league_l1->rank;
                        $rank_list[$tier] = array();
                        $total_players_list[$tier] = array();

                        $this->db_read->where('league_id', $league_l1->league_id);
                        $league_full_q = $this->db_read->get('summoner_league_' . $region_lower . '_1');
                        $league_full = $league_full_q->result();
                        foreach ($league_full as $league_summoner) {
                            $tier_ = $this->lol->tierToNumber($league_summoner->tier, true);
                            $rank_ = $this->lol->rankToNumber($league_summoner->rank, true);
                            if (!isset($rank_list[$tier_][$rank_])) {
                                $rank_list[$tier_][$rank_] = array();
                                $total_players_list[$tier_][$rank_] = 0;
                            }
                            $rank_list[$tier_][$rank_][] = array('lp' => $league_summoner->points, 'id' => $league_summoner->summoner_id);
                            $total_players_list[$tier_][$rank_]++;
                        }
                    }
                }
            }
            if ($league_from_db == false && $league_status == false) {
                $positions = $this->lol->api('lol/league/v3/positions/by-summoner/' . $summoner_id, '', 1800);
                if ($positions) {
                    foreach ($positions as $position) {
                        if ($position->queueType == 'RANKED_SOLO_5x5') {
                            $league_status = true;
                            $tier_number = $this->lol->tierToNumber($position->tier);
                            $tier = $position->tier;
                            $rank_number = $this->lol->rankToNumber($position->rank);
                            $rank = $position->rank;

                            $rank_list[$tier] = array();
                            $total_players_list[$tier] = array();

                            $league_data = $this->lol->api('lol/league/v3/leagues/' . $position->leagueId, '', 1800);
                            if (isset($league_data->entries)) {
                                foreach ($league_data->entries as $entry) {
                                    if (!isset($rank_list[$league_data->tier][$entry->rank])) {
                                        $rank_list[$league_data->tier][$entry->rank] = array();
                                        $total_players_list[$league_data->tier][$entry->rank] = 0;
                                    }
                                    $rank_list[$league_data->tier][$entry->rank][] = array('lp' => $entry->leaguePoints, 'id' => $entry->playerOrTeamId);
                                    $total_players_list[$league_data->tier][$entry->rank]++;
                                }
                            }
                            break;
                        }
                    }
                }
            }


            if ($league_status) {
                if (isset($tier) && isset($rank) && isset($tier_number) && isset($rank_number)) {
                    usort($rank_list[$tier][$rank], function ($item1, $item2) {
                        $item1['a'] = 'b';
                        return $item2['lp'] <=> $item1['lp'];
                    });


                    $max_ = 0;
                    foreach ($rank_list[$tier][$rank] as $key => $value) {
                        $max_ = $max_ == 0 ? $value['lp'] : $max_;
                        if ($value['id'] == $summoner_id) {
                            $place = $key;
                        }
                    }

                    if ($max_ > 100) {
                        $distribute_point = $max_ / 100 * $distribute_point;
                    }

                    $total_players = $total_players_list[$tier][$rank];

                    $mmr_min = $mmr_list[$tier_number][$rank_number]['min'];
                    //$mmr_max = $mmr_list[$tier_number][$rank_number]['max'];
                    if (isset($place)) {
                        $league_add_point = $distribute_point / $total_players * ($total_players - $place);
                    }


                }
                $game_type_filter = '4,420,42,410';
                $match_per_page = 20;
                $page_start = 0;

                $matches = (array)$this->lol->getSummonerMatches($account_id, $match_per_page, $page_start, $game_type_filter);

                $matches = array_reverse($matches);
                $streak_count = 1;
                $last_game_status = '';
                $match_total_points = 0;
                $match_counter = 1;
                if ($matches && count($matches)) {
                    foreach ($matches as $match_) {
                        if (!isset($match_->match)) {
                            continue;
                        }
                        $match = $match_->match;
                        foreach ($match->participants as $participant_key => $participant) {
                            if ($participant->identity->summonerId == $summoner_id) {
                                $game_status = $participant->stats->win == 1 ? 'win' : 'lose';
                                if ($last_game_status == $game_status) {
                                    $streak_count++;
                                } else {
                                    $streak_count = 0;
                                }
                                $match_counter++;
                                $last_game_status = $game_status;

                                $multiplier = ($streak_base + ($streak_base / 100 * ($streak_count * $streak_percent)));

                                $multiplier += ($multiplier / 100 * ($new_match_percent * $match_counter));
                                if ($game_status == 'lose') {
                                    $multiplier *= -1;
                                }

                                $match_total_points += $multiplier;
                            }
                        }
                    }
                }
                $total_points = $match_total_points + $mmr_min + $league_add_point;

                //$test_echo .= '<br>total: '.$total_points.'<br>';


                $data->status = true;
                $data->mmr = floor($total_points);
                foreach ($mmr_list as $tier => $ranks) {
                    foreach ($ranks as $rank => $values) {
                        if ($values['min'] <= $total_points && $values['max'] >= $total_points) {
                            $data->mmrTier = $this->lol->tierToNumber($tier, true);
                            $data->mmrRank = $this->lol->rankToNumber($rank, true);

                            if ($data->mmrTier == 'CHALLENGER' || $data->mmrTier == 'MASTER') {
                                $data->mmrTD = $data->mmrTier;
                            } else {
                                $data->mmrTD = $data->mmrTier . '_' . $data->mmrRank;
                            }
                        } else if ($total_points < 800) {
                            $data->mmrTier = '';
                            $data->mmrRank = '';
                            $data->mmrTD = 'UNRANKED';
                        }
                    }
                }
            }


            $this->cache->redis->save($cache_key, $data, 3600);

        }
        echo json_encode($data);
    }
}
