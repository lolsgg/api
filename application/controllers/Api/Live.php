<?php

class Live extends CI_Controller
{
    private $db_read;

    function __construct()
    {
        parent::__construct();
        $this->db_read = $this->db;

        //$this->output->enable_profiler(TRUE);

        /*$this->output->enable_profiler(TRUE);
        if (false && rand(0, 1) === 1) {
            $this->db_read = $this->load->database('read', TRUE, TRUE);
        } else {
            $this->db_read = $this->db;
        }*/
    }

    public function test($region, $summoner_id)
    {
        $_POST['summonerId'] = $summoner_id;
        $_POST['region'] = $region;
        $_POST['lang'] = 'en';
        $this->matchDetails();
    }


    /**
     * Gets live match details by summoner ID and region
     *
     * @uses CI_Input::post($summonerId) Summoner ID
     * @uses CI_Input::post($region) Region
     * @uses CI_Input::post($lang) Language
     *
     */
    public function matchDetails()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $teams = array();

        $request = new stdClass();
        $request->summonerId = $this->input->post('summonerId');
        $request->region = $this->input->post('region');
        $request->lang = $this->input->post('lang');

        if ($request->summonerId && $request->region && $request->lang) {
            $summoner_id = $request->summonerId;
            $region = mb_strtolower($request->region);
            $this->lol->setRegion($region);
            $this->lol->setLang($request->lang);
            $recent_match_insert_list = array();

            $this->load->model('Runes');

            $region_lower = $this->lol->getRegion();

            $runes = $this->lol->getRunes();

            $data->leagueMaintenanceStatus = $league_maintenance_status = (bool)$this->cache->redis->get('maintenance:league:' . $region_lower);
            $data->championMasteryMaintenanceStatus = $champion_mastery_maintenance_status = (bool)$this->cache->redis->get('maintenance:champmastery:' . $region_lower);
            if ($this->Request->isLanguageExist($request->lang)) {
                $this->lang->load('lol', mb_strtolower($request->lang));
                $this->lang->load('site', mb_strtolower($request->lang));
            } else {
                $this->lang->load('lol', "en");
                $this->lang->load('site', "en");
            }


            $match = $this->lol->api("lol/spectator/v3/active-games/by-summoner/" . $summoner_id, '', 15);

            if (isset($match) && isset($match->participants)) {
                $data->status = true;

                $data->spectate = '';
                if (isset($match->observers) && isset($match->observers->encryptionKey)) {
                    $data->spectate = $this->lol->getSpactatorCode($region, $match->observers->encryptionKey, $match->gameId, $match->platformId);
                }


                $maps = $this->lol->getMaps();

                $data->mapName = '';
                $data->mapId = $match->mapId;

                if (isset($match->mapId) && isset($maps->{$match->mapId})) {
                    $data->mapName = $this->lol->getMapName($maps->{$match->mapId}->MapName);
                }

                $data->gameId = $match->gameId;
                if (!isset($match->gameQueueConfigId)) {
                    $match->gameQueueConfigId = 0;
                }

                // $match->gameStartTime += 3*60*1000;

                $data->gameType = $this->lol->getGameTypeById($match->gameQueueConfigId);
                $data->startDeltaTime = ($match->gameStartTime);
                $data->gameLength = microtime(true) - (($match->gameStartTime) / 1000);
                if ($data->gameLength < 0 || $data->gameLength > 10800) {
                    $data->gameLength = 0;
                }

                $data->team1 = new stdClass();
                $data->team2 = new stdClass();

                $data->team1->summonerCount = 0;
                $data->team2->summonerCount = 0;

                $data->team1->summoners = array();
                $data->team2->summoners = array();

                $data->team1->bannedChampions = false;
                $data->team2->bannedChampions = false;

                $data->team1->champions = '';
                $data->team2->champions = '';

                $champ_mastery_updates = array();
                $champ_mastery_db_statuses = array();


                if (isset($match->bannedChampions) && count($match->bannedChampions) > 0) {
                    $data->team1->bannedChampions = array();
                    $data->team2->bannedChampions = array();
                    foreach ($match->bannedChampions as $banned_champion) {
                        $champ = $this->lol->getChampionById2($banned_champion->championId);
                        $bannedChamp = new stdClass();
                        $bannedChamp->championName = isset($champ->name) ? $champ->name : '';
                        $bannedChamp->championKey = isset($champ->key) ? $champ->key : '';
                        $bannedChamp->turn = $banned_champion->pickTurn;
                        $data->{$banned_champion->teamId == 100 ? 'team1' : 'team2'}->bannedChampions[$bannedChamp->turn] = $bannedChamp;
                    }
                }

                $position_api_usage = (int)$this->cache->redis->get("limit:v1:c:" . $region_lower . ':league/v3/positions/by-summoner');
                $position_api_limit = $this->lol->api_limits[$region_lower]['league/v3/positions/by-summoner'][0];

                $participant_ids = array();
                $summoner_preolad_urls = array();
                $league_preolad_urls = array();
                $league_position_preload_ids = array();
                foreach ($match->participants as $participant_key => $participant) {
                    $participant_ids[] = $participant->summonerId;
                    $summoner_preolad_urls[] = array('url' => 'lol/summoner/v3/summoners/' . $participant->summonerId, 'parameters' => '', 'expire' => 10800);


                    $summoner = new stdClass();
                    $champ = $this->lol->getChampionById2($participant->championId);
                    $spell1 = $this->lol->getSpellById($participant->spell1Id);
                    $spell2 = $this->lol->getSpellById($participant->spell2Id);


                    if ($participant->summonerId == $summoner_id) {
                        $main_summoner_champion = $champ;
                    }

                    $summoner->summonerId = $participant->summonerId;
                    $summoner->team = $participant->teamId == 100 ? 'team1' : 'team2';
                    $summoner->summonerName = $participant->summonerName;
                    $summoner->championName = $champ->name;
                    $summoner->championKey = $champ->key;
                    $summoner->championId = $champ->id;
                    $summoner->championTitle = $champ->title;
                    $summoner->championPosition = 0;
                    $summoner->spell1Id = $participant->spell1Id;
                    $summoner->spell1Name = $spell1->name;
                    $summoner->spell1Description = $spell1->description;
                    $summoner->spell1Key = $this->lol->cleanText($spell1->key);
                    $summoner->spell1KeyFull = $spell1->key;
                    $summoner->spell2Id = $participant->spell2Id;
                    $summoner->spell2Name = $spell2->name;
                    $summoner->spell2Description = $spell2->description;
                    $summoner->spell2Key = $this->lol->cleanText($spell2->key);
                    $summoner->spell2KeyFull = $spell2->key;
                    $summoner->ferocity = 0;
                    $summoner->cunning = 0;
                    $summoner->resolve = 0;
                    $summoner->rankedWins = 0;
                    $summoner->rankedLoses = 0;
                    $summoner->leagueTier = lang('site_text_unranked');
                    $summoner->leagueDivision = "";
                    $summoner->leaguePoints = 0;
                    $summoner->leagueTD = lang('site_text_unranked');
                    $summoner->leagueFlexTier = lang('site_text_unranked');
                    $summoner->leagueFlexDivision = "";
                    $summoner->leagueFlexPoints = 0;
                    $summoner->leagueFlexTD = lang('site_text_unranked');
                    $summoner->series = false;
                    $summoner->seriesWLN = false;
                    $summoner->summonerLevel = false;
                    $summoner->championMasteryLevel = 0;
                    $summoner->championMasteryDesc = '';
                    $summoner->championMasteryPoints = 0;
                    $summoner->rankedWinRatio = 0;

                    $recent_match_insert_list[$summoner->summonerId] = array(
                        'summoner_name' => $summoner->summonerName,
                        'summoner_tier' => $summoner->leagueTier,
                        'summoner_division' => $summoner->leagueDivision,
                        'summoner_league_points' => $summoner->leagueTD,
                        'series' => '',
                        'team' => $participant->teamId,
                        'champion_id' => $summoner->championId
                    );

                    //GET LEAGUE DATA FROM DB IF EXISTS
                    if (!$league_maintenance_status) {
                        $this->db_read->where('summoner_id', $participant->summonerId);
                        if ($position_api_usage == 0 || $position_api_usage < $position_api_limit) { // if limit not enough use latest data
                            $this->db_read->where('date_added>', date('Y-m-d H:i:s', time() - (3600)));
                        }
                        $league1_query = $this->db_read->get('summoner_league_' . $region_lower . '_1');

                        $this->db_read->where('summoner_id', $participant->summonerId);
                        if ($position_api_usage == 0 || $position_api_usage < $position_api_limit) { // if limit not enough use latest data
                            $this->db_read->where('date_added>', date('Y-m-d H:i:s', time() - (3600)));
                        }
                        $league_l2_query = $this->db_read->get('summoner_league_' . $region_lower . '_2');

                        if ($league1_query->num_rows() == 0 || $league_l2_query->num_rows() == 0) {
                            $league_position_preload_ids[] = $participant->summonerId;
                            $summoner_preolad_urls[] = array('url' => 'lol/league/v3/positions/by-summoner/' . $participant->summonerId, 'parameters' => '', 'expire' => 3600);
                        }
                        if ($league1_query->num_rows()) {
                            $league_l1 = $league1_query->row();
                            if ($league_l1->league_id != 'Unranked') {
                                $summoner->leagueTier = $this->lol->tierToNumber($league_l1->tier, true);
                                $summoner->leagueDivision = $this->lol->rankToNumber($league_l1->rank, true);
                                $summoner->leaguePoints = $league_l1->points;
                                $summoner->leagueTD = $this->lol->divisionTierCombine($this->lol->rankToNumber($league_l1->rank, true), $this->lol->tierToNumber($league_l1->tier, true));
                                if ($league_l1->series) {
                                    $miniseries = unserialize($league_l1->series);
                                    $summoner->series = str_replace(array('W', 'L', 'N'), array('✔', '✖', '–'), $miniseries->progress);
                                    $summoner->seriesWLN = $miniseries->progress;
                                }
                                $summoner->rankedWins = $league_l1->wins;
                                $summoner->rankedLoses = $league_l1->losses;

                                if ($league_l1->wins > 0) {
                                    $summoner->rankedWinRatio = 100 / ($summoner->rankedWins + $summoner->rankedLoses) * $summoner->rankedWins;
                                }

                                $recent_match_insert_list[$summoner->summonerId]['summoner_tier'] = $summoner->leagueTier;
                                $recent_match_insert_list[$summoner->summonerId]['summoner_division'] = $summoner->leagueDivision;
                                $recent_match_insert_list[$summoner->summonerId]['summoner_league_points'] = $summoner->leaguePoints;
                                $recent_match_insert_list[$summoner->summonerId]['series'] = isset($summoner->series) ? $summoner->series : '';
                            }
                        }

                        if ($league_l2_query->num_rows()) {
                            $league_l2 = $league_l2_query->row();
                            if ($league_l2->league_id != 'Unranked') {
                                $summoner->leagueFlexTier = $this->lol->tierToNumber($league_l2->tier, true);
                                $summoner->leagueFlexDivision = $this->lol->rankToNumber($league_l2->rank, true);
                                $summoner->leagueFlexPoints = $league_l2->points;
                                $summoner->leagueFlexTD = $this->lol->divisionTierCombine($this->lol->rankToNumber($league_l2->rank, true), $this->lol->tierToNumber($league_l2->tier, true));
                            }
                        }
                    } else {
                        $league_position_preload_ids[] = $participant->summonerId;
                        $summoner_preolad_urls[] = array('url' => 'lol/league/v3/positions/by-summoner/' . $participant->summonerId, 'parameters' => '', 'expire' => 3600);
                    }
                    //*END* GET LEAGUE DATA FROM DB IF EXISTS


                    $summoner->perks = new stdClass();
                    $summoner->perks->perkStyle = new stdClass();
                    $summoner->perks->perkStyle->id = '';
                    $summoner->perks->perkStyle->name = '';
                    $summoner->perks->perkStyle->description = '';
                    $summoner->perks->perkStyle->runes = array();
                    $summoner->perks->perkSubStyle = new stdClass();
                    $summoner->perks->perkSubStyle->id = '';
                    $summoner->perks->perkSubStyle->name = '';
                    $summoner->perks->perkSubStyle->description = '';
                    $summoner->perks->perkSubStyle->bonus = '';
                    $summoner->perks->perkSubStyle->runes = array();


                    if (isset($participant->perks)) {
                        $perks = $participant->perks;
                        if ($perks) {
                            if (mb_strtolower(trim($request->lang)) == 'tr') {
                                $perk_style = $this->Runes->perk_styles_tr[$perks->perkStyle];
                                $perk_sub_style = $this->Runes->perk_styles_tr[$perks->perkSubStyle];
                            } else {
                                $perk_style = $this->Runes->perk_styles_en[$perks->perkStyle];
                                $perk_sub_style = $this->Runes->perk_styles_en[$perks->perkSubStyle];
                            }

                            $summoner->perks->perkStyle->id = $perks->perkStyle;
                            $summoner->perks->perkStyle->name = $perk_style['name'];
                            $summoner->perks->perkStyle->description = $perk_style['description'];
                            $summoner->perks->perkSubStyle->id = $perks->perkSubStyle;
                            $summoner->perks->perkSubStyle->name = $perk_sub_style['name'];
                            $summoner->perks->perkSubStyle->description = $perk_sub_style['description'];
                            $summoner->perks->perkSubStyle->bonus = $perk_style['combinations'][$perks->perkSubStyle];
                            $summoner->runesTotal[] = '*' . $perk_style['name'] . '*';
                            $summoner->runesTotal[] = '*' . $perk_sub_style['name'] . '*';

                            if (isset($perks->perkIds) && count($perks->perkIds)) {
                                for ($i = 0; $i < 4; $i++) {
                                    $summoner->perks->perkStyle->runes[$i] = array('description' => '', 'id' => 0, 'line' => $i, 'name' => '', 'style_id' => 0);
                                }

                                foreach ($perks->perkIds as $perk_id) {
                                    if (isset($runes[$perk_id])) {
                                        $perk = $runes[$perk_id];

                                        if ($perk['style_id'] == $perks->perkStyle) {
                                            $summoner->perks->perkStyle->runes[$perk['line']] = $perk;
                                        } else {
                                            $summoner->perks->perkSubStyle->runes[$perk['line']] = $perk;
                                        }
                                        $summoner->runesTotal[] = $perk['name'];
                                    }
                                }
                            }
                        }
                    }


                    $summoner->championInfo = array();
                    $summoner->championInfo[0] = array('name' => lang('lol_statAttack'), 'value' => $champ->info->attack);
                    $summoner->championInfo[1] = array('name' => lang('lol_statDefense'), 'value' => $champ->info->defense);
                    $summoner->championInfo[2] = array('name' => lang('lol_statDifficulty'), 'value' => $champ->info->difficulty);
                    $summoner->championInfo[3] = array('name' => lang('lol_Magic'), 'value' => $champ->info->magic);

                    $summoner->championTags = array();
                    foreach ($champ->tags as $champ_tag) {
                        $summoner->championTags[] = lang('lol_' . $champ_tag);
                    }

                    $per_level_lang = lang('site_per_level');

                    $summoner->championStats = array();
                    $chstat = $champ->stats;
                    $chattackspeed = number_format((0.625 / (1 + $chstat->attackspeedoffset)), 3, '.', '.');

                    $summoner->championStats[] = array('key' => 'armor', 'name' => lang('lol_Armor'), 'description' => $chstat->armor . ' (+' . $chstat->armorperlevel . ' ' . $per_level_lang . ')');
                    $summoner->championStats[] = array('key' => 'attackdamage', 'name' => lang('lol_Damage'), 'description' => $chstat->attackdamage . ' (+' . $chstat->attackdamageperlevel . ' ' . $per_level_lang . ')');
                    $summoner->championStats[] = array('key' => 'attackspeed', 'name' => lang('lol_AttackSpeed'), 'description' => $chattackspeed . ' (+' . $chstat->attackspeedperlevel . '% ' . $per_level_lang . ')');
                    $summoner->championStats[] = array('key' => 'hp', 'name' => lang('lol_Health'), 'description' => $chstat->hp . ' (+' . $chstat->hpperlevel . ' ' . $per_level_lang . ')');
                    $summoner->championStats[] = array('key' => 'hpregen', 'name' => lang('lol_HealthRegen'), 'description' => $chstat->hpregen . ' (+' . $chstat->hpregenperlevel . ' ' . $per_level_lang . ')');
                    $summoner->championStats[] = array('key' => 'spellblock', 'name' => lang('lol_SpellBlock'), 'description' => $chstat->spellblock . ' (+' . $chstat->spellblockperlevel . ' ' . $per_level_lang . ')');
                    $summoner->championStats[] = array('key' => 'mp', 'name' => lang('lol_Mana'), 'description' => $chstat->mp . ' (+' . $chstat->mpperlevel . ' ' . $per_level_lang . ')');
                    $summoner->championStats[] = array('key' => 'mpregen', 'name' => lang('lol_ManaRegen'), 'description' => $chstat->mpregen . ' (+' . $chstat->mpregenperlevel . ' ' . $per_level_lang . ')');
                    $summoner->championStats[] = array('key' => 'movespeed', 'name' => lang('lol_Movement'), 'description' => $chstat->movespeed);

                    $summoner->champions = array();
                    $spell_keys = array(0 => 'Q', 1 => 'W', 2 => 'E', 3 => 'R');

                    $summoner->tips = new stdClass();
                    $summoner->tips->langAlly = lang('site_as_ally');
                    $summoner->tips->langEnemy = lang('site_as_enemy');
                    if (isset($champ->spells)) {
                        foreach ($champ->spells as $spell_key => $spell) {
                            $champ->spells[$spell_key]->key = $spell_keys[$spell_key];
                        }
                    }

                    if (isset($champ->allytips) && count($champ->allytips) > 0) {
                        $summoner->tips->ally = array();
                        foreach ($champ->allytips as $allytip) {
                            $allytip_ = $allytip;
                            foreach ($champ->spells as $spell) {
                                $allytip_ = str_replace($spell->name, $spell->name . '(' . $spell->key . ')', $allytip_);

                            }
                            $summoner->tips->ally[] = $allytip_;
                        }
                    }

                    if (isset($champ->enemytips) && count($champ->enemytips) > 0) {
                        $summoner->tips->enemy = array();
                        foreach ($champ->enemytips as $enemytip) {
                            $enemytip_ = $enemytip;
                            foreach ($champ->spells as $spell) {
                                $enemytip_ = str_replace($spell->name, $spell->name . '(' . $spell->key . ')', $enemytip_);
                            }
                            $summoner->tips->enemy[] = $enemytip_;
                        }
                    }

                    $champ_mastery_updates[$participant->summonerId] = array();
                    $champ_mastery_db_statuses[$participant->summonerId] = false;

                    if (!$champion_mastery_maintenance_status) {

                        $this->db_read->where('summoner_id', $participant->summonerId);
                        $champ_mastery_query = $this->db_read->get('champ_mastery_' . $region_lower);
                        if ($champ_mastery_query->num_rows() == 0) {
                            $summoner_preolad_urls[] = array('url' => 'lol/champion-mastery/v3/champion-masteries/by-summoner/' . $participant->summonerId, 'parameters' => '', 'expire' => 7200);
                        } else {
                            $champ_mastery_results = $champ_mastery_query->result();
                            foreach ($champ_mastery_results as $champ_result) {
                                if ($summoner->championId == $champ_result->champion_id) {
                                    $summoner->championPosition = $champ_result->position > 100000 ? 0 : (int)$champ_result->position;
                                }

                                $champ_mastery_updates[$participant->summonerId][$champ_result->champion_id] = $champ_result->summoner_champion_id;
                                if (strtotime($champ_result->date_updated) > (time() - 7200)) {
                                    if ($summoner->championId == $champ_result->champion_id) {
                                        $summoner->championMasteryLevel = $champ_result->level;
                                        $summoner->championMasteryDesc = $champ_result->points;
                                        $champ_mastery_db_statuses[$participant->summonerId] = true;
                                    }
                                    if (strtotime($champ_result->last_played) > (time() - (60 * 60 * 24 * 14)) && count($summoner->champions) < 11) {
                                        $champ = $this->lol->getChampionById2($champ_result->champion_id);
                                        $champ_result_ = new stdClass();
                                        $champ_result_->championName = $champ->name;
                                        $champ_result_->championKey = $champ->key;
                                        $champ_result_->championLevel = $champ_result->level;
                                        $champ_result_->championPoints = $champ_result->points;
                                        $champ_result_->championId = $champ_result->champion_id;
                                        $summoner->champions[] = $champ_result_;
                                    }
                                }
                            }
                            if ($champ_mastery_db_statuses[$participant->summonerId] == false) {
                                $summoner_preolad_urls[] = array('url' => 'lol/champion-mastery/v3/champion-masteries/by-summoner/' . $participant->summonerId, 'parameters' => '', 'expire' => 7200);
                            }
                        }
                    } else {
                        $summoner_preolad_urls[] = array('url' => 'lol/champion-mastery/v3/champion-masteries/by-summoner/' . $participant->summonerId, 'parameters' => '', 'expire' => 7200);
                    }


                    $teams[$participant->summonerId] = $participant->teamId == 100 ? 'team1' : 'team2';
                    $data->{$participant->teamId == 100 ? 'team1' : 'team2'}->summonerCount++;
                    $data->{$participant->teamId == 100 ? 'team1' : 'team2'}->summoners[$participant->summonerId] = $summoner;
                }


                $champ_mastery_update_list = array();
                $champ_mastery_insert_list = array();

                if (count($participant_ids) > 0) {
                    $this->lol->apiPreload($summoner_preolad_urls, count($summoner_preolad_urls) + 1);

                    // GET LEAGUE DATA FROM RIOT API
                    $league_ids = array();
                    $update_league_data_tmp = array();
                    $update_league_data_tmp['RANKED_SOLO_5x5'] = array();
                    $update_league_data_tmp['RANKED_FLEX_SR'] = array();
                    if (count($league_position_preload_ids)) {

                        foreach ($league_position_preload_ids as $position_summoner_id) {
                            $positions = $this->lol->apiOnlyCache('lol/league/v3/positions/by-summoner/' . $position_summoner_id, '');
                            $summoner_tmp = &$data->{$teams[$position_summoner_id]}->summoners[$position_summoner_id];
                            if ($positions) {
                                foreach ($positions as $position) {
                                    if ($position->queueType == 'RANKED_SOLO_5x5') {
                                        $summoner_tmp->leagueTier = $position->tier;
                                        $summoner_tmp->leagueDivision = $position->rank;
                                        $summoner_tmp->leaguePoints = $position->leaguePoints;
                                        $summoner_tmp->leagueTD = $this->lol->divisionTierCombine($position->rank, $position->tier);
                                        $summoner_tmp->rankedWins = $position->wins;
                                        $summoner_tmp->rankedLoses = $position->losses;
                                        if ($summoner_tmp->rankedWins > 0) {
                                            $summoner_tmp->rankedWinRatio = 100 / ($summoner_tmp->rankedWins + $summoner_tmp->rankedLoses) * $summoner_tmp->rankedWins;
                                        }
                                        if (isset($position->miniSeries)) {
                                            $summoner_tmp->series = str_replace(array('W', 'L', 'N'), array('✔', '✖', '–'), $position->miniSeries->progress);
                                            $summoner_tmp->seriesWLN = $position->miniSeries->progress;
                                        }

                                        $recent_match_insert_list[$position_summoner_id]['summoner_tier'] = $summoner_tmp->leagueTier;
                                        $recent_match_insert_list[$position_summoner_id]['summoner_division'] = $summoner_tmp->leagueDivision;
                                        $recent_match_insert_list[$position_summoner_id]['summoner_league_points'] = $summoner_tmp->leaguePoints;
                                        $recent_match_insert_list[$position_summoner_id]['series'] = isset($summoner_tmp->series) ? $summoner_tmp->series : '';
                                    } else if ($position->queueType == 'RANKED_FLEX_SR') {
                                        $summoner_tmp->leagueFlexTier = $position->tier;
                                        $summoner_tmp->leagueFlexDivision = $position->rank;
                                        $summoner_tmp->leagueFlexPoints = $position->leaguePoints;
                                        $summoner_tmp->leagueFlexTD = ($position->rank) ? $position->tier . '_' . $position->rank : $position->tier;
                                    }
                                    if (!$league_maintenance_status) {
                                        if (!$this->cache->redis->get('update_league_v1_id:' . $region . ':' . $position->leagueId)) { // update league every 2 hours
                                            $this->cache->redis->save('update_league_v1_id:' . $region . ':' . $position->leagueId, TRUE, 3600);
                                            $league_preolad_urls[] = array('url' => 'lol/league/v3/leagues/' . $position->leagueId, 'parameters' => '', 'expire' => 3600);
                                            $league_ids[$position->leagueId] = $position->leagueId;
                                        }
                                    }
                                }
                            }
                            if ($summoner_tmp->leagueTier == lang('site_text_unranked')) {
                                $update_league_data_tmp['RANKED_SOLO_5x5'][] = array(
                                    'summoner_id' => $position_summoner_id, 'league_id' => 'Unranked',
                                    'summoner_name' => $summoner_tmp->summonerName, 'tier' => NULL, 'rank' => NULL,
                                    'name' => NULL, 'points' => NULL, 'wins' => NULL, 'losses' => NULL, 'veteran' => NULL,
                                    'inactive' => NULL, 'freshblood' => NULL, 'hotstreak' => NULL, 'ranking' => NULL, 'series' => '', 'date_added' => date('Y-m-d H:i:s')
                                );
                            }
                            if ($summoner_tmp->leagueFlexTier == lang('site_text_unranked')) {
                                $update_league_data_tmp['RANKED_FLEX_SR'][] = array(
                                    'summoner_id' => $position_summoner_id, 'league_id' => 'Unranked',
                                    'summoner_name' => $summoner_tmp->summonerName, 'tier' => NULL, 'rank' => NULL,
                                    'name' => NULL, 'points' => NULL, 'wins' => NULL, 'losses' => NULL, 'veteran' => NULL,
                                    'inactive' => NULL, 'freshblood' => NULL, 'hotstreak' => NULL, 'ranking' => NULL, 'series' => '', 'date_added' => date('Y-m-d H:i:s')
                                );
                            }
                        }
                    }
                    // *END* GET LEAGUE DATA FROM RIOT API

                    $account_ids = array();

                    foreach ($participant_ids as $participant_id) {

                        // $summoner = $this->lol->api("lol/summoner/v3/summoners/".$participant_id,'',43200);
                        $summoner = $this->lol->apiOnlyCache("lol/summoner/v3/summoners/" . $participant_id, '');
                        if ($summoner && isset($summoner->id)) {
                            $data->{$teams[$participant_id]}->summoners[$participant_id]->summonerLevel = $summoner->summonerLevel;
                            $data->{$teams[$participant_id]}->summoners[$participant_id]->summonerProfileIconId = $summoner->profileIconId;
                        }

                        if ($summoner && isset($summoner->accountId)) {
                            $account_ids[] = $summoner->accountId;
                        }

                        if ($champ_mastery_db_statuses[$participant_id] == false) {
                            // $champion_masteries = $this->lol->api("lol/champion-mastery/v3/champion-masteries/by-summoner/".$participant_id,'',7200);
                            $champion_masteries = $this->lol->apiOnlyCache("lol/champion-mastery/v3/champion-masteries/by-summoner/" . $participant_id, '');
                            if ($update_champ_mastery_db = (!$this->cache->redis->get('l_cmastery_upd:' . $region . ':' . $participant_id) AND !$champion_mastery_maintenance_status)) {
                                $this->cache->redis->save('l_cmastery_upd:' . $region . ':' . $participant_id, TRUE, 7200);
                            }

                            if ($champion_masteries && count($champion_masteries) > 0) {
                                foreach ($champion_masteries as $champ_mastery) {
                                    if (isset($champ_mastery->championId) && $data->{$teams[$participant_id]}->summoners[$participant_id]->championId == $champ_mastery->championId) {
                                        $data->{$teams[$participant_id]}->summoners[$participant_id]->championMasteryLevel = $champ_mastery->championLevel;
                                        $data->{$teams[$participant_id]}->summoners[$participant_id]->championMasteryDesc = $champ_mastery->championPoints;
                                    }
                                    if (isset($champ_mastery->lastPlayTime) && $champ_mastery->lastPlayTime > (time() - (60 * 60 * 24 * 14)) * 1000 && count($data->{$teams[$participant_id]}->summoners[$participant_id]->champions) < 11) {
                                        $champ = $this->lol->getChampionById2($champ_mastery->championId);
                                        $champ_mastery->championName = $champ->name;
                                        $champ_mastery->championKey = $champ->key;
                                        $data->{$teams[$participant_id]}->summoners[$participant_id]->champions[] = $champ_mastery;
                                    }


                                    if (!$champion_mastery_maintenance_status && isset($champ_mastery->championLevel)) {
                                        $data->{$teams[$participant_id]}->summoners[$participant_id]->championMasteryPoints += $champ_mastery->championLevel;
                                        if (isset($update_champ_mastery_db) && $update_champ_mastery_db) {
                                            if (isset($champ_mastery_updates[$participant_id][$champ_mastery->championId])) {
                                                if (isset($summoner->id)) {
                                                    $champ_mastery_update_list[] = array(
                                                        'summoner_champion_id' => $champ_mastery_updates[$participant_id][$champ_mastery->championId],
                                                        'summoner_name' => $summoner->name,
                                                        'level' => $champ_mastery->championLevel,
                                                        'points' => $champ_mastery->championPoints,
                                                        'last_played' => date('Y-m-d H:i:s', $champ_mastery->lastPlayTime / 1000),
                                                        'date_updated' => date('Y-m-d H:i:s')
                                                    );
                                                }
                                            } else {
                                                if (isset($summoner->id)) {
                                                    $champ_mastery_insert_list[] = array(
                                                        'summoner_id' => $participant_id,
                                                        'champion_id' => $champ_mastery->championId,
                                                        'summoner_name' => $summoner->name,
                                                        'level' => $champ_mastery->championLevel,
                                                        'points' => $champ_mastery->championPoints,
                                                        'last_played' => date('Y-m-d H:i:s', $champ_mastery->lastPlayTime / 1000)
                                                    );
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // GET ALL SUMMONERS' MATCH DATA FROM MONGODB
                if (FALSE && count($account_ids) > 0) {
                    $this->db->where_in('game_id', "(SELECT game_id FROM summoner_to_match_" . $region_lower . " WHERE account_id IN( " . implode(',', $account_ids) . "))", false);
                    $this->db->where_in('queue_type', array(4, 420, 42, 410, 440, 400, 41, 9, 430));
                    $this->db->where('status', 1);
                    $this->db->where('start_time > ', time() - (60*60*24*7));
                    $this->db->from('match_history_' . $region_lower);

                    $foo = $this->db->get();
                    $summoner_matches = $data->test2 = $foo->result();
                    $match_ids  = array();
                    foreach ($summoner_matches as $summoner_match) {
                        $match_ids[] = (int)$summoner_match->game_id;
                    }
                    $this->load->model('matchhistory');
                    $data->test3 = $this->matchhistory->getMatchHistory($region, $match_ids);
                }
                // *END* GET ALL SUMMONERS' MATCH DATA FROM MONGODB


                //Add this game to latest games
                if (count($recent_match_insert_list) > 0) {
                    $recent_match_check = $this->db_read->get_where('recent_match', array('game_id' => $match->gameId));
                    if (!$recent_match_check->num_rows()) {
                        $this->db->insert('recent_match', array(
                            'region' => $region,
                            'game_id' => $match->gameId,
                            'map_title' => $data->mapName,
                            'map_id' => $match->mapId,
                            'game_type_title' => $data->gameType,
                            'game_start_time' => $match->gameStartTime
                        ));
                        $recent_match_id = $this->db->insert_id();
                        foreach ($recent_match_insert_list as $key => $value) {
                            $recent_match_insert_list[$key]['recent_match_id'] = (int)$recent_match_id;
                        }
                        $this->db->insert_batch('recent_match_participant', $recent_match_insert_list);
                    }
                }


                // update league DB with new data
                if (isset($league_ids) && count($league_ids)) {
                    $this->lol->apiPreload($league_preolad_urls, count($league_preolad_urls) + 1);
                    foreach ($league_ids as $league_id) {
                        $league_data = $this->lol->apiOnlyCache('lol/league/v3/leagues/' . $league_id);

                        if (isset($league_data->entries)) {
                            if (!isset($update_league_data_tmp[$league_data->queue])) {
                                $update_league_data_tmp[$league_data->queue] = array();
                            }
                            $tier_number = $this->lol->tierToNumber($league_data->tier);
                            foreach ($league_data->entries as $entry) {
                                $rank_number = $this->lol->rankToNumber($entry->rank);
                                $update_league_data_tmp[$league_data->queue][] = array(
                                    'summoner_id' => (int)$entry->playerOrTeamId,
                                    'league_id' => $league_id,
                                    'summoner_name' => $entry->playerOrTeamName,
                                    'tier' => $tier_number,
                                    'rank' => $rank_number,
                                    'name' => $league_data->name,
                                    'points' => $entry->leaguePoints,
                                    'wins' => $entry->wins,
                                    'losses' => $entry->losses,
                                    'veteran' => $entry->veteran,
                                    'inactive' => $entry->inactive,
                                    'freshblood' => $entry->freshBlood,
                                    'hotstreak' => $entry->hotStreak,
                                    'ranking' => (int)((6 - $tier_number) . (5 - $rank_number) . str_pad($entry->leaguePoints, 4, '0', STR_PAD_LEFT) . str_pad($entry->wins, 4, '0', STR_PAD_LEFT)),
                                    'series' => isset($entry->miniSeries) ? serialize($entry->miniSeries) : '',
                                    'date_added' => date('Y-m-d H:i:s')
                                );
                            }
                        }
                        unset($league_data);
                    }
                }
                if (isset($update_league_data_tmp) && count($update_league_data_tmp)) {
                    $update_keys = array('league_id', 'summoner_name', 'tier', 'rank', 'name', 'points', 'wins', 'losses', 'veteran', 'inactive', 'freshblood', 'hotstreak', 'ranking', 'series', 'date_added');
                    if (isset($update_league_data_tmp['RANKED_SOLO_5x5']) && $update_league_data_tmp['RANKED_SOLO_5x5']) {
                        $this->db->insert_on_duplicate_update_batch('summoner_league_' . $region_lower . '_1', $update_league_data_tmp['RANKED_SOLO_5x5'], true, 100, $update_keys);
                        unset($update_league_data_tmp['RANKED_SOLO_5x5']);
                    }
                    if (isset($update_league_data_tmp['RANKED_FLEX_SR']) && $update_league_data_tmp['RANKED_FLEX_SR']) {
                        $this->db->insert_on_duplicate_update_batch('summoner_league_' . $region_lower . '_2', $update_league_data_tmp['RANKED_FLEX_SR'], true, 100, $update_keys);
                        unset($update_league_data_tmp['RANKED_FLEX_SR']);
                    }
                    if (isset($update_league_data_tmp['RANKED_FLEX_TT']) && $update_league_data_tmp['RANKED_FLEX_TT']) {
                        $this->db->insert_on_duplicate_update_batch('summoner_league_' . $region_lower . '_3', $update_league_data_tmp['RANKED_FLEX_TT'], true, 100, $update_keys);
                        unset($update_league_data_tmp['RANKED_FLEX_TT']);
                    }
                    unset($update_league_data_tmp);
                }
                // Update league end


                if (count($champ_mastery_update_list)) {
                    $this->db->update_batch('champ_mastery_' . $region_lower, $champ_mastery_update_list, 'summoner_champion_id');
                }
                if (count($champ_mastery_insert_list)) {
                    $this->db->insert_batch('champ_mastery_' . $region_lower, $champ_mastery_insert_list);
                }


                //Social network share
                /*
                // Otomatik sosyal ağ paylaşımı
                $signature_summmoner = isset($data->team1->summoners[$request->summonerId])?$data->team1->summoners[$request->summonerId]:$data->team2->summoners[$request->summonerId];
                $signature_data['summoner_name'] = $signature_summmoner->summonerName;
                $signature_data['league'] = $signature_summmoner->summonerLevel==30?($signature_summmoner->leagueTier.' '.$signature_summmoner->leagueDivision):('Level '.$signature_summmoner->summonerLevel);
                $signature_data['lp'] = $signature_summmoner->leaguePoints;
                $signature_data['series'] = $signature_summmoner->seriesWLN;
                $signature_data['champion_key'] = $signature_summmoner->championKey;
                $signature_data['champion_name'] = $signature_summmoner->championName;
                $signature_data['champion_points'] = str_replace(array('.',','),'',$signature_summmoner->championMasteryDesc);
                $signature_data['champion_mastery_level'] = $signature_summmoner->championMasteryLevel;
                $signature_data['ferocity'] = $signature_summmoner->ferocity;
                $signature_data['cunning'] = $signature_summmoner->cunning;
                $signature_data['resolve'] = $signature_summmoner->resolve;
                $signature_data['win'] = $signature_summmoner->rankedWins;
                $signature_data['lose'] = $signature_summmoner->rankedLoses;
                $signature_data['win_lose_ratio'] = number_format($signature_summmoner->rankedWinRatio,2,'.','').'%';
                $signature_data['spell1_key'] = $signature_summmoner->spell1KeyFull;
                $signature_data['spell2_key'] = $signature_summmoner->spell2KeyFull;
                $signature_data['profile_icon_id'] = isset($signature_summmoner->summonerProfileIconId) ? $signature_summmoner->summonerProfileIconId : '';
                $signature_data['mastery_id'] = $signature_summmoner->masteryId;
                $signature_data['region'] = $request->region;
                $signature_data['map_name'] = $this->lol->getMapNameById($data->mapId);
                $signature_data['game_type'] = $data->gameType;

                $signature = $this->Share->liveSignatureShare($signature_data);

                */
            }
        }

        if (isset($data->team1) && isset($data->team2)) {
            $data->teams = new stdClass();
            $data->teams->{'1'} = $data->team1;
            $data->teams->{'2'} = $data->team2;
            unset($data->team1);
            unset($data->team2);
        }


        // Build
        if (isset($main_summoner_champion)) {
            $data->build = array();
            $data->buildChampion = $main_summoner_champion->name;
            $build_version = 1528182845;
            $this->load->model('Build_model');
            $lanes = $this->Build_model->getChampionLanes($build_version, $main_summoner_champion->id);

            $orders = array('TOP' => 0, 'JUNGLE' => 1, 'MIDDLE' => 2, 'BOTTOM' => 3);
            $lower_names = array('TOP' => 'top', 'JUNGLE' => 'jungle', 'MIDDLE' => 'middle', 'BOTTOM' => 'bot');
            $max = -1;
            $max_lane = false;
            foreach ($lanes as $lane => $ratio) {
                if (isset($orders[$lane])) {
                    $data->build[$lane] = $this->Build_model->items($build_version, $main_summoner_champion->id, $lane);
                    $data->build[$lane]['ratio'] = number_format($ratio, 2);
                    $data->build[$lane]['order'] = $orders[$lane];
                    $data->build[$lane]['lower'] = $lower_names[$lane];
                    $data->build[$lane]['active'] = false;
                    if ($max < $ratio) {
                        $max = $ratio;
                        $max_lane = $lane;
                    }
                }
            }
            if ($max_lane) {
                $data->build[$max_lane]['active'] = true;
            }


        }
        // *END* Build


        //Machine Learning data collector
        /*if (false && isset($region) && ENVIRONMENT == 'development') {

            if (isset($data->teams) && count($data->teams->{'1'}->summoners) == 5 && count($data->teams->{'2'}->summoners) == 5) {
                if ($this->db->get_where("ml_data", array('game_id' => $data->gameId, 'region' => $region))->num_rows() == 0) {
                    $ml_data = array(
                        'region' => $region,
                        'game_id' => $data->gameId,
                        'game_type' => $data->gameType,
                        'map_id' => $data->mapId
                    );
                    $player = 1;
                    foreach ($data->teams->{'1'}->summoners as $sum_) {
                        $ml_data['p' . $player . '_champ_id'] = $sum_->championId;
                        $ml_data['p' . $player . '_mastery_level'] = $sum_->championMasteryLevel;
                        $ml_data['p' . $player . '_mastery_points'] = $sum_->championMasteryDesc;
                        $ml_data['p' . $player . '_mastery_ferocity'] = $sum_->ferocity;
                        $ml_data['p' . $player . '_mastery_cunning'] = $sum_->cunning;
                        $ml_data['p' . $player . '_mastery_resolve'] = $sum_->resolve;
                        $ml_data['p' . $player . '_mastery_id'] = $sum_->masteryId;
                        $ml_data['p' . $player . '_spell1_id'] = $sum_->spell1Id;
                        $ml_data['p' . $player . '_spell2_id'] = $sum_->spell2Id;

                        if (isset($sum_->champions['0'])) {
                            $ml_data['p' . $player . '_main_champ1_id'] = $sum_->champions['0']->championId;
                            $ml_data['p' . $player . '_main_champ1_mastery_points'] = $sum_->champions['0']->championPoints;
                        }

                        if (isset($sum_->champions['1'])) {
                            $ml_data['p' . $player . '_main_champ2_id'] = $sum_->champions['1']->championId;
                            $ml_data['p' . $player . '_main_champ2_mastery_points'] = $sum_->champions['1']->championPoints;
                        }

                        $player++;
                    }
                    foreach ($data->teams->{'2'}->summoners as $sum_) {
                        $ml_data['p' . $player . '_champ_id'] = $sum_->championId;
                        $ml_data['p' . $player . '_mastery_level'] = $sum_->championMasteryLevel;
                        $ml_data['p' . $player . '_mastery_points'] = $sum_->championMasteryDesc;
                        $ml_data['p' . $player . '_mastery_ferocity'] = $sum_->ferocity;
                        $ml_data['p' . $player . '_mastery_cunning'] = $sum_->cunning;
                        $ml_data['p' . $player . '_mastery_resolve'] = $sum_->resolve;
                        $ml_data['p' . $player . '_mastery_id'] = $sum_->masteryId;
                        $ml_data['p' . $player . '_spell1_id'] = $sum_->spell1Id;
                        $ml_data['p' . $player . '_spell2_id'] = $sum_->spell2Id;

                        if (isset($sum_->champions['0'])) {
                            $ml_data['p' . $player . '_main_champ1_id'] = $sum_->champions['0']->championId;
                            $ml_data['p' . $player . '_main_champ1_mastery_points'] = $sum_->champions['0']->championPoints;
                        }

                        if (isset($sum_->champions['1'])) {
                            $ml_data['p' . $player . '_main_champ2_id'] = $sum_->champions['1']->championId;
                            $ml_data['p' . $player . '_main_champ2_mastery_points'] = $sum_->champions['1']->championPoints;
                        }

                        $player++;
                    }
                    $data->mldata = $ml_data;
                    $this->db->insert('ml_data', $ml_data);
                }
            }
        }*/

        echo json_encode($data);
    }

    /**
     * Gets recently played games
     *
     * @param region (Optional) Filtrelenecek bölge
     * @param page (Optional) Getirilecek sayfa
     *
     */
    public function latestMatches()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->page = $this->input->post('page');
        $request->region = $this->input->post('region');
        $request->lang = $this->input->post('lang');


        if ($request->lang && $this->Request->isLanguageExist($request->lang)) {
            $this->lol->setLang($request->lang);
            $lang_tmp = mb_strtolower($request->lang);
            $this->lang->load('site', mb_strtolower($request->lang));
        } else {
            $this->lol->setLang("en");
            $lang_tmp = "en";
            $this->lang->load('site', "en");
        }

        $cache_key = 'recent_matches_c1_' . md5($request->region . $request->page . $lang_tmp);
        if ($request->page < 15 && $request->page > 0 && $result = $this->cache->redis->get($cache_key)) {
            echo $result;
            die();
        }

        $maps = $this->lol->getMaps();

        if (isset($request->page)) {
            $page = $request->page;
        } else {
            $page = 0;
        }

        $this->db_read->order_by('recent_match_id', 'desc');
        if (isset($request->region) && !empty($request->region)) {
            $this->db_read->where('region', mb_strtolower($request->region));
        }

        $this->db_read->limit(5, ($page * 5) - 5);
        $this->db_read->from('recent_match');
        $this->db_read->where('game_start_time>', 0);
        $this->db_read->where('date_added>', date('Y-m-d H:i:s', time() - (60 * 60 * 24)));
        $query = $this->db_read->get();

        if ($query->num_rows()) {
            $data->status = true;
            $matches_ = $query->result();
            $matches = array();

            if (isset($request->region) && !empty($request->region)) {
                $this->db_read->where('region', mb_strtolower($request->region));
            }

            $this->db_read->from('recent_match');
            $this->db_read->where('game_start_time>', 0);
            $this->db_read->where('date_added>', date('Y-m-d H:i:s', time() - (60 * 60 * 24)));
            $this->db_read->limit(50);
            $query_all = $this->db_read->get();
            $data->total = $query_all->num_rows();

            $match_ids = array();
            foreach ($matches_ as $match) {
                $match_ = new StdClass();
                $match_->time = $match->game_start_time == 0 ? 0 : (microtime(true) - (($match->game_start_time) / 1000));

                $match_ids[] = $match->recent_match_id;
                $match_->gameType = $match->game_type_title;
                if ($match->map_id > 0 && isset($maps->{$match->map_id})) {
                    $map_title = $maps->{$match->map_id}->MapName;
                } else {
                    $map_title = $match->map_title;
                }
                $match_->map = $map_title;
                $match_->region = $match->region;
                $match_->gameId = $match->game_id;
                $timepast = strtotime($match->date_added) - time();
                if ($timepast < 0) {
                    $timepast = time() - strtotime($match->date_added);
                }
                $search_time = $this->Request->secondToTime($timepast);
                $match_->searchTime = $search_time[0] . ' ' . lang('site_' . $search_time[1]);

                $matches[$match->recent_match_id] = $match_;
                $matches[$match->recent_match_id]->participants = new StdClass();
                $matches[$match->recent_match_id]->participants->team1 = array();
                $matches[$match->recent_match_id]->participants->team2 = array();
            }

            $this->db_read->order_by('rmp.recent_match_id', 'desc');
            // $this->db_read->order_by('rmp.recent_match_participant_id','asc');
            $this->db_read->where_in('rmp.recent_match_id', $match_ids);
            $this->db_read->from('recent_match_participant rmp');
            $query_participants = $this->db_read->get();
            if ($query_participants->num_rows()) {
                $participants = $query_participants->result();

                foreach ($participants as $participant) {
                    if ($participant->champion_id) {
                        $champ = $this->lol->getChampionById2($participant->champion_id);
                        $summoner = new StdClass();
                        $summoner->championName = isset($champ->name) ? $champ->name : '';
                        $summoner->championKey = isset($champ->key) ? $champ->key : '';
                        $summoner->series = $participant->series;
                        $summoner->name = $participant->summoner_name;
                        $summoner->tier = $participant->summoner_tier;
                        $summoner->division = $participant->summoner_division;
                        $summoner->leaguePoint = $participant->summoner_league_points;
                        $summoner->td = $this->lol->divisionTierCombine($participant->summoner_division, $participant->summoner_tier);

                        $matches[$participant->recent_match_id]->participants->{$participant->team == 100 ? 'team1' : 'team2'}[] = $summoner;
                        $matches[$participant->recent_match_id]->summoner = $participant->summoner_name;
                    }
                }
            }

            $matches2 = array();
            foreach ($matches as $match) {
                $matches2[] = $match;
            }

            $data->matches = $matches2;
            $this->cache->redis->save($cache_key, json_encode($data), 60);
        }

        echo json_encode($data);
    }

    /**
     * son 20dk içinde aranan maçlarda oynayan rastgele bir summoner getirir
     *
     */
    public function randomSummoner()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $this->db_read->limit(1, 0);
        $this->db_read->order_by('rmp.recent_match_id', 'desc');
        $this->db_read->order_by('rmp.recent_match_id', 'RANDOM');
        $this->db_read->join('recent_match rm', 'rm.recent_match_id=rmp.recent_match_id', 'left');
        $this->db_read->from('recent_match_participant rmp');
        $this->db_read->where('rm.date_added>', date('Y-m-d H:i:s', time() - (60 * 20)));
        $summoner_q = $this->db_read->get();
        if ($summoner_q->num_rows()) {
            $data->status = true;
            $summoner = $summoner_q->row();
            $data->summonerName = $summoner->summoner_name;
            $data->region = $summoner->region;
        }
        echo json_encode($data);
    }

    /**
     * Aranan maçın süresini getirir
     *
     * @uses CI_Input::post($summonerId) Summoner ID
     * @uses CI_Input::post($region) Region
     *
     */
    public function matchTime()
    {
        header('Content-Type: application/json');
        $data = new stdClass();
        $data->status = false;

        $request = new stdClass();
        $request->summonerId = $this->input->post('summonerId');
        $request->region = $this->input->post('region');

        if (isset($request->summonerId) && isset($request->region)) {
            $summoner_id = $request->summonerId;
            $region = mb_strtolower($request->region);
            $this->lol->setRegion($region);

            $match = $this->lol->api("lol/spectator/v3/active-games/by-summoner/" . $summoner_id, '', 15);

            if (isset($match) && isset($match->participants)) {
                $data->status = true;

                $data->gameLength = microtime(true) - (($match->gameStartTime) / 1000);
                if ($data->gameLength < 0 || $data->gameLength > 10800) {
                    $data->gameLength = 0;
                }
                $this->db->where(array('game_id' => $match->gameId, 'region' => $region));
                $this->db->update('recent_match', array(
                    'game_start_time' => $match->gameStartTime
                ));
            }
        }
        echo json_encode($data);
    }


    /**
     * lolnexus.com sitesinde son aranan maçlardan 10 tanesini arkaplanda arar
     *
     */
    public function cronsearch()
    {
        /*
        set_time_limit(0);
        $regions = array('North America' => 'NA', 'Europe West' => 'EUW', 'Europe Nordic & East' => 'EUNE',
        'Brazil' => 'BR', 'Turkey' => 'TR', 'Russia' => 'RU', 'Latin America North' => 'LAN', 'Latin America South' => 'LAS',
        'Oceania' => 'OCE', 'Korea' => 'KR', 'Japan' => 'JP',);
        require_once(APPPATH.'libraries/simple_html_dom.php');

        $count = 0;


        for ($i=0; $i < 10; $i++) {
            if ($count<20) {
                set_time_limit(0);
                $page = '';
                if ($i>1) {
                    $page = '&page='.$i;
                }
                $url = 'http://www.lolnexus.com/recent-games?cookieTest=1'.$page;//&page=2

                $str = $this->Request->curlWithProxy($url);

                $html = new simple_html_dom();
                $html->load($str);

                foreach ($html->find('.recent-game') as $element) {
                    $region_long = str_replace('&amp;','&',$element->find('small',0)->innertext);
                    $player = $element->find('.player h4',0)->innertext;
                    $time = (int)str_replace(':','',$element->find('.elapsed',0)->innertext);
                    $region = mb_strtolower($regions[$region_long]);
                    if ($time>300 && $time<20000) {
                        $count++;
                        if ($count<20) {
                            $summoner_name = urldecode(trim(str_replace(array(' ','+'),'',mb_strtolower($player))));
                            $this->lol->setRegion($region);
                            $this->lol->setLang('en');
                            $summoner = $this->lol->api("lol/summoner/v3/summoners/by-name/".$summoner_name,'',21600);
                            if (isset($summoner->id)) {
                                echo $player.'<br>';
                                $_POST['summonerId'] = $summoner->id;
                                $_POST['region'] = $region;
                                $_POST['lang'] = 'en';
                                $this->matchDetails();
                                sleep(rand(1,2));
                            }
                        }
                    }
                }
            }
        }
        */
    }
}
