<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| LOL Config
| -------------------------------------------------------------------
| This file contain arrays of configs for versioning and api limits
|
*/
/**
 * @return array Config data
 */
function getConfig()
{
    $lol_config = array();
    $lol_config['lol_api_version'] = $_SERVER['LOLSGG_API_VERSION'] ?? '8.10.1';
    $lol_config['api_url'] = 'https://tr1.api.riotgames.com/';
    $lol_config['api_default_url'] = 'https://tr1.api.riotgames.com/';
    //$lol_config['api_global_url'] = 'https://global.api.riotgames.com/';
    $lol_config['dragon_data_url'] = 'http://ddragon.leagueoflegends.com/cdn/' . $lol_config['lol_api_version'] .  '/data/';
    $lol_config['dragon_sprite_url'] = 'http://ddragon.leagueoflegends.com/cdn/' . $lol_config['lol_api_version'] .  '/img/sprite/';
    $lol_config['dragon_img_url'] = 'http://ddragon.leagueoflegends.com/cdn/' . $lol_config['lol_api_version'] .  '/img/';
    $lol_config['lang'] = 'en_US';
    $lol_config['region'] = 'tr';
    $lol_config['platform'] = 'tr1';
    $lol_config['dragon_expire_time'] = 1000000;
    $lol_config['region_list'] = array(
        'br'=>'br1','eune'=>'eun1','euw'=>'euw1','jp'=>'jp1','kr'=>'kr','lan'=>'la1','las'=>'la2','na'=>'na1','oce'=>'oc1','tr'=>'tr1','ru'=>'ru','pbe'=>'pbe1','global'=>''
    );
    $lol_config['platform_list'] = array(
        'BR1' => 'BR', 'EUN1' => 'EUNE', 'EUW1' => 'EUW', 'JP1' => 'JP', 'KR' => 'KR', 'LA1' => 'LAN', 'LA2' => 'LAS', 'NA1' => 'NA', 'OC1' => 'OCE', 'TR1' => 'TR', 'RU' => 'RU', 'PBE1' => 'PBE'
    );
    $lol_config['region_code_list'] = array(
        'br'=>1001,'eune'=>2001,'euw'=>3001,'jp'=>4001,'kr'=>5001,'lan'=>6001,'las'=>7001,'na'=>8001,'oce'=>9001,'tr'=>10001,'ru'=>11001,'pbe'=>12001,'global'=>13001
    );

    $lol_config['api_app_limits'] = array(2800,11);
    $lol_config['api_limits'] = array(
        'tr'=> array(
            'summoner/v3/summoners/by-name' => array(1300-80, 61),
            'summoner/v3/summoners' => array(1300-80, 61),
            'league/v3/positions/by-summoner' => array(500-80, 61),
            'league/v3/leagues' => array(500-80, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-50, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-10, 10),
            'match/v3/timelines/by-match' => array(500-50, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(1000-80, 11)
        ),
        'euw'=> array(
            'summoner/v3/summoners/by-name' => array(2000-80, 61),
            'summoner/v3/summoners' => array(2000-80, 61),
            'league/v3/positions/by-summoner' => array(900-80, 61),
            'league/v3/leagues' => array(900-80, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-80, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-80, 10),
            'match/v3/timelines/by-match' => array(500-50, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(1000-80, 11)
        ),
        'na'=> array(
            'summoner/v3/summoners/by-name' => array(2000-80, 61),
            'summoner/v3/summoners' => array(2000-80, 61),
            'league/v3/positions/by-summoner' => array(900-80, 61),
            'league/v3/leagues' => array(900-80, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-10, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-10, 10),
            'match/v3/timelines/by-match' => array(500-50, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(1000-80, 11)
        ),
        'oce'=> array(
            'summoner/v3/summoners/by-name' => array(800-80, 61),
            'summoner/v3/summoners' => array(800-80, 61),
            'league/v3/positions/by-summoner' => array(55-25, 61),
            'league/v3/leagues' => array(55-20, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-10, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-10, 10),
            'match/v3/timelines/by-match' => array(505030, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(500-80, 11)
        ),
        'eune'=> array(
            'summoner/v3/summoners/by-name' => array(1600-80, 61),
            'summoner/v3/summoners' => array(1600-80, 61),
            'league/v3/positions/by-summoner' => array(165-40, 61),
            'league/v3/leagues' => array(165-40, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-10, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-10, 10),
            'match/v3/timelines/by-match' => array(500-50, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(500-80, 11)
        ),
        'br'=> array(
            'summoner/v3/summoners/by-name' => array(1300-80, 61),
            'summoner/v3/summoners' => array(1300-80, 61),
            'league/v3/positions/by-summoner' => array(90-25, 61),
            'league/v3/leagues' => array(90-25, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-10, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-10, 10),
            'match/v3/timelines/by-match' => array(500-50, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(500-80, 11)
        ),
        'jp'=> array(
            'summoner/v3/summoners/by-name' => array(800-80, 61),
            'summoner/v3/summoners' => array(800-80, 61),
            'league/v3/positions/by-summoner' => array(35-20, 61),
            'league/v3/leagues' => array(35-20, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-10, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-10, 10),
            'match/v3/timelines/by-match' => array(505030, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(500-80, 11)
        ),
        'kr'=> array(
            'summoner/v3/summoners/by-name' => array(2000-80, 61),
            'summoner/v3/summoners' => array(2000-80, 61),
            'league/v3/positions/by-summoner' => array(90-30, 61),
            'league/v3/leagues' => array(90-30, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-10, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-10, 10),
            'match/v3/timelines/by-match' => array(500-50, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(500-80, 11)
        ),
        'lan'=> array(
            'summoner/v3/summoners/by-name' => array(1000-80, 61),
            'summoner/v3/summoners' => array(1000-80, 61),
            'league/v3/positions/by-summoner' => array(80-30, 61),
            'league/v3/leagues' => array(80-30, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-10, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-10, 10),
            'match/v3/timelines/by-match' => array(500-50, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(500-80, 11)
        ),
        'las'=> array(
            'summoner/v3/summoners/by-name' => array(1000-80, 61),
            'summoner/v3/summoners' => array(1000-80, 61),
            'league/v3/positions/by-summoner' => array(80-30, 61),
            'league/v3/leagues' => array(80-30, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-10, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-10, 10),
            'match/v3/timelines/by-match' => array(500-50, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(500-80, 11)
        ),
        'ru'=> array(
            'summoner/v3/summoners/by-name' => array(600-80, 61),
            'summoner/v3/summoners' => array(600-80, 61),
            'league/v3/positions/by-summoner' => array(35-20, 61),
            'league/v3/leagues' => array(35-20, 61),
            'champion-mastery/v3/champion-masteries/by-summoner' => array(20000-10, 10),
            'spectator/v3/active-games/by-summoner' => array(20000-10, 10),
            'match/v3/timelines/by-match' => array(505030, 11),
            'match/v3/matchlists/by-account' => array(1000-80, 11),
            'match/v3/matches' => array(500-80, 11)
        ),
    );


    return $lol_config;
}
