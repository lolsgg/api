<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Redis Config
| -------------------------------------------------------------------
| This file contains an array of redis configs
|
*/

$config['socket_type'] = 'tcp'; //`tcp` or `unix`
$config['socket'] = '/var/run/redis.sock'; // in case of `unix` socket type
if (ENVIRONMENT == 'development') {
    $config['host'] = '192.168.1.113';
    $config['port'] = 6379;
} else {
    $config['host'] = 'lolsgg-cache-v4.ocdl7f.ng.0001.use2.cache.amazonaws.com';
    $config['port'] = 6379;
}
$config['timeout'] = 0;
