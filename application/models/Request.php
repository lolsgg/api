<?php

class Request extends CI_Model
{
    /**
     * @param string $date
     * @return string format date to d.m.Y
     */
    public function shortDateFormat($date)
    {
        return date('d.m.Y', strtotime($date));
    }

    /**
     * @param string $date
     * @return string format date to d.m.Y H:i
     */
    public function longDateFormat($date)
    {
        return date('d.m.Y H:i', strtotime($date));
    }

    /**
     * @param $since
     * @return array
     */
    public function timeSince($since)
    {
        $chunks = array(
            array(60 * 60 * 24 * 365, 'year'),
            array(60 * 60 * 24 * 30, 'month'),
            // array(60 * 60 * 24 * 7, 'Week'),
            array(60 * 60 * 24, 'day'),
            array(60 * 60, 'hour'),
            array(60, 'min'),
            array(1, 'sec')
        );

        $count = 0;
        $name = '';
        for ($i = 0, $j = count($chunks); $i < $j; $i++) {
            $seconds = $chunks[$i][0];
            $name = $chunks[$i][1];
            if (($count = floor($since / $seconds)) != 0) {
                break;
            }
        }

        $print = ($count == 1) ? array(1, $name) : array($count, $name);
        return $print;
    }

    /**
     * @param $time
     * @return array
     */
    public function secondToTime($time)
    {
        $chunks = array(
            array(60 * 60 * 24 * 365, 'year'),
            array(60 * 60 * 24 * 30, 'month'),
            array(60 * 60 * 24 * 7, 'week'),
            array(60 * 60 * 24, 'day'),
            array(60 * 60, 'hour'),
            array(60, 'min'),
            array(1, 'sec')
        );

        $count = 0;
        $name = '';
        for ($i = 0, $j = count($chunks); $i < $j; $i++) {
            $seconds = $chunks[$i][0];
            $name = $chunks[$i][1];
            if (($count = floor($time / $seconds)) != 0) {
                break;
            }
        }

        $print = ($count == 1) ? array(1, $name) : array($count, $name);
        return $print;
    }

    /**
     * @param $time
     * @return string
     */
    public function timestampToMinSec($time)
    {
        $chunks = array(
            array(60, 'Min'),
            array(1, 'Sec')
        );

        $count = 0;
        $name = '';
        for ($i = 0, $j = count($chunks); $i < $j; $i++) {
            $seconds = $chunks[$i][0];
            $name = $chunks[$i][1];
            if (($count = floor($time / 1000 / $seconds)) != 0) {
                break;
            }
        }

        $print = ($count == 1) ? '1 ' . $name : "$count {$name}";
        return $print;
    }

    /**
     * @param $time
     * @return string
     */
    public function timestampToMin($time)
    {
        $chunks = array(
            array(60, 'Min')
        );

        $count = 0;
        $name = '';
        for ($i = 0, $j = count($chunks); $i < $j; $i++) {
            $seconds = $chunks[$i][0];
            $name = $chunks[$i][1];
            if (($count = floor($time / 1000 / $seconds)) != 0) {
                break;
            }
        }

        $print = ($count == 1) ? '1 ' . $name : "$count {$name}";
        return $print;
    }

    /**
     * @param string $header_string Header data
     * @return array Header data as array
     */
    public function headerStringToArray(string $header_string)
    {
        $headers = array();
        $exp = explode("\r\n", $header_string);
        foreach ($exp as $header) {
            $exp_ = explode(':', $header, 2);
            if (count($exp_) > 1) {
                $headers[$exp_[0]] = $exp_[1];
            }

        }
        return $headers;
    }

    /**
     * @return string User's IP Address
     */
    public function getClientIp()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    /**
     * Sends reCaptcha code to reCaptcha server and validates
     * @param string $captcha
     * @return bool
     */
    public function reCaptchaCheck(string $captcha)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $ch = curl_init();

        $secretcode = '6Lf0nh8UAAAAAFcPCMa8bmK-pxWHtHqkqkU3uQKy';

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "secret=" . $secretcode . "&response=" . $captcha);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec($ch));
        curl_close($ch);
        if ($data) {
            return $data->success;
        }
        return false;
    }

    /**
     * Makes Curl request
     * @param string $url
     * @param string $data
     * @return bool|string Data from $url
     */
    public function curl(string $url, string $data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);

        curl_close($ch);

        if ($server_output) {
            return $server_output;
        } else {
            return false;
        }
    }

    /**
     * Makes curl request and caches it until expire time
     * @param string $url
     * @param int $expire
     * @return mixed
     */
    public function curlWithCache(string $url, int $expire = 300)
    {
        if (!$result = $this->cache->redis->get(md5($url))) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            $result = curl_exec($ch);
            $this->cache->redis->save(md5($url), $result, $expire);
        }
        return $result;
    }

    /**
     * Makes curl request with predefined proxy server
     * @param $url
     * @return mixed
     */
    public function curlWithProxy(string $url)
    {
        $proxy_uri = 'user:05380538proxy**@94.177.234.184:13857';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_PROXY, $proxy_uri);
        $result = curl_exec($ch);

        return $result;
    }

    /**
     * @return bool
     */
    public function isDebug()
    {
        if ($this->input->ip_address() == '195.155.173.237') {
            return true;
        }
        return false;
    }

    /**
     * Makes the text seo friendly
     * @param string $text
     * @return mixed|string
     */
    public function seoText(string $text)
    {
        $text = mb_strtolower($this->replaceTr($text));
        $text = preg_replace("/[^A-Za-z0-9 ]/", '', $text);
        $text = str_replace(array("    ", "   ", "  ", " "), "-", $text);
        return $text;
    }

    /**
     * Change turkish characters with english characters
     * @param string $text
     * @return string
     */
    public function replaceTr(string $text)
    {
        $text = trim($text);
        $search = array('Ç', 'ç', 'Ğ', 'ğ', 'ı', 'İ', 'Ö', 'ö', 'Ş', 'ş', 'Ü', 'ü');
        $replace = array('c', 'c', 'g', 'g', 'i', 'i', 'o', 'o', 's', 's', 'u', 'u');
        $new_text = str_replace($search, $replace, $text);
        return $new_text;
    }

    /**
     * @param array $array
     * @param $on
     * @param int $order
     * @return object
     */
    public function championSort(array $array, $on, $order = SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            $s = 1;
            foreach ($sortable_array as $k => $v) {
                $new_array[$s] = (object)$array[$k];
                if (is_array($new_array)) {
                    $new_array[$s]->sort = $s;
                } else {
                    $new_array->{$s}->sort = $s;
                }

                $s++;
            }
        }

        return (object)$new_array;
    }

    /**
     * @param string|array $email_list E-Mail List
     * @param string $subject E-Mail Subject
     * @param string $message E-Mail Body
     * @param string $reply_to optional Reply-To
     * @return bool E-Mail status
     */

    public function sendEmailFromNoreply($email_list, string $subject, string $message, string $reply_to = '')
    {
        $email = 'info@lols.gg';
        $pass = '18711565onmus';
        $server = 'ssl://smtp-relay.gmail.com';

        $config['protocol'] = "smtp";
        $config['smtp_host'] = $server;
        $config['smtp_port'] = "465";
        $config['smtp_user'] = $email;
        $config['smtp_pass'] = $pass;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $this->load->library('email', $config);

        $this->email->from($email, 'Lols.gg');
        $this->email->to($email_list);
        if ($reply_to) {
            $this->email->reply_to($reply_to);
        }

        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();
    }


    /**
     * Send e-mail with aws ses
     * @param string $from_email From E-Mail.
     * @param array $email_list E-Mail List
     * @param string $subject E-Mail Subject
     * @param string $html_message E-Mail HTML Body
     * @return bool E-Mail status
     */

    public function sendEmailAwsSes(string $from_email, array $email_list, string $subject, string $html_message)
    {
        $sharedConfig = [
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => [
                'key' => 'AKIAJQJBVX232WFPD77A',
                'secret' => '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',
            ]
        ];

        try {
            $sdk = new Aws\Sdk($sharedConfig);
            $sesClient = $sdk->createSes();
            $sesClient->sendEmail(
                array('Destination' => ['ToAddresses' => $email_list],
                    'Message' => [
                        'Body' => [
                            'Html' => [
                                'Charset' => "UTF-8",
                                'Data' => $html_message,
                            ],
                        ],
                        'Subject' => [
                            'Charset' => "UTF-8",
                            'Data' => $subject,
                        ],
                    ],
                    'Source' => $from_email
                )
            );
            return true;

        } catch (Aws\Exception\AwsException $e) {
            return false;
        }
    }

    /**
     * Checks if language exists
     * @param string $lang
     * @return bool
     */
    public function isLanguageExist(string $lang)
    {
        $lang = str_replace(array('.', ' ', '"', "'"), array(''), $lang);
        if (strlen($lang) > 0 && strlen($lang) < 5) {
            return file_exists(APPPATH . "language/" . mb_strtolower($lang) . "/" . 'site_lang.php');
        }
        return false;
    }

    public function pr($pr)
    {
        echo '<pre>';
        print_r($pr);
        echo '</pre>';
    }

    /**
     * Generate a random string
     * @param int $length
     * @return string
     */
    public function generateRandomString($length = 5)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function pointsToK($points)
    {
        return number_format($points / 1000, 1, '.', '') . 'K';
    }

    /**
     * @param string $remote_file File Name
     * @param string $contents Contents
     * @return bool
     */
    public function uploadFileToCdn77(string $remote_file, string $contents)
    {
        $tmpfile = tmpfile();
        fwrite($tmpfile, $contents);

        $metaDatas = stream_get_meta_data($tmpfile);
        $tmpFilename = $metaDatas['uri'];

        $this->load->library('ftp');

        $config['hostname'] = 'push-19.cdn77.com';
        $config['username'] = 'user_o2slv6f9';
        $config['password'] = 'GAoS1XpYrq4kbkq0DdUH';

        $this->ftp->connect($config);

        $status = $this->ftp->upload($tmpFilename, $remote_file, 'ascii', 0775);

        $this->ftp->close();

        fclose($tmpfile);

        return $status;
    }

    /**
     * Purge CDN77 Cachepurge
     * @param array $files
     * @return bool
     */
    public function cdn77Purge(array $files)
    {
        if (count($files)) {
            $purge_data = array(
                'login' => 'info@lols.gg',
                'passwd' => 'fTs3Im5adhXkLJcO90WMVFBxEnbYvqz2',
                'cdn_id' => 133765,
                'url' => $files
            );
            $purge_result = json_decode($this->curl("https://api.cdn77.com/v2.0/data/purge", http_build_query($purge_data)));
            if (isset($purge_result->status) && $purge_result->status == "ok") {
                return true;
            }
        }
        return false;
    }

}
