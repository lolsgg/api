<?php

class Throttle extends CI_Model
{
    public function set($key_, $limit, $seconds)
    {
        $key = $key_ . '_' . $limit . '_' . $seconds . '::Throttle';
        $key_end_time = $key . ':time';
        $current_count = $this->cache->redis->get($key);
        $end_time = $this->cache->redis->get($key_end_time);
        if ($current_count) {
            if ($current_count >= $limit) {
                $remain = $end_time - time();
                if ($remain > 0) {
                    sleep($remain);
                }
            }
            $this->cache->redis->increment($key, 1);
        } else {
            $this->cache->redis->save($key, 1, $seconds);
            $this->cache->redis->save($key_end_time, time() + $seconds, $seconds);
        }
    }
}