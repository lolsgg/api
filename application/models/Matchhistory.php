<?php

class Matchhistory extends CI_Model
{
    private $client;

    /**
     * Matchhistory constructor.
     */
    function __construct()
    {
        if (ENVIRONMENT == 'development') {
            $dns = "mongodb://192.168.1.113:27017";
            //$dns = "mongodb://lolsgg:mongo*18711565*aws@18.218.103.55:27017";
        } else {
            $dns = "mongodb://lolsgg:mongo*18711565*aws@172.31.26.234:27017";
        }

        try {
            $this->client = new MongoDB\Client($dns);
        } catch (Exception $e) {

        }
    }

    /**
     * @deprecated
     * @param $region
     * @param $match_data
     * @return \MongoDB\UpdateResult
     */
    public function addMatchHistory($region, $match_data)
    {
        $region = mb_strtolower($region);
        //$start = microtime(true);

        $collection = $this->client->match_history->{$region};

        $updateResult = $collection->updateOne(
            ['_id' => $match_data->matchId],
            ['$set' => $match_data],
            ['upsert' => true]
        );
        return $updateResult;
    }

    /**
     * @param string $region
     * @param $matches
     * @return bool
     */
    public function addBulkMatchHistory(string $region, $matches)
    {
        $region = mb_strtolower($region);

        $collection = $this->client->match_history->{$region};

        try {
            $collection->insertMany($matches);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param string $region
     * @param array $match_ids
     * @param bool $timeline_status
     * @return array|stdClass
     */
    public function getMatchHistory(string $region, array $match_ids, bool $timeline_status = false)
    {
        $region = mb_strtolower($region);

        $collection = $this->client->match_history->{$region};

        $timeline = array();
        if (!$timeline_status) {
            $timeline = array('projection' => array('timeline' => false));
        }
        try {
            $cursor = $collection->find(array('_id' => array('$in' => $match_ids)), $timeline);
        } catch (Exception $e) {
            return array();
        }

        $cursor->setTypeMap(['root' => 'object', 'document' => 'object', 'array' => 'array']);

        $results = new stdClass();
        foreach ($cursor as $document) {
            $results->{$document->_id} = $document;
        }

        return $results;
    }

    /**
     * @param string $region
     * @param int $gameId
     * @param mixed $timeline
     * @return array|\MongoDB\UpdateResult
     */
    public function addMatchHistoryTimeline(string $region, int $gameId, $timeline)
    {
        $region = mb_strtolower($region);
        //$start = microtime(true);
        try {
            $collection = $this->client->match_history->{$region};
        } catch (Exception $e) {
            return array();
        }
        $match_data = new stdClass();
        $match_data->timeline = $timeline;


        $updateResult = $collection->updateOne(
            ['_id' => $gameId],
            ['$set' => $match_data],
            ['upsert' => true]
        );
        return $updateResult;
    }

    /**
     * @param string $region
     * @param int $game_id
     * @return int
     */
    public function deleteMatchHistory(string $region, int $game_id)
    {
        $region = mb_strtolower($region);

        $collection = $this->client->match_history->{$region};

        $deleteResult = $collection->deleteOne(['_id' => $game_id]);

        return $deleteResult->getDeletedCount();

    }
}