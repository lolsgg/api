<?php

class Runes extends CI_Model
{
    public $perk_styles_en = array(
        8000 => array(
            'name' => 'PRECISION',
            'description' => 'IMPROVED ATTACKS AND SUSTAINED DAMAGE',
            'combinations' => array(
                8100 => '+18% Attack Speed',
                8200 => '+18% Attack Speed',
                8400 => '+18% Attack Speed',
                8300 => '+18% Attack Speed'
            )
        ),
        8100 => array(
            'name' => 'DOMINATION',
            'description' => 'BURST DAMAGE AND TARGET ACCESS',
            'combinations' => array(
                8000 => '+11 Attack Damage or +18 Ability Power, Adaptive',
                8200 => '+11 Attack Damage or +18 Ability Power, Adaptive',
                8400 => '+11 Attack Damage or +18 Ability Power, Adaptive',
                8300 => '+11 Attack Damage or +18 Ability Power, Adaptive'
            )
        ),
        8200 => array(
            'name' => 'SORCERY',
            'description' => 'EMPOWERED ABILITIES AND RESOURCE MANIPULATION',
            'combinations' => array(
                8000 => '+15 Attack Damage or +25 Ability Power, Adaptive',
                8100 => '+15 Attack Damage or +25 Ability Power, Adaptive',
                8400 => '+15 Attack Damage or +25 Ability Power, Adaptive',
                8300 => '+15 Attack Damage or +25 Ability Power, Adaptive'
            )
        ),
        8400 => array(
            'name' => 'RESOLVE',
            'description' => 'DURABILITY AND CROWD CONTROL',
            'combinations' => array(
                8000 => '+130 Health',
                8100 => '+130 Health',
                8200 => '+130 Health',
                8300 => '+130 Health'
            )
        ),
        8300 => array(
            'name' => 'INSPIRATION',
            'description' => 'CREATIVE TOOLS AND RULE BENDING',
            'combinations' => array(
                8000 => '+20% Potion and Elixir Duration, +20% Attack Speed',
                8100 => '+20% Potion and Elixir Duration, +16 Attack Damage or +27 Ability Power, Adaptive',
                8200 => '+20% Potion and Elixir Duration, +16 Attack Damage or +27 Ability Power, Adaptive',
                8400 => '+20% Potion and Elixir Duration, +145 Health'
            )
        ),
    );

    public $perk_styles_tr = array(
        8000 => array(
            'name' => 'İsabet',
            'description' => 'Güçlendirilmiş saldırılar ve sürekli hasar',
            'combinations' => array(
                8100 => '+%18 Saldırı Hızı',
                8200 => '+%18 Saldırı Hızı',
                8400 => '+%18 Saldırı Hızı',
                8300 => '+%18 Saldırı Hızı'
            )
        ),
        8100 => array(
            'name' => 'Hâkimiyet',
            'description' => 'Anlık hasar ve hedefe ulaşma',
            'combinations' => array(
                8000 => '+11 Saldırı Gücü veya +18 Yetenek Gücü, Değişken',
                8200 => '+11 Saldırı Gücü veya +18 Yetenek Gücü, Değişken',
                8400 => '+11 Saldırı Gücü veya +18 Yetenek Gücü, Değişken',
                8300 => '+11 Saldırı Gücü veya +18 Yetenek Gücü, Değişken'
            )
        ),
        8200 => array(
            'name' => 'Büyücülük',
            'description' => 'Güçlendirilmiş yetenekler ve kaynak yönetimi',
            'combinations' => array(
                8000 => '+15 Saldırı Gücü or +25 Yetenek Gücü, Değişken',
                8100 => '+15 Saldırı Gücü or +25 Yetenek Gücü, Değişken',
                8400 => '+15 Saldırı Gücü or +25 Yetenek Gücü, Değişken',
                8300 => '+15 Saldırı Gücü or +25 Yetenek Gücü, Değişken'
            )
        ),
        8400 => array(
            'name' => 'Azim',
            'description' => 'Dayanıklılık ve kitle kontrolü',
            'combinations' => array(
                8000 => '+130 Can',
                8100 => '+130 Can',
                8200 => '+130 Can',
                8300 => '+130 Can'
            )
        ),
        8300 => array(
            'name' => 'İlham',
            'description' => 'Yaratıcı araçlar ve kural esnetme',
            'combinations' => array(
                8000 => '+%20 İksir ve Karışım Süresi, +%20 Saldırı Hızı',
                8100 => '+%20 İksir ve Karışım Süresi, +16 Saldırı Gücü veya +27 Yetenek Gücü, Değişken',
                8200 => '+%20 İksir ve Karışım Süresi, +16 Saldırı Gücü veya +27 Yetenek Gücü, Değişken',
                8400 => '+%20 İksir ve Karışım Süresi, +145 Can'
            )
        )
    );



    function __construct()
    {
        parent::__construct();
    }
}
