<?php

class Lol extends CI_Model
{
    private $lol_api_version;
    private $api_url;
    private $api_default_url;
    private $dragon_data_url;
    private $dragon_sprite_url;
    private $dragon_img_url;
    private $lang;
    private $region;
    private $platform;
    private $dragon_expire_time;
    private $region_list;
    private $platform_list;
    private $region_code_list;
    public $api_app_limits;
    public $api_limits;

    public $match_history_update_waits = array();
    public $match_history_add_waits = array();
    public $match_history_summoners_add_waits = array();

    /**
     * Lol constructor.
     */
    public function __construct()
    {
        parent::__construct();

        //Load default configs
        include('application/config/lol.php');
        $config = getConfig();
        $this->lol_api_version = $config['lol_api_version'];
        $this->api_url = $config['api_url'];
        $this->api_default_url = $config['api_default_url'];
        $this->dragon_data_url = $config['dragon_data_url'];
        $this->dragon_sprite_url = $config['dragon_sprite_url'];
        $this->dragon_img_url = $config['dragon_img_url'];
        $this->lang = $config['lang'];
        $this->region = $config['region'];
        $this->platform = $config['platform'];
        $this->dragon_expire_time = $config['dragon_expire_time'];
        $this->region_list = $config['region_list'];
        $this->platform_list = $config['platform_list'];
        $this->region_code_list = $config['region_code_list'];
        $this->api_app_limits = $config['api_app_limits'];
        $this->api_limits = $config['api_limits'];
    }

    /**
     * @return string
     */
    public function getDragonImgUrl()
    {
        return $this->dragon_img_url;
    }

    /**
     * @return array
     */
    public function getRegionList()
    {
        return $this->region_list;
    }

    /**
     * @param string $region
     */
    public function setRegion(string $region)
    {
        $region = mb_strtolower($region);
        if (isset($this->region_list[$region])) {
            $this->region = $region;
            $this->platform = $this->region_list[$region];
            $this->api_url = 'https://' . $this->platform . '.api.riotgames.com/';
        }
    }

    /**
     * @param string $version
     */
    public function setVersion(string $version)
    {
        $this->lol_api_version = $version;
        $this->dragon_data_url = 'http://ddragon.leagueoflegends.com/cdn/' . $version . '/data/';
        $this->dragon_sprite_url = 'http://ddragon.leagueoflegends.com/cdn/' . $version . '/img/sprite/';
        $this->dragon_img_url = 'http://ddragon.leagueoflegends.com/cdn/' . $version . '/img/';
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->lol_api_version;
    }

    /**
     * @return string Lowercased region
     */
    public function getRegion()
    {
        return mb_strtolower($this->region);
    }

    /**
     * @return string Uppercased platform
     */
    public function getPlatform()
    {
        return mb_strtoupper($this->platform);
    }

    /**
     * @param string $region
     * @return int
     */
    public function getRegionCode(string $region)
    {
        $region = strtolower($region);
        return $this->region_code_list[$region];
    }

    /**
     * @param string $lang
     */
    public function setLang(string $lang)
    {
        $this->lang = $this->languageGetShort($lang);
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $url Request URL
     * @param int $expire Cache expire time
     * @param bool $onlycache If this true it doesn't make request. Gets cached data if exists.
     * @return bool|string False or Response Data
     */
    public function request(string $url, int $expire = 2, bool $onlycache = FALSE)
    {
        set_time_limit(0);
        if (!$result = $this->cache->redis->get('url_' . md5($url))) {
            if (!$onlycache) {

                //Limit control
                $reg = $this->getRegion();

                $first_request_time_app = $this->cache->redis->get("limit:v1:f:app:" . $reg);
                if ($first_request_time_app && (time() - $first_request_time_app) < $this->api_app_limits[1]) {
                    $usage_app = $this->cache->redis->increment("limit:v1:c:app:" . $reg, 1);
                    if ($usage_app > $this->api_app_limits[0]) {
                        return '';
                    }
                } else {
                    $this->cache->redis->save("limit:v1:f:app:" . $reg, time() + 1, $this->api_app_limits[1]);
                    $this->cache->redis->save("limit:v1:c:app:" . $reg, 1, $this->api_app_limits[1]);
                }

                if (isset($this->api_limits[$reg])) {
                    foreach ($this->api_limits[$reg] as $limit_url => $limits) {
                        if (strpos($url, $limit_url) !== FALSE) {
                            $first_request_time = $this->cache->redis->get("limit:v1:f:" . $reg . ':' . $limit_url);
                            if ($first_request_time && (time() - $first_request_time) < $limits[1]) {
                                $usage = $this->cache->redis->increment("limit:v1:c:" . $reg . ':' . $limit_url, 1);
                                if ($usage > $limits[0]) {
                                    return '';
                                }
                            } else {
                                $this->cache->redis->save("limit:v1:f:" . $reg . ':' . $limit_url, time() + 1, $limits[1]);
                                $this->cache->redis->save("limit:v1:c:" . $reg . ':' . $limit_url, 1, $limits[1]);
                            }
                            break;
                        }
                    }
                }

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7"));
                curl_setopt($ch, CURLOPT_HEADER, TRUE);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                if (defined('MustafaLocal')) {
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                }
                $response = curl_exec($ch);
                $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
                $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                $result = substr($response, $header_size);
                if ($http_code == 200) {
                    $this->cache->redis->save('url_' . md5($url), $result, $expire);
                }
                if ($http_code == 404) {
                    $this->cache->redis->save('url_' . md5($url), $result, ($expire > 36000 ? 36000 : $expire));
                }

                $this->addRequestLog($url, $expire, $http_code);
            }


        }
        return $result;
    }

    /**
     * @param string $url
     * @param int $expire
     * @param mixed $http_code
     */
    public function addRequestLog(string $url, int $expire, $http_code)
    {
        $this->cache->redis->lPush('lol_request_log', time() . '**' . $expire . '**' . $http_code . '**' . $this->getRegion() . '**' . $url);
        $this->cache->redis->lTrim('lol_request_log', 0, 5000);
    }

    /**
     * Generates riot api request link and makes request.
     * @param string $url
     * @param string $parameters
     * @param int $expire
     * @return bool|mixed False or Response data
     */
    public function api(string $url, string $parameters = '', int $expire = 2)
    {
        $result = json_decode($this->request(($this->api_url . $url . '?api_key=' . $this->config->item('lol_api_key') . $parameters), $expire));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        return $result ? $result : false;
    }

    /**
     * Generates riot api request link and gets cached data if exists.
     * @param string $url
     * @param string $parameters
     * @return bool|mixed False or Response data
     */
    public function apiOnlyCache(string $url, string $parameters = '')
    {
        $result = json_decode($this->request(($this->api_url . $url . '?api_key=' . $this->config->item('lol_api_key') . $parameters), 0, true));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        return $result ? $result : false;
    }

    /**
     * Generate requests from an array and caches them.
     * @param array $urls
     * @param int $limit
     */
    public function apiPreload(array $urls, int $limit)
    {
        $reg = $this->getRegion();
        $agent = new CurlX\Agent($limit);
        foreach ($urls as $url_) {
            $url = $this->api_url . $url_['url'] . '?api_key=' . $this->config->item('lol_api_key') . $url_['parameters'];

            if ($result = $this->cache->redis->get('url_' . md5($url))) {
                continue;
            }

            $continue = false;
            $first_request_time_app = $this->cache->redis->get("limit:v1:f:app:" . $reg);
            if ($first_request_time_app && (time() - $first_request_time_app) < $this->api_app_limits[1]) {
                $usage_app = $this->cache->redis->increment("limit:v1:c:app:" . $reg, 1);
                if ($usage_app > $this->api_app_limits[0]) {
                    $continue = true;
                }
            } else {
                $this->cache->redis->save("limit:v1:f:app:" . $reg, time() + 1, $this->api_app_limits[1]);
                $this->cache->redis->save("limit:v1:c:app:" . $reg, 1, $this->api_app_limits[1]);
            }
            if (isset($this->api_limits[$reg])) {
                foreach ($this->api_limits[$reg] as $limit_url => $limits) {
                    if (strpos($url, $limit_url) !== false) {
                        $first_request_time = $this->cache->redis->get("limit:v1:f:" . $reg . ':' . $limit_url);
                        if ($first_request_time && (time() - $first_request_time) < $limits[1]) {
                            $usage = $this->cache->redis->increment("limit:v1:c:" . $reg . ':' . $limit_url, 1);
                            if ($usage > $limits[0]) {
                                $continue = true;
                            }
                        } else {
                            $this->cache->redis->save("limit:v1:f:" . $reg . ':' . $limit_url, time() + 1, $limits[1]);
                            $this->cache->redis->save("limit:v1:c:" . $reg . ':' . $limit_url, 1, $limits[1]);
                        }
                        break;
                    }
                }
            }
            if ($continue) {
                continue;
            }

            $request = $agent->newRequest($url);
            $request->timeout = 10000;
            $request->options = [CURLOPT_HTTPHEADER => array("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7")];
            $request->options = [CURLOPT_RETURNTRANSFER => true];
            $request->options = [CURLOPT_BINARYTRANSFER => true];
            $request->options = [CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4];

            $request->userdata = $url_['expire'];

            // $this->Throttle->set('lol_api_request_'.$this->region,1500,5);

            $request->addListener(function (CurlX\RequestInterface $request) {
                $expire = $request->userdata;
                $url = $request->url;

                if ($request->http_code == 200) {
                    $this->cache->redis->save('url_' . md5($url), $request->response, $expire);
                }

                if ($request->http_code == 404) {
                    $this->cache->redis->save('url_' . md5($url), $request->response, ($expire > 36000 ? 36000 : $expire));
                }

                $this->addRequestLog($url, $expire, $request->http_code . ':preload');
            });
        }
        $agent->execute();
    }

    /**
     * @return bool|mixed
     */
    public function getRunesFull()
    {
        $result = json_decode($this->request($this->dragon_data_url . $this->lang . '/runesReforged.json', ($this->dragon_expire_time)));

        return $result ?: FALSE;
    }

    /**
     * @return bool|mixed
     */
    public function getRunes()
    {
        $cache_key = 'getrunes_v2_' . md5($this->lol_api_version . $this->lang);

        if ($result = $this->cache->redis->get($cache_key)) {
            return $result;
        }
        $runes_ = $this->getRunesFull();
        $runes = array();
        foreach ($runes_ as $rune_style) {
            $line = 0;
            foreach ($rune_style->slots as $slot) {
                foreach ($slot->runes as $rune) {
                    $runes[$rune->id] = array();
                    $runes[$rune->id]['id'] = $rune->id;
                    $runes[$rune->id]['key'] = $rune->key;
                    $runes[$rune->id]['name'] = $rune->name;
                    $runes[$rune->id]['shortDescription'] = str_replace(array('<hr></hr>', '<br><br>'), '<br>', $rune->shortDesc);
                    $runes[$rune->id]['description'] = str_replace(array('<hr></hr>', '<br><br>'), '<br>', $rune->longDesc);
                    $runes[$rune->id]['style_id'] = $rune_style->id;
                    $runes[$rune->id]['line'] = $line;

                }
                $line++;
            }
        }
        $this->cache->redis->save($cache_key, $runes, $this->dragon_expire_time);
        return $runes;
    }


    public function getRuneById($rune_id)
    {
        $cache_key = 'getrunesbyid_v2_' . md5($this->lol_api_version . $this->lang . '_' . $rune_id);

        if ($result = $this->cache->redis->get($cache_key)) {
            return (array)$result;
        }
        $runes = $this->getRunesFull();
        foreach ($runes as $rune_style) {
            $line = 0;
            foreach ($rune_style->slots as $slot) {
                foreach ($slot->runes as $rune) {
                    if ($rune->id == $rune_id) {
                        $rune_tmp = new stdClass();
                        $rune_tmp->id = $rune->id;
                        $rune_tmp->key = $rune->key;
                        $rune_tmp->name = $rune->name;
                        $rune_tmp->shortDescription = str_replace(array('<hr></hr>', '<br><br>'), '<br>', $rune->shortDesc);
                        $rune_tmp->description = str_replace(array('<hr></hr>', '<br><br>'), '<br>', $rune->longDesc);
                        $rune_tmp->style_id = $rune_style->id;
                        $rune_tmp->line = $line;
                        $this->cache->redis->save($cache_key, $rune_tmp, $this->dragon_expire_time);
                        return (array)$rune_tmp;
                    }
                }
                $line++;
            }
        }
        return FALSE;
    }

    /**
     * @return bool|mixed
     */
    public function getChampions()
    {
        $result = json_decode($this->request($this->dragon_data_url . $this->lang . '/champion.json', ($this->dragon_expire_time)));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        return $result ? $result->data : false;
    }

    /**
     * @return bool|mixed
     */
    public function getMaps()
    {
        $result = json_decode($this->request($this->dragon_data_url . $this->lang . '/map.json', ($this->dragon_expire_time)));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        return $result ? $result->data : false;
    }

    /**
     * Gets champion details by name
     * @param string $name
     * @return bool|mixed
     */
    public function getChampion(string $name)
    {
        $result = json_decode($this->request($this->dragon_data_url . $this->lang . '/champion/' . $name . '.json', ($this->dragon_expire_time)));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        return $result ? $result->data : false;
    }


    /**
     * @deprecated
     * @param $id
     * @param string $champData
     * @return bool|mixed
     */
    public function getChampionById(int $id, $champData = '')
    {
        $result = json_decode($this->request($this->api_default_url . 'lol/static-data/v3/champions/' . $id . '?locale=' . $this->lang . '&champData=' . $champData . '&api_key=' . $this->config->item('lol_api_key'), ($this->dragon_expire_time)));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        return $result ? $result : false;
    }

    /**
     * Gets champion details by id
     * @param int $id Champion ID
     * @return bool|mixed
     */
    public function getChampionById2(int $id)
    {
        $champData = 'all';
        $cache_key = 'getchampionbyid2_v2_' . md5($this->lol_api_version . $this->lang . '_' . $id . $champData);

        if ($result = $this->cache->redis->get($cache_key)) {
            return $result;
        }
        $result = json_decode($this->request($this->dragon_data_url . $this->lang . '/champion.json', ($this->dragon_expire_time)));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        $champions = $result->data;
        foreach ($champions as $key => $champion) {
            if ($champion->key == $id) {
                $champ_data = $this->getChampion($champion->id);

                if (isset($champ_data->{$champion->id})) {
                    $champ_data->{$champion->id}->key = $champion->id;
                    $champ_data->{$champion->id}->id = $id;
                    $this->cache->redis->save($cache_key, $champ_data->{$champion->id}, $this->dragon_expire_time);
                    return $champ_data->{$champion->id};
                }
            }
        }
        //$this->cache->redis->save($cache_key,$result,$this->dragon_expire_time);
        return false;
    }

    /**
     * @param int $id
     * @return bool|mixed
     */
    public function getSpellById(int $id)
    {
        $spellData = '';
        $cache_key = 'getspellbyid_v2_' . md5($this->lol_api_version . $this->lang . '_' . $id . $spellData);
        if ($result = $this->cache->redis->get($cache_key)) {
            return $result;
        }
        $result = json_decode($this->request($this->dragon_data_url . $this->lang . '/summoner.json', ($this->dragon_expire_time)));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        $spells = $result->data;
        foreach ($spells as $key => $spell) {

            if ($spell->key == $id) {
                $spell->key = $spell->id;
                $spell->id = $id;
                $this->cache->redis->save($cache_key, $spell, $this->dragon_expire_time);
                return $spell;
            }
        }
        $this->cache->redis->save($cache_key, $result, $this->dragon_expire_time);
        return $result ? $result : false;
    }

    /**
     * @deprecated
     * @param int $id
     * @param string $spellData
     * @return bool|mixed
     */
    public function getSpellById2(int $id, string $spellData = '')
    {
        $result = json_decode($this->request($this->api_default_url . 'lol/static-data/v3/summoner-spells/' . $id . '?locale=' . $this->lang . '&spellData=' . $spellData . '&api_key=' . $this->config->item('lol_api_key'), ($this->dragon_expire_time)));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        return $result ? $result : false;
    }

    /**
     * @return bool|mixed
     */
    public function getLanguageString()
    {
        $result = json_decode($this->request($this->api_url . 'lol/static-data/v3/language-strings?locale=' . $this->lang . '&api_key=' . $this->config->item('lol_api_key'), ($this->dragon_expire_time)));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        return $result ? $result->data : false;
    }

    /**
     * @return bool|mixed
     */
    public function getItems()
    {
        $result = json_decode($this->request($this->dragon_data_url . $this->lang . '/item.json?api_key=' . $this->config->item('lol_api_key'), ($this->dragon_expire_time)));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        return $result ? $result->data : false;
    }

    /**
     * @return bool|mixed
     */
    public function getProfileIcons()
    {
        $result = json_decode($this->request($this->dragon_data_url . $this->lang . '/profileicon.json?api_key=' . $this->config->item('lol_api_key'), ($this->dragon_expire_time)));
        return $result ? $result->data : false;
    }

    /**
     * @return bool|mixed
     */
    public function getSummonerSpells()
    {
        $result = json_decode($this->request($this->dragon_data_url . $this->lang . '/summoner.json?api_key=' . $this->config->item('lol_api_key'), ($this->dragon_expire_time)));
        if (gettype($result) == 'string') {
            $result = json_decode($result);
        }
        return $result ? $result->data : false;
    }

    /**
     * @param string $text
     * @return string
     */
    public function cleanText(string $text)
    {
        return mb_strtolower(str_replace(array(' ', "'"), array('-', ''), $text));
    }

    /**
     * @param int $id
     * @return string
     */
    public function getGameTypeById(int $id)
    {
        $gameTypes = array();
        $gameTypes[2] = '5v5 Blind Pick';
        $gameTypes[4] = '5v5 Ranked Solo/Duo';
        $gameTypes[6] = '5v5 Ranked Premade';
        $gameTypes[7] = 'Co-op vs AI';
        $gameTypes[8] = '3v3 Normal';
        $gameTypes[9] = '3v3 Ranked Flex';
        $gameTypes[14] = '5v5 Draft Pick';
        $gameTypes[16] = '5v5 Dominion Blind Pick';
        $gameTypes[17] = '5v5 Dominion Draft Pick';
        $gameTypes[25] = 'Dominion Co-op vs AI';
        $gameTypes[31] = 'Co-op vs AI Intro Bot';
        $gameTypes[32] = 'Co-op vs AI Beginner Bot';
        $gameTypes[33] = 'Co-op vs AI Intermediate Bot';
        $gameTypes[41] = '3v3 Ranked Team';
        $gameTypes[42] = '5v5 Ranked Team';
        $gameTypes[52] = 'Co-op vs AI';
        $gameTypes[61] = '5v5 Team Builder';
        $gameTypes[65] = '5v5 ARAM';
        $gameTypes[70] = 'One for All';
        $gameTypes[72] = '1v1 Snowdown Showdown';
        $gameTypes[73] = '2v2 Snowdown Showdown';
        $gameTypes[75] = '6v6 Hexakill';
        $gameTypes[76] = 'Ultra Rapid Fire';
        $gameTypes[78] = 'Mirrored One for All';
        $gameTypes[83] = 'Co-op vs AI Ultra Rapid Fire';
        $gameTypes[91] = 'Doom Bots Rank 1';
        $gameTypes[92] = 'Doom Bots Rank 2';
        $gameTypes[93] = 'Doom Bots Rank 5';
        $gameTypes[96] = 'Ascension';
        $gameTypes[98] = '6v6 Hexakill';
        $gameTypes[100] = '5v5 ARAM';
        $gameTypes[300] = 'King Poro';
        $gameTypes[310] = 'Nemesis';
        $gameTypes[313] = 'Black Market Brawlers';
        $gameTypes[315] = 'Nexus Siege';
        $gameTypes[317] = 'Definitely Not Dominion';
        $gameTypes[318] = 'All Random URF';
        $gameTypes[325] = 'All Random';
        $gameTypes[400] = '5v5 Draft Pick';
        $gameTypes[410] = '5v5 Ranked Dynamic';
        $gameTypes[420] = '5v5 Ranked Solo/Duo';
        $gameTypes[430] = '5v5 Blind Pick';
        $gameTypes[440] = '5v5 Ranked Flex';
        $gameTypes[450] = '5v5 ARAM';
        $gameTypes[460] = '3v3 Blind Pick';
        $gameTypes[470] = '3v3 Ranked Flex';
        $gameTypes[600] = 'Blood Hunt Assassin';
        $gameTypes[610] = 'Dark Star';
        $gameTypes[800] = 'Co-op vs. AI Intermediate Bot';
        $gameTypes[810] = 'Co-op vs. AI Intro Bot';
        $gameTypes[820] = 'Co-op vs. AI Beginner Bot';
        $gameTypes[830] = 'Co-op vs. AI Intro Bot';
        $gameTypes[840] = 'Co-op vs. AI Beginner Bot';
        $gameTypes[850] = 'Co-op vs. AI Intermediate Bot';
        $gameTypes[940] = 'Nexus Siege';
        $gameTypes[950] = 'Doom Bots /w difficulty voting';
        $gameTypes[960] = 'Doom Bots';
        $gameTypes[980] = 'Star Guardian Invasion: Normal';
        $gameTypes[990] = 'Star Guardian Invasion: Onslaught';
        $gameTypes[1000] = 'PROJECT: Hunters';
        $gameTypes[1010] = 'Snow URF';
        $gameTypes[1020] = 'One for All games';

        return isset($gameTypes[$id]) ? $gameTypes[$id] : '';
    }

    /**
     * @param string $type
     * @return string
     */
    public function getGameTypeByType(string $type)
    {
        $gameTypes = array();
        $gameTypes['CUSTOM'] = "Custom Game";
        $gameTypes['NORMAL_3x3'] = "Normal 3v3";
        $gameTypes['NORMAL_5x5_BLIND'] = "Normal 5v5 Blind Pick";
        $gameTypes['NORMAL_5x5_DRAFT'] = "Normal 5v5 Draft Pick";
        $gameTypes['RANKED_SOLO_5x5'] = "Ranked Solo/Duo";
        $gameTypes['RANKED_PREMADE_5x5'] = "Ranked Premade 5v5";
        $gameTypes['RANKED_FLEX_TT'] = "Ranked Flex 3v3";
        $gameTypes['RANKED_TEAM_3x3'] = "Ranked 3v3";
        $gameTypes['RANKED_TEAM_5x5'] = "Ranked 5v5";
        $gameTypes['ODIN_5x5_BLIND'] = "Dominion 5v5 Blind Pick";
        $gameTypes['ODIN_5x5_DRAFT'] = "Dominion 5v5 Draft Pick";
        $gameTypes['BOT_5x5'] = "Coop vs AI";
        $gameTypes['BOT_ODIN_5x5'] = "Dominion Coop vs AI";
        $gameTypes['BOT_5x5_INTRO'] = "Coop vs Intro Bot";
        $gameTypes['BOT_5x5_BEGINNER'] = "Coop vs Beginner Bot";
        $gameTypes['BOT_5x5_INTERMEDIATE'] = "Coop vs Intermediate Bot";
        $gameTypes['BOT_TT_3x3'] = "Coop vs AI";
        $gameTypes['GROUP_FINDER_5x5'] = "Team Builder";
        $gameTypes['ARAM_5x5'] = "ARAM";
        $gameTypes['ONEFORALL_5x5'] = "One for All";
        $gameTypes['FIRSTBLOOD_1x1'] = "Snowdown Showdown 1v1";
        $gameTypes['FIRSTBLOOD_2x2'] = "Snowdown Showdown 2v2";
        $gameTypes['SR_6x6'] = "6x6 Hexakill";
        $gameTypes['URF_5x5'] = "URF";
        $gameTypes['ONEFORALL_MIRRORMODE_5x5'] = "One for All - Mirror";
        $gameTypes['BOT_URF_5x5'] = "URF vs. AI";
        $gameTypes['NIGHTMARE_BOT_5x5_RANK1'] = "Doom Bots Rank 1";
        $gameTypes['NIGHTMARE_BOT_5x5_RANK2'] = "Doom Bots Rank 2";
        $gameTypes['NIGHTMARE_BOT_5x5_RANK5'] = "Doom Bots Rank 5";
        $gameTypes['ASCENSION_5x5'] = "Ascension";
        $gameTypes['HEXAKILL'] = "6v6 Hexakill";
        $gameTypes['BILGEWATER_ARAM_5x5'] = "Butcher's Bridge";
        $gameTypes['KING_PORO_5x5'] = "King Poro";
        $gameTypes['COUNTER_PICK'] = "Nemesis";
        $gameTypes['BILGEWATER_5x5'] = "Black Market Brawlers";
        $gameTypes['SIEGE'] = "Nexus Siege";
        $gameTypes['DEFINITELY_NOT_DOMINION_5x5'] = "Definitely Not Dominion";
        $gameTypes['ARURF_5X5'] = "URF Random";
        $gameTypes['ARSR_5x5'] = "All Random Summoner's Rift";
        $gameTypes['TEAM_BUILDER_DRAFT_UNRANKED_5x5'] = "Normal Draft Pick";
        $gameTypes['TEAM_BUILDER_DRAFT_RANKED_5x5'] = "Ranked 5v5";
        $gameTypes['TEAM_BUILDER_RANKED_SOLO'] = "Ranked Solo/Duo";
        $gameTypes['TB_BLIND_SUMMONERS_RIFT_5x5'] = "Normal 5v5 Blind Pick";
        $gameTypes['RANKED_FLEX_SR'] = "Ranked Flex 5v5";
        $gameTypes['ASSASSINATE_5x5'] = "Blood Hunt Assassin";
        $gameTypes['DARKSTAR_3x3'] = "Darkstar";
        $gameTypes['STARGUARDIAN'] = "Star Guardian Invasion games";
        $gameTypes['PROJECT'] = "PROJECT: Hunters games";
        $gameTypes['SNOWURF'] = "Snow URF";
        return isset($gameTypes[$type]) ? $gameTypes[$type] : '';
    }

    /**
     * @param string $type
     * @return bool
     */
    public function isMatchTypeNormal(string $type)
    {
        $gameTypes['NONE'] = true;
        $gameTypes['NORMAL'] = true;
        $gameTypes['NORMAL_3x3'] = true;
        $gameTypes['ODIN_UNRANKED'] = true;
        $gameTypes['ARAM_UNRANKED_5x5'] = true;
        $gameTypes['BOT'] = true;
        $gameTypes['BOT_3x3'] = true;
        $gameTypes['ONEFORALL_5x5'] = true;
        $gameTypes['FIRSTBLOOD_1x1'] = true;
        $gameTypes['FIRSTBLOOD_2x2'] = true;
        $gameTypes['SR_6x6'] = true;
        $gameTypes['CAP_5x5'] = true;
        $gameTypes['SNOWURF'] = true;
        $gameTypes['URF'] = true;
        $gameTypes['URF_BOT'] = true;
        $gameTypes['NIGHTMARE_BOT'] = true;
        $gameTypes['ASCENSION'] = true;
        $gameTypes['HEXAKILL'] = true;
        $gameTypes['KING_PORO'] = true;
        $gameTypes['COUNTER_PICK'] = true;
        $gameTypes['BILGEWATER'] = true;
        $gameTypes['SIEGE'] = true;
        return isset($gameTypes[$type]) ? $gameTypes[$type] : false;
    }

    /**
     * @param int $account_id
     * @return bool
     */
    public function updateSummonerMatches(int $account_id)
    {
        $insert = array();
        $summoner_to_match_insert = array();
        $match_list = $this->api("lol/match/v3/matchlists/by-account/" . $account_id, '', 600);
        if (isset($match_list->matches)) {
            foreach ($match_list->matches as $match) {
                $season = $match->season;
                $queue = $match->queue;
                $timestamp = $match->timestamp;
                $gameId = $match->gameId;
                //$champion_id = $match->champion;
                $insert[] = array('game_id' => $gameId, 'season' => $season, 'queue_type' => $queue, 'start_time' => round($timestamp / 1000));
                $summoner_to_match_insert[] = array('account_id' => $account_id, 'game_id' => $gameId);
            }
        }
        if (count($insert) > 0) {
            $region_lower = $this->lol->getRegion();
            $this->db->insert_ignore_batch('match_history_' . $region_lower, $insert);
            $this->db->insert_ignore_batch('summoner_to_match_' . $region_lower, $summoner_to_match_insert);
            return true;
        }
        return false;
    }

    /**
     * @param string $division
     * @param string $tier
     * @return string
     */
    public function divisionTierCombine(string $division, string $tier)
    {
        $result = 'Unranked';
        if ($tier == 'MASTER' || $tier == 'CHALLENGER') {
            $result = $tier;
        } else if (trim($division)) {
            $result = $tier . '_' . $division;
        }
        return $result;
    }

    /**
     * @param int $account_id
     * @param int $limit
     * @param int $start
     * @param string $game_type_filter
     * @return bool|stdClass
     */
    public function getSummonerMatches(int $account_id, int $limit = 20, int $start = 0, string $game_type_filter = '')
    {
        $region = mb_strtoupper($this->region);
        $region_lower = $this->lol->getRegion();
        $this->db->where_in('game_id', "(SELECT game_id FROM summoner_to_match_" . $region_lower . " WHERE account_id = " . (int)$account_id . ")", false);
        $this->db->from('match_history_' . $region_lower);
        if (!empty($game_type_filter)) {
            $game_type_filter = array_map('intval', explode(",", $game_type_filter));
            $this->db->where_in('queue_type', $game_type_filter);
        }

        $this->db->order_by('start_time', 'desc');
        $this->db->limit($limit, $start);
        $matches_q = $this->db->get();

        if ($matches_q->num_rows()) {
            $match_results = new stdClass();
            $matches = $matches_q->result();
            $match_ids = array();
            $download_match_ids = array();


            foreach ($matches as $match) {

                $result = new stdClass();
                $result->info = $match;
                if ($match->status == 0) {
                    $download_match_ids[] = (int)$match->game_id;
                }
                $match_ids[] = (int)$match->game_id;
                $match_results->{$match->game_id} = $result;
            }

            if (count($download_match_ids)) {
                $urls = array();
                foreach ($download_match_ids as $download_match_id) {
                    //$region_lower = mb_strtolower($region);
                    $urls[] = array('url' => "lol/match/v3/matches/" . $download_match_id, 'parameters' => '', 'match_q' => $match_results->{$download_match_id}->info);
                }
                $this->matchHistoryRequest($urls, count($download_match_ids));

                if (count($this->match_history_add_waits)) {
                    $this->matchhistory->addBulkMatchHistory($region, $this->match_history_add_waits);
                    //$addBulkMatchHistoryResult =
                    //if (!$addBulkMatchHistoryResult) {
                    // return false;
                    //}
                }
                $region_lower = $this->lol->getRegion();
                if (count($this->match_history_summoners_add_waits)) {
                    $this->db->insert_ignore_batch('summoner_to_match_' . $region_lower, $this->match_history_summoners_add_waits);
                }
                if (count($this->match_history_update_waits)) {
                    $this->db->update_batch('match_history_' . $region_lower, $this->match_history_update_waits, 'game_id');
                }
            }

            $matches_details = $this->matchhistory->getMatchHistory($region, $match_ids);

            foreach ($matches_details as $match_details) {
                $match_results->{$match_details->gameId}->match = $match_details;
            }

            return $match_results;
        } else {
            return false;
        }
    }

    /**
     * @param array $urls
     * @param int $limit
     */
    public function matchHistoryRequest(array $urls, int $limit = 10)
    {
        $agent = new CurlX\Agent($limit);
        $reg = $this->getRegion();
        foreach ($urls as $url_) {
            $url = $this->api_url . $url_['url'] . '?api_key=' . $this->config->item('lol_api_key') . $url_['parameters'];


            $continue = false;
            $first_request_time_app = $this->cache->redis->get("limit:v1:f:app:" . $reg);
            if ($first_request_time_app && (time() - $first_request_time_app) < $this->api_app_limits[1]) {
                $usage_app = $this->cache->redis->increment("limit:v1:c:app:" . $reg, 1);
                if ($usage_app > $this->api_app_limits[0]) {
                    $continue = true;
                }
            } else {
                $this->cache->redis->save("limit:v1:f:app:" . $reg, time() + 1, $this->api_app_limits[1]);
                $this->cache->redis->save("limit:v1:c:app:" . $reg, 1, $this->api_app_limits[1]);
            }
            if (isset($this->api_limits[$reg])) {
                foreach ($this->api_limits[$reg] as $limit_url => $limits) {
                    if (strpos($url, $limit_url) !== false) {
                        $first_request_time = $this->cache->redis->get("limit:v1:f:" . $reg . ':' . $limit_url);
                        if ($first_request_time && (time() - $first_request_time) < $limits[1]) {
                            $usage = $this->cache->redis->increment("limit:v1:c:" . $reg . ':' . $limit_url, 1);
                            if ($usage > $limits[0]) {
                                $continue = true;
                            }
                        } else {
                            $this->cache->redis->save("limit:v1:f:" . $reg . ':' . $limit_url, time() + 1, $limits[1]);
                            $this->cache->redis->save("limit:v1:c:" . $reg . ':' . $limit_url, 1, $limits[1]);
                        }
                        break;
                    }
                }
            }

            if ($continue) {
                continue;
            }

            $request = $agent->newRequest($url);
            $request->timeout = 10000;
            $request->options = [CURLOPT_HTTPHEADER => array("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7")];
            $request->options = [CURLOPT_RETURNTRANSFER => true];
            $request->options = [CURLOPT_BINARYTRANSFER => true];
            $request->options = [CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4];
            if (defined('MustafaLocal')) {
                $request->options = [CURLOPT_SSL_VERIFYPEER => false];
            }

            $request->userdata = $url_;

            $request->addListener(function (CurlX\RequestInterface $request) {
                $match = json_decode($request->response);

                $url_ = $request->userdata;
                $match_q = $url_['match_q'];
                $this->addRequestLog($this->api_url . $url_['url'] . '?api_key=' . $this->config->item('lol_api_key') . $url_['parameters'], 0, $request->http_code . ':match');
                $game_id = (int)$match_q->game_id;
                if (isset($match->matchId) || isset($match->gameId)) {
                    $match->_id = $match->gameId;

                    $match_history_update = array();
                    $match_history_update['version'] = $match->gameVersion;
                    $match_history_update['game_id'] = $game_id;
                    $match_history_update['game_length'] = $match->gameDuration;
                    $match_history_update['map_id'] = $match->mapId;
                    $match_history_update['game_mode'] = $match->gameMode;
                    $match_history_update['game_type'] = $match->gameType;
                    $match_history_update['update_date'] = date('Y-m-d H:i:s');
                    $match_history_update['status'] = 1;

                    if (isset($match->participants) && count($match->participants) > 0) {
                        //$participant_insert = array();
                        $summoner_identities = array();

                        foreach ($match->participantIdentities as $participant) {
                            $summoner_identities[$participant->participantId] = $participant->player;
                        }

                        foreach ($match->participants as $participant) {
                            $participant_id = $participant->participantId;
                            $participant->identity = $summoner_identities[$participant_id];
                            if (isset($participant->identity->accountId) && $participant->identity->accountId > 0) {
                                $account_id = (int)$participant->identity->accountId;


                                $this->match_history_summoners_add_waits[] = array('account_id' => $account_id, 'game_id' => $game_id);
                            }
                        }
                        $this->match_history_update_waits[] = $match_history_update;
                        $this->match_history_add_waits[] = $match;

                    }
                }
            });
        }
        $agent->execute();
    }

    /**
     * @deprecated
     * @param $match_q
     * @return bool|stdClass
     */
    public function getMatchDetails($match_q)
    {
        /*
         * DEPRECATED
         * DEPRECATED
         * DEPRECATED
         * DEPRECATED
         */
        $match_id = $match_q->match_id;
        $region = mb_strtoupper($this->region);
        $match = $this->api("api/lol/" . $region . "/v2.2/match/" . $match_id, '&includeTimeline=true', 3600);//süre değiştirilecek
        if (isset($match->matchId)) {
            $match_history_update = array();
            $match_history_update['version'] = $match->matchVersion;
            $match_history_update['game_length'] = $match->matchDuration;
            $match_history_update['map_id'] = $match->mapId;
            $match_history_update['match_mode'] = $match->matchMode;
            $match_history_update['match_type'] = $match->matchType;
            $match_history_update['status'] = 1;

            if (isset($match->participants) && count($match->participants) > 0) {
                //$participant_insert = array();
                $summoner_identities = array();
                $summoner_to_match_insert = array();

                foreach ($match->participantIdentities as $participant) {
                    $summoner_identities[$participant->participantId] = $participant->player;
                }

                foreach ($match->participants as $participant) {
                    $participant_id = $participant->participantId;
                    $participant->identity = $summoner_identities[$participant_id];
                    $summoner_id = $participant->identity->summonerId;


                    $summoner_to_match_insert[] = array('summoner_id' => $summoner_id, 'match_id' => $match_id);
                }

                $region_lower = $this->lol->getRegion();

                $this->db->set('update_date', 'NOW()', FALSE);
                $this->db->where('game_id', $match_id);
                $this->db->update('match_history_' . $region_lower, $match_history_update);

                $this->db->insert_ignore_batch('summoner_to_match_' . $region_lower, $summoner_to_match_insert);

                //$this->matchhistory->addMatchHistory($region, $match);

                $result = new stdClass();
                $result->info = (object)array_merge((array)$match_q, $match_history_update);
                return $result;
            }
        }
        return false;
    }

    /**
     * @param string $map_name
     * @return string
     */
    public function getMapName(string $map_name)
    {
        $maps = array();
        $maps['NewTwistedTreeline'] = "Twisted Treeline";
        $maps['SummonersRift'] = "Old Summoner's Rift";
        $maps['WIPUpdate'] = "Map Work In Progress";
        $maps['CrystalScar'] = "Crystal Scar";
        $maps['SummonersRiftNew'] = "Summoner's Rift";
        $maps['ProvingGroundsNew'] = "Proving Grounds";
        return isset($maps[$map_name]) ? $maps[$map_name] : $map_name;
    }

    /**
     * @param int $map_id
     * @return int|string
     */
    public function getMapNameById(int $map_id)
    {
        $maps = array();
        $maps[1] = "Summoner's Rift";
        $maps[2] = "Summoner's Rift";
        $maps[3] = "The Proving Grounds";
        $maps[4] = "Twisted Treeline";
        $maps[8] = "The Crystal Scar";
        $maps[10] = "Twisted Treeline";
        $maps[11] = "Summoner's Rift";
        $maps[12] = "Howling Abyss";
        $maps[14] = "Butcher's Bridge";
        $maps[16] = "Cosmic Ruins";
        $maps[18] = "Valoran City Park";
        $maps[19] = "Substructure 43";
        return isset($maps[$map_id]) ? $maps[$map_id] : $map_id;
    }

    /**
     * @param string $lang
     * @return string
     */
    public function languageCheck(string $lang)
    {
        $languages = array(
            "en_US" => "en_US",
            "cs_CZ" => "cs_CZ",
            "de_DE" => "de_DE",
            "el_GR" => "el_GR",
            "en_AU" => "en_AU",
            "en_GB" => "en_GB",
            "en_PH" => "en_PH",
            "en_PL" => "en_PL",
            "en_SG" => "en_SG",
            "es_AR" => "es_AR",
            "es_ES" => "es_ES",
            "es_MX" => "es_MX",
            "fr_FR" => "fr_FR",
            "hu_HU" => "hu_HU",
            "id_ID" => "id_ID",
            "it_IT" => "it_IT",
            "ja_JP" => "ja_JP",
            "ko_KR" => "ko_KR",
            "ms_MY" => "ms_MY",
            "pl_PL" => "pl_PL",
            "pt_BR" => "pt_BR",
            "ro_RO" => "ro_RO",
            "ru_RU" => "ru_RU",
            "th_TH" => "th_TH",
            "tr_TR" => "tr_TR",
            "vn_VN" => "vn_VN",
            "zh_CN" => "zh_CN",
            "zh_MY" => "zh_MY",
            "zh_TW" => "zh_TW"
        );
        return isset($languages[$lang]) ? $lang : 'en_US';
    }

    /**
     * @param string $lang
     * @return string
     */
    public function languageCheckShort(string $lang)
    {
        $languages = array(
            "en" => true,
            "cs" => true,
            "de" => true,
            "es" => true,
            "fr" => true,
            "hu" => true,
            "id" => true,
            "it" => true,
            "ja" => true,
            "ko" => true,
            "ms" => true,
            "pl" => true,
            "pt" => true,
            "ro" => true,
            "ru" => true,
            "th" => true,
            "tr" => true,
            "vn" => true,
            "zh" => true
        );
        return isset($languages[$lang]) ? $lang : 'en';
    }

    /**
     * @param string $lang
     * @return string
     */
    public function languageGetShort(string $lang)
    {
        $languages = array(
            "en" => "en_US",
            "cs" => "cs_CZ",
            "de" => "de_DE",
            "el" => "el_GR",
            "es" => "es_ES",
            "fr" => "fr_FR",
            "hu" => "hu_HU",
            "id" => "id_ID",
            "it" => "it_IT",
            "ja" => "ja_JP",
            "ko" => "ko_KR",
            "ms" => "ms_MY",
            "pl" => "pl_PL",
            "pt" => "pt_BR",
            "ro" => "ro_RO",
            "ru" => "ru_RU",
            "th" => "th_TH",
            "tr" => "tr_TR",
            "vn" => "vn_VN",
            "zh" => "zh_CN"
        );
        return isset($languages[$lang]) ? $languages[$lang] : 'en_US';
    }

    /**
     * @return array
     */
    public function getLanguageList()
    {
        $languages = array(
            "en" => "en_US",
            "cs" => "cs_CZ",
            "de" => "de_DE",
            "el" => "el_GR",
            "es" => "es_ES",
            "fr" => "fr_FR",
            "hu" => "hu_HU",
            "id" => "id_ID",
            "it" => "it_IT",
            "ja" => "ja_JP",
            "ko" => "ko_KR",
            "ms" => "ms_MY",
            "pl" => "pl_PL",
            "pt" => "pt_BR",
            "ro" => "ro_RO",
            "ru" => "ru_RU",
            "th" => "th_TH",
            "tr" => "tr_TR",
            "vn" => "vn_VN",
            "zh" => "zh_CN"
        );
        return $languages;
    }

    /**
     * @param mixed $key
     * @param bool $reverse
     * @return int|string
     */
    public function tierToNumber($key, bool $reverse = false)
    {
        if ($reverse) {
            $tier_list_reverse = array('CHALLENGER', 'MASTER', 'DIAMOND', 'PLATINUM', 'GOLD', 'SILVER', 'BRONZE');
            return isset($tier_list_reverse[$key]) ? $tier_list_reverse[$key] : '';
        }
        $tier_list = array('CHALLENGER' => 0, 'MASTER' => 1, 'DIAMOND' => 2, 'PLATINUM' => 3, 'GOLD' => 4, 'SILVER' => 5, 'BRONZE' => 6);
        return isset($tier_list[$key]) ? $tier_list[$key] : '';
    }

    /**
     * @param mixed $key
     * @param bool $reverse
     * @return int|string
     */
    public function rankToNumber($key, bool $reverse = false)
    {
        if ($reverse) {
            $rank_list = array('', 'I', 'II', 'III', 'IV', 'V');
            return isset($rank_list[$key]) ? $rank_list[$key] : '';
        }
        $rank_list = array('I' => 1, 'II' => 2, 'III' => 3, 'IV' => 4, 'V' => 5);
        return isset($rank_list[$key]) ? $rank_list[$key] : '';
    }

    /**
     * @param string $region
     * @param string $encryptionKey
     * @param int $game_id
     * @param string $platform_id
     * @return string
     */
    public function getSpactatorCode(string $region, string $encryptionKey, int $game_id, string $platform_id)
    {
        $spactator_domains = array(
            'na' => 'spectator.na.lol.riotgames.com:80',
            'euw' => 'spectator.euw1.lol.riotgames.com:80',
            'eune' => 'spectator.eu.lol.riotgames.com:8088',
            'jp' => 'spectator.jp1.lol.riotgames.com:80',
            'kr' => 'spectator.kr.lol.riotgames.com:80',
            'oce' => 'spectator.oc1.lol.riotgames.com:80',
            'br' => 'spectator.br.lol.riotgames.com:80',
            'lan' => 'spectator.la1.lol.riotgames.com:80',
            'las' => 'spectator.la2.lol.riotgames.com:80',
            'ru' => 'spectator.ru.lol.riotgames.com:80',
            'tr' => 'spectator.tr.lol.riotgames.com:80',
            'pbe' => 'pectator.pbe1.lol.riotgames.com:8088'
        );
        if (!isset($spactator_domains[$region])) {
            $region = "na";
        }
        return '"spectator ' . $spactator_domains[$region] . ' ' . $encryptionKey . ' ' . $game_id . ' ' . $platform_id;
    }

    public function getSummonerLeaguesLive($summoner_ids)
    {

    }
}
