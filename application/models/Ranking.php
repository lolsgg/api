<?php

class Ranking extends CI_Model
{
    /**
     * @param array $regions
     */
    public function updateLeagueRanking(array $regions)
    {
        foreach ($regions as $region) {
            $region_lower = strtolower($region);

            $this->cache->redis->save('maintenance:league:' . $region, true, 7200);

            $this->db->query("UPDATE summoner_league_" . $region_lower . "_1 SET position_7 = position_6, position_6 = position_5, position_5 = position_4, position_4 = position_3, position_3 = position_2, position_2 = position_1, position_1 = position;");
            $this->db->query("SET @r_counter_l1_" . $region_lower . " = 0;");
            $this->db->query("UPDATE summoner_league_" . $region_lower . "_1 SET position = (@r_counter_l1_" . $region_lower . " := @r_counter_l1_" . $region_lower . " + 1) ORDER BY ranking DESC;");

            $this->db->query("UPDATE summoner_league_" . $region_lower . "_2 SET position_7 = position_6, position_6 = position_5, position_5 = position_4, position_4 = position_3, position_3 = position_2, position_2 = position_1, position_1 = position;");
            $this->db->query("SET @r_counter_l2_" . $region_lower . " = 0;");
            $this->db->query("UPDATE summoner_league_" . $region_lower . "_2 SET position = (@r_counter_l2_" . $region_lower . " := @r_counter_l2_" . $region_lower . " + 1) ORDER BY ranking DESC;");

            $this->db->query("UPDATE summoner_league_" . $region_lower . "_3 SET position_7 = position_6, position_6 = position_5, position_5 = position_4, position_4 = position_3, position_3 = position_2, position_2 = position_1, position_1 = position;");
            $this->db->query("SET @r_counter_l3_" . $region_lower . " = 0;");
            $this->db->query("UPDATE summoner_league_" . $region_lower . "_3 SET position = (@r_counter_l3_" . $region_lower . " := @r_counter_l3_" . $region_lower . " + 1) ORDER BY ranking DESC;");

            $this->cache->redis->save('maintenance:league:' . $region, false, 7200);
        }
    }

    /**
     * @param array $regions
     */
    public function updateChampionMasteryRanking(array $regions)
    {
        $champions = $this->lol->getChampions();
        foreach ($regions as $region) {
            $this->cache->redis->save('maintenance:champmastery:' . $region, true, 10800);
            foreach ($champions as $champion) {
                if ($champion->key >= 0) {
                    $champion_id = (int)$champion->key;
                    $this->db->query("SET @r_counter_" . $region . $champion->key . " = 0");
                    $this->db->query("UPDATE champ_mastery_" . strtolower($region) . " SET position = (@r_counter_" . $region . $champion->key . " := @r_counter_" . $region . $champion->key . " + 1) WHERE champion_id = " . (int)$champion_id . " ORDER BY points DESC LIMIT 100000;");
                }
            }
            $this->cache->redis->save('maintenance:champmastery:' . $region, false, 10800);
        }
    }

    /**
     * @param array $regions
     */
    public function updateChampionMasteryRankingLeaderboard(array $regions)
    {
        foreach ($regions as $region) {
            $this->db->query("TRUNCATE TABLE leaderboard_champ_mastery_" . $region);
            $this->db->query("INSERT INTO leaderboard_champ_mastery_" . $region . " SELECT * FROM champ_mastery_" . $region . " WHERE position IS NOT NULL AND position < 1100;");
            $this->cache->redis->save('leaderboard:champ_mastery:update:' . $region, time(), 86400);
        }
    }
}
