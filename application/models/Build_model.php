<?php

class Build_model extends CI_Model
{
    private $client;
    private $mongodb_dns;

    function __construct()
    {
        parent::__construct();
        if (ENVIRONMENT == 'development') {
            $this->mongodb_dns = "mongodb://192.168.1.113:27017";
        } else {
            $this->mongodb_dns = "mongodb://lolsgg:mongo*18711565*aws@172.31.26.234:27017";
        }
    }

    /**
     * @param int $build_version Build Version
     * @param int $champion_id Champion ID
     * @param string $champion_lane Champion LAne
     * @param bool $debug Optional
     * @return array
     */
    public function items(int $build_version, int $champion_id, string $champion_lane, bool $debug = false)
    {
        $cache_key = 'build_items_v5_' . $build_version . '_TR_' . $this->lol->getLang() . '_' . $champion_id . '_' . $champion_lane;
        if (!$debug && $cache_results = $this->cache->redis->get($cache_key)) {
            return $cache_results;
        }
        $results = array();
        $this->db->select('*, (item_wins+item_losses) AS item_total');
        $this->db->where('build_version', $build_version);
        $this->db->where('champion_id', $champion_id);
        $this->db->where('champion_lane', $champion_lane);
        $this->db->order_by('item_total', 'DESC');
        $query = $this->db->get('build_items');
        if ($query->num_rows()) {
            $items = $query->result();

            $items_lol = $this->lol->getItems();

            $champion = $this->lol->getChampionById2($champion_id);

            $group_sets = array(
                array(3069, 3092, 3401),// SUPPORT and VISION
                array(2302, 2301, 2303), // VISION
                array(1400, 1401, 1402, 1408, 1409, 1410, 1412, 1413, 1414, 1416, 1418, 1419, 3671, 3672, 3673, 3675, 3706, 3711, 3715), // JUNGLE
            );

            if ($champion_id == 516) {
                $group_sets[] = array(3371, 3031); // ORNN
                $group_sets[] = array(3373, 3068); // ORNN
                $group_sets[] = array(3374, 3089); // ORNN
                $group_sets[] = array(3379, 3001); // ORNN
                $group_sets[] = array(3380, 3071); // ORNN
                $group_sets[] = array(3382, 3107); // ORNN
                $group_sets[] = array(3383, 3190); // ORNN
                $group_sets[] = array(3384, 3078); // ORNN
                $group_sets[] = array(3385, 3090); // ORNN
                $group_sets[] = array(3386, 3157); // ORNN
            }


            $ap_ad_exceptions = array(3065, 3068, 3026, 3102, 3157, 3800);
            $final_exceptions = array(3040, 3042, 3043, 3135, 3089, 2049, 3031, 3026, 3041, 3116, 3095);

            $starter_ids = array(1054, 1055, 1056, 1082, 1083);
            $jungle_starter_ids = array(1039, 1041);
            $support_starter_ids = array(3301, 3303, 3302);
            $support_to_starter_ids = array(
                3069 => 3301,
                3092 => 3303,
                3401 => 3302,
            );


            $support_starter_items = array();

            $starter_item = false;
            $jungle_starter_item = false;


            $group_sets_status = array();
            $recommend_sets_status = array();
            for ($i = 0; $i < count($group_sets); $i++) {
                $group_sets_status[] = false;
                $recommend_sets_status[] = false;
            }

            $recommend_sets_status[0] = true;
            $recommend_sets_status[2] = true;

            $recommends_tmp = array();
            $recommends = array();

            $total_item_count = 0;


            $log = '';

            $set = array('AD' => array(), 'AP' => array());

            $boots = array('AD' => array(), 'AP' => array());

            $trinkets = array();


            foreach ($items as $items_key => $item_stats) {
                $group_item_status = false;
                if ($champion_lane != 'JUNGLE' and in_array($item_stats->item_id, $group_sets[2])) {

                } else {
                    if (isset($items_lol->{$item_stats->item_id})) {
                        $item = $items_lol->{$item_stats->item_id};
                        if (!$starter_item && in_array($item_stats->item_id, $starter_ids)) {
                            $starter_item = array('stats' => $item_stats, 'item' => $item);
                        }
                        if (!$jungle_starter_item && in_array($item_stats->item_id, $jungle_starter_ids)) {
                            $jungle_starter_item = array('stats' => $item_stats, 'item' => $item);
                        }
                        if (in_array($item_stats->item_id, $support_starter_ids)) {
                            $support_starter_items[$item_stats->item_id] = array('stats' => $item_stats, 'item' => $item);
                        }
                        if (isset($item->tags) && in_array('Trinket', $item->tags)) { // if item is a trinket
                            $trinkets[] = array('stats' => $item_stats, 'item' => $item);
                        } elseif (isset($item->tags) && in_array('Boots', $item->tags)) { // is item is a boots
                            if (isset($item->depth) && $item->depth == 2) {
                                if (isset($item->tags) && in_array('Armor', $item->tags) && in_array('SpellBlock', $item->tags)) {
                                    $boots['AD'][] = array('stats' => $item_stats, 'item' => $item);
                                    $boots['AP'][] = array('stats' => $item_stats, 'item' => $item);
                                } elseif (isset($item->tags) && in_array('SpellBlock', $item->tags)) {
                                    $boots['AP'][] = array('stats' => $item_stats, 'item' => $item);
                                } elseif (isset($item->tags) && in_array('Armor', $item->tags)) {
                                    $boots['AD'][] = array('stats' => $item_stats, 'item' => $item);
                                } else {
                                    $boots['AD'][] = array('stats' => $item_stats, 'item' => $item);
                                    $boots['AP'][] = array('stats' => $item_stats, 'item' => $item);
                                }
                            }
                        } elseif (isset($item->consumed) && $item->consumed) { // if item is a consumable

                        } else {
                            $exception_item = false;
                            if (in_array($item_stats->item_id, $final_exceptions)) {
                                $exception_item = true;
                            }
                            if (isset($item->depth) && $item->depth > 2 || $exception_item) { // if item is final or exception
                                if (isset($item->requiredChampion) && $item->requiredChampion != $champion->key) { // if item requires a champion

                                } else if (in_array($item_stats->item_id, array(3144))) { // if Bilgewater

                                } else if  ($champion_id == 81 && $champion_lane == 'MIDDLE' && isset($item->tags) && !in_array('SpellDamage', $item->tags)) { // Ezreal and mid and spelldamage

                                } else {
                                    if ($item->maps->{'11'} == true) {

                                        foreach ($group_sets as $group_key => $group_set) {

                                            foreach ($group_set as $group_item_id) { // magic happens...
                                                if ($group_item_id == $item_stats->item_id) {
                                                    if ($group_sets_status[$group_key] === true) {
                                                        $group_item_status = true;
                                                        if ($recommend_sets_status[$group_key] === true && isset($recommends_tmp[$group_key])) {
                                                            $recommends[$recommends_tmp[$group_key]] = array('stats' => $item_stats, 'item' => $item);
                                                            unset($recommends_tmp[$group_key]);
                                                        }
                                                    } else {
                                                        $group_sets_status[$group_key] = true;
                                                        if ($recommend_sets_status[$group_key]) {
                                                            $recommends_tmp[$group_key] = $item_stats->item_id;
                                                        }
                                                    }
                                                }
                                            }
                                            if ($group_item_status) {
                                                break;
                                            }
                                        }
                                        if ($group_item_status === false) {
                                            $log .= '<div><img style="width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $item_stats->item_id . '.png"/>';
                                            $log .= $item->name;
                                            if ($item_stats->item_wins + $item_stats->item_losses > 0) {
                                                $win_ratio = 100 / ($item_stats->item_wins + $item_stats->item_losses) * $item_stats->item_wins;
                                                $log .= ' ';
                                                $log .= number_format($win_ratio, 2);
                                                $log .= '__ ' . $item_stats->item_wins;
                                            }


                                            if ((in_array($item_stats->item_id, $ap_ad_exceptions)) || (isset($item->tags) && in_array('Armor', $item->tags) && in_array('SpellBlock', $item->tags)) || in_array($item_stats->item_id, $group_sets[0])) {
                                                $log .= '<span style="margin-left: 10px; color: black; font-weight: bold;">AD and AP</span>';
                                                $log .= '<br>';
                                                if (count($set['AP']) < 50) {
                                                    $set['AP'][] = array('stats' => $item_stats, 'item' => $item);
                                                }
                                                if (count($set['AD']) < 50) {
                                                    $set['AD'][] = array('stats' => $item_stats, 'item' => $item);
                                                }
                                            } elseif (isset($item->tags) && in_array('SpellBlock', $item->tags)) {
                                                $log .= '<span style="margin-left: 10px; color: blue; font-weight: bold;">AP</span>';
                                                $log .= '<br>';
                                                if (count($set['AP']) < 50) {
                                                    $set['AP'][] = array('stats' => $item_stats, 'item' => $item);
                                                }
                                            } elseif (isset($item->tags) && in_array('Armor', $item->tags)) {
                                                $log .= '<span style="margin-left: 10px; color: red; font-weight: bold;">AD</span>';
                                                $log .= '<br>';
                                                if (count($set['AD']) < 50) {
                                                    $set['AD'][] = array('stats' => $item_stats, 'item' => $item);
                                                }
                                            } else {
                                                $log .= '<span style="margin-left: 10px; color: black; font-weight: bold;">AD and AP</span>';
                                                $log .= '<br>';
                                                if (count($set['AP']) < 50) {
                                                    $set['AP'][] = array('stats' => $item_stats, 'item' => $item);
                                                }
                                                if (count($set['AD']) < 50) {
                                                    $set['AD'][] = array('stats' => $item_stats, 'item' => $item);
                                                }
                                            }
                                            $total_item_count += $item_stats->item_wins + $item_stats->item_losses;
                                            $log .= '</div>';
                                        }

                                    }


                                    $log .= '<br>';
                                }
                            }
                        }
                    }
                }
            }

            if ($champion_lane == 'JUNGLE') {
                foreach ($set as $ad_ap => $items) {
                    foreach ($items as $item_key => $item) {
                        if (in_array($item['stats']->item_id, $group_sets[2])) {
                            unset($set[$ad_ap][$item_key]);
                            array_unshift($set[$ad_ap], $item);
                            if ($jungle_starter_item) {
                                $starter_item = $jungle_starter_item;
                            }
                            break;
                        }
                    }
                }
            }

            if ($champion_lane == 'BOTTOM') {
                foreach ($set as $ad_ap => $items) {
                    $s = 0;
                    foreach ($items as $item_key => $item) {
                        if ($s++ > 7) {
                            break;
                        }
                        if (in_array($item['stats']->item_id, $group_sets[0])) {
                            unset($set[$ad_ap][$item_key]);
                            array_unshift($set[$ad_ap], $item);
                            $support_starter_id = $support_to_starter_ids[$item['stats']->item_id];
                            $starter_item = $support_starter_items[$support_starter_id];
                            break;
                        }
                    }
                }
            }

            $item_limit = 5;
            if  ($champion_id == 69) { // Cassiopeia
                $item_limit = 6;
            }

            if ($debug) {
                echo '<div style="text-align: center;"><img src="https://cdn.lols.gg/img/champion/' . $champion->key . '.png" style="width:50px;">';
                echo '<h3>' . $champion->key . ' ' . $champion_lane . '</h3></div>';

                echo '<div style="padding:10px;border:1px solid black;width: 365px;float:left;"><h3 style="margin: 0 0 10px 0; text-align:center;">AD Karşı</h3>';
                $s = 0;
                foreach ($set['AD'] as $item) {
                    $s++;
                    if ($s > $item_limit) {
                        break;
                    }
                    echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $item['stats']->item_id . '.png"/>';
                    echo '<span>' . $item['item']->name . '<span style="font-size: 0.9em; margin-left: 10px; color:darkgray;">' . ($item['stats']->item_wins + $item['stats']->item_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_item_count * ($item['stats']->item_wins + $item['stats']->item_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    if (isset($recommends[$item['stats']->item_id])) {
                        echo '<img title="' . $recommends[$item['stats']->item_id]['item']->name . '" style="vertical-align: middle; width: 30px;height:30px;padding:3px;margin-left:5px;" src="https://cdn.lols.gg/img/item/' . $recommends[$item['stats']->item_id]['stats']->item_id . '.png"/>';
                    }
                    echo '</div>';
                }
                if (count($boots['AD'])) {
                    echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $boots['AD'][0]['stats']->item_id . '.png"/>';
                    echo '<span>' . $boots['AD'][0]['item']->name . '<span style="font-size: 0.9em; margin-left: 10px; color:darkgray;">' . ($boots['AD'][0]['stats']->item_wins + $boots['AD'][0]['stats']->item_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_item_count * ($boots['AD'][0]['stats']->item_wins + $boots['AD'][0]['stats']->item_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    if (count($boots['AD']) > 1) {
                        echo '<img title="' . $boots['AD'][1]['item']->name . '" style="vertical-align: middle; width: 30px;height:30px;padding:3px;margin-left:5px;" src="https://cdn.lols.gg/img/item/' . $boots['AD'][1]['stats']->item_id . '.png"/>';
                    }
                    echo '</div>';
                }
                if (count($trinkets)) {
                    echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $trinkets[0]['stats']->item_id . '.png"/>';
                    echo '<span>' . $trinkets[0]['item']->name . '<span style="font-size: 0.9em; margin-left: 10px; color:darkgray;">' . ($trinkets[0]['stats']->item_wins + $trinkets[0]['stats']->item_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_item_count * ($trinkets[0]['stats']->item_wins + $trinkets[0]['stats']->item_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    if (count($trinkets) > 1) {
                        echo '<img title="' . $trinkets[1]['item']->name . '" style="vertical-align: middle; width: 30px;height:30px;padding:3px;margin-left:5px;" src="https://cdn.lols.gg/img/item/' . $trinkets[1]['stats']->item_id . '.png"/>';
                    }
                    echo '</div>';
                }
                if ($starter_item) {
                    echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $starter_item['stats']->item_id . '.png"/>';
                    echo '<span>Starter:' . $starter_item['item']->name . '<span style="font-size: 0.9em; margin-left: 10px; color:darkgray;">' . ($starter_item['stats']->item_wins + $starter_item['stats']->item_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_item_count * ($starter_item['stats']->item_wins + $starter_item['stats']->item_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    echo '</div>';
                    if (isset($starter_item['item']->gold) && (500 - $starter_item['item']->gold->base) >= 150) {
                        $pot_item = $items_lol->{2031};
                        echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/2031.png"/>';
                        echo '<span>Starter:' . $pot_item->name . '</span>';
                        echo '</div>';
                    } else if (isset($starter_item['item']->gold) && (500 - $starter_item['item']->gold->base) >= 100) {
                        $pot_item = $items_lol->{2003};
                        echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/2003.png"/>';
                        echo '<span>Starter:' . $pot_item->name . '</span>';
                        echo '</div>';
                        echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/2003.png"/>';
                        echo '<span>Starter:' . $pot_item->name . '</span>';
                        echo '</div>';
                    } else if (isset($starter_item['item']->gold) && (500 - $starter_item['item']->gold->base) >= 50) {
                        $pot_item = $items_lol->{2003};
                        echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/2003.png"/>';
                        echo '<span>Starter:' . $pot_item->name . '</span>';
                        echo '</div>';
                    }
                }
                echo '</div>';


                echo '<div style="padding:10px;border:1px solid black;width: 365px;float:left;margin-left: 25px;"><h3 style="margin: 0 0 10px 0; text-align:center;">AP Karşı</h3>';
                $s = 0;
                foreach ($set['AP'] as $item) {
                    $s++;
                    if ($s > $item_limit) {
                        break;
                    }
                    echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $item['stats']->item_id . '.png"/>';
                    echo '<span>' . $item['item']->name . '<span style="font-size: 0.9em; margin-left: 10px; color:darkgray;">' . ($item['stats']->item_wins + $item['stats']->item_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_item_count * ($item['stats']->item_wins + $item['stats']->item_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    if (isset($recommends[$item['stats']->item_id])) {
                        echo '<img title="' . $recommends[$item['stats']->item_id]['item']->name . '" style="vertical-align: middle; width: 30px;height:30px;padding:3px;margin-left:5px;" src="https://cdn.lols.gg/img/item/' . $recommends[$item['stats']->item_id]['stats']->item_id . '.png"/>';
                    }
                    echo '</div>';
                }
                if (count($boots['AP'])) {
                    echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $boots['AP'][0]['stats']->item_id . '.png"/>';
                    echo '<span>' . $boots['AP'][0]['item']->name . '<span style="font-size: 0.9em; margin-left: 10px; color:darkgray;">' . ($boots['AP'][0]['stats']->item_wins + $boots['AP'][0]['stats']->item_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_item_count * ($boots['AP'][0]['stats']->item_wins + $boots['AP'][0]['stats']->item_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    if (count($boots['AP']) > 1) {
                        echo '<img title="' . $boots['AP'][1]['item']->name . '" style="vertical-align: middle; width: 30px;height:30px;padding:3px;margin-left:5px;" src="https://cdn.lols.gg/img/item/' . $boots['AP'][1]['stats']->item_id . '.png"/>';
                    }
                    echo '</div>';
                }
                if (count($trinkets)) {
                    echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $trinkets[0]['stats']->item_id . '.png"/>';
                    echo '<span>' . $trinkets[0]['item']->name . '<span style="font-size: 0.9em; margin-left: 10px; color:darkgray;">' . ($trinkets[0]['stats']->item_wins + $trinkets[0]['stats']->item_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_item_count * ($trinkets[0]['stats']->item_wins + $trinkets[0]['stats']->item_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    if (count($trinkets) > 1) {
                        echo '<img title="' . $trinkets[1]['item']->name . '" style="vertical-align: middle; width: 30px;height:30px;padding:3px;margin-left:5px;" src="https://cdn.lols.gg/img/item/' . $trinkets[1]['stats']->item_id . '.png"/>';
                    }
                    echo '</div>';
                }
                if ($starter_item) {
                    echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $starter_item['stats']->item_id . '.png"/>';
                    echo '<span>Starter:' . $starter_item['item']->name . '<span style="font-size: 0.9em; margin-left: 10px; color:darkgray;">' . ($starter_item['stats']->item_wins + $starter_item['stats']->item_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_item_count * ($starter_item['stats']->item_wins + $starter_item['stats']->item_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    echo '</div>';
                    if (isset($starter_item['item']->gold) && (500 - $starter_item['item']->gold->base) >= 150) {
                        $pot_item = $items_lol->{2031};
                        echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/2031.png"/>';
                        echo '<span>Starter:' . $pot_item->name . '</span>';
                        echo '</div>';
                    } else if (isset($starter_item['item']->gold) && (500 - $starter_item['item']->gold->base) >= 100) {
                        $pot_item = $items_lol->{2003};
                        echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/2003.png"/>';
                        echo '<span>Starter:' . $pot_item->name . '</span>';
                        echo '</div>';
                        echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/2003.png"/>';
                        echo '<span>Starter:' . $pot_item->name . '</span>';
                        echo '</div>';
                    } else if (isset($starter_item['item']->gold) && (500 - $starter_item['item']->gold->base) >= 50) {
                        $pot_item = $items_lol->{2003};
                        echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;height:30px;padding:3px;" src="https://cdn.lols.gg/img/item/2003.png"/>';
                        echo '<span>Starter:' . $pot_item->name . '</span>';
                        echo '</div>';
                    }
                }
                echo '</div>';


                echo '<div class="details" style="display:none; padding:10px;border:1px solid black;width: 365px;float:left; margin-top:30px; ">';
                $s = 0;
                foreach ($set['AD'] as $item) {
                    $s++;
                    if ($s < 6) {
                        continue;
                    }
                    echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $item['stats']->item_id . '.png"/>';
                    echo '<span>' . $item['item']->name . '<span style="font-size: 0.9em; margin-left: 10px; color:darkgray;">' . ($item['stats']->item_wins + $item['stats']->item_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_item_count * ($item['stats']->item_wins + $item['stats']->item_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    if (isset($recommends[$item['stats']->item_id])) {
                        echo '<img title="' . $recommends[$item['stats']->item_id]['item']->name . '" style="vertical-align: middle; width: 30px;height:30px;padding:3px;margin-left:5px;" src="https://cdn.lols.gg/img/item/' . $recommends[$item['stats']->item_id]['stats']->item_id . '.png"/>';
                    }
                    echo '</div>';
                }
                echo '</div>';

                echo '<div class="details" style="display:none; padding:10px;border:1px solid black;width: 365px;float:left; margin-top:30px; margin-left: 25px;">';
                $s = 0;
                foreach ($set['AP'] as $item) {
                    $s++;
                    if ($s < 6) {
                        continue;
                    }
                    echo '<div style="margin:0; padding:0;"><img style="vertical-align: middle; width: 30px;padding:3px;" src="https://cdn.lols.gg/img/item/' . $item['stats']->item_id . '.png"/>';
                    echo '<span>' . $item['item']->name . '<span style="font-size: 0.9em; margin-left: 10px; color:darkgray;">' . ($item['stats']->item_wins + $item['stats']->item_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_item_count * ($item['stats']->item_wins + $item['stats']->item_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    if (isset($recommends[$item['stats']->item_id])) {
                        echo '<img title="' . $recommends[$item['stats']->item_id]['item']->name . '" style="vertical-align: middle; width: 30px;height:30px;padding:3px;margin-left:5px;" src="https://cdn.lols.gg/img/item/' . $recommends[$item['stats']->item_id]['stats']->item_id . '.png"/>';
                    }
                    echo '</div>';
                }
                echo '</div>';


                echo '<div style="clear: left;margin-bottom:50px; "></div>';


                //echo $log;
            } else {
                $results['AP'] = array();
                $results['AP']['items'] = array();
                $results['AP']['boots'] = array();
                $results['AP']['trinket'] = array();
                $results['AP']['items_extra'] = array();
                $results['AP']['starter_item'] = array();
                $results['AP']['starter_pot'] = array();
                $results['AD'] = array();
                $results['AD']['items'] = array();
                $results['AD']['boots'] = array();
                $results['AD']['trinket'] = array();
                $results['AD']['items_extra'] = array();
                $results['AD']['starter_item'] = array();
                $results['AD']['starter_pot'] = array();
                foreach ($set as $ad_ap => $set_items) {
                    $s = 0;
                    foreach ($set_items as $item_key => $item) {
                        $s++;
                        if ($s < 6) {
                            $results[$ad_ap]['items'][$item_key] = array();
                            $results[$ad_ap]['items'][$item_key]['id'] = $item['stats']->item_id;
                            $results[$ad_ap]['items'][$item_key]['name'] = $item['item']->name;
                            $results[$ad_ap]['items'][$item_key]['image'] = $item['item']->image->full;
                            $results[$ad_ap]['items'][$item_key]['description'] = $item['item']->description;
                            if (isset($recommends[$item['stats']->item_id])) {
                                $recommended_item = $recommends[$item['stats']->item_id]['item'];
                                $results[$ad_ap]['items'][$item_key]['recommended'] = array();
                                $results[$ad_ap]['items'][$item_key]['recommended']['id'] = $recommends[$item['stats']->item_id]['stats']->item_id;
                                $results[$ad_ap]['items'][$item_key]['recommended']['name'] = $recommended_item->name;
                                $results[$ad_ap]['items'][$item_key]['recommended']['image'] = $recommended_item->image->full;
                                $results[$ad_ap]['items'][$item_key]['recommended']['description'] = $recommended_item->description;
                            }
                        } else if ($s < 12) {
                            $results[$ad_ap]['items_extra'][$item_key] = array();
                            $results[$ad_ap]['items_extra'][$item_key]['id'] = $item['stats']->item_id;
                            $results[$ad_ap]['items_extra'][$item_key]['name'] = $item['item']->name;
                            $results[$ad_ap]['items_extra'][$item_key]['image'] = $item['item']->image->full;
                            $results[$ad_ap]['items_extra'][$item_key]['description'] = $item['item']->description;
                        } else {
                            break;
                        }
                    }
                    if (count($boots[$ad_ap])) {
                        $results[$ad_ap]['boots']['id'] = $boots[$ad_ap][0]['stats']->item_id;
                        $results[$ad_ap]['boots']['name'] = $boots[$ad_ap][0]['item']->name;
                        $results[$ad_ap]['boots']['image'] = $boots[$ad_ap][0]['item']->image->full;
                        $results[$ad_ap]['boots']['description'] = $boots[$ad_ap][0]['item']->description;
                        if (count($boots[$ad_ap]) > 1) {
                            $results[$ad_ap]['boots']['recommended'] = array();
                            $results[$ad_ap]['boots']['recommended']['id'] = $boots[$ad_ap][1]['stats']->item_id;
                            $results[$ad_ap]['boots']['recommended']['name'] = $boots[$ad_ap][1]['item']->name;
                            $results[$ad_ap]['boots']['recommended']['image'] = $boots[$ad_ap][1]['item']->image->full;
                            $results[$ad_ap]['boots']['recommended']['description'] = $boots[$ad_ap][1]['item']->description;
                        }
                    }
                    if (count($trinkets)) {
                        $results[$ad_ap]['trinket']['id'] = $trinkets[0]['stats']->item_id;
                        $results[$ad_ap]['trinket']['name'] = $trinkets[0]['item']->name;
                        $results[$ad_ap]['trinket']['image'] = $trinkets[0]['item']->image->full;
                        $results[$ad_ap]['trinket']['description'] = $trinkets[0]['item']->description;
                        if (count($trinkets) > 1) {
                            $results[$ad_ap]['trinket']['recommended'] = array();
                            $results[$ad_ap]['trinket']['recommended']['id'] = $trinkets[1]['stats']->item_id;
                            $results[$ad_ap]['trinket']['recommended']['name'] = $trinkets[1]['item']->name;
                            $results[$ad_ap]['trinket']['recommended']['image'] = $trinkets[1]['item']->image->full;
                            $results[$ad_ap]['trinket']['recommended']['description'] = $trinkets[1]['item']->description;
                        }
                    }
                    if ($starter_item) {
                        $results[$ad_ap]['starter_item']['id'] = $starter_item['stats']->item_id;
                        $results[$ad_ap]['starter_item']['name'] = $starter_item['item']->name;
                        $results[$ad_ap]['starter_item']['image'] = $starter_item['item']->image->full;
                        $results[$ad_ap]['starter_item']['description'] = $starter_item['item']->description;
                        if (isset($starter_item['item']->gold) && (500 - $starter_item['item']->gold->base) >= 150) {
                            $pot_item = $items_lol->{2031};
                            $results[$ad_ap]['starter_pot']['id'] = 2031;
                            $results[$ad_ap]['starter_pot']['name'] = $pot_item->name;
                            $results[$ad_ap]['starter_pot']['image'] = $pot_item->image->full;
                            $results[$ad_ap]['starter_pot']['description'] = $pot_item->description;
                            $results[$ad_ap]['starter_pot']['count'] = 1;
                        } else if (isset($starter_item['item']->gold) && (500 - $starter_item['item']->gold->base) >= 100) {
                            $pot_item = $items_lol->{2003};
                            $results[$ad_ap]['starter_pot']['id'] = 2003;
                            $results[$ad_ap]['starter_pot']['name'] = $pot_item->name;
                            $results[$ad_ap]['starter_pot']['image'] = $pot_item->image->full;
                            $results[$ad_ap]['starter_pot']['description'] = $pot_item->description;
                            $results[$ad_ap]['starter_pot']['count'] = 2;
                        } else if (isset($starter_item['item']->gold) && (500 - $starter_item['item']->gold->base) >= 50) {
                            $pot_item = $items_lol->{2003};
                            $results[$ad_ap]['starter_pot']['id'] = 2003;
                            $results[$ad_ap]['starter_pot']['name'] = $pot_item->name;
                            $results[$ad_ap]['starter_pot']['image'] = $pot_item->image->full;
                            $results[$ad_ap]['starter_pot']['description'] = $pot_item->description;
                            $results[$ad_ap]['starter_pot']['count'] = 1;
                        }
                    }
                }
            }
        }
        $this->cache->redis->save($cache_key, $results, 6000000);
        return $results;
    }

    public function runes($build_version, $champion_id, $champion_lane, $debug = false)
    {
        $cache_key = 'build_runes_v4_' . $build_version . '_TR_' . $this->lol->getLang() . '_' . $champion_id . '_' . $champion_lane;
        if (!$debug && $cache_results = $this->cache->redis->get($cache_key)) {
            return $cache_results;
        }
        $results = array();
        $this->db->select('*, (rune_wins+rune_losses) AS rune_total');
        $this->db->where('build_version', $build_version);
        $this->db->where('champion_id', $champion_id);
        $this->db->where('champion_lane', $champion_lane);
        $this->db->where('type', 0);
        $this->db->order_by('rune_total', 'DESC');
        $query = $this->db->get('build_runes');
        if ($query->num_rows()) {
            $runes = $query->result();
            $this->load->model('Runes');
            $runes_lol = $this->lol->getRunes();

            $this->db->where('build_version', $build_version);
            $this->db->where('champion_id', $champion_id);
            $this->db->where('champion_lane', $champion_lane);
            $this->db->where('type', 1);
            $this->db->limit(1);
            $this->db->order_by('rune_wins', 'DESC');
            $primary_query = $this->db->get('build_runes');

            if ($primary_query->num_rows()) {
                $primary_rune = $primary_query->row();

                $this->db->where('build_version', $build_version);
                $this->db->where('champion_id', $champion_id);
                $this->db->where('champion_lane', $champion_lane);
                $this->db->where('type', 2);
                $this->db->where('rune_id !=', $primary_rune->rune_id);
                $this->db->limit(1);
                $this->db->order_by('rune_wins', 'DESC');
                $sub_query = $this->db->get('build_runes');
                if ($sub_query->num_rows()) {

                    $sub_rune = $sub_query->row();

                    $lines = array(
                        array('rune_wins' => 0),
                        array('rune_wins' => 0),
                        array('rune_wins' => 0),
                        array('rune_wins' => 0)
                    );

                    if ($debug) {
                        $champion = $this->lol->getChampionById2($champion_id);
                        echo '<div style="text-align: center;"><img src="https://cdn.lols.gg/img/champion/' . $champion->key . '.png" style="width:50px;">';
                        echo '<h3>' . $champion->key . ' ' . $champion_lane . '</h3></div>';

                        echo '<div style="width: 300px; text-align: center;">';
                        echo '<img style="padding:10px;" src="https://cdn.lols.gg/img/perkstyle/' . $primary_rune->rune_id . '.png" />';
                        echo '<img style="padding:10px;" src="https://cdn.lols.gg/img/perkstyle/' . $sub_rune->rune_id . '.png" />';
                        echo '</div>';

                        $secondary_styles = array();
                        foreach ($runes as $rune_stats) {
                            if(isset($runes_lol[$rune_stats->rune_id])) {
                                $rune_details = $runes_lol[$rune_stats->rune_id];
                                if ($rune_details['style_id'] == $primary_rune->rune_id) {
                                    if ($rune_stats->rune_wins > $lines[$rune_details['line']]['rune_wins']) {
                                        $lines[$rune_details['line']]['rune'] = $rune_stats;
                                        $lines[$rune_details['line']]['rune_wins'] = $rune_stats->rune_wins;
                                    }
                                } elseif ($rune_details['style_id'] == $sub_rune->rune_id && count($secondary_styles) < 2) {
                                    $secondary_styles[$rune_stats->rune_id] = array('rune_wins' => $rune_stats->rune_wins, 'rune' => $rune_stats);
                                }
                            }
                        }

                        echo '<div style="width:150px; float:left;">';
                        foreach ($lines as $line => $rune_stats) {
                            if(isset($runes_lol[$rune_stats['rune']->rune_id])) {
                                $rune_details = $runes_lol[$rune_stats['rune']->rune_id];
                                echo '<div style="height: 170px;">';
                                echo '<img style="padding:10px;" src="https://cdn.lols.gg/img/perk/' . $rune_stats['rune']->rune_id . '.png" /><br>';
                                echo '<div style="text-align:center;">' . $rune_details['name'] . '</div>';
                                echo '</div>';
                            }

                        }
                        echo '</div>';
                        echo '<div style="width:150px; float:left;">';
                        foreach ($secondary_styles as $rune_id => $rune_stats) {
                            if(isset($runes_lol[$rune_id])) {
                                $rune_details = $runes_lol[$rune_id];
                                echo '<div style="height: 170px;">';
                                echo '<img style="padding:10px;" src="https://cdn.lols.gg/img/perk/' . $rune_id . '.png" /><br>';
                                echo '<div style="text-align:center;">' . $rune_details['name'] . '</div>';
                                echo '</div>';
                            }

                        }
                        echo '</div>';
                        echo '<div style="clear: left;"></div>';
                    } else {

                        $results['primaryStyle'] = array();
                        $results['primaryStyle']['style_id'] = $primary_rune->rune_id;
                        $results['primaryStyle']['runes'] = array();
                        $results['subStyle'] = array();
                        $results['subStyle']['style_id'] = $sub_rune->rune_id;
                        $results['subStyle']['runes'] = array();
                        if ($this->lol->getLang() == 'tr_TR') {
                            $results['primaryStyle']['name'] = $this->Runes->perk_styles_tr[$primary_rune->rune_id]['name'];
                            $results['primaryStyle']['description'] = $this->Runes->perk_styles_tr[$primary_rune->rune_id]['description'];
                            $results['subStyle']['name'] = $this->Runes->perk_styles_tr[$sub_rune->rune_id]['name'];
                            $results['subStyle']['description'] = $this->Runes->perk_styles_tr[$sub_rune->rune_id]['description'];
                        } else {
                            $results['primaryStyle']['name'] = $this->Runes->perk_styles_en[$primary_rune->rune_id]['name'];
                            $results['primaryStyle']['description'] = $this->Runes->perk_styles_en[$primary_rune->rune_id]['description'];
                            $results['subStyle']['name'] = $this->Runes->perk_styles_en[$sub_rune->rune_id]['name'];
                            $results['subStyle']['description'] = $this->Runes->perk_styles_en[$sub_rune->rune_id]['description'];
                        }


                        $secondary_styles = array();
                        foreach ($runes as $rune_stats) {
                            if(isset($runes_lol[$rune_stats->rune_id])) {
                                $rune_details = $runes_lol[$rune_stats->rune_id];
                                if ($rune_details['style_id'] == $primary_rune->rune_id) {
                                    if ($rune_stats->rune_wins > $lines[$rune_details['line']]['rune_wins']) {
                                        $lines[$rune_details['line']]['rune'] = $rune_stats;
                                        $lines[$rune_details['line']]['rune_wins'] = $rune_stats->rune_wins;
                                    }
                                } elseif ($rune_details['style_id'] == $sub_rune->rune_id && count($secondary_styles) < 2) {
                                    $secondary_styles[$rune_stats->rune_id] = array('rune_wins' => $rune_stats->rune_wins, 'rune' => $rune_stats);
                                }
                            }


                        }

                        foreach ($lines as $line => $rune_stats) {
                            if(isset($runes_lol[$rune_stats['rune']->rune_id])) {
                                $rune_details = $runes_lol[$rune_stats['rune']->rune_id];
                                $rune_details['rune_id'] = (int)$rune_stats['rune']->rune_id;
                                $results['primaryStyle']['runes'][$line] = $rune_details;
                            }

                        }
                        foreach ($secondary_styles as $rune_id => $rune_stats) {
                            if(isset($runes_lol[$rune_id])) {
                                $rune_details = $runes_lol[$rune_id];
                                $rune_details['rune_id'] = (int)$rune_id;
                                $results['subStyle']['runes'][] = $rune_details;
                            }
                        }
                    }
                }
            }
        }
        $this->cache->redis->save($cache_key, $results, 6000000);
        return $results;
    }

    public function spells($build_version, $champion_id, $champion_lane, $debug = false)
    {
        $cache_key = 'build_spells_v4_' . $build_version . '_TR_' . $this->lol->getLang() . '_' . $champion_id . '_' . $champion_lane;
        if (!$debug && $cache_results = $this->cache->redis->get($cache_key)) {
            return $cache_results;
        }
        $results = array();
        $this->db->select('*, (spell_wins+spell_losses) AS spell_total');
        $this->db->where('build_version', $build_version);
        $this->db->where('champion_id', $champion_id);
        $this->db->where('champion_lane', $champion_lane);
        $this->db->order_by('spell_total', 'DESC');
        $query = $this->db->get('build_spells');
        if ($query->num_rows()) {
            $spells = $query->result();

            $total_spell_count = 0;


            foreach ($spells as $spell) {
                $total_spell_count += $spell->spell_wins + $spell->spell_losses;
            }

            if ($debug) {
                $champion = $this->lol->getChampionById2($champion_id);
                echo '<div style="text-align: center;"><img src="https://cdn.lols.gg/img/champion/' . $champion->key . '.png" style="width:50px;">';
                echo '<h3>' . $champion->key . ' ' . $champion_lane . '</h3></div>';

                foreach ($spells as $spell) {
                    $spell_details = $this->lol->getSpellById($spell->spell_id);
                    echo '<div style="padding: 5px;">';
                    echo '<img style="vertical-align: middle;" src="http://api.lols.gg/assets/api/images/spells/20/' . $spell_details->key . '.png">';
                    echo '<span style="margin-left:5px;">' . $spell_details->name . '<span style="font-size: 0.9em; margin-left: 10px; color: #000000; font-weight: bold;">' . ($spell->spell_wins + $spell->spell_losses) . '</span>' . '</span>';
                    $use_rate = 100 / $total_spell_count * ($spell->spell_wins + $spell->spell_losses);
                    echo ' | %' . number_format($use_rate, 2);
                    echo '<br>';
                    echo '</div>';
                }
            } else {
                $s = 0;
                foreach ($spells as $spell_key => $spell) {
                    $s++;
                    $spell_details = $this->lol->getSpellById($spell->spell_id);
                    $results[$spell_key] = array();
                    $results[$spell_key]['id'] = $spell_details->id;
                    $results[$spell_key]['name'] = $spell_details->name;
                    $results[$spell_key]['description'] = $spell_details->description;
                    $results[$spell_key]['key'] = $this->lol->cleanText($spell_details->key);
                    $results[$spell_key]['keyFull'] = $spell_details->key;
                    if ($s >= 2) {
                        break;
                    }
                }
            }
        }
        $this->cache->redis->save($cache_key, $results, 6000000);
        return $results;
    }

    /**
     * Gets a champion's lanes
     * @param int $build_version
     * @param int $champion_id
     * @return array
     */
    public function getChampionLanes(int $build_version, int $champion_id): array
    {
        $cache_key = 'build_champ_lanes_v5_' . $build_version . '_TR_' . '_' . $champion_id;
        if ($cache_results = $this->cache->redis->get($cache_key)) {
            return $cache_results;
        }

        $results = array();
        $this->db->select("champion_lane, SUM(item_wins+item_losses) as total");
        $this->db->where("build_version", $build_version);
        $this->db->where("champion_id", $champion_id);
        $this->db->group_by("champion_lane");
        $query = $this->db->get("build_items");
        if ($query->num_rows()) {
            $total = 0;
            $lanes = $query->result();
            foreach ($lanes as $lane) {
                $total += $lane->total;
                $results[$lane->champion_lane] = $lane->total;
            }
            foreach ($results as &$result) {
                $result = 100 / $total * $result;
            }
        }
        $this->cache->redis->save($cache_key, $results, 6000000);
        return $results;
    }

    public function getLaneMatches($account_id)
    {
        $this->load->model('matchhistory');
        $matches = $this->lol->getSummonerMatches($account_id, 50, 0, '4,420,40,410');
        return $matches;
    }

    public function make(string $region = 'tr', int $limit = 10): string
    {
        ini_set('memory_limit', '700M');
        set_time_limit(0);
        $time_start = microtime(true);
        $build_version = time();
        $log = "";
        $log .= "Build Version: " . $build_version;
        $log .= "<br>";

        if ($limit > 20000000) {
            $limit = 20000000;
        }
        if ($limit < 1) {
            $limit = 1;
        }


        $tiers = array('SILVER', 'GOLD', 'DIAMOND', 'CHALLENGER', 'PLATINUM', 'MASTER');
        $queue_ids = array(4, 420, 42, 410, 440, 400, 41, 9, 430);
        $game_start_time = 1519344000;
        $game_duration_sec = 1200;
        $game_per_slice = 5000;
        $query_limit = 100000;


        $this->load->model('Runes');

        $this->lol->setRegion($region);
        $this->lol->setLang('tr');

        $total_match_counter = 0;
        $calculated_match_counter = 0;
        $total_champion_counter = 0;
        $calculated_champion_counter = 0;

        $log .= 'Phase 1.1 Time:' . number_format(microtime(true) - $time_start, 4);
        $log .= '<br>';
        $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
        $log .= '<br>';
        $log .= '<br>';

        $game_ids = array();
        for ($i = 0; $i < ceil($limit / $query_limit); $i++) {
            $this->db->select("game_id");
            $this->db->where_in('queue_type', $queue_ids);
            $this->db->where('start_time > ', $game_start_time);
            $this->db->where('game_length > ', $game_duration_sec);
            $this->db->where('status', 1);
            $this->db->limit($query_limit, $i * $query_limit);
            $matches_q = $this->db->get('match_history_tr');
            $matches = $matches_q->result();
            unset($matches_q);
            foreach ($matches as $match) {
                $game_ids[] = (int)$match->game_id;

            }
            unset($match);
            unset($matches);
        }

        $match_count = count($game_ids);

        $log .= 'Phase 1.2 Time:' . number_format(microtime(true) - $time_start, 4);
        $log .= '<br>';
        $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
        $log .= '<br>';
        $log .= '<br>';

        if ($match_count) {

            $game_id_slices = array();
            for ($i = 0; $i < ceil($match_count / $game_per_slice); $i++) {
                $game_id_slices[] = array_slice($game_ids, $i * $game_per_slice, $game_per_slice);
            }


            $log .= 'Match count:' . $match_count;
            $log .= '<br>';

            unset($game_ids);

            $log .= 'Phase 2 Time:' . number_format(microtime(true) - $time_start, 4);
            $log .= '<br>';
            $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
            $log .= '<br>';
            $log .= '<br>';


            if (!$this->client) {
                try {
                    $this->client = new MongoDB\Client($this->mongodb_dns);
                } catch (Exception $e) {
                    echo $e;
                    die();
                }
            }


            $collection = $this->client->match_history->{$this->lol->getRegion()};
            $params = array('projection' => array('timeline' => false));

            $log .= 'Phase 3 Time:' . number_format(microtime(true) - $time_start, 4);
            $log .= '<br>';
            $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
            $log .= '<br>';
            $log .= '<br>';


            $champions = array();
            foreach ($game_id_slices as $game_id_slice) {
                $cursor = $collection->find(
                    array(
                        '_id' => array('$in' => $game_id_slice)
                    ), $params);
                foreach ($cursor as $document) {
                    $total_match_counter++;
                    $match_counter_tmp = 0;

                    foreach ($document->participants as $participant) {
                        $total_champion_counter++;
                        if (isset($participant->highestAchievedSeasonTier) && in_array($participant->highestAchievedSeasonTier, $tiers)) {
                            $calculated_champion_counter++;
                            $match_counter_tmp = 1;

                            $lane = $participant->timeline->lane;
                            $champion_id = $participant->championId;
                            $win = $participant->stats->win;

                            if (!isset($champions[$champion_id])) {
                                $champions[$champion_id] = array();
                            }
                            if (!isset($champions[$champion_id][$lane])) {
                                $champions[$champion_id][$lane] = array('items' => array(), 'spells' => array(), 'runes' => array(), 'runesPrimary' => array(), 'runesSub' => array(), 'total_match' => 0);
                            }

                            $champions[$champion_id][$lane]['total_match']++;

                            for ($i = 0; $i <= 6; $i++) {
                                $item = $participant->stats->{'item' . $i};
                                if (!isset($champions[$champion_id][$lane]['items'][$item])) {
                                    $champions[$champion_id][$lane]['items'][$item] = array('win' => 0, 'lose' => 0);
                                }
                                if ($item != '0') {
                                    $champions[$champion_id][$lane]['items'][$item][$win == 1 ? 'win' : 'lose']++;
                                }
                            }


                            $spell1Id = $participant->spell1Id;
                            $spell2Id = $participant->spell2Id;
                            if (!isset($champions[$champion_id][$lane]['spells'][$spell1Id])) {
                                $champions[$champion_id][$lane]['spells'][$spell1Id] = array('win' => 0, 'lose' => 0);
                            }
                            if (!isset($champions[$champion_id][$lane]['spells'][$spell2Id])) {
                                $champions[$champion_id][$lane]['spells'][$spell2Id] = array('win' => 0, 'lose' => 0);
                            }
                            $champions[$champion_id][$lane]['spells'][$spell1Id][$win == 1 ? 'win' : 'lose']++;
                            $champions[$champion_id][$lane]['spells'][$spell2Id][$win == 1 ? 'win' : 'lose']++;


                            if (isset($participant->stats->perk0)) {
                                for ($perk_i = 0; $perk_i <= 5; $perk_i++) {
                                    if (isset($participant->stats->{'perk' . $perk_i})) {
                                        $perk_id = $participant->stats->{'perk' . $perk_i};
                                        if (!isset($champions[$champion_id][$lane]['runes'][$perk_id])) {
                                            $champions[$champion_id][$lane]['runes'][$perk_id] = array('win' => 0, 'lose' => 0);
                                        }
                                        $champions[$champion_id][$lane]['runes'][$perk_id][$win == 1 ? 'win' : 'lose']++;
                                    }
                                }
                            }

                            if (isset($participant->stats->perkPrimaryStyle)) {
                                $perk_primary_id = $participant->stats->perkPrimaryStyle;
                                if (!isset($champions[$champion_id][$lane]['runesPrimary'][$perk_primary_id])) {
                                    $champions[$champion_id][$lane]['runesPrimary'][$perk_primary_id] = array('win' => 0, 'lose' => 0);
                                }
                                $champions[$champion_id][$lane]['runesPrimary'][$perk_primary_id][$win == 1 ? 'win' : 'lose']++;
                            }

                            if (isset($participant->stats->perkSubStyle)) {
                                $perk_sub_id = $participant->stats->perkSubStyle;
                                if (!isset($champions[$champion_id][$lane]['runesSub'][$perk_sub_id])) {
                                    $champions[$champion_id][$lane]['runesSub'][$perk_sub_id] = array('win' => 0, 'lose' => 0);
                                }
                                $champions[$champion_id][$lane]['runesSub'][$perk_sub_id][$win == 1 ? 'win' : 'lose']++;
                            }
                        }
                    }


                    $calculated_match_counter += $match_counter_tmp;
                    //echo $document->_id;
                    //echo "<br>";
                }
            }


            $log .= 'Phase 4 Time:' . number_format(microtime(true) - $time_start, 4);
            $log .= '<br>';
            $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
            $log .= '<br>';
            $log .= '<br>';
            $add_item_data = array();


            foreach ($champions as $champion_id => $champion_lanes) {
                foreach ($champion_lanes as $champion_lane => $champion) {
                    if (isset($champion['items'])) {
                        foreach ($champion['items'] as $item_id => $item) {
                            $add_item_data[] = array(
                                'build_version' => $build_version,
                                'champion_id' => $champion_id,
                                'champion_lane' => $champion_lane,
                                'item_id' => $item_id,
                                'item_wins' => $item['win'],
                                'item_losses' => $item['lose']
                            );
                        }
                        unset($champions[$champion_id][$champion_lane]['items']);
                    }
                }
            }

            $log .= 'Phase 5 Time:' . number_format(microtime(true) - $time_start, 4);
            $log .= '<br>';
            $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
            $log .= '<br>';
            $log .= '<br>';

            $this->db->insert_batch('build_items', $add_item_data);
            unset($add_item_data);

            $log .= 'Phase 6 Time:' . number_format(microtime(true) - $time_start, 4);
            $log .= '<br>';
            $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
            $log .= '<br>';
            $log .= '<br>';

            $add_rune_data = array();

            foreach ($champions as $champion_id => $champion_lanes) {
                foreach ($champion_lanes as $champion_lane => $champion) {
                    if (isset($champion['runes'])) {
                        foreach ($champion['runes'] as $rune_id => $rune) {
                            $add_rune_data[] = array(
                                'build_version' => $build_version,
                                'champion_id' => $champion_id,
                                'champion_lane' => $champion_lane,
                                'rune_id' => $rune_id,
                                'rune_wins' => $rune['win'],
                                'rune_losses' => $rune['lose'],
                                'type' => 0
                            );
                        }
                        unset($champions[$champion_id][$champion_lane]['runes']);
                    }
                    if (isset($champion['runesPrimary'])) {
                        foreach ($champion['runesPrimary'] as $rune_id => $rune) {
                            $add_rune_data[] = array(
                                'build_version' => $build_version,
                                'champion_id' => $champion_id,
                                'champion_lane' => $champion_lane,
                                'rune_id' => $rune_id,
                                'rune_wins' => $rune['win'],
                                'rune_losses' => $rune['lose'],
                                'type' => 1
                            );
                        }
                        unset($champions[$champion_id][$champion_lane]['runesPrimary']);
                    }
                    if (isset($champion['runesSub'])) {
                        foreach ($champion['runesSub'] as $rune_id => $rune) {
                            $add_rune_data[] = array(
                                'build_version' => $build_version,
                                'champion_id' => $champion_id,
                                'champion_lane' => $champion_lane,
                                'rune_id' => $rune_id,
                                'rune_wins' => $rune['win'],
                                'rune_losses' => $rune['lose'],
                                'type' => 2
                            );
                        }
                        unset($champions[$champion_id][$champion_lane]['runesSub']);
                    }
                }
            }

            $log .= 'Phase 7 Time:' . number_format(microtime(true) - $time_start, 4);
            $log .= '<br>';
            $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
            $log .= '<br>';
            $log .= '<br>';

            $this->db->insert_batch('build_runes', $add_rune_data);
            unset($add_rune_data);

            $log .= 'Phase 8 Time:' . number_format(microtime(true) - $time_start, 4);
            $log .= '<br>';
            $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
            $log .= '<br>';
            $log .= '<br>';

            $add_spell_data = array();

            foreach ($champions as $champion_id => $champion_lanes) {
                foreach ($champion_lanes as $champion_lane => $champion) {
                    if (isset($champion['spells'])) {
                        foreach ($champion['spells'] as $rune_id => $rune) {
                            $add_spell_data[] = array(
                                'build_version' => $build_version,
                                'champion_id' => $champion_id,
                                'champion_lane' => $champion_lane,
                                'spell_id' => $rune_id,
                                'spell_wins' => $rune['win'],
                                'spell_losses' => $rune['lose']
                            );
                        }
                        unset($champions[$champion_id][$champion_lane]['spells']);
                    }
                }
            }

            $log .= 'Phase 9 Time:' . number_format(microtime(true) - $time_start, 4);
            $log .= '<br>';
            $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
            $log .= '<br>';
            $log .= '<br>';

            $this->db->insert_batch('build_spells', $add_spell_data);
            unset($add_spell_data);

            $log .= 'Phase 10 Time:' . number_format(microtime(true) - $time_start, 4);
            $log .= '<br>';
            $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
            $log .= '<br>';
            $log .= '<br>';


        } else {
            $log .= 'No matches';
            $log .= '<br>';
        }
        $log .= '<br>';
        $log .= 'Ram Usage: ' . (memory_get_peak_usage(1) / 1024 / 1024) . " MB";
        $log .= '<br>';
        $log .= 'Total Time:' . number_format(microtime(true) - $time_start, 4);
        $log .= '<br>';
        $log .= '<br>';
        $log .= 'Counters';
        $log .= '<br>';
        $log .= 'Total_match_counter: ' . $total_match_counter;
        $log .= '<br>';
        $log .= 'Calculated_match_counter: ' . $calculated_match_counter;
        $log .= '<br>';
        $log .= 'Total_champion_counter: ' . $total_champion_counter;
        $log .= '<br>';
        $log .= 'Calculated_champion_counter: ' . $calculated_champion_counter;
        $log .= '<br>';


        $this->buildsUpload($build_version . '-log.html', $log);
        //memprof_dump_callgrind(fopen("/Users/eyupakman/Sites/tmp/" . $build_version . "-callgrind.out", "w"));
        //memprof_dump_pprof(fopen("/Users/eyupakman/Sites/tmp/" . $build_version . "-profile.heap", "w"));

        return $log;
    }

    /**
     * Gets profilers from assets/profiler folder and uploads to the S3 Bucket
     * @param string $filename
     * @param string $data
     * @return bool
     */
    private function buildsUpload(string $filename, string $data)
    {
        $status = false;
        try {
            $sharedConfig = [
                'region' => 'us-east-2',
                'version' => 'latest',
                'credentials' => [
                    'key' => 'AKIAJQJBVX232WFPD77A',
                    'secret' => '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',
                ]
            ];
            $sdk = new Aws\Sdk($sharedConfig);
            $s3Client = $sdk->createS3();

            $target_file = 'builds/' . date('Y-m-d') . '/' . $filename;
            $bucket = 'lolsgg-logs';
            try {
                $s3Client->putObject(array(
                    'Bucket' => $bucket,
                    'Key' => $target_file,
                    'Body' => $data,
                    'ContentType' => 'text/html'
                ));
                $status = true;
            } catch (Aws\S3\Exception\S3Exception $e) {

            } catch (Aws\Exception\AwsException $e) {

            }
        } catch (Aws\Exception\AwsException $e) {

        }
        return $status;
    }
}