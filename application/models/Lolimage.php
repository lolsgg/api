<?php

class Lolimage extends CI_Model
{
    /**
     * Lolimage constructor.
     */
    function __construct()
    {
        $this->load->library('image_lib');
    }

    public function champion($key, $size = 'source')
    {
        $filename = FCPATH . 'assets/api/images/champions/' . $size . '/' . $key . '.png';
        if (file_exists($filename)) {
            return $filename;
        }

        if (!is_dir(FCPATH . 'assets/api/images/champions/' . $size)) {
            mkdir(FCPATH . 'assets/api/images/champions/' . $size);
        }

        $url = $this->lol->getDragonImgUrl() . 'champion/' . $key . '.png';
        file_put_contents($filename, file_get_contents($url));
        if ((int)$size > 0) {
            $this->resize($filename, $size);
        }
        return $filename;
    }

    public function summonerSpell($key, $size = 'source')
    {
        $filename = FCPATH . 'assets/api/images/spells/' . $size . '/' . $key . '.png';
        if (file_exists($filename)) {
            return $filename;
        }

        if (!is_dir(FCPATH . 'assets/api/images/spells/' . $size)) {
            mkdir(FCPATH . 'assets/api/images/spells/' . $size);
        }

        $url = $this->lol->getDragonImgUrl() . 'spell/' . $key . '.png';
        file_put_contents($filename, file_get_contents($url));
        if ((int)$size > 0) {
            $this->resize($filename, $size);
        }
        return $filename;
    }

    public function profileIcon($key, $size = 'source')
    {
        $filename = FCPATH . 'assets/api/images/profileicons/' . $size . '/' . $key . '.png';
        if (file_exists($filename)) {
            return $filename;
        }

        if (!is_dir(FCPATH . 'assets/api/images/profileicons/' . $size)) {
            mkdir(FCPATH . 'assets/api/images/profileicons/' . $size);
        }

        $url = $this->lol->getDragonImgUrl() . 'profileicon/' . $key . '.png';
        $headers = get_headers($url);
        $status = substr($headers[0], 9, 3);
        if ($status == '200') {
            file_put_contents($filename, file_get_contents($url));
            if ((int)$size > 0) {
                $this->resize($filename, $size);
            }
            return $filename;
        }
        return false;
    }

    public function mastery($key, $size = 'source')
    {
        $filename = FCPATH . 'assets/api/images/masteries/' . $size . '/' . $key . '.png';
        if (file_exists($filename)) {
            return $filename;
        }

        if (!is_dir(FCPATH . 'assets/api/images/masteries/' . $size)) {
            mkdir(FCPATH . 'assets/api/images/masteries/' . $size);
        }

        $url = $this->lol->getDragonImgUrl() . 'mastery/' . $key . '.png';
        file_put_contents($filename, file_get_contents($url));
        if ((int)$size > 0) {
            $this->resize($filename, $size);
        }
        return $filename;
    }

    private function resize($filename, $size)
    {
        $config = array();
        $config['image_library'] = 'gd2';
        $config['source_image'] = $filename;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $size;
        $config['height'] = $size;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

    public function liveSignature($summoner, $filename)
    {
        $color = new GDText\Color;

        $summoner_name = $summoner['summoner_name'];
        $league = mb_strtoupper($summoner['league']);
        $lp = $summoner['lp'];
        $series = $summoner['series'];
        $champion_key = $summoner['champion_key'];
        $champion_name = $summoner['champion_name'];
        $champion_points = number_format($summoner['champion_points'], 0, '', ',');
        $champion_mastery_level = $summoner['champion_mastery_level'];
        $map_name = $summoner['map_name'];
        $game_type = $summoner['game_type'];
        $ferocity = $summoner['ferocity'];
        $cunning = $summoner['cunning'];
        $resolve = $summoner['resolve'];
        $win = $summoner['win'];
        $lose = $summoner['lose'];
        $win_lose_ratio = $summoner['win_lose_ratio'];
        $spell1_key = $summoner['spell1_key'];
        $spell2_key = $summoner['spell2_key'];
        $profile_icon_id = $summoner['profile_icon_id'];
        $mastery_id = $summoner['mastery_id'];
        $region = strtoupper($summoner['region']);

        if (file_exists('assets/api/images/livecards/' . $champion_key . '.jpg')) {
            $im = imagecreatefromjpeg('assets/api/images/livecards/' . $champion_key . '.jpg');
        } else {
            $im = imagecreatefromjpeg('assets/api/images/livecards/Aatrox.jpg');
        }


        $champion_image = imagecreatefrompng($this->champion($champion_key, 42));
        $spell1_image = imagecreatefrompng($this->summonerSpell($spell1_key, 20));
        $spell2_image = imagecreatefrompng($this->summonerSpell($spell2_key, 20));
        $mastery_image = imagecreatefrompng($this->mastery($mastery_id, 20));
        $profile_image = imagecreatefrompng($this->profileIcon($profile_icon_id, 78));

        $series_length = strlen($series);

        if ($series_length > 0) {
            $series_circle = imagecreatefrompng(FCPATH . 'assets/api/images/series/circle.png');
            $series_check = imagecreatefrompng(FCPATH . 'assets/api/images/series/check.png');
            $series_times = imagecreatefrompng(FCPATH . 'assets/api/images/series/times.png');
            if ($series_length == 3) {
                $series = '  ' . $series;
                $series_length = 5;
            }
            if ($series_length == 4) {
                $series = ' ' . $series;
                $series_length = 5;
            }
            for ($i = 0; $i < $series_length; $i++) {
                if ($series[$i] == 'N') {
                    imagecopy($im, $series_circle, 265 + (20 * $i), 73, 0, 0, imagesx($series_circle), imagesy($series_circle));
                } else if ($series[$i] == 'W') {
                    imagecopy($im, $series_check, 265 + (20 * $i), 73, 0, 0, imagesx($series_check), imagesy($series_check));
                } else if ($series[$i] == 'L') {
                    imagecopy($im, $series_times, 265 + (20 * $i), 75, 0, 0, imagesx($series_times), imagesy($series_times));
                }

            }
        }


        // Champion Image
        imagecopy($im, $champion_image, 15, 125, 0, 0, imagesx($champion_image), imagesy($champion_image));

        // Profile Image
        imagecopy($im, $profile_image, 19, 23, 0, 0, imagesx($profile_image), imagesy($profile_image));

        // Spell 1 Image
        imagecopy($im, $spell1_image, 60, 125, 0, 0, imagesx($spell1_image), imagesy($spell1_image));

        // Spell 2 Image
        imagecopy($im, $spell2_image, 60, 147, 0, 0, imagesx($spell2_image), imagesy($spell2_image));

        // Mastery Image
        imagecopy($im, $mastery_image, 84, 135, 0, 0, imagesx($mastery_image), imagesy($mastery_image));

        // Summoner Name
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('f9f9f9'));
        if (mb_strlen($summoner_name) > 24) {
            $box->setFontSize(18);
        } else if (mb_strlen($summoner_name) > 19) {
            $box->setFontSize(20);
        } else if (mb_strlen($summoner_name) > 15) {
            $box->setFontSize(22);
        } else {
            $box->setFontSize(27);
        }
        $box->setBox(116, 30, 260, 33);
        // $box->enableDebug();
        $box->setTextAlign('left', 'center');
        $box->draw($summoner_name);

        // League
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('53fee1'));
        $box->setFontSize(24);
        $box->setBox(116, 61, 200, 30);
        // $box->enableDebug();
        $box->setTextAlign('left', 'top');
        $box->draw($league);

        // LP
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('f9f9f9'));
        $box->setFontSize(24);
        $box->setBox(381, 67, 67, 30);
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw($lp);

        // Champion Name
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('53fee1'));
        $box->setFontSize(16);
        $box->setBox(113, 126, 120, 20);
        // $box->enableDebug();
        $box->setTextAlign('left', 'center');
        $box->draw($champion_name);

        // Champion Mastery Points
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('f9f9f9'));
        $box->setFontSize(16);
        $box->setBox(113, 146, 120, 20);
        // $box->enableDebug();
        $box->setTextAlign('left', 'center');
        $box->draw($champion_points);

        // Champion Mastery Level
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('f9f9f9'));
        $box->setFontSize(13);
        $box->setBox(214, 148, 20, 20);
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw($champion_mastery_level);


        $win_lose_x_add = 0;
        if ($win > 99) {
            $win_lose_x_add += 15;
        }

        // Win
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('5effc4'));
        $box->setFontSize(26);
        $box->setBox(245 + $win_lose_x_add, 121, 46, 49);
        // $box->enableDebug();
        $box->setTextAlign('right', 'center');
        $box->draw($win);

        // Slash
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('0e8470'));
        $box->setFontSize(26);
        $box->setBox(300 + $win_lose_x_add, 121, 10, 49);
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw("/");

        // Lose
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('ff5f5f'));
        $box->setFontSize(26);
        $box->setBox(318 + $win_lose_x_add, 121, 46, 49);
        // $box->enableDebug();
        $box->setTextAlign('left', 'center');
        $box->draw($lose);

        // Win Lose Ratio
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('f5f5f5'));
        $box->setFontSize(22);
        $box->setBox(363, 121, 100, 49);
        // $box->enableDebug();
        $box->setTextAlign('right', 'center');
        $box->draw($win_lose_ratio);

        // Ferocity
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/arial-bold.ttf');
        $box->setFontColor($color->parseString('f5f5f5'));
        $box->setFontSize(18);
        $box->setBox(219, 216, 65, 25);
        $box->setStrokeSize(2);
        $box->setStrokeColor($color->parseString('000000'));
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw($ferocity);

        // Cunning
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/arial-bold.ttf');
        $box->setFontColor($color->parseString('FFFFFF'));
        $box->setFontSize(18);
        $box->setBox(307, 216, 65, 25);
        $box->setStrokeSize(2);
        $box->setStrokeColor($color->parseString('000000'));
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw($cunning);

        // Resolve
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/arial-bold.ttf');
        $box->setFontColor($color->parseString('FFFFFF'));
        $box->setFontSize(18);
        $box->setBox(392, 216, 65, 25);
        $box->setStrokeSize(2);
        $box->setStrokeColor($color->parseString('000000'));
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw($resolve);

        // Map Name
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('53fee1'));
        $box->setFontSize(18);
        $box->setBox(15, 192, 165, 25);
        $box->setStrokeSize(2);
        $box->setStrokeColor($color->parseString('000000'));
        // $box->enableDebug();
        $box->setTextAlign('left', 'center');
        $box->draw($map_name);

        // Game Type
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('f3f4f4'));
        $box->setFontSize(15);
        $box->setBox(15, 213, 200, 25);
        $box->setStrokeSize(2);
        $box->setStrokeColor($color->parseString('000000'));
        // $box->enableDebug();
        $box->setTextAlign('left', 'center');
        $box->draw($game_type);


        // Region
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/arial-bold.ttf');
        $box->setFontColor($color->parseString('FFFFFF'));
        $box->setFontSize(13);
        $box->setBox(430, 1, 40, 22);
        $box->setStrokeSize(1);
        $box->setStrokeColor($color->parseString('000000'));
        // $box->enableDebug();
        $box->setTextAlign('right', 'center');
        $box->draw($region);


        // header("Content-type: image/png");
        imagepng($im, $filename);
    }

    public function summarySignature($summoner, $image_name)
    {
        $color = new GDText\Color;

        $region = strtoupper($summoner['region']);
        $summoner_name = $summoner['summoner_name'];
        $league = mb_strtoupper($summoner['league']);
        $lp = $summoner['lp'];
        $series = $summoner['series'];
        $win_ratio = $summoner['win_ratio'];
        $profile_icon_id = $summoner['profile_icon_id'];
        $kills = $summoner['kills'];
        $deaths = $summoner['deaths'];
        $assists = $summoner['assists'];
        $kda = $summoner['kda'];
        $kill_participant = $summoner['kill_participant'];

        $stat_1_key = '';
        if (isset($summoner['stat_1_key'])) {
            $stat_1_name = $summoner['stat_1_name'];
            $stat_1_key = $summoner['stat_1_key'];
            $stat_1_win = $summoner['stat_1_win'];
            $stat_1_lose = $summoner['stat_1_lose'];
            $stat_1_kills = $summoner['stat_1_kills'];
            $stat_1_deaths = $summoner['stat_1_deaths'];
            $stat_1_assists = $summoner['stat_1_assists'];
            $stat_1_kda = $summoner['stat_1_kda'];
            $stat_1_winratio = $summoner['stat_1_winratio'];
            $stat_1_kill_participant = $summoner['stat_1_kill_participant'];
        }

        $stat_2_key = '';
        if (isset($summoner['stat_2_key'])) {
            $stat_2_name = $summoner['stat_2_name'];
            $stat_2_key = $summoner['stat_2_key'];
            $stat_2_win = $summoner['stat_2_win'];
            $stat_2_lose = $summoner['stat_2_lose'];
            $stat_2_kills = $summoner['stat_2_kills'];
            $stat_2_deaths = $summoner['stat_2_deaths'];
            $stat_2_assists = $summoner['stat_2_assists'];
            $stat_2_kda = $summoner['stat_2_kda'];
            $stat_2_winratio = $summoner['stat_2_winratio'];
            $stat_2_kill_participant = $summoner['stat_2_kill_participant'];
        }

        $champ_1_key = '';
        if (isset($summoner['champ_1_key'])) {
            $champ_1_key = $summoner['champ_1_key'];
            $champ_1_level = $summoner['champ_1_level'];
            $champ_1_points = $summoner['champ_1_points'];
        }

        $champ_2_key = '';
        if (isset($summoner['champ_2_key'])) {
            $champ_2_key = $summoner['champ_2_key'];
            $champ_2_level = $summoner['champ_2_level'];
            $champ_2_points = $summoner['champ_2_points'];
        }

        $champ_3_key = '';
        if (isset($summoner['champ_3_key'])) {
            $champ_3_key = $summoner['champ_3_key'];
            $champ_3_level = $summoner['champ_3_level'];
            $champ_3_points = $summoner['champ_3_points'];
        }

        if ($stat_1_key) {
            $im = imagecreatefromjpeg('assets/api/images/summarycards/' . $stat_1_key . '.jpg');
        } else if ($stat_2_key) {
            $im = imagecreatefromjpeg('assets/api/images/summarycards/' . $stat_2_key . '.jpg');
        } else if ($champ_1_key) {
            $im = imagecreatefromjpeg('assets/api/images/summarycards/' . $champ_1_key . '.jpg');
        } else if ($champ_2_key) {
            $im = imagecreatefromjpeg('assets/api/images/summarycards/' . $champ_2_key . '.jpg');
        } else if ($champ_3_key) {
            $im = imagecreatefromjpeg('assets/api/images/summarycards/' . $champ_3_key . '.jpg');
        } else {
            $im = imagecreatefromjpeg('assets/api/images/summarycards/Ahri.jpg');
        }

        $profile_icon_src = $this->profileIcon($profile_icon_id, 78);

        if ($profile_icon_src == false) {
            return false;
        }

        $profile_image = imagecreatefrompng($profile_icon_src);
        imagecopy($im, $profile_image, 19, 23, 0, 0, imagesx($profile_image), imagesy($profile_image));

        $win_rate_number = (float)str_replace(array('%', ','), array('', '.'), $win_ratio);
        $win_rate_length = (98 / 100 * $win_rate_number) + 14;

        $green = imagecolorallocate($im, 94, 255, 196);
        imagefilledrectangle($im, 14, 146, $win_rate_length, 153, $green);

        if ($champ_1_key) {
            $champ_1_image = imagecreatefrompng($this->champion($champ_1_key, 41));
            imagecopy($im, $champ_1_image, 347, 110, 0, 0, imagesx($champ_1_image), imagesy($champ_1_image));
        }
        if ($champ_2_key) {
            $champ_2_image = imagecreatefrompng($this->champion($champ_2_key, 41));
            imagecopy($im, $champ_2_image, 347, 157, 0, 0, imagesx($champ_2_image), imagesy($champ_2_image));
        }
        if ($champ_3_key) {
            $champ_3_image = imagecreatefrompng($this->champion($champ_3_key, 41));
            imagecopy($im, $champ_3_image, 347, 204, 0, 0, imagesx($champ_3_image), imagesy($champ_3_image));
        }

        if ($stat_1_key) {
            $stat_1_image = imagecreatefrompng($this->champion($stat_1_key, 60));
            imagecopy($im, $stat_1_image, 142, 112, 0, 0, imagesx($stat_1_image), imagesy($stat_1_image));
        }
        if ($stat_2_key) {
            $stat_2_image = imagecreatefrompng($this->champion($stat_2_key, 60));
            imagecopy($im, $stat_2_image, 142, 184, 0, 0, imagesx($stat_2_image), imagesy($stat_2_image));
        }


        $series_length = strlen($series);
        if ($series_length > 0) {
            $series_circle = imagecreatefrompng(FCPATH . 'assets/api/images/series/circle.png');
            $series_check = imagecreatefrompng(FCPATH . 'assets/api/images/series/check.png');
            $series_times = imagecreatefrompng(FCPATH . 'assets/api/images/series/times.png');
            if ($series_length == 3) {
                $series = '  ' . $series;
                $series_length = 5;
            }
            if ($series_length == 4) {
                $series = ' ' . $series;
                $series_length = 5;
            }
            for ($i = 0; $i < $series_length; $i++) {
                if ($series[$i] == 'N') {
                    imagecopy($im, $series_circle, 265 + (20 * $i), 73, 0, 0, imagesx($series_circle), imagesy($series_circle));
                } else if ($series[$i] == 'W') {
                    imagecopy($im, $series_check, 265 + (20 * $i), 73, 0, 0, imagesx($series_check), imagesy($series_check));
                } else if ($series[$i] == 'L') {
                    imagecopy($im, $series_times, 265 + (20 * $i), 75, 0, 0, imagesx($series_times), imagesy($series_times));
                }

            }
        }

        $star = imagecreatefrompng(FCPATH . 'assets/api/images/stars/star.png');
        $staro = imagecreatefrompng(FCPATH . 'assets/api/images/stars/star-o.png');

        if ($champ_1_key && $champ_1_level) {
            for ($i = 1; $i <= 7; $i++) {
                if ($champ_1_level >= $i) {
                    imagecopy($im, $star, 384 + (11 * $i), 137, 0, 0, imagesx($star), imagesy($star));
                } else {
                    imagecopy($im, $staro, 384 + (11 * $i), 137, 0, 0, imagesx($staro), imagesy($staro));
                }

            }
        }

        if ($champ_2_key && $champ_2_level) {
            for ($i = 1; $i <= 7; $i++) {
                if ($champ_2_level >= $i) {
                    imagecopy($im, $star, 384 + (11 * $i), 184, 0, 0, imagesx($star), imagesy($star));
                } else {
                    imagecopy($im, $staro, 384 + (11 * $i), 184, 0, 0, imagesx($staro), imagesy($staro));
                }

            }
        }

        if ($champ_3_key && $champ_3_level) {
            for ($i = 1; $i <= 7; $i++) {
                if ($champ_3_level >= $i) {
                    imagecopy($im, $star, 384 + (11 * $i), 231, 0, 0, imagesx($star), imagesy($star));
                } else {
                    imagecopy($im, $staro, 384 + (11 * $i), 231, 0, 0, imagesx($staro), imagesy($staro));
                }

            }
        }

        // Summoner Name
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('f9f9f9'));
        if (mb_strlen($summoner_name) > 24) {
            $box->setFontSize(18);
        } else if (mb_strlen($summoner_name) > 19) {
            $box->setFontSize(20);
        } else if (mb_strlen($summoner_name) > 15) {
            $box->setFontSize(22);
        } else {
            $box->setFontSize(27);
        }
        $box->setBox(116, 32, 260, 33);
        // $box->enableDebug();
        $box->setTextAlign('left', 'center');
        $box->draw($summoner_name);

        // League
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('53fee1'));
        $box->setFontSize(24);
        $box->setBox(116, 61, 200, 30);
        // $box->enableDebug();
        $box->setTextAlign('left', 'top');
        $box->draw($league);

        // LP
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('f9f9f9'));
        $box->setFontSize(22);
        $box->setBox(382, 65, 67, 30);
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw($lp);

        // Win Ratio
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('f9f9f9'));
        $box->setFontSize(20);
        $box->setBox(14, 115, 98, 30);
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw($win_ratio);

        // Kills
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Regular.ttf');
        $box->setFontColor($color->parseString('5effc4'));
        $box->setFontSize(12);
        $box->setBox(14, 160, 28, 15);
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw($kills);

        // Deaths
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/arial-bold.ttf');
        $box->setFontColor($color->parseString('ff5f5f'));
        $box->setFontSize(12);
        $box->setBox(50, 160, 28, 15);
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw($deaths);

        // Assists
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('dfa425'));
        $box->setFontSize(12);
        $box->setBox(85, 160, 28, 15);
        // $box->enableDebug();
        $box->setTextAlign('center', 'center');
        $box->draw($assists);


        // KDA :1 kısmı
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('f9f9f9'));
        $box->setFontSize(13);
        $box->setBox(80, 187, 28, 15);
        // $box->enableDebug();
        $box->setTextAlign('left', 'center');
        $box->draw(":1");

        // KDA
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('f9f9f9'));
        $box->setFontSize(22);
        $box->setBox(0, 178, 78, 30);
        // $box->enableDebug();
        $box->setTextAlign('right', 'center');
        $box->draw(number_format($kda, 2, '.', '.'));

        // Kill Participant
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('f9f9f9'));
        $box->setFontSize(15);
        $box->setBox(53, 214, 50, 19);
        // $box->enableDebug();
        $box->setTextAlign('left', 'center');
        $box->draw($kill_participant);


        if ($stat_1_key) {
            // Stat 1 Win Ratio
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('f9f9f9'));
            $box->setFontSize(14);
            $box->setBox(210, 115, 48, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($stat_1_winratio);

            // Stat 1 Win
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('5effc4'));
            $box->setFontSize(12);
            $box->setBox(256, 115, 25, 18);
            // $box->enableDebug();
            $box->setTextAlign('right', 'center');
            $box->draw($stat_1_win);

            // Stat 1 Win W
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('5effc4'));
            $box->setFontSize(12);
            $box->setBox(284, 115, 8, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw("W");

            // Stat 1 Lose
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('ff5f5f'));
            $box->setFontSize(12);
            $box->setBox(298, 115, 15, 18);
            // $box->enableDebug();
            $box->setTextAlign('right', 'center');
            $box->draw($stat_1_lose);

            // Stat 1 Lose L
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('ff5f5f'));
            $box->setFontSize(12);
            $box->setBox(316, 115, 8, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw('L');

            // Stat 1 Kill Participation
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('f9f9f9'));
            $box->setFontSize(13);
            $box->setBox(287, 155, 45, 17);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($stat_1_kill_participant);

            // Stat 1 KDA
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('f9f9f9'));
            $box->setFontSize(14);
            $box->setBox(207, 154, 48, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($stat_1_kda . ':1');

            // Stat 1 Kills
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('5effc4'));
            $box->setFontSize(14);
            $box->setBox(213, 135, 30, 18);
            // $box->enableDebug();
            $box->setTextAlign('right', 'center');
            $box->draw($stat_1_kills);

            // Stat 1 Deaths
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('ff5f5f'));
            $box->setFontSize(14);
            $box->setBox(252, 135, 30, 18);
            // $box->enableDebug();
            $box->setTextAlign('center', 'center');
            $box->draw($stat_1_deaths);

            // Stat 1 Assists
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('dfa425'));
            $box->setFontSize(14);
            $box->setBox(292, 135, 30, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($stat_1_assists);
        }
        if ($stat_2_key) {
            // Stat 2 Win Ratio
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('f9f9f9'));
            $box->setFontSize(14);
            $box->setBox(210, 186, 48, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($stat_2_winratio);

            // Stat 2 Win
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('5effc4'));
            $box->setFontSize(12);
            $box->setBox(256, 186, 25, 18);
            // $box->enableDebug();
            $box->setTextAlign('right', 'center');
            $box->draw($stat_2_win);

            // Stat 2 Win W
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('5effc4'));
            $box->setFontSize(12);
            $box->setBox(284, 186, 8, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw("W");

            // Stat 2 Lose
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('ff5f5f'));
            $box->setFontSize(12);
            $box->setBox(298, 186, 15, 18);
            // $box->enableDebug();
            $box->setTextAlign('right', 'center');
            $box->draw($stat_2_lose);

            // Stat 2 Lose L
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('ff5f5f'));
            $box->setFontSize(12);
            $box->setBox(316, 186, 8, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw('L');

            // Stat 2 Kill Participation
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('f9f9f9'));
            $box->setFontSize(13);
            $box->setBox(287, 226, 45, 17);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($stat_2_kill_participant);

            // Stat 2 KDA
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('f9f9f9'));
            $box->setFontSize(14);
            $box->setBox(207, 225, 48, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($stat_2_kda . ':1');

            // Stat 2 Kills
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('5effc4'));
            $box->setFontSize(14);
            $box->setBox(213, 208, 30, 18);
            // $box->enableDebug();
            $box->setTextAlign('right', 'center');
            $box->draw($stat_2_kills);

            // Stat 2 Deaths
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('ff5f5f'));
            $box->setFontSize(14);
            $box->setBox(252, 208, 30, 18);
            // $box->enableDebug();
            $box->setTextAlign('center', 'center');
            $box->draw($stat_2_deaths);

            // Stat 2 Assists
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('dfa425'));
            $box->setFontSize(14);
            $box->setBox(292, 208, 30, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($stat_2_assists);
        }


        if ($champ_1_key) {
            // Champion 1 Points
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('f9f9f9'));
            $box->setFontSize(13);
            $box->setBox(395, 119, 75, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($champ_1_points);
        }

        if ($champ_2_key) {
            // Champion 2 Points
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('f9f9f9'));
            $box->setFontSize(13);
            $box->setBox(395, 166, 75, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($champ_2_points);

        }

        if ($champ_3_key) {
            // Champion 3 Points
            $box = new GDText\Box($im);
            $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
            $box->setFontColor($color->parseString('f9f9f9'));
            $box->setFontSize(13);
            $box->setBox(395, 213, 75, 18);
            // $box->enableDebug();
            $box->setTextAlign('left', 'center');
            $box->draw($champ_2_points);
        }


        // Slash
        $box = new GDText\Box($im);
        $box->setFontFace('assets/api/fonts/Arame-Bold.ttf');
        $box->setFontColor($color->parseString('0e8470'));
        $box->setFontSize(13);
        $box->setTextAlign('center', 'center');
        $box->setBox(44, 161, 7, 16);
        $box->draw("/");
        $box->setBox(79, 161, 7, 16);
        $box->draw("/");
        if ($stat_1_key) {
            $box->setBox(247, 136, 7, 16);
            $box->draw("/");
            $box->setBox(280, 136, 7, 16);
            $box->draw("/");
        }
        if ($stat_2_key) {
            $box->setBox(247, 209, 7, 16);
            $box->draw("/");
            $box->setBox(280, 209, 7, 16);
            $box->draw("/");
        }

        if (ENVIRONMENT == 'development') {
            //Eski sistem
            $img_location = SIGNATURE_LOCATION . $image_name;
            $result = imagepng($im, $img_location, 9);
            return $result;
        } else {
            $tmpfile = tempnam(sys_get_temp_dir(), "img" . rand(1000, 9999));

            $save_result = imagepng($im, $tmpfile, 9);
            if ($save_result) {
                $bucket = 'lolsgg-media';
                try {
                    $sharedConfig = [
                        'region' => 'us-east-2',
                        'version' => 'latest',
                        'credentials' => [
                            'key' => 'AKIAJQJBVX232WFPD77A',
                            'secret' => '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',
                        ]
                    ];
                    $sdk = new Aws\Sdk($sharedConfig);
                    $s3Client = $sdk->createS3();

                    $result = $s3Client->putObject(array(
                        'Bucket' => $bucket,
                        'Key' => $image_name,
                        'SourceFile' => $tmpfile,
                        'ContentType' => 'image/png',
                        'ACL' => 'public-read'
                    ));
                    return true;
                } catch (Aws\S3\Exception\S3Exception $e) {

                } catch (Aws\Exception\AwsException $e) {

                }


            }
        }
        return false;

    }
}
