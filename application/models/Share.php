<?php

class Share extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function liveSignatureShare($summoner)
    {
        $twitter_share_timer = $this->cache->redis->get('live_share_twitter_timer');
        if ((int)$twitter_share_timer < time() - 7200) {
            if (ENVIRONMENT == 'development') {
                // $tmpfile = 'assets/api/test.png';
                // $this->lolimage->liveSignature($summoner,$tmpfile);
            } else {
                $tmpfile = tempnam(sys_get_temp_dir(), "img");
                $this->lolimage->liveSignature($summoner, $tmpfile);
                $media_files = array($tmpfile);
                $text = $summoner['summoner_name'] . ' is playing now. https://lols.gg/summoner/live/' . strtoupper($summoner['region']) . '/' . str_replace(' ', '', $summoner['summoner_name']) . ' #LeagueOfLegends #lol #lolesports #' . str_replace(array(' ', '.', "'"), '', $summoner['champion_name']);
                $this->twitter($text, $media_files);
                unlink($tmpfile);
            }

            $this->cache->redis->save('live_share_twitter_timer', time(), 21600);
        }
    }

    public function twitter($text, $media_files)
    {
        \Codebird\Codebird::setConsumerKey('jxx5FnFWIbyXjqSN3ypC9rZhQ', 'T7AhbhfjYGvA4ygyGxYx1R208F2Qp94epQmoD3rpulBknUo4KR');
        $cb = \Codebird\Codebird::getInstance();

        $cb->setToken('867701749464870912-Vo3KEm5W3C31KKgNTtmF2ECIAJBSob8', 'Rwrci57wJFWOrKPBiTNaWAZeAxpirsPTfirxvJpWHM8Xl');


        foreach ($media_files as $file) {
            $reply = $cb->media_upload([
                'media' => $file
            ]);
            if (isset($reply->media_id_string)) {
                $media_ids[] = $reply->media_id_string;
            }

        }

        $media_ids = implode(',', $media_ids);

        $params = [
            'status' => $text,
            'media_ids' => $media_ids
        ];

        $reply = $cb->statuses_update($params);

    }

    public function facebook()
    {
        $facebook_album_id = '436407946704626';
        $image_url = 'https://lols.gg///media.lols.gg/img/profileiconicon/898.png';
        $mesaj = 'This is a test message';

        $accessToken = 'EAAVfhBe9S6IBABiT0VXcTX3TeciU6rEjr1WtAXflRfZCJnq4WhMK9u4XEevd1TUq9F9gZBY3uXpUCCs9UYWzZArtWyO73aBmXfUrjC2aCO77K2jZCxaGfbpqXmqmIXqTT2tj1gwHuMsaPXIE2ngfQTRu3zMrYW4UWwnRM6pblga9lDuOTXvy';

        $app_id = '1512395822156706';
        $secret = 'd6b58d633b9ca47851ebb80247495e08';
        $fb = new Facebook\Facebook(array(
            'app_id' => $app_id,
            'app_secret' => $secret,
            'default_graph_version' => 'v2.8'));

        $fb->setDefaultAccessToken($accessToken);

        // Görsel ve mesaj
        $media_request = $fb->request('POST', '/' . $facebook_album_id . '/photos', array('url' => $image_url, 'message' => $mesaj));
        try {
            $response = $fb->getClient()->sendRequest($media_request);
            $graphNode = $response->getDecodedBody();
            $this->Request->pr($graphNode);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo '<br>Bir hata oluştu. Lütfen paylaşımınızı facebook sayfasından kontrol ediniz. Graph returned an error: ' . $e->getMessage();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo '<br>Bir hata oluştu. Lütfen paylaşımınızı facebook sayfasından kontrol ediniz. Facebook SDK returned an error: ' . $e->getMessage();
        }

        // Sadece Mesaj
        // $feed_request = $fb->request('POST', '/LolsggOfficial/feed',array('message'=>$mesaj));
        // try {
        //     $response = $fb->getClient()->sendRequest($feed_request);
        //     $graphNode = $response->getDecodedBody();
        //     $this->Request->pr($graphNode);
        // } catch(Facebook\Exceptions\FacebookResponseException $e) {
        //     echo '<br>Bir hata oluştu. Lütfen paylaşımınızı facebook sayfasından kontrol ediniz. Graph returned an error: ' . $e->getMessage();
        // } catch(Facebook\Exceptions\FacebookSDKException $e) {
        //     echo '<br>Bir hata oluştu. Lütfen paylaşımınızı facebook sayfasından kontrol ediniz. Facebook SDK returned an error: ' . $e->getMessage();
        // }
    }

    public function instagram()
    {
        $image_url = 'https://lols.gg///media.lols.gg/img/profileiconicon/898.png';
        $image_location = '/www/sites/api/898.png';
        $caption = 'This is a test message';
        $username = "lolsggofficial";
        $password = "18711565onmus";

        $instagram = new \Instagram\Instagram();
        $instagram->login($username, $password);
        // $instagram->postPhoto($image_location, $caption);
    }

    public function facebookconnect()
    {
        // $accessToken = '';
        // $app_id = '1512395822156706';
        // $secret = 'd6b58d633b9ca47851ebb80247495e08';
        // $fb = new Facebook\Facebook(array(
        //     'app_id' => $app_id,
        // 	'app_secret' => $secret,
        // 	'default_graph_version' => 'v2.8'));
        // $accessTokenResponse = $fb->getOAuth2Client()->getLongLivedAccessToken($accessToken);
        //
        // $longAccessToken = $accessTokenResponse->getValue();
        //
        // $fb->setDefaultAccessToken($longAccessToken);
        //
        // $request = $fb->request('GET', '/LolsggOfficial?fields=access_token');
        //
        // $response = $fb->getClient()->sendRequest($request);
        //
        // $body = $response->getDecodedBody();
        // if(isset($body['access_token']))
        // {
        // 	echo $body['access_token'];
        // }
    }

    public function twitterconnect()
    {
        // \Codebird\Codebird::setConsumerKey('jxx5FnFWIbyXjqSN3ypC9rZhQ', 'T7AhbhfjYGvA4ygyGxYx1R208F2Qp94epQmoD3rpulBknUo4KR');
        // $cb = \Codebird\Codebird::getInstance();
        //
        // $reply = $cb->oauth_requestToken();
        // $this->Request->pr($reply);
        // $cb->setToken($reply->oauth_token, $reply->oauth_token_secret);
        // echo $reply->oauth_token;
        // echo '<br>';
        // echo $reply->oauth_token_secret;
        // echo '<br>';
        // echo $cb->oauth_authorize();
    }


    public function twitterconnect2()
    {
        // \Codebird\Codebird::setConsumerKey('jxx5FnFWIbyXjqSN3ypC9rZhQ', 'T7AhbhfjYGvA4ygyGxYx1R208F2Qp94epQmoD3rpulBknUo4KR');
        // $cb = \Codebird\Codebird::getInstance();
        //
        // $cb->setToken('2I664wAAAAAA0K39AAABXD9mNY8', 'fS3dVdetVpiCggmrRUpSb3ylu4cSR1nM');
        //
        // $reply = $cb->oauth_accessToken([
        //     'oauth_verifier' => '9224693'
        // ]);
        //
        // $this->Request->pr($reply);
    }
}
