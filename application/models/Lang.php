<?php

class Lang extends CI_Model
{
    private $selected_lang = 'en_US';
    private $lang_data = false;
    private $version = '1.1.4';

    function __construct()
    {
        parent::__construct();
    }

    public function setLang($lang)
    {
        $langs = array();
        $langs['en_US'] = true;
        $langs['ko_KR'] = true;
        $langs['ja_JP'] = true;
        $langs['pl_PL'] = true;
        $langs['fr_FR'] = true;
        $langs['de_DE'] = true;
        $langs['es_ES'] = true;
        $langs['ru_RU'] = true;
        $langs['hu_HU'] = true;
        $langs['tr_TR'] = true;
        $langs['ro_RO'] = true;
        $langs['pt_BR'] = true;
        $langs['zh_CN'] = true;
        $langs['it_IT'] = true;
        $langs['id_ID'] = true;
        $langs['ms_MY'] = true;
        $langs['th_TH'] = true;
        $this->selected_lang = isset($langs[$lang]) ? $lang : 'en_US';
    }

    public function loadLang($lang)
    {
        if ($result = $this->cache->redis->get('language_v' . $this->version . '::' . $lang)) {
            $this->lang_data = $result;
            return true;
        }
        $file = FCPATH . 'assets/lang/' . $lang . '.json';

        if (file_exists($file)) {
            $lang_json = json_decode(file_get_contents($file));
            if ($result = $this->cache->redis->save('language_v' . $this->version . '::' . $lang, $lang_json, 600000)) {
                $this->lang_data = $lang_json;
                return true;
            }
        }
        return false;
    }

    public function get($key, $sub_key = false)
    {
        if ($this->lang_data == false) {
            $this->loadLang($this->selected_lang);
        }
        if (isset($this->lang_data->{$key})) {
            if (is_object($this->lang_data->{$key})) {
                if ($sub_key && isset($this->lang_data->{$key}->{$sub_key})) {
                    return $this->lang_data->{$key}->{$sub_key};
                } else {
                    return $sub_key;
                }
            } else {
                return $this->lang_data->{$key};
            }
        } else {
            return $sub_key ? $sub_key . '+' : $key;
        }
    }

    public function createLangFile($target_lang)
    {
        $source = 'source';
        $source = FCPATH . 'assets/lang/' . $source . '.json';
        if (file_exists($source)) {
            $source_file = file_get_contents($source);
            $new_lang_ = json_decode($source_file);
            $new_lang = new stdClass();
            foreach ($new_lang_ as $lang_key => $lang_text) {
                if (is_object($lang_text)) {
                    $new_lang->{$lang_key} = new stdClass();
                    foreach ($lang_text as $lang_key_ => $lang_text_) {
                        // $translated_text = $this->translateText($target_lang,$lang_text_);
                        $new_lang->{$lang_key}->{$lang_key_} = $lang_text_;
                    }
                } else {
                    // $translated_text = $this->translateText($target_lang,$lang_text);
                    $new_lang->{$lang_key} = $lang_text;
                }
            }
            $this->lol->setRegion('na');
            $this->lol->setLang($target_lang);
            $lol_language_strings = $this->lol->getLanguageString();
            if (!$lol_language_strings) {
                echo 'error';
                exit;
            }
            $new_lang->lol = $lol_language_strings;
            // $this->Request->pr(json_encode($new_lang));
            // $this->Request->pr($new_lang);
            file_put_contents(FCPATH . 'assets/lang/' . $target_lang . '.json', json_encode($new_lang, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        }
    }

    public function translateText($target, $text)
    {
        $lang_google = array();
        $lang_google['en_US'] = 'en';
        $lang_google['ko_KR'] = 'ko';
        $lang_google['ja_JP'] = 'ja';
        $lang_google['pl_PL'] = 'pl';
        $lang_google['fr_FR'] = 'fr';
        $lang_google['de_DE'] = 'de';
        $lang_google['es_ES'] = 'es';
        $lang_google['ru_RU'] = 'ru';
        $lang_google['hu_HU'] = 'hu';
        $lang_google['tr_TR'] = 'tr';
        $lang_google['ro_RO'] = 'ro';
        $lang_google['pt_BR'] = 'pt';
        $lang_google['zh_CN'] = 'zh';
        $lang_google['it_IT'] = 'it';
        $lang_google['id_ID'] = 'id';
        $lang_google['ms_MY'] = 'ms';
        $lang_google['th_TH'] = 'th';
        if (!isset($lang_google[$target])) {
            return $text;
        }
        if ($result = $this->cache->redis->get('ggl_translate__::' . md5($target . $text))) {
            return $result;
        }
        $url = 'https://translate.google.com/m?sl=auto';
        $url .= '&tl=' . urlencode($lang_google[$target]);
        $url .= '&q=' . urlencode($text);
        $header = array();
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $google = curl_exec($ch);
        $result_exp_ = explode('<div dir="ltr" class="t0">', $google);
        if (!isset($result_exp_[1])) {
            return $text;
        }
        $result_exp = explode('</div>', $result_exp_[1]);
        $result = $result_exp[0];
        $this->cache->redis->save('ggl_translate__::' . md5($target . $text), $result, 86400);
        return $result;
    }
}
